﻿using AppKit;
using Foundation;

namespace methinksPatcherTool
{
    [Register("AppDelegate")]
    public class AppDelegate : NSApplicationDelegate
    {
        main_worker _mainWorker = new main_worker_unix();
        public AppDelegate()
        {}

        public override void DidFinishLaunching(NSNotification notification)
        {
            // Insert code here to initialize your application
            _mainWorker.Initialize();
        }

        [Export("openDocument:")]
        void OpenDialog(NSObject sender)
        {
            var dlg = NSOpenPanel.OpenPanel;
            dlg.CanChooseFiles = true;
            dlg.CanChooseDirectories = false;

            if (dlg.RunModal() == 1)
            {
                /*
                var alert = new NSAlert()
                {
                    AlertStyle = NSAlertStyle.Informational,
                    InformativeText = "At this point we should do something with the folder that the user just selected in the Open File Dialog box...",
                    MessageText = "Folder Selected"
                };
                alert.RunModal();
                */

                string fullPathFileName = dlg.Filename;
                _mainWorker.OpenFile(fullPathFileName);
            }
        }

        public override void WillTerminate(NSNotification notification)
        {
            // Insert code here to tear down your application
            _mainWorker.UnInitialize();
        }
    }
}
