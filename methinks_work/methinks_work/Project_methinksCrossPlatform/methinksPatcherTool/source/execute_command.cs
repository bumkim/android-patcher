﻿#define _SYNC_2019_0118_1400

using System;

namespace methinksPatcherTool
{
    public class execute_command
    {
        protected string _commandName = "execute_command";
        protected string _processName = "Process";

        public string CommandName
        {
            get
            {
                return _commandName;
            }
        }

        public string ProcessName
        {
            get
            {
                return _processName;
            }
        }
         
        public execute_command()
        {}

        public execute_command(string commandName,string processName)
        {
            _commandName = commandName;
            _processName = processName;
        }


        public string StartProcessName()
        {
            return "[Start -> ] :" + " " + _processName;
        }

        public virtual int Run()
        {
            Console.WriteLine("[ - Run - ] : " + _processName);
            return -1;
        }

        public string FinishProcessName()
        {
            return "[ <- Finish] :" + " " + _processName;
        }
    }
}
