﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_distribute_get_device_info : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_get_device_info()
        {
            _processName = "distribute_get_device_info";
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            int resultCode = 0;
            int testDeviceSDKVersion = 0;
            string osVersion = "";
            if (utility.IsWindowsOS())
            {
                osVersion = "windows";
            }
            else if (utility.IsUnixOS())
            {
                osVersion = "unix";
            }
            string adbFullPathFileName = global_set.WorkerFolderPathName + "/patcher/patcher_tool/adb/" + osVersion + "/adb";
            string recompiledSignedFullPathFileName = global_set.WorkerFolderPathName + "/log/" +
                global_set.WorkFileTitleName + "/" +
                global_set.RecompiledSignedFileFullName;

            //adb shell getprop ro.build.version.sdk
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = adbFullPathFileName;
            startInfo.Arguments = "shell getprop ro.build.version.sdk";

            Process process = null;
            try
            {
                startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process = Process.Start(startInfo);

                while (!process.StandardOutput.EndOfStream)
                {
                    string deviceSDKVersion = process.StandardOutput.ReadLine();
                    Console.WriteLine(_processName + " - DeviceSDKVersion : " + deviceSDKVersion);
                    global_set.WorkerProcessLogList.Add("DeviceSDKVersion : " + deviceSDKVersion);

                    testDeviceSDKVersion = int.Parse(deviceSDKVersion);
                }
            } 
            catch (Exception ex)
            {
                global_set.InstallError = _processName + " - " + ex.Message;
                Console.WriteLine(_processName + " - Exception : " + ex.Message);
                global_set.WorkerProcessLogList.Add("Exception : " + ex.Message);
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }

            if (0 != testDeviceSDKVersion)
            {
                if (null == global_set.AndroidManifestBinary)
                {
                    global_set.InstallError = _processName + " - Error : Not Parse AndroidManifest Yet";
                    Console.WriteLine(global_set.InstallError);
                    global_set.WorkerProcessLogList.Add(global_set.InstallError);

                    resultCode = 2;
                }

                if (global_set.AndroidManifestBinary.GetMinSDKVersion() > testDeviceSDKVersion)
                {
                    global_set.InstallError = _processName + " - Error : Device SDK Version is lower";
                    Console.WriteLine(global_set.InstallError);
                    global_set.WorkerProcessLogList.Add(global_set.InstallError);

                    resultCode = 3;
                }
            }
            else
            {
                global_set.InstallError = _processName + " - Exception : Environment Set (Not Connected Device)";
                Console.WriteLine(global_set.InstallError);
                global_set.WorkerProcessLogList.Add(global_set.InstallError);

                resultCode = 1;
            }

            Thread.Sleep(1000);
            return resultCode;
        }
    }
}
