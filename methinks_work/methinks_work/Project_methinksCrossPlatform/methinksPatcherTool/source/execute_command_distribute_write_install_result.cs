﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace methinksPatcherTool
{
    public class install_result
    {
        public string Result { get; set; }
        public string Error { get; set; }
    }


    public class execute_command_distribute_write_install_result : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_write_install_result()
        {
            _processName = "distribute_write_install_result";
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            string result = "failure";
            if (string.IsNullOrEmpty(global_set.InstallError))
            {
                result = "success";
            }

            install_result InstallResult = new install_result
            {
                Result = result,
                Error = global_set.InstallError,
            };

            string installResultJson = JsonConvert.SerializeObject(InstallResult, Formatting.Indented);
            Console.WriteLine(installResultJson);

            string installResultTextFile = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName + "/install_result_" + global_set.WorkFileTitleName + ".txt";
            File.WriteAllText(installResultTextFile, installResultJson);

            return 0;
        }
    }
}
