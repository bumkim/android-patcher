﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_recompile_apktool : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_recompile_apktool()
        {
            _processName = "recompile_apktool";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            int resultCode = 0;
            string apktoolJarPath = global_set.WorkerFolderPathName + @"/patcher/patcher_tool/apktool_2.3.3.jar";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = global_set.Java8FullPathFileName;
            //startInfo.FileName = "java";
            //startInfo.FileName = "/Library/Java/JavaVirtualMachines/jdk1.8.0_181.jdk/Contents/Home/bin/java";
            //startInfo.FileName = "/Library/Java/JavaVirtualMachines/jdk1.8.0_191.jdk/Contents/Home/bin/java";
            startInfo.Arguments = "-jar " + apktoolJarPath + " b " + global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName;
              
            Process process = null;
            try
            { 
                startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process = Process.Start(startInfo);

                bool writeProcessLog = true;
                if (false == writeProcessLog)
                {
                    do
                    {  
                        if (!process.HasExited)
                        {
                            process.Refresh();
                        }
                    }
                    while (!process.WaitForExit(1000));               

                }
                else
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        string readLine = process.StandardOutput.ReadLine();
                        if (true == readLine.ToLower().Contains("exception") || true == readLine.ToLower().Contains("error"))
                        {
                            global_set.PatchError = _processName + " - " + readLine;
                            resultCode = 1;
                        }

                        Console.WriteLine(readLine);
                        global_set.WorkerProcessLogList.Add(readLine);
                    }
                }
            }
            catch (Exception ex)
            {
                global_set.PatchError = _processName + " - " + ex.Message;
                Console.WriteLine(_processName + " - Exception : " + ex.Message);
                global_set.WorkerProcessLogList.Add("Exception : " + ex.Message);

                resultCode = 1;
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }

#if _NOT_USE_YET
            Thread.Sleep(4000);
            File.Move(global_set.WorkerFolderPathName + "/" + fileTitleName + "/dist/" + fileTitleName + ".apk", global_set.WorkerFolderPathName + "/" + fileTitleName + "_recompile.apk");
            Thread.Sleep(2000);
#endif
            return resultCode;
        }
    }
}
