﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_decompile_manifest : execute_command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:methinksPatcherTool.execute_command_decompile_manifest"/> class.
        /// </summary>
        public execute_command_decompile_manifest()
        {
            _processName = "decompile_manifest";
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            //java -jar AXMLPrinter2.jar AndroidManifest.xml > Manifest.xml
            string AXMLPrinter2JarPath = global_set.WorkerFolderPathName + "/patcher/patcher_tool/AXMLPrinter2.jar";
            string androidManifestPath = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/AndroidManifest.xml";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "java";
            startInfo.Arguments = "-jar " + AXMLPrinter2JarPath + " " + androidManifestPath;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;

            Process process = Process.Start(startInfo);
            StreamReader streamReader = process.StandardOutput;
            string androidManifestXML = streamReader.ReadToEnd();
            Console.WriteLine(androidManifestXML);
            process.WaitForExit();
            process.Close();
            
            //android manifest - text out file
            bool writeAndroidManifestFile = true;
            if (true == writeAndroidManifestFile)
            {
                string androidManifestOutPath = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName + "/AndroidManifest_text_origin.xml";
                if (!File.Exists(androidManifestOutPath))
                    File.WriteAllText(androidManifestOutPath, androidManifestXML, Encoding.UTF8);

                Thread.Sleep(3000);

#if _NOT_USE_YET
                ProcessStartInfo startInfo2 = new ProcessStartInfo();
                startInfo2.FileName = "explorer";
                startInfo2.Arguments = androidManifestOutPath;
                Process.Start(startInfo2);
#endif
            }

            return 0;
        }
    }
}
