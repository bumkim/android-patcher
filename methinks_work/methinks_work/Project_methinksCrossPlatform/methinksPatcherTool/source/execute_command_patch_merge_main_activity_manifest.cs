﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_merge_main_activity_manifest : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_merge_main_activity_manifest()
        {
            _processName = "patch_merge_main_activity_manifest";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 1;

            string newMainActivityFullPackageName = "com.kint.kintframeworkaosaar.methinksMainActivity";
            global_set.AndroidManifestBinary.SetMainActivityFullPackageName(newMainActivityFullPackageName);

            return 0;
        }
    }
}
