﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace methinksPatcherTool
{
    internal enum e_chunk_type
    {
        Invalid,
        String = 0x001C0001,
        ResourceID = 0x00080180,
        StartNamespace = 0x00100100,
        EndNamespace = 0x00100101,
        StartTag = 0x00100102,
        EndTag = 0x00100103,
        Text = 0x00100104,
    }

    internal enum e_resource_value_type
    {
        // Contains no data.
        NULL = 0x00,
        // The 'data' holds a ResTable_ref, a reference to another resource
        // table entry.
        REFERENCE = 0x01,
        // The 'data' holds an attribute resource identifier.
        ATTRIBUTE = 0x02,
        // The 'data' holds an index into the containing resource table's
        // global value string pool.
        STRING = 0x03,
        // The 'data' holds a single-precision floating point number.
        FLOAT = 0x04,
        // The 'data' holds a complex number encoding a dimension value,
        // such as "100in".
        DIMENSION = 0x05,
        // The 'data' holds a complex number encoding a fraction of a
        // container.
        FRACTION = 0x06,

        // Beginning of integer flavors...
        FIRST_INT = 0x10,

        // The 'data' is a raw integer value of the form n..n.
        INT_DEC = 0x10,
        // The 'data' is a raw integer value of the form 0xn..n.
        INT_HEX = 0x11,
        // The 'data' is either 0 or 1, for input "false" or "true" respectively.
        INT_BOOLEAN = 0x12,

        // Beginning of color integer flavors...
        FIRST_COLOR_INT = 0x1c,
        // The 'data' is a raw integer value of the form #aarrggbb.
        INT_COLOR_ARGB8 = 0x1c,
        // The 'data' is a raw integer value of the form #rrggbb.
        INT_COLOR_RGB8 = 0x1d,
        // The 'data' is a raw integer value of the form #argb.
        INT_COLOR_ARGB4 = 0x1e,
        // The 'data' is a raw integer value of the form #rgb.
        INT_COLOR_RGB4 = 0x1f,
        // ...end of integer flavors.
        LAST_COLOR_INT = 0x1f,
        // ...end of integer flavors.
        LAST_INT = 0x1f,
    }



    public class string_pool
    {
        short _stringLength;
        byte[] _contentBytes;
        short _stringEndNull;

        int _countIndex = -1;
        int _offset = -1;
        string _contentName;

        public string ContentName
        {
            get
            {
                return _contentName;
            }
        }

        public int CountIndex
        {
            get
            {
                return _countIndex;
            }
            set
            {
                _countIndex = value;
            }
        }

        public int Offset
        {
            get
            {
                return _offset;
            }
            set
            {
                _offset = value;
            }
        }

        public int ContentBytesLength
        {
            get
            {
                return _contentBytes.Length;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <param name="offset"></param>
        /// <param name="stringPoolString"></param>
        public string_pool(int countIndex, int offset, string stringPoolString)
        {
            _countIndex = countIndex;
            _offset = offset;
            _stringLength = (short)stringPoolString.Length;
            _contentName = stringPoolString;
            _contentBytes = Encoding.Unicode.GetBytes(_contentName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <param name="offset"></param>
        /// <param name="binaryReader"></param>
        public string_pool(int countIndex, int offset, BinaryReader binaryReader)
        {
            _countIndex = countIndex;
            _stringLength = binaryReader.ReadInt16();
            _contentBytes = binaryReader.ReadBytes(_stringLength * 2);
            _stringEndNull = binaryReader.ReadInt16();
            _offset = offset;
            _contentName = Encoding.Unicode.GetString(_contentBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_stringLength);
            binaryWriter.Write(_contentBytes);
            binaryWriter.Write(_stringEndNull);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, "StringChunk - " + newLine +
                "CountIndex : " + _countIndex + newLine +
                "Length : " + _stringLength + " " + utility.ConvertShortBytes(_stringLength) + newLine +
                "ContentByte : " + utility.ConvertBytesBytes(_contentBytes) + newLine +
                "StringEndNull : " + utility.ConvertShortBytes(_stringEndNull) + newLine +
                "Offset : " + _offset + newLine +
                "ContentName : " + _contentName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GenerateNextStringOffset()
        {
            return ((_stringLength + 2) * 2) + _offset;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentName"></param>
        public void SetConentName(string contentName)
        {
            _stringLength = (short)contentName.Length;
            _contentName = contentName;
            _contentBytes = Encoding.Unicode.GetBytes(_contentName);
        }
    }

    public class attribute
    {
        int _namespaceUri;
        int _name;
        int _value;
        int _type;
        int _data;

        //
        string _namespaceUriString;
        string _nameString;

        public int Name
        { 
            get
            {
                return _name;
            }
        }

        public int NamespaceUri
        {
            get
            {
                return _namespaceUri;
            }
            set
            {
                _namespaceUri = value;
            }
        }

        public string NameString
        {
            get
            {
                return _nameString;
            }
            set
            {
                _nameString = value;
            }
        }

        public int Value
        {
            get
            {
                return _value;
            }
        }

        public int Data
        {
            get
            {
                return _data;
            }
        }

        public static int Size()
        {
            return sizeof(int) * 5;
        }

        public void ResetName(int value, int data, string_chunk stringChunk)
        {
            _value = value;
            _data = data;

            if (50331656 == _type)
            {
                _nameString = stringChunk.GetStringPool(_data).ContentName;
            }
            else
            {
                _nameString = stringChunk.GetStringPool(_name).ContentName;
            }
        }

        public attribute(int namespaceUri, int name, int value, int type, int data, string_chunk stringChunk)
        {
            _namespaceUri = namespaceUri;
            _name = name;
            _value = value;
            _type = type;
            _data = data;

            if (-1 != _namespaceUri)
                _namespaceUriString = stringChunk.GetStringPool(_namespaceUri).ContentName;

            if (50331656 == _type)
            {
                _nameString = stringChunk.GetStringPool(_data).ContentName;
            }
            else
            {
                _nameString = stringChunk.GetStringPool(_name).ContentName;
            }
        }

        public attribute(BinaryReader binaryReader, string_chunk stringChunk)
        {
            _namespaceUri = binaryReader.ReadInt32();
            _name = binaryReader.ReadInt32();
            _value = binaryReader.ReadInt32();
            _type = binaryReader.ReadInt32();
            _data = binaryReader.ReadInt32();

            if (-1 != _namespaceUri)
                _namespaceUriString = stringChunk.GetStringPool(_namespaceUri).ContentName;

            if (string.IsNullOrEmpty(_namespaceUriString))
            {
                int iiiii = 0;
            }


            if (50331656 == _type)
            {
                _nameString = stringChunk.GetStringPool(_data).ContentName;
            }
            else
            {
                _nameString = stringChunk.GetStringPool(_name).ContentName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_namespaceUri);
            binaryWriter.Write(_name);
            binaryWriter.Write(_value);
            binaryWriter.Write(_type);
            binaryWriter.Write(_data);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            //301989896 : "true"
            //16777224 : resource id
            //268435464 : integer  
            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, " ---- Attribute - " + newLine +
                "NamespaceUri : " + _namespaceUri + utility.ConvertIntBytes(_namespaceUri) + "[" + _namespaceUriString + "]" + newLine +
                "Name : " + _name + utility.ConvertIntBytes(_name) + "[" + _nameString + "]" + newLine +
                "Value : " + _value + utility.ConvertIntBytes(_value) + newLine +
                "Type : " + _type + utility.ConvertIntBytes(_type) + newLine +
                "Data : " + _data + utility.ConvertIntBytes(_data));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldString"></param>
        /// <param name="newString"></param>
        public void ReplaceString(string oldString, string newString)
        {
            if (!string.IsNullOrEmpty(_namespaceUriString))
            {
                if (_namespaceUriString.Equals(oldString))
                {
                    _namespaceUriString = newString;
                }
            }

            if (!string.IsNullOrEmpty(_nameString))
            {
                if (_nameString.Equals(oldString))
                {
                    _nameString = newString;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringChunk"></param>
        public void ResetNameUri(string_chunk stringChunk)
        {
            string_pool stringPool = null;
            if (-1 != _namespaceUri)
            {
                stringPool = stringChunk.SearchStringPool(_namespaceUriString);
                _namespaceUri = stringPool.CountIndex;

                if (-1 == _value)
                {
                    stringPool = stringChunk.SearchStringPool(_nameString);
                    _name = stringPool.CountIndex;
                }
                else
                {
                    stringPool = stringChunk.SearchStringPool(_nameString);
                    _value = stringPool.CountIndex;
                    _data = _value;
                }

            }
            else
            {
                stringPool = stringChunk.SearchStringPool(_nameString);
                if (50331656 == _type)
                {
                    _data = stringPool.CountIndex;
                    _value = _data;

                    stringPool = stringChunk.SearchStringPool("package");
                    _name = stringPool.CountIndex;
                }
                else if (268435464 == _type)//268435464 : integer  
                {
                    //_data = stringPool.CountIndex;
                    //_value = _data;
                    _name = stringPool.CountIndex;
                }
                else if (67108872 == _type)
                {
                    _name = stringPool.CountIndex;
                }


            }
        }

        public bool Search(string attributeName, string_chunk stringChunk)
        {
            switch (_type)
            {
                case 50331656:
                case 268435464://minSDKVersion
                case 16777224://icon
                {
                    if (stringChunk.GetStringPool(_name).ContentName.Equals(attributeName))
                        return true;
                }
                break;

                default:
                    break;
            }
            return false;

            //NamespaceUri: 29[1D 00 00 00][http://schemas.android.com/apk/res/android] , 
            //Name : 3[03 00 00 00][com.kint.kintframeworkaosaar.methinksPatcherApplication],
            //Value: 27[1B 00 00 00],
            //Type: 50331656[08 00 00 03],
            //Data: 27[1B 00 00 00]
        }

        public bool Search(int attributeName)
        {
            if (_name == attributeName)
                return true;
            return false;

        }
    }

    public class base_chunk
    {
        protected int _chunkType;
        protected int _chunkSize;
        protected base_chunk _parentChunk;
        protected List<base_chunk> _childChunkList;

        public int ChunkType
        {
            get
            {
                return _chunkType;
            }
        }

        public virtual string NameString
        {
            get
            {
                return "";
            }
        }

        public base_chunk ParentChunk
        {
            get
            {
                return _parentChunk;
            }
            set
            {
                _parentChunk = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResetHierarchy()
        {
            _parentChunk = null;
            if (null != _childChunkList)
            {
                _childChunkList.Clear();
                _childChunkList = null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        public int AddChildChunk(base_chunk child)
        {
            if (null == _childChunkList)
                _childChunkList = new List<base_chunk>();

            _childChunkList.Add(child);
            return _childChunkList.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public base_chunk SearchChildChunk(string name)
        {
            if (null == _childChunkList)
                return null;

            for (int countIndex = 0; countIndex < _childChunkList.Count; ++countIndex)
            {
                if (_childChunkList[countIndex].NameString.Equals(name))
                    return _childChunkList[countIndex];
            }

            return null;
        }


        public void IncreaseChunkSize(int increaseSize)
        {
            _chunkSize += increaseSize;
        }

    }


    public class string_chunk : base_chunk
    {
        int _stringCount;
        int _styleCount;
        int _unknown = -1;
        int _stringOffset;
        int _styleOffset;
        short _styleLength;
        int _align4byte;

        List<int> _stringOffsetList;
        List<string_pool> _stringPoolList;
        List<int> _styleList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <returns></returns>
        public string_pool GetStringPool(int countIndex)
        {
            return _stringPoolList[countIndex];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileSize"></param>
        public void Align4Byte(ref int fileSize)
        {
            fileSize += _align4byte;
            _chunkSize += _align4byte;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        public string_chunk(BinaryReader binaryReader)
        {
            _chunkType = (int)e_chunk_type.String;
            _chunkSize = binaryReader.ReadInt32();
            _stringCount = binaryReader.ReadInt32();
            _styleCount = binaryReader.ReadInt32();
            _unknown = binaryReader.ReadInt32();
            _stringOffset = binaryReader.ReadInt32();
            _styleOffset = binaryReader.ReadInt32();

            int countIndex = 0;
            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                if (null == _stringOffsetList)
                    _stringOffsetList = new List<int>();
                _stringOffsetList.Add(binaryReader.ReadInt32());
            }

            for (countIndex = 0; countIndex < _styleCount; ++countIndex)
            {
                if (null == _styleList)
                    _styleList = new List<int>();
                _styleList.Add(binaryReader.ReadInt32());
            }

            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                if (null == _stringPoolList)
                    _stringPoolList = new List<string_pool>();

                string_pool newStringPool = new string_pool(countIndex, _stringOffsetList[countIndex], binaryReader);


                _stringPoolList.Add(newStringPool);
            }

            //temp
            if (0 == _styleCount && 0 == _styleOffset)
            {
                short temp = binaryReader.ReadInt16();
                if (0 == temp)
                {

                }
                else
                {
                    _styleLength = temp;
                    binaryReader.BaseStream.Position -= sizeof(short);
                }
            }


            if (0 != _styleCount && 0 != _styleOffset)
            {
                _styleLength = binaryReader.ReadInt16();
                if (_styleLength > 0)
                { }

                /*
                ap->string_chunk->style_poll_len = ap->string_chunk->chunk_size - ap->string_chunk->style_poll_offset;
                ap->string_chunk->style_poll_data = (unsigned char *)malloc(ap->string_chunk->style_poll_len);
                CopyData(ap, ap->string_chunk->style_poll_data, ap->string_chunk->style_poll_len);
                //SkipInt32(ap, (ap->string_chunk->chunk_size - ap->string_chunk->style_poll_offset) / 4);
                */

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            binaryWriter.Write(_stringCount);
            binaryWriter.Write(_styleCount);
            binaryWriter.Write(_unknown);
            binaryWriter.Write(_stringOffset);
            binaryWriter.Write(_styleOffset);

            int countIndex = 0;
            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                binaryWriter.Write(_stringOffsetList[countIndex]);
            }

            for (countIndex = 0; countIndex < _styleCount; ++countIndex)
            {
                binaryWriter.Write(_styleList[countIndex]);
            }

            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                string_pool stringPool = _stringPoolList[countIndex];
                stringPool.WriteBinary(binaryWriter);
            }

            if (0 == _styleCount && 0 == _styleOffset)
            {
                binaryWriter.Write(_styleLength);
                if (0 != _styleLength)
                {
                    binaryWriter.BaseStream.Position -= sizeof(short);
                }
            }

            if (0 != _styleCount && 0 != _styleOffset)
            {
                binaryWriter.Write(_styleLength);
                if (_styleLength > 0)
                {
                }
            }

            byte zero = 0x00;
            for (countIndex = 0; countIndex < _align4byte; ++countIndex)
            {
                binaryWriter.Write(zero);
            }


            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, @"StringChunk - " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize) + newLine +
                "StringCount : " + _stringCount + utility.ConvertIntBytes(_stringCount) + newLine +
                "StyleCount : " + _styleCount + utility.ConvertIntBytes(_styleCount) + newLine +
                "Unknown : " + _unknown + utility.ConvertIntBytes(_unknown) + newLine +
                "StringOffset :" + _stringOffset + utility.ConvertIntBytes(_stringOffset) + newLine +
                "StyleOffset : " + _styleOffset + utility.ConvertIntBytes(_styleOffset));

            int countIndex = 0;
            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                utility.WriteText(file, @"StringChunk - StringList[" + countIndex + "/" + _stringCount + "] : " + _stringOffsetList[countIndex] +
                    utility.ConvertIntBytes(_stringOffsetList[countIndex]));
            }

            for (countIndex = 0; countIndex < _styleCount; ++countIndex)
            {
                utility.WriteText(file, @"StringChunk - StyleList[" + countIndex + "/" + _styleCount + "] : " + _styleList[countIndex] +
                    utility.ConvertIntBytes(_styleList[countIndex]));
            }

            for (countIndex = 0; countIndex < _stringCount; ++countIndex)
            {
                _stringPoolList[countIndex].WriteParseText(file);
            }

            if (0 != _styleCount && 0 != _styleOffset)
            {
                utility.WriteText(file, @"StringChunk - StyleLength : " + _styleLength);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringPoolString"></param>
        /// <returns></returns>
        string_pool _AddStringPool(string stringPoolString, ref int fileSize)
        {
            string_pool lastStringPool = _LastStringPool();
            int newStringOffset = lastStringPool.GenerateNextStringOffset();
            //
            string_pool stringPool = new string_pool(_stringPoolList.Count, newStringOffset, stringPoolString);
            _stringPoolList.Add(stringPool);
            _stringOffsetList.Add(newStringOffset);

            //arrage
            _ArrageStringPoolOrder();

            _stringCount = _stringOffsetList.Count;

            int size = sizeof(int) + sizeof(short) + (stringPoolString.Length * 2) + sizeof(short);

            _chunkSize += size;
            _stringOffset += sizeof(int);

            fileSize += size;
            _align4byte = _chunkSize % 4;
            return stringPool;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <param name="stringPoolString"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        string_pool _InsertStringPool(int countIndex, string stringPoolString, ref int fileSize)
        {
            string_pool newStringPool = new string_pool(countIndex, -1, stringPoolString);
            _stringPoolList.Insert(countIndex, newStringPool);
            _stringOffsetList.Insert(countIndex, -1);

            //arrage
            _ArrageStringPoolOrder();

            _stringCount = _stringOffsetList.Count;
            int size = sizeof(int) + sizeof(short) + (stringPoolString.Length * 2) + sizeof(short);

            _chunkSize += size;
            _stringOffset += sizeof(int);
            fileSize += size;

            _align4byte = _chunkSize % 4;

            return newStringPool;
        }

        /// <summary>
        /// 
        /// </summary>
        void _ArrageStringPoolOrder()
        {
            int listCountIndex = 0;
            _stringPoolList[listCountIndex].Offset = 0;
            for (listCountIndex = 1; listCountIndex < _stringPoolList.Count; ++listCountIndex)
            {
                _stringPoolList[listCountIndex].CountIndex = listCountIndex;
                _stringPoolList[listCountIndex].Offset = _stringPoolList[listCountIndex - 1].GenerateNextStringOffset();
            }

            for (listCountIndex = 0; listCountIndex < _stringOffsetList.Count; ++listCountIndex)
            {
                _stringOffsetList[listCountIndex] = _stringPoolList[listCountIndex].Offset;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public string_pool SearchStringPool(string stringPoolString)
        {
            if (null == _stringPoolList || 0 == _stringPoolList.Count)
                return null;

            for (int countIndex = 0; countIndex < _stringPoolList.Count; ++countIndex)
            {
                if (true == _stringPoolList[countIndex].ContentName.Equals(stringPoolString))
                    return _stringPoolList[countIndex];
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringPoolString"></param>
        /// <returns></returns>
        public string_pool SearchAddStringPool(string stringPoolString, ref int fileSize)
        {
            string_pool searchStringPool = SearchStringPool(stringPoolString);
            if (null != searchStringPool)
                return searchStringPool;

            return _AddStringPool(stringPoolString, ref fileSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchStringPoolString"></param>
        /// <param name="stringPoolString"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public string_pool SearchInsertStringPool(string searchStringPoolString, string stringPoolString, ref int fileSize, ref int insertCountIndex)
        {
            string_pool searchStringPool = SearchStringPool(stringPoolString);
            if (null != searchStringPool)
                return searchStringPool;

            searchStringPool = SearchStringPool(searchStringPoolString);
            insertCountIndex = searchStringPool.CountIndex;
            return _InsertStringPool(searchStringPool.CountIndex, stringPoolString, ref fileSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="insertCountIndex"></param>
        /// <param name="stringPoolString"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public string_pool SearchInsertStringPool(int insertCountIndex, string stringPoolString, ref int fileSize)
        {
            return _InsertStringPool(insertCountIndex, stringPoolString, ref fileSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <param name="stringPoolString"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public string_pool ReplaceStringPool(string oldString, string newString, ref int fileSize)
        {
            string_pool stringPool = SearchStringPool(oldString);
            int oldLen = stringPool.ContentBytesLength;

            stringPool.SetConentName(newString);
            int newLen = stringPool.ContentBytesLength;

            int compare = newLen - oldLen;
            fileSize += compare;
            _chunkSize += compare;
            _align4byte = _chunkSize % 4;

            _ArrageStringPoolOrder();

            return stringPool;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string_pool _LastStringPool()
        {
            if (null == _stringPoolList || 0 == _stringPoolList.Count)
                return null;
            return _stringPoolList[_stringPoolList.Count - 1];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <returns></returns>
        string_pool _GetStringPool(int countIndex)
        {
            if (null == _stringPoolList || 0 == _stringPoolList.Count)
                return null;
            return _stringPoolList[countIndex];
        }
    }

    public class resource_id_chunk : base_chunk
    {
        List<int> _idList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        public resource_id_chunk(BinaryReader binaryReader)
        {
            _chunkType = (int)e_chunk_type.ResourceID;
            _chunkSize = binaryReader.ReadInt32();

            for (int i = 0; i < _chunkSize / 4 - 2; ++i)
            {
                if (null == _idList)
                    _idList = new List<int>();

                _idList.Add(binaryReader.ReadInt32());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            for (int i = 0; i < _idList.Count; ++i)
                binaryWriter.Write(_idList[i]);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, "ResourceID -  " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize));

            for (int countIndex = 0; countIndex < _chunkSize / 4 - 2; ++countIndex)
            {
                utility.WriteText(file, "[" + countIndex + "] : " + _idList[countIndex] + utility.ConvertIntBytes(_idList[countIndex]));
            }
        }
    }

    public class start_namespace_chunk : base_chunk
    {
        int _lineNumber;
        int _unknown = -1;
        int _prefix;
        int _uri;
        string _prefixString = "";
        string _uriString = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringChunk"></param>
        public void ResetNameUri(string_chunk stringChunk)
        {
            string_pool stringPool = stringChunk.SearchStringPool(_uriString);
            _uri = stringPool.CountIndex;

            stringPool = stringChunk.SearchStringPool(_prefixString);
            _prefix = stringPool.CountIndex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <param name="stringChunk"></param>
        public start_namespace_chunk(BinaryReader binaryReader, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.StartNamespace;
            _chunkSize = binaryReader.ReadInt32();
            _lineNumber = binaryReader.ReadInt32();
            _unknown = binaryReader.ReadInt32();
            _prefix = binaryReader.ReadInt32();
            _uri = binaryReader.ReadInt32();

            if (-1 != _prefix)
                _prefixString = stringChunk.GetStringPool(_prefix).ContentName;
            if (-1 != _uri)
                _uriString = stringChunk.GetStringPool(_uri).ContentName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            binaryWriter.Write(_lineNumber);
            binaryWriter.Write(_unknown);
            binaryWriter.Write(_prefix);
            binaryWriter.Write(_uri);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            utility.WriteText(file, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, "StartNamespaceChunk -  " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize) + newLine +
                "LineNumber : " + _lineNumber + utility.ConvertIntBytes(_lineNumber) + newLine +
                "Unknown : " + _unknown + utility.ConvertIntBytes(_unknown) + newLine +
                "Prefix : " + _prefix + utility.ConvertIntBytes(_prefix) + _prefixString + newLine +
                "Uri : " + _uri + utility.ConvertIntBytes(_uri) + _uriString);
        }
    }


    public class end_namespace_chunk : base_chunk
    {
        int _lineNumber;
        int _unknown = -1;
        int _prefix;
        int _uri;
        string _prefixString = "";
        string _uriString = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringChunk"></param>
        public void ResetNameUri(string_chunk stringChunk)
        {
            string_pool stringPool = stringChunk.SearchStringPool(_uriString);
            _uri = stringPool.CountIndex;

            stringPool = stringChunk.SearchStringPool(_prefixString);
            _prefix = stringPool.CountIndex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <param name="stringChunk"></param>
        public end_namespace_chunk(BinaryReader binaryReader, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.EndNamespace;
            _chunkSize = binaryReader.ReadInt32();
            _lineNumber = binaryReader.ReadInt32();
            _unknown = binaryReader.ReadInt32();
            _prefix = binaryReader.ReadInt32();
            _uri = binaryReader.ReadInt32();

            if (-1 != _prefix)
                _prefixString = stringChunk.GetStringPool(_prefix).ContentName;
            if (-1 != _uri)
                _uriString = stringChunk.GetStringPool(_uri).ContentName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            binaryWriter.Write(_lineNumber);
            binaryWriter.Write(_unknown);
            binaryWriter.Write(_prefix);
            binaryWriter.Write(_uri);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, "EndNamespaceChunk - " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize) + newLine +
                "LineNumber : " + _lineNumber + utility.ConvertIntBytes(_lineNumber) + newLine +
                "Unknown : " + _unknown + utility.ConvertIntBytes(_unknown) + newLine +
                "Prefix : " + _prefix + utility.ConvertIntBytes(_prefix) + _prefixString + newLine +
                "Uri : " + _uri + utility.ConvertIntBytes(_uri) + _uriString);


            utility.WriteText(file, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
    }

    public class start_tag_chunk : base_chunk
    {
        int _lineNumber;
        int _unknown = -1;
        int _namespaceUri = -1;
        int _name;
        int _flags;
        int _attributeCount;
        int _classAttribute;
        List<attribute> _attributeList;

        //
        string _namespaceUriString;
        string _nameString;

        public override string NameString
        {
            get
            {
                return _nameString;
            }
        }

        public int LineNumber
        {
            get
            {
                return _lineNumber;
            }
            set
            {
                _lineNumber = value;
            }
        }

        public int Size()
        {
            return sizeof(int) * 9 + _attributeCount * attribute.Size();
        }

        public int AttributeSize()
        {
            return attribute.Size();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="chunkSize"></param>
        /// <param name="lineNumber"></param>
        /// <param name="flags"></param>
        /// <param name="classAttribute"></param>
        /// <param name="stringChunk"></param>
        public start_tag_chunk(int name, int chunkSize, int lineNumber, int flags, int classAttribute, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.StartTag;
            _chunkSize = chunkSize;
            _lineNumber = lineNumber;
            _name = name;
            _flags = flags;
            _classAttribute = classAttribute;

            _nameString = stringChunk.GetStringPool(_name).ContentName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <param name="stringChunk"></param>
        public start_tag_chunk(BinaryReader binaryReader, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.StartTag;
            _chunkSize = binaryReader.ReadInt32();

            _lineNumber = binaryReader.ReadInt32();
            _unknown = binaryReader.ReadInt32();
            _namespaceUri = binaryReader.ReadInt32();
            _name = binaryReader.ReadInt32();
            _flags = binaryReader.ReadInt32();
            _attributeCount = binaryReader.ReadInt32();
            _classAttribute = binaryReader.ReadInt32();

            // 
            if (-1 != _namespaceUri)
                _namespaceUriString = stringChunk.GetStringPool(_namespaceUri).ContentName;
            _nameString = stringChunk.GetStringPool(_name).ContentName;

            //
            for (int countIndex = 0; countIndex < _attributeCount; ++countIndex)
            {
                if (null == _attributeList)
                    _attributeList = new List<attribute>();

                attribute attribute = new attribute(binaryReader, stringChunk);
                _attributeList.Add(attribute);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            binaryWriter.Write(_lineNumber);
            binaryWriter.Write(_unknown);
            binaryWriter.Write(_namespaceUri);
            binaryWriter.Write(_name);
            binaryWriter.Write(_flags);
            binaryWriter.Write(_attributeCount);
            binaryWriter.Write(_classAttribute);

            for (int countIndex = 0; countIndex < _attributeCount; ++countIndex)
            {
                _attributeList[countIndex].WriteBinary(binaryWriter);
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, " -- StartTagChunk - " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize) + newLine +
                "LineNumber : " + _lineNumber + utility.ConvertIntBytes(_lineNumber) + newLine +
                "Unknown : " + _unknown + utility.ConvertIntBytes(_unknown) + newLine +
                "NamespaceUri : " + _namespaceUri + utility.ConvertIntBytes(_namespaceUri) + "[" + _namespaceUriString + "]" + newLine +
                "Name : " + _name + utility.ConvertIntBytes(_name) + "[" + _nameString + "]" + newLine +
                "Flags : " + _flags + utility.ConvertIntBytes(_flags) + newLine +
                "AttributeCount : " + _attributeCount + utility.ConvertIntBytes(_attributeCount) + newLine +
                "ClassAttribute : " + _classAttribute + utility.ConvertIntBytes(_classAttribute));

            //
            for (int countIndex = 0; countIndex < _attributeCount; ++countIndex)
            {
                _attributeList[countIndex].WriteParseText(file);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public int SearchAttribute(string attributeName)
        {
            if (null == _attributeList)
                return -1;

            for (int countIndex = 0; countIndex < _attributeList.Count; ++countIndex)
            {
                if (_attributeList[countIndex].NameString.Equals(attributeName))
                    return countIndex;
            }
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countIndex"></param>
        /// <returns></returns>
        public attribute GetAttribute(int countIndex)
        {
            return _attributeList[countIndex];
        }


        public attribute SearchAttributeByName(string attributeName, string_chunk stringChunk)
        {
            if (null == _attributeList)
                return null;

            for (int countIndex = 0; countIndex < _attributeList.Count; ++countIndex)
            {
                if (true == _attributeList[countIndex].Search(attributeName, stringChunk))
                    return _attributeList[countIndex];
            }
            return null;
        }


        public attribute SearchAttributeByName(int attributeName)
        {
            if (null == _attributeList)
                return null;

            for (int countIndex = 0; countIndex < _attributeList.Count; ++countIndex)
            {
                if (true == _attributeList[countIndex].Search(attributeName))
                    return _attributeList[countIndex];
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetAttributeCount()
        {
            if (null == _attributeList)
                return 0;
            return _attributeList.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="namespaceUri"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        /// <param name="stringChunk"></param>
        /// <returns></returns>
        public attribute AddAttribute(int namespaceUri, int name, int value, int type, int data, string_chunk stringChunk)
        {
            if (null == _attributeList)
                _attributeList = new List<attribute>();

            attribute attribute = new attribute(namespaceUri, name, value, type, data, stringChunk);
            _attributeList.Add(attribute);
            _attributeCount = _attributeList.Count;
            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringChunk"></param>
        public void ResetNameUri(string_chunk stringChunk)
        {
            string_pool stringPool = stringChunk.SearchStringPool(_nameString);
            _name = stringPool.CountIndex;

            if (null == _attributeList)
                return;

            for (int countIndex = 0; countIndex < _attributeList.Count; ++countIndex)
            {
                attribute attribute = _attributeList[countIndex];
                attribute.ResetNameUri(stringChunk);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldString"></param>
        /// <param name="newString"></param>
        public void ReplaceString(string oldString, string newString)
        {
            if (_nameString.Equals(oldString))
            {
                _nameString = newString;
            }

            if (null == _attributeList)
                return;

            for (int countIndex = 0; countIndex < _attributeList.Count; ++countIndex)
            {
                attribute attribute = _attributeList[countIndex];
                attribute.ReplaceString(oldString, newString);
            }
        }



        public void IncreaseLineNumber(bool recursive)
        {
            ++LineNumber;
            if (true == recursive)
            {

            }
        }
    }

    public class end_tag_chunk : base_chunk
    {
        int _lineNumber;
        int _unknown = -1;
        int _namespaceUri = -1;
        int _name;

        string _namespaceUriString;
        string _nameString;

        public override string NameString
        {
            get
            {
                return _nameString;
            }
        }

        public int LineNumber
        {
            get
            {
                return _lineNumber;
            }
            set
            {
                _lineNumber = value;
            }
        }


        public static int Size()
        {
            return sizeof(int) * 6;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="chunkSize"></param>
        /// <param name="lineNumber"></param>
        /// <param name="stringChunk"></param>
        public end_tag_chunk(int name, int chunkSize, int lineNumber, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.EndTag;
            _chunkSize = chunkSize;

            _lineNumber = lineNumber;
            _name = name;

            if (-1 != _namespaceUri)
                _namespaceUriString = stringChunk.GetStringPool(_namespaceUri).ContentName;
            _nameString = stringChunk.GetStringPool(_name).ContentName;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <param name="stringChunk"></param>
        public end_tag_chunk(BinaryReader binaryReader, string_chunk stringChunk)
        {
            _chunkType = (int)e_chunk_type.EndTag;
            _chunkSize = binaryReader.ReadInt32();
            _lineNumber = binaryReader.ReadInt32();
            _unknown = binaryReader.ReadInt32();
            _namespaceUri = binaryReader.ReadInt32();
            _name = binaryReader.ReadInt32();

            //
            if (-1 != _namespaceUri)
                _namespaceUriString = stringChunk.GetStringPool(_namespaceUri).ContentName;
            _nameString = stringChunk.GetStringPool(_name).ContentName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);
            binaryWriter.Write(_lineNumber);
            binaryWriter.Write(_unknown);
            binaryWriter.Write(_namespaceUri);
            binaryWriter.Write(_name);

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WriteParseText(StreamWriter file)
        {
            string newLine = " , ";
            if (global_set.ApplyNewLine)
            {
                newLine += Environment.NewLine;
            }

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, " -- EndTagChunk - " + newLine +
                "ChunkType : " + _chunkType + utility.ConvertIntBytes(_chunkType) + newLine +
                "ChunkSize : " + _chunkSize + utility.ConvertIntBytes(_chunkSize) + newLine +
                "LineNumber : " + _lineNumber + utility.ConvertIntBytes(_lineNumber) + newLine +
                "Unknown : " + _unknown + utility.ConvertIntBytes(_unknown) + newLine +
                "NamespaceUri : " + _namespaceUri + utility.ConvertIntBytes(_namespaceUri) + "[" + _namespaceUriString + "]" + newLine +
                "Name : " + _name + utility.ConvertIntBytes(_name) + "[" + _nameString + "]");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringChunk"></param>
        public void ResetNameUri(string_chunk stringChunk)
        {
            string_pool stringPool = stringChunk.SearchStringPool(_nameString);
            _name = stringPool.CountIndex;
        }
    }

    public class text_chunk : base_chunk
    {
        public text_chunk(BinaryReader binaryReader)
        {
            _chunkType = (int)e_chunk_type.Text;
            _chunkSize = binaryReader.ReadInt32();
        }

        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_chunkType);
            binaryWriter.Write(_chunkSize);

            return 0;
        }
    }

    public class android_manifest_binary
    {
        int _magicNumber;
        int _fileSize;
        string_chunk _stringChunk;
        resource_id_chunk _resourceIDChunk;
        List<base_chunk> _baseChunkList;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetPackageName()
        {
            start_tag_chunk startTagChunk = null;
            attribute attribute = null;
            int countIndex = -1;
            return _GetPackageName(ref startTagChunk, ref attribute, ref countIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startTagChunk"></param>
        /// <param name="attribute"></param>
        /// <param name="countIndex"></param>
        /// <returns></returns>
        string _GetPackageName(ref start_tag_chunk startTagChunk, ref attribute attribute, ref int countIndex)
        {
            startTagChunk = (start_tag_chunk)_SearchFirstChunk(e_chunk_type.StartTag, "manifest", ref countIndex);
            for (int countIndex_ = 0; countIndex_ < startTagChunk.GetAttributeCount(); ++countIndex_)
            {
                attribute startTagChunkAttribute = startTagChunk.GetAttribute(countIndex_);
                string[] split = startTagChunkAttribute.NameString.Split('.');
                if (split.Length > 2)
                {
                    int number = 0;
                    bool result = int.TryParse(split[0], out number);
                    if (true == result)
                        continue;

                    attribute = startTagChunkAttribute;
                    return attribute.NameString;
                }
            }
            return "";
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        start_tag_chunk _GetMainActivityChunk()
        {
            base_chunk baseChunk = null;
            int countIndex = 0;
            string attributeName = "";
            bool loop = true;
            for (; countIndex < _baseChunkList.Count; ++countIndex)
            {
                baseChunk = _baseChunkList[countIndex];
                if (e_chunk_type.StartTag == (e_chunk_type)baseChunk.ChunkType)
                {
                    start_tag_chunk startTagChunk = (start_tag_chunk)baseChunk;
                    if (-1 != startTagChunk.SearchAttribute("android.intent.action.MAIN"))
                    {
                        base_chunk parentChunk = baseChunk.ParentChunk;
                        while (null != parentChunk.ParentChunk && true == loop)
                        {
                            if (parentChunk.NameString.Equals("activity"))
                                return (start_tag_chunk)parentChunk;
                            else if (parentChunk.NameString.Equals("activity-alias"))
                            {
                                string_pool searchStringPool = _stringChunk.SearchStringPool("targetActivity");
                                if (null != searchStringPool)
                                {
                                    attribute searchAttribute = ((start_tag_chunk)parentChunk).SearchAttributeByName(searchStringPool.CountIndex);
                                    if(null != searchAttribute)
                                    {
                                        attributeName = searchAttribute.NameString;
                                        countIndex = _baseChunkList.Count;
                                        loop = false;
                                    }
                                }

                            }

                            parentChunk = parentChunk.ParentChunk;
                        }

                        if (true == string.IsNullOrEmpty(attributeName))
                            return null;
                    }
                }
            }

            if (false == string.IsNullOrEmpty(attributeName))
            {
                countIndex = 0;
                for (; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    baseChunk = _baseChunkList[countIndex];
                    if (e_chunk_type.StartTag == (e_chunk_type)baseChunk.ChunkType)
                    {
                        start_tag_chunk startTagChunk = (start_tag_chunk)baseChunk;
                        if (-1 != startTagChunk.SearchAttribute(attributeName))
                        {
                            base_chunk parentChunk = baseChunk;
                            while (null != parentChunk)
                            {
                                if (parentChunk.NameString.Equals("activity"))
                                    return (start_tag_chunk)parentChunk;
                                parentChunk = parentChunk.ParentChunk;
                            }

                            return null;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetMainActivityName(bool includePackageName)
        {
            start_tag_chunk startTagChunk = _GetMainActivityChunk();
            for (int countIndex = 0; countIndex < startTagChunk.GetAttributeCount(); ++countIndex)
            {
                attribute startTagChunkAttribute = startTagChunk.GetAttribute(countIndex);

                //data : 32
                //name: 8
                //namespaceuri: 20
                //value: 32

                string[] split = startTagChunkAttribute.NameString.Split('.');
                if (split.Length > 2)
                {
                    if (false == includePackageName)
                    {
                        return split[split.Length - 1];
                    }
                    return startTagChunkAttribute.NameString;
                }
                else if(2 == split.Length)
                {
                    //.AppEntry
                    if(true == string.IsNullOrEmpty(split[0]))
                    {
                        if (false == includePackageName)
                        {
                            return split[split.Length - 1];
                        }
                        return startTagChunkAttribute.NameString;
                    }

                }
            }
            return "";
        }

        /// <summary>
        /// Sets the name of the main activity full package.
        /// </summary>
        /// <param name="mainActivityFullPackageNameName">Main activity full package name name.</param>
        public void SetMainActivityFullPackageName(string mainActivityFullPackageNameName)
        {
            //string packageName = global_set.AndroidManifestBinary.PackageName;//com.mfs.cuescreen
            //string mainActivityName = global_set.AndroidManifestBinary.MainActivityFullName;//com.mfs.cuescreen.activities.appmain.MainActivity

            int countIndex = 0;
            start_tag_chunk startTagChunk = _GetMainActivityChunk();
            //string newMainActivityName = "";
            attribute startTagChunkAttribute = null;
            string[] split = null;
            for (countIndex = 0; countIndex < startTagChunk.GetAttributeCount(); ++countIndex)
            {
                split = startTagChunk.GetAttribute(countIndex).NameString.Split('.');
                if (split.Length > 2)
                {
                    startTagChunkAttribute = startTagChunk.GetAttribute(countIndex);
                    break;
                }
                else if (2 == split.Length)
                {
                    //.AppEntry
                    if (true == string.IsNullOrEmpty(split[0]))
                    {
                        startTagChunkAttribute = startTagChunk.GetAttribute(countIndex);
                        break;
                    }

                }
            }

            /*
            split = startTagChunkAttribute.NameString.Split('.');
            for (countIndex = 0; countIndex < split.Length; ++countIndex)
            {
                if (split.Length - 1 != countIndex)
                    newMainActivityName += split[countIndex] + ".";
                else
                    newMainActivityName += mainActivityName;
            }
            */
            string oldNameString = startTagChunkAttribute.NameString;
            _stringChunk.ReplaceStringPool(oldNameString, mainActivityFullPackageNameName, ref _fileSize);
            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                base_chunk baseChunk = _baseChunkList[countIndex];
                e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                switch (chunkType)
                {
                    case e_chunk_type.StartNamespace:
                        { }
                        break;

                    case e_chunk_type.EndNamespace:
                        { }
                        break;

                    case e_chunk_type.StartTag:
                        {
                            start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                            if (startTagChunk_.NameString.Equals("activity"))
                            {
                                startTagChunk_.ReplaceString(oldNameString, mainActivityFullPackageNameName);
                            }
                        }
                        break;

                    case e_chunk_type.EndTag:
                        { }
                        break;
                }
            }


            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                base_chunk baseChunk = _baseChunkList[countIndex];
                e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                switch (chunkType)
                {
                    case e_chunk_type.StartNamespace:
                        {
                            start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                            startNamespaceChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.EndNamespace:
                        {
                            end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                            endNamespaceChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.StartTag:
                        {
                            start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                            startTagChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.EndTag:
                        {
                            end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                            endTagChunk_.ResetNameUri(_stringChunk);
                        }
                        break;
                }
            }

            _RebuildHierarchy();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainActivityName"></param>
        public void SetMainActivityName(string mainActivityName)
        {
            int countIndex = 0;
            start_tag_chunk startTagChunk = _GetMainActivityChunk();
            string newMainActivityName = "";
            attribute startTagChunkAttribute = null;
            string[] split = null;

            for (countIndex = 0; countIndex < startTagChunk.GetAttributeCount(); ++countIndex)
            {
                split = startTagChunk.GetAttribute(countIndex).NameString.Split('.');
                if (split.Length > 2)
                {
                    startTagChunkAttribute = startTagChunk.GetAttribute(countIndex);
                    break;
                }
            }

            split = startTagChunkAttribute.NameString.Split('.');
            for (countIndex = 0; countIndex < split.Length; ++countIndex)
            {
                if (split.Length - 1 != countIndex)
                    newMainActivityName += split[countIndex] + ".";
                else
                    newMainActivityName += mainActivityName;
            }
            _stringChunk.ReplaceStringPool(startTagChunkAttribute.NameString, newMainActivityName, ref _fileSize);


            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                base_chunk baseChunk = _baseChunkList[countIndex];
                e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                switch (chunkType)
                {
                    case e_chunk_type.StartNamespace:
                        { }
                        break;

                    case e_chunk_type.EndNamespace:
                        { }
                        break;

                    case e_chunk_type.StartTag:
                        {
                            start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                            startTagChunk_.ReplaceString(startTagChunkAttribute.NameString, newMainActivityName);
                        }
                        break;

                    case e_chunk_type.EndTag:
                        { }
                        break;
                }
            }

            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                base_chunk baseChunk = _baseChunkList[countIndex];
                e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                switch (chunkType)
                {
                    case e_chunk_type.StartNamespace:
                        {
                            start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                            startNamespaceChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.EndNamespace:
                        {
                            end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                            endNamespaceChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.StartTag:
                        {
                            start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                            startTagChunk_.ResetNameUri(_stringChunk);
                        }
                        break;

                    case e_chunk_type.EndTag:
                        {
                            end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                            endTagChunk_.ResetNameUri(_stringChunk);
                        }
                        break;
                }
            }

            _RebuildHierarchy();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunkType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        base_chunk _SearchFirstChunk(e_chunk_type chunkType, string name, ref int countIndex_)
        {
            base_chunk baseChunk = null;
            for (int countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                baseChunk = _baseChunkList[countIndex];
                if (chunkType == (e_chunk_type)baseChunk.ChunkType)
                {
                    if (baseChunk.NameString.Equals(name))
                    {
                        countIndex_ = countIndex;
                        return baseChunk;
                    }
                }
            }

            countIndex_ = -1;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunkType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        base_chunk _SearchLastChunk(e_chunk_type chunkType, string name, ref int countIndex_)
        {
            base_chunk baseChunk = null;
            for (int countIndex = _baseChunkList.Count - 1; countIndex > -1; --countIndex)
            {
                baseChunk = _baseChunkList[countIndex];
                if (chunkType == (e_chunk_type)baseChunk.ChunkType)
                {
                    if (baseChunk.NameString.Equals(name))
                    {
                        countIndex_ = countIndex;
                        return baseChunk;
                    }
                }
            }

            countIndex_ = -1;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<base_chunk> _SearchAllChunks(string[] names)
        {
            List<base_chunk> baseChunkList = null;
            for (int countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                base_chunk baseChunk = _baseChunkList[countIndex];

                for (int countIndex2 = 0; countIndex2 < names.Length; ++countIndex2)
                {
                    if (baseChunk.NameString.Equals(names[countIndex2]))
                    {
                        if (null == baseChunkList)
                            baseChunkList = new List<base_chunk>();

                        baseChunkList.Add(baseChunk);
                    }
                }
            }
            return baseChunkList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="attributeKey"></param>
        /// <param name="attributeName"></param>
        public string SearchAddAttribute(string tagName, string attributeKey, string attributeName)
        {
#if _HELP_CODE
            ----Attribute -  , 
            NamespaceUri: 29[1D 00 00 00][http://schemas.android.com/apk/res/android] , 
            Name : 3[03 00 00 00][com.kint.kintframeworkaosaar.methinksPatcherApplication],
            Value: 27[1B 00 00 00],
            Type: 50331656[08 00 00 03],
            Data: 27[1B 00 00 00]
#endif
            string originApplicationName = "";
            int listApplicationCountIndex = -1;
            start_tag_chunk applicationStartTagChunk = (start_tag_chunk)_SearchFirstChunk(e_chunk_type.StartTag, tagName, ref listApplicationCountIndex);
            attribute searchedAttriute = applicationStartTagChunk.SearchAttributeByName(attributeKey, _stringChunk);
            if (null != searchedAttriute)
            {
                originApplicationName = searchedAttriute.NameString;

                string_pool stringPool = _stringChunk.ReplaceStringPool(originApplicationName, attributeName, ref _fileSize);
                searchedAttriute.ResetName(stringPool.CountIndex, stringPool.CountIndex, _stringChunk);
            }
            else
            {
                string_pool stringPool = _stringChunk.SearchAddStringPool(attributeName, ref _fileSize);

                string_pool nameStringPool = _stringChunk.SearchStringPool(attributeKey);

                string_pool stringPoolNamespaceUri = _stringChunk.SearchStringPool("http://schemas.android.com/apk/res/android");
                applicationStartTagChunk.AddAttribute(stringPoolNamespaceUri.CountIndex, nameStringPool.CountIndex, stringPool.CountIndex, 50331656, stringPool.CountIndex, _stringChunk);

                _fileSize += attribute.Size();
                applicationStartTagChunk.IncreaseChunkSize(attribute.Size());

            }

            _RebuildHierarchy();
            return originApplicationName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usesPermissionName"></param>
        public void SearchAddUsesPermission(string usesPermissionName)
        {
            int countIndex = 0;
            int insertCountIndex = -1;

            string_pool stringPoolUsesPermissionTag = _stringChunk.SearchInsertStringPool("uses-sdk", "uses-permission", ref _fileSize, ref insertCountIndex);
            if (-1 != insertCountIndex)
            {
                /*
                //line merge
                List<base_chunk> searchAllChunkList = _SearchAllChunks(new string[] { "application" , "activity" ,"intent-filter" ,"action","category" });
                for (countIndex = 0; countIndex < searchAllChunkList.Count; ++countIndex)
                {
                    e_chunk_type chunkType = (e_chunk_type)searchAllChunkList[countIndex].ChunkType;

                    if (e_chunk_type.StartTag == chunkType)
                    {
                        ++((start_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                    else if (e_chunk_type.EndTag == chunkType)
                    {
                        ++((end_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                }
                */

                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                                startTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                                endTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            insertCountIndex = -1;
            string_pool stringPoolUsesPermissionContent = _stringChunk.SearchInsertStringPool("application", usesPermissionName, ref _fileSize, ref insertCountIndex);
            if (-1 != insertCountIndex)
            {
                /*
                //line merge
                List<base_chunk> searchAllChunkList = _SearchAllChunks(new string[] { "application", "activity", "intent-filter", "action", "category" });
                for (countIndex = 0; countIndex < searchAllChunkList.Count; ++countIndex)
                {
                    e_chunk_type chunkType = (e_chunk_type)searchAllChunkList[countIndex].ChunkType;

                    if (e_chunk_type.StartTag == chunkType)
                    {
                        ++((start_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                    else if (e_chunk_type.EndTag == chunkType)
                    {
                        ++((end_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                }
                */

                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                                startTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                                endTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;
                    }
                }

                //
                int listUsesPermission = -1;
                start_tag_chunk usesPermissionStartTagChunk = (start_tag_chunk)_SearchLastChunk(e_chunk_type.StartTag, "uses-permission", ref listUsesPermission);

                int listApplicationCountIndex = -1;
                start_tag_chunk applicationStartTagChunk = (start_tag_chunk)_SearchFirstChunk(e_chunk_type.StartTag, "application", ref listApplicationCountIndex);

                int lineNumber = 1;
                if (-1 != listUsesPermission)
                {
                    usesPermissionStartTagChunk = (start_tag_chunk)_baseChunkList[listUsesPermission];
                    lineNumber = usesPermissionStartTagChunk.LineNumber + 1;
                }
                else
                {
                    lineNumber = applicationStartTagChunk.LineNumber;
                }
                //

                string_pool stringPoolNamespaceUri = _stringChunk.SearchStringPool("http://schemas.android.com/apk/res/android");

                //Start Tag
                start_tag_chunk startTagChunk = new start_tag_chunk(stringPoolUsesPermissionTag.CountIndex, 56, lineNumber, 1310740, 0, _stringChunk);
                _baseChunkList.Insert(listApplicationCountIndex, startTagChunk);

                //Attribute
                startTagChunk.AddAttribute(stringPoolNamespaceUri.CountIndex, 3, stringPoolUsesPermissionContent.CountIndex, 50331656, stringPoolUsesPermissionContent.CountIndex, _stringChunk);

                //End Tag
                end_tag_chunk endTagChunk = new end_tag_chunk(stringPoolUsesPermissionTag.CountIndex, 24, lineNumber, _stringChunk);
                applicationStartTagChunk = (start_tag_chunk)_SearchFirstChunk(e_chunk_type.StartTag, "application", ref listApplicationCountIndex);
                _baseChunkList.Insert(listApplicationCountIndex, endTagChunk);

                _fileSize += startTagChunk.Size();
                _fileSize += end_tag_chunk.Size();

                _RebuildHierarchy();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        public void SearchAddService(string serviceName)
        {
            int countIndex = 0;
            int insertCountIndex = -1;

            string_pool stringPoolServiceTag = _stringChunk.SearchInsertStringPool("uses-permission", "service", ref _fileSize, ref insertCountIndex);
            if (-1 != insertCountIndex)
            {
                /*
                //line merge
                List<base_chunk> searchAllChunkList = _SearchAllChunks(new string[] { "application" , "activity" ,"intent-filter" ,"action","category" });
                for (countIndex = 0; countIndex < searchAllChunkList.Count; ++countIndex)
                {
                    e_chunk_type chunkType = (e_chunk_type)searchAllChunkList[countIndex].ChunkType;

                    if (e_chunk_type.StartTag == chunkType)
                    {
                        ++((start_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                    else if (e_chunk_type.EndTag == chunkType)
                    {
                        ++((end_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                }
                */

                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                                startTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                                endTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (false == serviceName.Contains(GetMainActivityName(true)))
                serviceName = GetPackageName() + "." + serviceName;


            string mainActivityFullName = GetMainActivityName(true);
            insertCountIndex = -1;
            string_pool stringPoolServiceContent = _stringChunk.SearchInsertStringPool(mainActivityFullName, serviceName, ref _fileSize, ref insertCountIndex);
            if (-1 != insertCountIndex)
            {
                /*
                //line merge
                List<base_chunk> searchAllChunkList = _SearchAllChunks(new string[] { "application", "activity", "intent-filter", "action", "category" });
                for (countIndex = 0; countIndex < searchAllChunkList.Count; ++countIndex)
                {
                    e_chunk_type chunkType = (e_chunk_type)searchAllChunkList[countIndex].ChunkType;

                    if (e_chunk_type.StartTag == chunkType)
                    {
                        ++((start_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                    else if (e_chunk_type.EndTag == chunkType)
                    {
                        ++((end_tag_chunk)searchAllChunkList[countIndex]).LineNumber;
                    }
                }
                */

                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                                startTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                                endTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;
                    }
                }

                //
                int listActivity = -1;
                start_tag_chunk activityStartTagChunk = (start_tag_chunk)_SearchLastChunk(e_chunk_type.StartTag, "activity", ref listActivity);

                int listApplicationCountIndex = -1;
                end_tag_chunk applicationEndTagChunk = (end_tag_chunk)_SearchLastChunk(e_chunk_type.EndTag, "application", ref listApplicationCountIndex);

                int lineNumber = 1;
                if (-1 != listActivity)
                {
                    activityStartTagChunk = (start_tag_chunk)_baseChunkList[listActivity];
                    lineNumber = activityStartTagChunk.LineNumber + 1;
                }
                else
                {
                    lineNumber = applicationEndTagChunk.LineNumber;
                }

                //
                string_pool stringPoolNamespaceUri = _stringChunk.SearchStringPool("http://schemas.android.com/apk/res/android");

                //Start Tag
                start_tag_chunk startTagChunk = new start_tag_chunk(stringPoolServiceTag.CountIndex, 56, lineNumber, 1310740, 0, _stringChunk);
                _baseChunkList.Insert(listApplicationCountIndex, startTagChunk);

                //Attribute
                startTagChunk.AddAttribute(stringPoolNamespaceUri.CountIndex, 3, stringPoolServiceContent.CountIndex, 50331656, stringPoolServiceContent.CountIndex, _stringChunk);

                //End Tag
                end_tag_chunk endTagChunk = new end_tag_chunk(stringPoolServiceTag.CountIndex, 24, lineNumber, _stringChunk);
                applicationEndTagChunk = (end_tag_chunk)_SearchLastChunk(e_chunk_type.EndTag, "application", ref listApplicationCountIndex);
                _baseChunkList.Insert(listApplicationCountIndex, endTagChunk);

                _fileSize += startTagChunk.Size();
                _fileSize += end_tag_chunk.Size();

                _RebuildHierarchy();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="androidManifestBinary"></param>
        /// <returns></returns>
        public int Parse(byte[] androidManifestBinary)
        {
            MemoryStream memoryStream = new MemoryStream(androidManifestBinary);
            BinaryReader binaryReader = new BinaryReader(memoryStream);

            _magicNumber = binaryReader.ReadInt32();
            if (0x00080003 != _magicNumber)
                return 1;
            _fileSize = binaryReader.ReadInt32();

            base_chunk currentChunk = null;
            while (binaryReader.BaseStream.Length != binaryReader.BaseStream.Position)
            {
                e_chunk_type chunkType = e_chunk_type.Invalid;
                try
                {
                    chunkType = (e_chunk_type)binaryReader.ReadInt32();
                }
                catch (EndOfStreamException e)
                {
                    Console.WriteLine("Error writing data: {0}.", e.GetType().Name);
                    return 2;
                }

                switch (chunkType)
                {
                    //string chunk
                    case e_chunk_type.String:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            _stringChunk = new string_chunk(binaryReader);
                            _baseChunkList.Add(_stringChunk);

                        }
                        break;

                    //resourceId chunk
                    case e_chunk_type.ResourceID:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            _resourceIDChunk = new resource_id_chunk(binaryReader);
                            _baseChunkList.Add(_resourceIDChunk);

                        }
                        break;

                    //start namespace chunk
                    case e_chunk_type.StartNamespace:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            start_namespace_chunk startNamespaceChunk = new start_namespace_chunk(binaryReader, _stringChunk);
                            _baseChunkList.Add(startNamespaceChunk);

                        }
                        break;

                    //end namespace chunk
                    case e_chunk_type.EndNamespace:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            end_namespace_chunk endNamespaceChunk = new end_namespace_chunk(binaryReader, _stringChunk);
                            _baseChunkList.Add(endNamespaceChunk);

                        }
                        break;

                    //start tag chunk
                    case e_chunk_type.StartTag:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            start_tag_chunk startTagChunk = new start_tag_chunk(binaryReader, _stringChunk);
                            _baseChunkList.Add(startTagChunk);

                            //build hierarchy
                            if (null != currentChunk)
                            {
                                if (e_chunk_type.StartTag == (e_chunk_type)currentChunk.ChunkType)
                                {
                                    currentChunk.AddChildChunk(startTagChunk);
                                    startTagChunk.ParentChunk = currentChunk;

                                    currentChunk = startTagChunk;
                                }
                            }
                            else
                            {
                                currentChunk = startTagChunk;
                            }

                        }
                        break;

                    //end tag chunk 
                    case e_chunk_type.EndTag:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            end_tag_chunk endTagChunk = new end_tag_chunk(binaryReader, _stringChunk);
                            _baseChunkList.Add(endTagChunk);

                            //if(null != currentChunk)
                            if (e_chunk_type.StartTag == (e_chunk_type)currentChunk.ChunkType)
                            {
                                currentChunk = currentChunk.ParentChunk;
                            }
                        }
                        break;

                    //text chunk
                    case e_chunk_type.Text:
                        {
                            if (null == _baseChunkList)
                                _baseChunkList = new List<base_chunk>();

                            text_chunk textChunk = new text_chunk(binaryReader);
                            _baseChunkList.Add(textChunk);
                        }
                        break;
                }
            }

            return 0;
        }

        /// <summary>
        /// Gets the minimum SDKV ersion.
        /// </summary>
        /// <returns>The minimum SDKV ersion.</returns>
        public int GetMinSDKVersion()
        {
            int searchUsesSDKCountIndex = -1;
            start_tag_chunk usesSDKStartTagChunk = (start_tag_chunk)_SearchFirstChunk(e_chunk_type.StartTag, "uses-sdk", ref searchUsesSDKCountIndex);

            attribute minSdkVersionAttribute = usesSDKStartTagChunk.SearchAttributeByName("minSdkVersion", _stringChunk);
            if (null != minSdkVersionAttribute)
            {
                return minSdkVersionAttribute.Name;
            }
            return -1;
        }

        /// <summary>
        /// Rebuilds the hierarchy.
        /// </summary>
        void _RebuildHierarchy()
        {
            base_chunk currentChunk = null;
            int countIndex = 0;
            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                currentChunk = _baseChunkList[countIndex];
                if ((e_chunk_type)currentChunk.ChunkType == e_chunk_type.StartTag)
                {
                    start_tag_chunk startTagChunk = (start_tag_chunk)currentChunk;
                    startTagChunk.ResetHierarchy();
                }
            }

            currentChunk = null;
            for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
            {
                e_chunk_type chunkType = (e_chunk_type)_baseChunkList[countIndex].ChunkType;
                switch (chunkType)
                {
                    //string chunk
                    case e_chunk_type.String:
                        { }
                        break;

                    //resourceId chunk
                    case e_chunk_type.ResourceID:
                        { }
                        break;

                    //start namespace chunk
                    case e_chunk_type.StartNamespace:
                        { }
                        break;

                    //end namespace chunk
                    case e_chunk_type.EndNamespace:
                        { }
                        break;

                    //start tag chunk
                    case e_chunk_type.StartTag:
                        {
                            start_tag_chunk startTagChunk = (start_tag_chunk)_baseChunkList[countIndex];
                            if (null != currentChunk)
                            {
                                if (e_chunk_type.StartTag == (e_chunk_type)currentChunk.ChunkType)
                                {
                                    currentChunk.AddChildChunk(startTagChunk);
                                    startTagChunk.ParentChunk = currentChunk;

                                    currentChunk = startTagChunk;
                                }
                            }
                            else
                            {
                                currentChunk = startTagChunk;
                            }

                        }
                        break;

                    //end tag chunk 
                    case e_chunk_type.EndTag:
                        {
                            end_tag_chunk endTagChunk = (end_tag_chunk)_baseChunkList[countIndex];
                            //if(null != currentChunk)
                            if (e_chunk_type.StartTag == (e_chunk_type)currentChunk.ChunkType)
                            {
                                currentChunk = currentChunk.ParentChunk;
                            }
                        }
                        break;

                    //text chunk
                    case e_chunk_type.Text:
                        { }
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binaryWriter"></param>
        /// <returns></returns>
        public int WriteBinary(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_magicNumber);
            if (null != _stringChunk)
            {
                _stringChunk.Align4Byte(ref _fileSize);
            }
            binaryWriter.Write(_fileSize);

            if (null != _baseChunkList)
            {
                int countIndex = 0;
                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk_ = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk_ = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk_ = (start_tag_chunk)baseChunk;
                                startTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk_ = (end_tag_chunk)baseChunk;
                                endTagChunk_.ResetNameUri(_stringChunk);
                            }
                            break;
                    }
                }

                for (countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.String:
                            {
                                string_chunk stringChunk = (string_chunk)baseChunk;
                                stringChunk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.ResourceID:
                            {
                                resource_id_chunk resourceIDChubnk = (resource_id_chunk)baseChunk;
                                resourceIDChubnk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk = (start_tag_chunk)baseChunk;
                                startTagChunk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk = (end_tag_chunk)baseChunk;
                                endTagChunk.WriteBinary(binaryWriter);
                            }
                            break;

                        case e_chunk_type.Text:
                            {
                                text_chunk textChunk = (text_chunk)baseChunk;
                                textChunk.WriteBinary(binaryWriter);
                            }
                            break;
                    }

                }
            }

            Console.WriteLine("AndroidManifestFile - binary FileSize : " + _fileSize);
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullPathFileName"></param>
        public void WriteParseTextFile(string fullPathFileName)
        {
            StreamWriter file = new StreamWriter(fullPathFileName, true);

            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, Environment.NewLine);
            utility.WriteText(file, "MagicNumber : " + _magicNumber + " " + utility.ConvertIntBytes(_magicNumber));
            utility.WriteText(file, "FileSize : " + _fileSize + " " + utility.ConvertIntBytes(_fileSize));

            if (null != _baseChunkList)
            {
                for (int countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.String:
                            {
                                string_chunk stringChunk = (string_chunk)baseChunk;
                                stringChunk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.ResourceID:
                            {
                                resource_id_chunk resourceIDChubnk = (resource_id_chunk)baseChunk;
                                resourceIDChubnk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk = (start_tag_chunk)baseChunk;
                                startTagChunk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk = (end_tag_chunk)baseChunk;
                                endTagChunk.WriteParseText(file);
                            }
                            break;

                        case e_chunk_type.Text:
                            {
                                text_chunk textChunk = (text_chunk)baseChunk;

                            }
                            break;
                    }

                }
            }

            file.Close();
        }
         

        /// <summary>
        /// 
        /// </summary>
        public void WriteConsole()
        {
            utility.WriteText(null, Environment.NewLine);
            utility.WriteText(null, Environment.NewLine);
            utility.WriteText(null, "MagicNumber : " + _magicNumber + " " + utility.ConvertIntBytes(_magicNumber));
            utility.WriteText(null, "FileSize : " + _fileSize + " " + utility.ConvertIntBytes(_fileSize));

            if (null != _baseChunkList)
            {
                for (int countIndex = 0; countIndex < _baseChunkList.Count; ++countIndex)
                {
                    base_chunk baseChunk = _baseChunkList[countIndex];
                    e_chunk_type chunkType = (e_chunk_type)baseChunk.ChunkType;
                    switch (chunkType)
                    {
                        case e_chunk_type.String:
                            {
                                string_chunk stringChunk = (string_chunk)baseChunk;
                                stringChunk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.ResourceID:
                            {
                                resource_id_chunk resourceIDChubnk = (resource_id_chunk)baseChunk;
                                resourceIDChubnk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.StartNamespace:
                            {
                                start_namespace_chunk startNamespaceChunk = (start_namespace_chunk)baseChunk;
                                startNamespaceChunk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.EndNamespace:
                            {
                                end_namespace_chunk endNamespaceChunk = (end_namespace_chunk)baseChunk;
                                endNamespaceChunk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.StartTag:
                            {
                                start_tag_chunk startTagChunk = (start_tag_chunk)baseChunk;
                                startTagChunk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.EndTag:
                            {
                                end_tag_chunk endTagChunk = (end_tag_chunk)baseChunk;
                                endTagChunk.WriteParseText(null);
                            }
                            break;

                        case e_chunk_type.Text:
                            {
                                text_chunk textChunk = (text_chunk)baseChunk;

                            }
                            break;
                    }

                }
            }
        }

    }
}
