﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_distribute_clear_all : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_clear_all()
        {
            _processName = "distribute_clear_all";
        }
          
        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            //delete folder
            string destinationWorkFolderPathName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName;
            int resultCode = utility.DeleteFolderOrFile(_processName, destinationWorkFolderPathName);
            Thread.Sleep(1000);
            return resultCode;
        }
    }
}