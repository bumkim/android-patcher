﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Runtime.ExceptionServices;


namespace methinksPatcherTool
{
    public class main_worker
    {
        protected bool _commandProcessThreadWorkerLoop = true;
        protected Thread _commandProcessThread;
        protected List<execute_command> _executeCommandList;
        protected int _executeCommandCount;
        protected FileSystemWatcher _fileSystemWatcher;

        protected bool _commandProcessLogThreadWorkerLoop = true;
        protected Thread _commandProcessLogThread;

        public main_worker()
        { }

        public void Initialize()
        {
            Console.WriteLine("[main_worker::Initialize]");
            _Initialize();
        }

        /// <summary>
        /// Unhandleds the exception trapper.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void _UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }

        /// <summary>
        /// Uns the initialize.
        /// </summary>
        public void UnInitialize()
        {
            Console.WriteLine("[main_worker::UnInitialize]");
            _UnInitialize();
        }

        /// <summary>
        /// Initialize this instance.
        /// </summary>
        protected virtual int _Initialize()
        {
            //File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "/CurrentDomainBaseDirectory.txt", AppDomain.CurrentDomain.BaseDirectory);
            System.AppDomain.CurrentDomain.UnhandledException += _UnhandledExceptionTrapper;

            //int[] jjj = null;
            //jjj[0] = 1;
            string fullPathFileName = global_set.SetStartArgument(Environment.GetCommandLineArgs());
            global_set.ReadPreset();

            // create log folder
            string logFolderFullPathFileName = global_set.WorkerFolderPathName + "/log";
            DirectoryInfo logDirectoryInfo = new DirectoryInfo(logFolderFullPathFileName);
            if (!logDirectoryInfo.Exists)
                logDirectoryInfo.Create();

            //File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "/Arguments.txt", global_set.Arguments[1]); 
            if (global_set.StartWithArgument)
            {
                OpenFile(fullPathFileName);
            }

            //open work folder
            System.Diagnostics.Process.Start(logFolderFullPathFileName);

            _commandProcessThread = new Thread(new ThreadStart(_WorkCommandProcessThread));
            _commandProcessThread.Start();

            _commandProcessLogThread = new Thread(new ThreadStart(_WorkCommandProcessLogThread));
            _commandProcessLogThread.Start();


#if _SAMPLE_CODE_TEST
            //test sample
            global_set.WorkFileTitleName = "smartweather";
            global_set.RecompiledSignedFileFullName = "smartweather_recompiled_signed_debug_1xxenskMdY_20190123_101948.apk";
            execute_command_distribute_install_device executeCommand = new execute_command_distribute_install_device();
            executeCommand.Run();

            execute_command_distribute_get_device_info executeCommand = new execute_command_distribute_get_device_info();
            executeCommand.Run();
#endif
            return 0;
        }

        /// <summary>
        /// Uns the initialize.
        /// </summary>
        protected virtual void _UnInitialize()
        {
            System.AppDomain.CurrentDomain.UnhandledException -= _UnhandledExceptionTrapper;
            _commandProcessThreadWorkerLoop = false;
            _commandProcessLogThreadWorkerLoop = false;
            Thread.Sleep(200);
            //_workerThread.Abort();
        }

        /// <summary>
        /// Quits the application.
        /// </summary>
        protected virtual void _QuitApplication()
        {
            Console.WriteLine("QuitApplication");
        }

        /// <summary>
        /// Opens the file.
        /// </summary>
        /// <param name="fullPathFileName">Full path file name.</param>
        public virtual void OpenFile(string fullPathFileName)
        {
            Console.WriteLine("[main_worker::OpenFile] - FullPathFileName " + fullPathFileName);

            string fullPathName = fullPathFileName.ToLower();
            string lowerWorkFileFullName = System.IO.Path.GetFileName(fullPathFileName).ToLower();

            // Console.WriteLine("ChangedFileSystemWatcher - FullPathName : " + fullPathName + " , FileName  : " + lowerWorkFileFullName);
            if (lowerWorkFileFullName.Contains(".apk"))
            {
                if (false == lowerWorkFileFullName.Contains("recompile") && false == lowerWorkFileFullName.Contains("signed"))
                {
                    if (0 == _executeCommandCount)
                    {
                        global_set.WorkFileFullPathName = fullPathFileName;
                        global_set.WorkFileFullName = System.IO.Path.GetFileName(global_set.WorkFileFullPathName);
                        global_set.WorkFileTitleName = System.IO.Path.GetFileNameWithoutExtension(global_set.WorkFileFullName);

                        string outputFolderFullPathFileName = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName;
                        DirectoryInfo outputDirectoryInfo = new DirectoryInfo(outputFolderFullPathFileName);
                        if (outputDirectoryInfo.Exists)
                        {
                            utility.DeleteFolderOrFile("OpenFile", outputFolderFullPathFileName);
                        }
                        outputDirectoryInfo.Create();

                        if (null == _executeCommandList)
                            _executeCommandList = new List<execute_command>();
                        _executeCommandList.Clear();

                        int firstSequenceType = 0;// 8;// 0;// 8;// 0;// 
                                                  //if (-1 != global_set.FirstSequenceType)
                                                  //    firstSequenceType = global_set.FirstSequenceType;
                        /*
                        //module file
                        if (global_set.WorkFileFullPathName.Contains(global_set.ModuleApkFileName.ToLower()))
                        {
                            firstSequenceType = 6;
                        }
                        */

                        switch (firstSequenceType)
                        {
                            case 0:
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_all2";
                                    //global_set.PatchModuleFolderName = "patcher_module_library";

                                    _executeCommandList.Add(new execute_command_reset_module_clear_all());
                                    _executeCommandList.Add(new execute_command_reset_module_sample_apk_file_apktool());
                                    _executeCommandList.Add(new execute_command_reset_module_copy_module());

                                    _executeCommandList.Add(new execute_command_distribute_clear_all());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    if (false == global_set.StartWithArgument)
                                    {
                                        _executeCommandList.Add(new execute_command_decompile_manifest());
                                    }
                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());
                                    _executeCommandList.Add(new execute_command_patch_copy_module());
                                    //_executeCommandList.Add(new execute_command_patch_copy_resource());

                                    for (int countIndex = 0; countIndex < global_set.PresetProjectInfo.ProjectInfoList.Count; ++countIndex)
                                    {
                                        project_info projectInfo = global_set.PresetProjectInfo.ProjectInfoList[countIndex];

                                        _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                        _executeCommandList.Add(new execute_command_recompile_apktool());
                                        _executeCommandList.Add(new execute_command_recompile_signapk());
                                        _executeCommandList.Add(new execute_command_distribute_extract_icon());
                                        _executeCommandList.Add(new execute_command_distribute_write_patch_result());

                                        if (-1 == projectInfo.DeviceInstallStep)
                                            continue;

                                        if (-1 < projectInfo.DeviceInstallStep)//0
                                            _executeCommandList.Add(new execute_command_distribute_get_device_info());
                                        if (0 < projectInfo.DeviceInstallStep)//1
                                            _executeCommandList.Add(new execute_command_distribute_install_device());
                                        if (1 < projectInfo.DeviceInstallStep)//2
                                            _executeCommandList.Add(new execute_command_distribute_execute_device());
                                        if (2 < projectInfo.DeviceInstallStep)//3
                                           _executeCommandList.Add(new execute_command_distribute_quit_device());
                                        if (3 < projectInfo.DeviceInstallStep)//4
                                            _executeCommandList.Add(new execute_command_distribute_uninstall_device());
                                             
                                        _executeCommandList.Add(new execute_command_distribute_write_install_result());
                                    }

                                    //if (true == global_set.StartWithArgument)
                                    //{
                                    //    _executeCommandList.Add(new execute_command_distribute_clear_all());
                                    //}
                                }
                                break;

                            case 1:
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_all";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_patch_copy_module());
                                    //_executeCommandList.Add(new execute_command_patch_copy_resource());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());

                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif

                                }
                                break;

                            case 2://merge permission ,  main activity , application ,copy module, copy resource
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_patch_copy_module());
                                    _executeCommandList.Add(new execute_command_patch_copy_resource());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 3://only application
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksPatcherApplication";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 4://main activity + permission
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksMainActivity";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 5://only main activity
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksMainActivity";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 6://only merge permission
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 7://just-decompile-self_merge-recompile , copy module //copy resource
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    //_executeCommandList.Add(new execute_command_patch_copy_resource());
                                    _executeCommandList.Add(new execute_command_patch_copy_module());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 8://just-decompile-self_merge-recompile
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 9://just-decompile-recompile
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());
#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;
                        }

                        _executeCommandCount = _executeCommandList.Count;
                    }
                }
            }
            else if (lowerWorkFileFullName.Contains(".xml"))
            {
                bool execute = false;
                if (true == execute)
                {
                    Thread.Sleep(1000);

#if _NOT_USE_YET
                    if (0 == _executeCommandCount)
                    {
                        global_set.WorkFileFullPathName = e.FullPath.ToLower();
                        global_set.WorkFileFullName = System.IO.Path.GetFileName(global_set.WorkFileFullPathName);
                        global_set.WorkFileTitleName = System.IO.Path.GetFileNameWithoutExtension(global_set.WorkFileFullName);

                        if (null == _executeCommandList)
                            _executeCommandList = new List<execute_command>();
                        _executeCommandList.Clear();

                       //
                       _executeCommandList.Add(new execute_command_decompile_manifest());
                       _executeCommandList.Add(new execute_command_patch_parse_manifest());
                       _executeCommandList.Add(new execute_command_patch_merge_manifest());
                       _executeCommandList.Add(new execute_command_patch_write_manifest());
                        
                        _executeCommandCount = _executeCommandList.Count;
                    }
#endif

#if _NOT_USE_YET
                    byte[] bytes = File.ReadAllBytes(fullPathName);

                    if (null == global_set.AndroidManifestBinary)
                        global_set.AndroidManifestBinary = new android_manifest_binary();
                    global_set.AndroidManifestBinary.Parse(bytes);

                    //_androidManifestBinary.SearchAddUsesPermission("android.permission.INTERNET");
                    //_androidManifestBinary.SearchAddUsesPermission("android.permission.WRITE_SETTINGS");

                    global_set.AndroidManifestBinary.WriteConsole();

                    /*
                    string packageName = _androidManifestBinary.GetPackageName();
                    string mainActivityName = _androidManifestBinary.GetMainActivityName(true);

                    Console.WriteLine("PackageName : " + packageName);
                    Console.WriteLine("OldMainActivityName : " + mainActivityName);

                    //string newMainActivityName = "com.nexxpace.changkiyoon.methinksandroidpatchersample.methinksMainActivity";
                    string newMainActivityName = "methinksMainActivity";
                    _androidManifestBinary.SetMainActivityName(newMainActivityName);
                    
                    bool writeFile = true;
                    if (true == writeFile)
                    {
                        string fullPathFileName = global_set.WorkerFolderPathName + "/AndroidManifest_merged.xml";
                        FileStream fileStream = new FileStream(fullPathFileName, FileMode.Create);
                        BinaryWriter binaryWriter = new BinaryWriter(fileStream);
                        _androidManifestBinary.WriteBinary(binaryWriter);
                        fileStream.Close();
                    }
                    */
#endif
                    int finished = 0;
                }

            }
            else if (lowerWorkFileFullName.Contains(".txt"))
            {
                //_Patch_Step_1_Execute_PatchModuleFile_Command(fullPathName, fileName);
            }
        }


        void _ChangedFileSystemWatcher(object source, FileSystemEventArgs e)
        {
            string fullPathName = e.FullPath.ToLower();
            string lowerWorkFileFullName = e.Name.ToLower();

            // Console.WriteLine("ChangedFileSystemWatcher - FullPathName : " + fullPathName + " , FileName  : " + lowerWorkFileFullName);
            if (lowerWorkFileFullName.Contains(".apk"))
            {
                if (false == lowerWorkFileFullName.Contains("recompile") && false == lowerWorkFileFullName.Contains("signed"))
                {
                    if (0 == _executeCommandCount)
                    {
                        global_set.WorkFileFullPathName = e.FullPath.ToLower();
                        global_set.WorkFileFullName = System.IO.Path.GetFileName(global_set.WorkFileFullPathName);
                        global_set.WorkFileTitleName = System.IO.Path.GetFileNameWithoutExtension(global_set.WorkFileFullName);

                        if (null == _executeCommandList)
                            _executeCommandList = new List<execute_command>();
                        _executeCommandList.Clear();

                        int firstSequenceType = 0;// 8;// 0;// 8;// 0;// 
                        //if (-1 != global_set.FirstSequenceType)
                        //    firstSequenceType = global_set.FirstSequenceType;

                        /*
                        //module file
                        if (global_set.WorkFileFullPathName.Contains(global_set.ModuleApkFileName.ToLower()))
                        {
                            firstSequenceType = 6;
                        }
                        */

                        switch (firstSequenceType)
                        {
                            case 0:
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_all2";

                                    _executeCommandList.Add(new execute_command_decompile_apktool());

                                    /*
                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_patch_copy_module());

                                    for (int countIndex = 0; countIndex < global_set.PresetProjectInfo.ProjectInfoList.Count; ++countIndex)
                                    {
                                        _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                        _executeCommandList.Add(new execute_command_recompile_apktool());
                                        _executeCommandList.Add(new execute_command_recompile_signapk());
                                    }
                                    */
                                }
                                break;

                            case 1:
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_all";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_patch_copy_module());
                                    //_executeCommandList.Add(new execute_command_patch_copy_resource());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());

                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif

                                }
                                break;

                            case 2://merge permission ,  main activity , application ,copy module, copy resource
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_patch_copy_module());
                                    _executeCommandList.Add(new execute_command_patch_copy_resource());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 3://only application
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksPatcherApplication";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_application_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 4://main activity + permission
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksMainActivity";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 5://only main activity
                                {
                                    global_set.PatchModuleFolderName = "patcher_module_methinksMainActivity";

                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_main_activity_module());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 6://only merge permission
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_merge_permission_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 7://just-decompile-self_merge-recompile , copy module //copy resource
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    //_executeCommandList.Add(new execute_command_patch_copy_resource());
                                    _executeCommandList.Add(new execute_command_patch_copy_module());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 8://just-decompile-self_merge-recompile
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());

#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());
                                    _executeCommandList.Add(new execute_command_decompile_manifest());

                                    _executeCommandList.Add(new execute_command_patch_parse_manifest());
                                    _executeCommandList.Add(new execute_command_patch_write_manifest());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;

                            case 9://just-decompile-recompile
                                {
                                    //_executeCommandList.Add(new execute_command_decompile_dex2jar());
#if _NOT_USE_YET
                                    //_executeCommandList.Add(new execute_command_decompile_jad());
                                    _executeCommandList.Add(new execute_command_decompile_apktool());

                                    _executeCommandList.Add(new execute_command_recompile_apktool());
                                    _executeCommandList.Add(new execute_command_recompile_signapk());
#endif
                                }
                                break;
                        }

                        _executeCommandCount = _executeCommandList.Count;
                    }
                }
            }
            else if (lowerWorkFileFullName.Contains(".xml"))
            {
                bool execute = false;
                if (true == execute)
                {
                    Thread.Sleep(1000);

#if _NOT_USE_YET
                    if (0 == _executeCommandCount)
                    {
                        global_set.WorkFileFullPathName = e.FullPath.ToLower();
                        global_set.WorkFileFullName = System.IO.Path.GetFileName(global_set.WorkFileFullPathName);
                        global_set.WorkFileTitleName = System.IO.Path.GetFileNameWithoutExtension(global_set.WorkFileFullName);

                        if (null == _executeCommandList)
                            _executeCommandList = new List<execute_command>();
                        _executeCommandList.Clear();

                       //
                       _executeCommandList.Add(new execute_command_decompile_manifest());
                       _executeCommandList.Add(new execute_command_patch_parse_manifest());
                       _executeCommandList.Add(new execute_command_patch_merge_manifest());
                       _executeCommandList.Add(new execute_command_patch_write_manifest());
                        
                        _executeCommandCount = _executeCommandList.Count;
                    }
#endif

#if _NOT_USE_YET
                    byte[] bytes = File.ReadAllBytes(fullPathName);

                    if (null == global_set.AndroidManifestBinary)
                        global_set.AndroidManifestBinary = new android_manifest_binary();
                    global_set.AndroidManifestBinary.Parse(bytes);

                    //_androidManifestBinary.SearchAddUsesPermission("android.permission.INTERNET");
                    //_androidManifestBinary.SearchAddUsesPermission("android.permission.WRITE_SETTINGS");

                    global_set.AndroidManifestBinary.WriteConsole();

                    /*
                    string packageName = _androidManifestBinary.GetPackageName();
                    string mainActivityName = _androidManifestBinary.GetMainActivityName(true);

                    Console.WriteLine("PackageName : " + packageName);
                    Console.WriteLine("OldMainActivityName : " + mainActivityName);

                    //string newMainActivityName = "com.nexxpace.changkiyoon.methinksandroidpatchersample.methinksMainActivity";
                    string newMainActivityName = "methinksMainActivity";
                    _androidManifestBinary.SetMainActivityName(newMainActivityName);
                    
                    bool writeFile = true;
                    if (true == writeFile)
                    {
                        string fullPathFileName = global_set.WorkerFolderPathName + "/AndroidManifest_merged.xml";
                        FileStream fileStream = new FileStream(fullPathFileName, FileMode.Create);
                        BinaryWriter binaryWriter = new BinaryWriter(fileStream);
                        _androidManifestBinary.WriteBinary(binaryWriter);
                        fileStream.Close();
                    }
                    */
#endif
                    int finished = 0;
                }

            }
            else if (lowerWorkFileFullName.Contains(".txt"))
            {
                //_Patch_Step_1_Execute_PatchModuleFile_Command(fullPathName, fileName);

            }
        }


        void _DeletedFileSystemWatcher(object source, FileSystemEventArgs e)
        {
            string fullPathName = e.FullPath.ToLower();
            string fileName = e.Name.ToLower();
            // Console.WriteLine("DeletedFileSystemWatcher : " + fileName);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="processLog"></param>
        protected virtual void _AddProcessLog(string processCommand,string processLog)
        {
            Console.WriteLine("[main_worker::_AddProcessLog] - " + processCommand + " : " + processLog);
        }

        /// <summary>
        /// Works the thread.
        /// </summary>
        protected void _WorkCommandProcessThread()
        {
            while (true == _commandProcessThreadWorkerLoop)
            {
                if (_executeCommandCount > 0)
                {
                    if (_executeCommandList.Count > 0)
                    {
                        global_set.WorkerProcessLogList.Add("execute_command[" + utility.NowDateTime() + "][" + (_executeCommandCount - _executeCommandList.Count) + " / " + _executeCommandCount + "] - " + _executeCommandList[0].StartProcessName());
                        int resultCode = _executeCommandList[0].Run();
                        global_set.WorkerProcessLogList.Add("execute_command[" + utility.NowDateTime() + "][" + (_executeCommandCount - _executeCommandList.Count) + " / " + _executeCommandCount + "] - " + _executeCommandList[0].FinishProcessName());

                        _executeCommandList.RemoveAt(0);

                        if (0 != resultCode)
                        {
                            bool searchedPatchResultProcess = false;
                            for(int countIndex=0; countIndex<_executeCommandList.Count; ++countIndex)
                            {
                                if (_executeCommandList[countIndex].ProcessName.Equals("distribute_write_patch_result"))
                                {
                                    searchedPatchResultProcess = true;
                                }
                            }

                            if(false == searchedPatchResultProcess)
                            {
                                while (_executeCommandList.Count > 1)
                                {
                                    _executeCommandList.RemoveAt(0);
                                }
                            }
                            else
                            {
                                _executeCommandList.Clear();
                                _executeCommandList.Add(new execute_command_distribute_write_patch_result());
                            }
                        }
                    }
                    else
                    {
                        global_set.WorkerProcessLogList.Add("execute_command[" + utility.NowDateTime() + "][" + (_executeCommandCount - _executeCommandList.Count) + " / " + _executeCommandCount + "] - " + "Finished Patch Process");
                        _executeCommandCount = 0;

                        if (global_set.StartWithArgument)
                        {
                            //last clear
                            execute_command_distribute_clear_all command = new execute_command_distribute_clear_all();
                            command.Run();

                            _commandProcessLogThreadWorkerLoop = false;
                            _commandProcessThreadWorkerLoop = false;
                            _QuitApplication();
                        }
                    }
                }

                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Works the command process detail thread.
        /// </summary>
        protected void _WorkCommandProcessLogThread()
        {
            while (true == _commandProcessLogThreadWorkerLoop)
            {
                if (global_set.WorkerProcessLogList.Count > 0)
                {
                    string processDetailLog = global_set.WorkerProcessLogList[0];

                    _AddProcessLog("", "[" + utility.NowDateTime() + "] - "+processDetailLog);
                    Console.WriteLine(processDetailLog);
                    global_set.WorkerProcessLogList.RemoveAt(0);
                }

                Thread.Sleep(50);
            }
        }

    }
}
