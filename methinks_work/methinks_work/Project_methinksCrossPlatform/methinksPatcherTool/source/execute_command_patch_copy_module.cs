﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_copy_module : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_copy_module()
        {
            _processName = "patch_copy_module";
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 1;

            string patchModulePath = global_set.WorkerFolderPathName + "/patcher/" + global_set.PatchModuleFolderName;
            string[] searchFullPathFileNames = Directory.GetFiles(patchModulePath, "*.smali", SearchOption.AllDirectories);

            string smaliFilePathName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/" + global_set.SmaliFolderName + "/" + "com/kint/kintframeworkaosaar";
            DirectoryInfo smaliDirectoryInfo = new DirectoryInfo(smaliFilePathName);
            if (!smaliDirectoryInfo.Exists)
                smaliDirectoryInfo.Create();

            for (int countIndex = 0; countIndex < searchFullPathFileNames.Length; ++countIndex)
            {
                if (true == searchFullPathFileNames[countIndex].Contains("Application"))
                    continue;
                if (true == searchFullPathFileNames[countIndex].Contains("MainActivity"))
                    continue;

                string fullFileName = System.IO.Path.GetFileName(searchFullPathFileNames[countIndex]);
                string allSmaliText = File.ReadAllText(searchFullPathFileNames[countIndex]);

                //write file
                //File.WriteAllText(global_set.DestinationWorkerFolderPathName + "/" + smaliFilePathName + "/" + fullFileName, allSmaliText);
                File.WriteAllText(smaliFilePathName + "/" + fullFileName, allSmaliText);

                //add log
                global_set.WorkerProcessLogList.Add(fullFileName);
                Console.WriteLine(fullFileName);
            }
              
            return 0;
        }
    }
}
