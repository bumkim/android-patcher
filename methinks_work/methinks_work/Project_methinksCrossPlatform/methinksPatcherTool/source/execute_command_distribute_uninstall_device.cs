﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_distribute_uninstall_device : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_uninstall_device()
        {
            _processName = "distribute_uninstall_device";
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 0;

            string packageName = global_set.AndroidManifestBinary.GetPackageName();

            //
            string osVersion = "";
            if (utility.IsWindowsOS())
            {
                osVersion = "windows";
            }
            else if (utility.IsUnixOS())
            {
                osVersion = "unix";
            }
            string adbFullPathFileName = global_set.WorkerFolderPathName + "/patcher/patcher_tool/adb/" + osVersion + "/adb";

            //adb shell pm uninstall --user 0 <package_name>
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = adbFullPathFileName;
            startInfo.Arguments = "shell pm uninstall --user 0 "+ packageName;

            Console.WriteLine("Argument - " + startInfo.Arguments);
            Process process = null;
            try
            {
                startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process = Process.Start(startInfo);

                bool writeProcessLog = true;
                if (false == writeProcessLog)
                {
                    do
                    {
                        if (!process.HasExited)
                        {
                            process.Refresh();
                        }
                    }
                    while (!process.WaitForExit(1000));               

                }
                else
                {

                    while (!process.StandardOutput.EndOfStream)
                    {
                        string readLine = process.StandardOutput.ReadLine();
                        Console.WriteLine(readLine);
                        global_set.WorkerProcessLogList.Add(readLine);
                    }
                }
            }
            catch (Exception ex)
            {
                global_set.InstallError = _processName + " - " + ex.Message;
                Console.WriteLine(_processName + " - Exception : " + ex.Message);
                global_set.WorkerProcessLogList.Add("Exception : " + ex.Message);
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }

            Thread.Sleep(5000);
            return 0;
        }
    }
}
