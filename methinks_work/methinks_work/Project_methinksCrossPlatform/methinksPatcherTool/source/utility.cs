﻿using System;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;


namespace methinksPatcherTool
{
    public class utility
    {

        /// <summary>
        /// Nows the date time.
        /// </summary>
        /// <returns>The date time.</returns>
        public static string NowDateTime()
        {
            return DateTime.Now.ToString("yyyyMMdd_HHmmss");
        }

        /// <summary>
        /// Ises the windows os.
        /// </summary>
        /// <returns><c>true</c>, if windows os was ised, <c>false</c> otherwise.</returns>
        public static bool IsWindowsOS()
        {
            if (Environment.OSVersion.ToString().ToLower().Contains("windows"))
                return true;
            return false;
        }

        /// <summary>
        /// Ises the unix os.
        /// </summary>
        /// <returns><c>true</c>, if unix os was ised, <c>false</c> otherwise.</returns>
        public static bool IsUnixOS()
        {
            if (Environment.OSVersion.ToString().ToLower().Contains("unix"))
                return true;
            return false;
        }

        public static string ConvertIntBytes(int convert)
        {
            byte[] convertBytes = BitConverter.GetBytes(convert);

            string result = "[";
            for (int countIndex = 0; countIndex < convertBytes.Length; ++countIndex)
            {
                result += string.Format("{0:X2} ", convertBytes[countIndex]);
            }
            result += "]";

            return result;
        }

        public static string ConvertShortBytes(short convert)
        {
            byte[] convertBytes = BitConverter.GetBytes(convert);

            string result = "[";
            for (int countIndex = 0; countIndex < convertBytes.Length; ++countIndex)
            {
                result += string.Format("{0:X2} ", convertBytes[countIndex]);
            }
            result += "]";

            return result;
        }

        public static string ConvertBytesBytes(byte[] convert)
        {
            string result = "[";
            for (int countIndex = 0; countIndex < convert.Length; ++countIndex)
            {
                result += string.Format("{0:X2} ", convert[countIndex]);
            }
            result += "]";

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="text"></param>
        public static void WriteText(StreamWriter file, string text)
        {
            if (null == file)
                Console.WriteLine(text);
            else
                file.WriteLine(text);
        }

        /// <summary>
        /// Deletes the folder or file.
        /// </summary>
        /// <param name="destinationPathName">Destination path name.</param>
        public static int DeleteFolderOrFile(string processName, string destinationPathName)
        {
            int resultCode = 0;
            if (!Directory.Exists(destinationPathName) && !File.Exists(destinationPathName))
                return resultCode;

            ProcessStartInfo startInfo = new ProcessStartInfo();
            if (IsWindowsOS())
            {

            }
            else if (IsUnixOS())
            {
                //rm -rf mactrashimmediately4.png
                startInfo.FileName = "rm";
                startInfo.Arguments = "-rf " + destinationPathName;
            }

            Process process = null;
            try
            {
                process = Process.Start(startInfo);
                do
                {
                    if (!process.HasExited)
                    {
                        process.Refresh();
                    }
                }
                while (!process.WaitForExit(1000));
            }
            catch (Exception ex)
            {
                Console.WriteLine(processName + " - Exception : " + ex.Message);
                resultCode = 1;
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }

            return resultCode;
        }

        /// <summary>
        /// Copy the specified sourceDirectory and targetDirectory.
        /// </summary>
        /// <param name="sourceDirectory">Source directory.</param>
        /// <param name="targetDirectory">Target directory.</param>
        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        /// <summary>
        /// Copies all.
        /// </summary>
        /// <param name="source">Source.</param>
        /// <param name="target">Target.</param>
        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fileInfo in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fileInfo.Name);
                fileInfo.CopyTo(Path.Combine(target.FullName, fileInfo.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        /// <summary>
        /// Gets the file SHA 1 to base64.
        /// </summary>
        /// <returns>The file SHA 1 to base64.</returns>
        /// <param name="fileFullPathName">File full path name.</param>
        public static string GetFileSHA1ToBase64(string fileFullPathName)
        {
            using (SHA1 SHA1 = SHA1Managed.Create())
            {
                using (FileStream fileStream = File.OpenRead(fileFullPathName))
                    return Convert.ToBase64String(SHA1.ComputeHash(fileStream));
            }
        }

        /// <summary>
        /// Gets the file SHA 256 to base64.
        /// </summary>
        /// <returns>The file SHA 256 to base64.</returns>
        /// <param name="fileFullPathName">File full path name.</param>
        public static string GetFileSHA256ToBase64(string fileFullPathName)
        {
            using (SHA256 SHA256 = SHA256Managed.Create())
            {
                using (FileStream fileStream = File.OpenRead(fileFullPathName))
                    return Convert.ToBase64String(SHA256.ComputeHash(fileStream));
            }
        }
    }
}
