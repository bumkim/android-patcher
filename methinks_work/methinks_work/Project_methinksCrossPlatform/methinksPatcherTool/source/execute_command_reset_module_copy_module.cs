﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_reset_module_copy_module : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_reset_module_copy_module()
        {
            _processName = "reset_module_copy_module";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (false == global_set.ApplyModuleApkFile)
                return 0;

            string sourceDirectory = global_set.DestinationWorkerFolderPathName + "/" + System.IO.Path.GetFileNameWithoutExtension(global_set.ModuleApkFileName);
            //string sourceDirectory = global_set.WorkerFolderPathName + "/" + System.IO.Path.GetFileNameWithoutExtension(global_set.SampleApkFileFullName);
            string targetDirectory = global_set.WorkerFolderPathName + "/patcher/" + global_set.PatchModuleFolderName;
            utility.Copy(sourceDirectory, targetDirectory); 
                  
            return 0;
        }
    }
}
