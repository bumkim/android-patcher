﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_reset_module_clear_all : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_reset_module_clear_all()
        {
            _processName = "reset_module_clear_all";
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            if (false == global_set.ApplyModuleApkFile)
                return 0;

            //delete folder
            string destinationWorkFolderPathName = 
                global_set.DestinationWorkerFolderPathName + "/" + System.IO.Path.GetFileNameWithoutExtension(global_set.ModuleApkFileName);
            int resultCode = utility.DeleteFolderOrFile(_processName, destinationWorkFolderPathName);

            Thread.Sleep(1000);
            return resultCode;
        }
    }
}