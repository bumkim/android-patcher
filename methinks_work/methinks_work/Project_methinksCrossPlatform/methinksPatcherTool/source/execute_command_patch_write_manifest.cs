﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_write_manifest : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_write_manifest()
        {
            _processName = "patch_write_manifest";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 1;

            global_set.AndroidManifestBinary.WriteConsole();

            bool writeMergedFile = true;
            if (true == writeMergedFile)
            {
                string androidManifestParseTextFile = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName + "/AndroidManifest_text_merged.txt";
                global_set.AndroidManifestBinary.WriteParseTextFile(androidManifestParseTextFile);
                Thread.Sleep(2000);
            }

            string androidManifestPath = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/AndroidManifest.xml";
            FileStream fileStream = new FileStream(androidManifestPath, FileMode.Create);
            BinaryWriter binaryWriter = new BinaryWriter(fileStream);
            global_set.AndroidManifestBinary.WriteBinary(binaryWriter);
            fileStream.Close();

            return 0;
        }
    }
}
