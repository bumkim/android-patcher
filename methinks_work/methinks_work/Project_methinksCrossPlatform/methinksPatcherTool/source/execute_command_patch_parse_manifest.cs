﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_parse_manifest : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_parse_manifest()
        {
            _processName = "patch_parse_manifest";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            Thread.Sleep(2000);

            int resultCode = 0;
            string androidManifestFullPathFileName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/"+"AndroidManifest.xml";
            byte[] androidManifestBytes = File.ReadAllBytes(androidManifestFullPathFileName);

            if (null == global_set.AndroidManifestBinary)
                global_set.AndroidManifestBinary = new android_manifest_binary();
            global_set.AndroidManifestBinary.Parse(androidManifestBytes);

            string packageName = global_set.AndroidManifestBinary.GetPackageName();

            //
            global_set.OriginMainActivityFullPackageNameDot = global_set.AndroidManifestBinary.GetMainActivityName(true);
            string[] splits = global_set.OriginMainActivityFullPackageNameDot.Split('.');
            if (2 == splits.Length)
            {
                //.AppEntry
                if (true == string.IsNullOrEmpty(splits[0]))
                {
                    global_set.OriginMainActivityFullPackageNameDot = packageName + global_set.OriginMainActivityFullPackageNameDot;
                }
            }
            global_set.OriginMainActivityFullPackageNameSlash = global_set.OriginMainActivityFullPackageNameDot.Replace(".", "/");

            //exist file check
            string fullPathFileName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/smali/" + global_set.OriginMainActivityFullPackageNameSlash + ".smali";
            if (true == File.Exists(fullPathFileName))
            {
                global_set.SmaliFolderName = "smali";
            }
            else
            { 
                fullPathFileName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/smali_classes2/" + global_set.OriginMainActivityFullPackageNameSlash + ".smali";
                if (true == File.Exists(fullPathFileName))
                {
                    global_set.SmaliFolderName = "smali_classes2";
                }
                else
                {
                    fullPathFileName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/smali_classes3/" + global_set.OriginMainActivityFullPackageNameSlash + ".smali";
                    if (true == File.Exists(fullPathFileName))
                    {
                        global_set.SmaliFolderName = "smali_classes3";
                    }
                }
            }

            global_set.AndroidManifestBinary.WriteConsole();

            string androidManifestParseTextFile = global_set.WorkerFolderPathName + "/" + global_set.LogFolderName + "/" + global_set.WorkFileTitleName + "/AndroidManifest_text_origin.txt";
            global_set.AndroidManifestBinary.WriteParseTextFile(androidManifestParseTextFile);
            Thread.Sleep(2000);

            //move origin manifest.xml
            string androidManifestOutPath = global_set.WorkerFolderPathName + "/"+ global_set.LogFolderName + "/" + global_set.WorkFileTitleName + "/AndroidManifest_binary_origin.xml";
            File.Move(androidManifestFullPathFileName, androidManifestOutPath);
            Thread.Sleep(2000);

            //result-check
            Console.WriteLine("[execute_command_patch_parse_manifest::Run] - PackageName : " + packageName);
            global_set.WorkerProcessLogList.Add("PackageName : " + packageName);
            if(string.IsNullOrEmpty(packageName))
            {
                global_set.PatchError = "Failure Parse PackageName";
                resultCode = 1;
            }

            string mainActivityName = global_set.AndroidManifestBinary.GetMainActivityName(true);
            Console.WriteLine("[execute_command_patch_parse_manifest::Run] - MainActivityName : " + mainActivityName);
            global_set.WorkerProcessLogList.Add("MainActivityName : " + mainActivityName);
            if(string.IsNullOrEmpty(mainActivityName))
            {
                global_set.PatchError = "Failure Parse MainActivityName";
                resultCode = 1;
            }

            return resultCode;
        }
    }
}
