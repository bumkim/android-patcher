﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace methinksPatcherTool
{
    public class patch_result
    {
        public string Result { get; set; }
        public string Error { get; set; }
        public string FileName { get; set; }
        public string MinSDKVersion { get; set; }
        public string PackageName { get; set; }
    }


    public class execute_command_distribute_write_patch_result : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_write_patch_result()
        {
            _processName = "distribute_write_patch_result";
        }
         
        /// <summary>
        /// Run this instance.
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            bool debugMode = false;
            string projectID = "projectID";
            if (null != global_set.PresetProjectInfo.ProjectInfoList && 0 < global_set.PresetProjectInfo.ProjectInfoList.Count)
            {
                debugMode = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].DebugMode;
                projectID = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].ID;
            }
            ++ global_set.BuildCount;

            string result = "failure";
            string fileName = "";
            if(string.IsNullOrEmpty(global_set.PatchError))
            {
                result = "success";
                fileName = global_set.RecompiledSignedFileFullName;
            }

            patch_result PatchResult = new patch_result
            {
                Result = result,
                Error = global_set.PatchError,
                FileName = fileName,
                MinSDKVersion = "" + global_set.AndroidManifestBinary.GetMinSDKVersion(),
                PackageName = global_set.AndroidManifestBinary.GetPackageName()
            };

            string patchResultJson = JsonConvert.SerializeObject(PatchResult, Formatting.Indented);
            Console.WriteLine(patchResultJson);

            string patchResultTextFile = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName + "/patch_result_"+ global_set.WorkFileTitleName + "_" + projectID+".txt";
            File.WriteAllText(patchResultTextFile, patchResultJson);

            return 0;
        }
    }
}
