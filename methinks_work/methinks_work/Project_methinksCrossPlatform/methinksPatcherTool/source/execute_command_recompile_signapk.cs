﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_recompile_signapk : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_recompile_signapk()
        { 
            _processName = "recompile_signapk";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            string signApkPath = global_set.WorkerFolderPathName + "/patcher/patcher_tool/signapk";
            string signApkJarPath = global_set.WorkerFolderPathName + "/patcher/patcher_tool/signapk/signapk.jar";

            bool debugMode = false;
            string projectID = "projectID";
            if (null != global_set.PresetProjectInfo.ProjectInfoList && 0 < global_set.PresetProjectInfo.ProjectInfoList.Count)
            {
                debugMode = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].DebugMode;
                projectID = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].ID;
            }

            global_set.RecompiledSignedFileFullName = global_set.WorkFileTitleName + "_recompiled_signed_";
          
            if (true == debugMode)
                global_set.RecompiledSignedFileFullName += "debug_" + projectID;
            else
                global_set.RecompiledSignedFileFullName += "release_" + projectID;
            global_set.RecompiledSignedFileFullName += "_" + utility.NowDateTime() + ".apk";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = global_set.Java8FullPathFileName;
            //startInfo.FileName = "java";
            //startInfo.FileName = "/Library/Java/JavaVirtualMachines/jdk1.8.0_181.jdk/Contents/Home/bin/java";
            //startInfo.FileName = "/Library/Java/JavaVirtualMachines/jdk1.8.0_191.jdk/Contents/Home/bin/java";
            startInfo.Arguments = "-jar " + signApkJarPath + " " +
                signApkPath + "/certificate.pem" + " " +
                signApkPath + "/key.pk8 " +
                global_set.DestinationWorkerFolderPathName + "/" +
                global_set.WorkFileTitleName + "/dist/" +
                global_set.WorkFileTitleName + ".apk " +

                global_set.WorkerFolderPathName + "/log/"+
                //global_set.DestinationWorkerFolderPathName + "/" +
                global_set.WorkFileTitleName + "/" +
                //global_set.WorkFileTitleName + "/dist/" +
                global_set.RecompiledSignedFileFullName;

            Process process = null;
            try
            {
                startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process = Process.Start(startInfo);

                bool writeProcessLog = true;
                if (false == writeProcessLog)
                {
                    do
                    {
                        if (!process.HasExited)
                        {
                            process.Refresh();
                        }
                    }
                    while (!process.WaitForExit(1000));               

                }
                else
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        string readLine = process.StandardOutput.ReadLine();
                        Console.WriteLine(readLine);
                        global_set.WorkerProcessLogList.Add(readLine);
                    }
                }
            }
            catch (Exception ex)
            {
                global_set.PatchError = _processName + " - " + ex.Message;
                Console.WriteLine(_processName + " - Exception : " + ex.Message);
                global_set.WorkerProcessLogList.Add("Exception : " + ex.Message);
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }
            return 0;
        }
    }
}
