﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_merge_permission_manifest : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_merge_permission_manifest()
        {
            _processName = "patch_merge_permission_manifest";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 1;

            string androidDotPermissionDot = "android.permission.";
            if (0 == global_set.PermissionList.Count)
            {
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"WRITE_SETTINGS");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"INTERNET");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"SYSTEM_ALERT_WINDOW");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"ACCESS_NETWORK_STATE");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"READ_EXTERNAL_STORAGE");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"WRITE_EXTERNAL_STORAGE");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"MODIFY_AUDIO_SETTINGS");
                global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot+"ACCESS_WIFI_STATE");
            }
            else
            { 
                for(int countIndex=0; countIndex<global_set.PermissionList.Count; ++countIndex)
                {
                    if(true == global_set.PermissionList[countIndex].Contains(androidDotPermissionDot))
                    {
                        global_set.AndroidManifestBinary.SearchAddUsesPermission(global_set.PermissionList[countIndex]);
                    }
                    else
                    {
                        global_set.AndroidManifestBinary.SearchAddUsesPermission(androidDotPermissionDot + global_set.PermissionList[countIndex]);
                    }
                }
            }

            return 0;
        }
    }
}
