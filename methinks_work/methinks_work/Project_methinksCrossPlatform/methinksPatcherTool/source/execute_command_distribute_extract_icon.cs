﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_distribute_extract_icon : execute_command
    {
        string _iconFullPathFileName;

        /// <summary>
        /// 
        /// </summary>
        public execute_command_distribute_extract_icon()
        {
            _processName = "distribute_extract_icon";
        }

        /// <summary>
        /// Finds the app icon file.
        /// </summary>
        /// <returns><c>true</c>, if app icon file was found, <c>false</c> otherwise.</returns>
        /// <param name="resourcPathName">Resourc path name.</param>
        string _FindAppIconFile(string findFolderName, string resourcPathName)
        {
            if (false == resourcPathName.Contains(findFolderName))
                return "";

            string iconFullPathFileName = resourcPathName + "/" + "launcher.png";
            //Console.WriteLine("FullPathFileName1 : "+fullPathFileName1);
            if (File.Exists(iconFullPathFileName))
            {
                return iconFullPathFileName;
            }

            iconFullPathFileName = resourcPathName + "/" + "ic_launcher.png";
            //Console.WriteLine("FullPathFileName1 : "+fullPathFileName1);
            if (File.Exists(iconFullPathFileName))
            {
                return iconFullPathFileName;
            }

            iconFullPathFileName = resourcPathName + "/" + "app_icon.png";
            //Console.WriteLine("FullPathFileName1 : " + iconFullPathFileName);
            if (File.Exists(iconFullPathFileName))
            {
                return iconFullPathFileName;
            }

            iconFullPathFileName = resourcPathName + "/" + "mc_icon.png";
            //Console.WriteLine("FullPathFileName1 : " + iconFullPathFileName);
            if (File.Exists(iconFullPathFileName))
            {
                return iconFullPathFileName;
            }

            switch (findFolderName)
            {
                case "mipmap":
                {
                    iconFullPathFileName = resourcPathName + "/" + "icon.png";
                    //Console.WriteLine("FullPathFileName1 : " + iconFullPathFileName);
                    if (File.Exists(iconFullPathFileName))
                    {
                        return iconFullPathFileName;
                    }
                }
                break;
            }

            string[] splits = global_set.WorkFileTitleName.Split('_');
            if(null != splits)
            { 
                for(int countIndex=0; countIndex<splits.Length; ++countIndex)
                {
                    iconFullPathFileName = resourcPathName + "/"+splits[countIndex].ToLower() + ".png";
                    //Console.WriteLine("FullPathFileName1 : " + iconFullPathFileName);
                    if (File.Exists(iconFullPathFileName))
                    {
                        return iconFullPathFileName;
                    }
                }
            }

            splits = global_set.WorkFileTitleName.Split(' ');
            if (null != splits)
            {
                for (int countIndex = 0; countIndex < splits.Length; ++countIndex)
                {
                    iconFullPathFileName = resourcPathName + "/" + splits[countIndex].ToLower() + ".png";
                    //Console.WriteLine("FullPathFileName1 : " + iconFullPathFileName);
                    if (File.Exists(iconFullPathFileName))
                    {
                        return iconFullPathFileName;
                    }
                }
            }

            return "";
        }  

        /// <summary>
        /// Run this instance.x
        /// </summary>
        /// <returns>The run.</returns>
        public override int Run()
        {
            //Thread.Sleep(1000);

            string[] iconResourceFolderPathNames = null;
            string iconResourceFolderPathName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/"+"res";
            DirectoryInfo iconResourceDirectoryInfo = new DirectoryInfo(iconResourceFolderPathName);
            if (iconResourceDirectoryInfo.Exists)
            {
                iconResourceFolderPathNames = Directory.GetDirectories(iconResourceFolderPathName);
                if (null != iconResourceFolderPathNames)
                {
                    int countIndex = 0;
                    for (; countIndex < iconResourceFolderPathNames.Length; ++countIndex)
                    {
                        if (false == string.IsNullOrEmpty(_iconFullPathFileName))
                            break;

                        _iconFullPathFileName = _FindAppIconFile("mipmap", iconResourceFolderPathNames[countIndex]);
                    }

                    countIndex = 0;
                    for (; countIndex < iconResourceFolderPathNames.Length; ++countIndex)
                    {
                        if (false == string.IsNullOrEmpty(_iconFullPathFileName))
                            break;

                        _iconFullPathFileName = _FindAppIconFile("drawable", iconResourceFolderPathNames[countIndex]);
                    }
                }
            }

            //copy icon file
            string iconDestinationFullPathFileName = global_set.WorkerFolderPathName + "/log/" + global_set.WorkFileTitleName + "/" + "patch_result_icon.png";
            if (true == string.IsNullOrEmpty(_iconFullPathFileName))
            {
                _iconFullPathFileName = global_set.WorkerFolderPathName + "/patcher/patch_result_icon.png";
            }
            File.Copy(_iconFullPathFileName, iconDestinationFullPathFileName, true);

            return 0;
        }
    }
}

