﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace methinksPatcherTool
{
    public class execute_command_patch_merge_main_activity_module : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_patch_merge_main_activity_module()
        {
            _processName = "patch_merge_main_activity_module";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            if (null == global_set.AndroidManifestBinary)
                return 1;

            //string originMainActivityFullPackageName = global_set.AndroidManifestBinary.GetMainActivityName(true);
            //string originMainActivityFullPackageName2 = originMainActivityFullPackageName.Replace(".", "/");
            //string newMainActivityFullPackageName = "com.kint.kintframeworkaosaar.methinksMainActivity";
            //global_set.AndroidManifestBinary.SetMainActivityFullPackageName(newMainActivityFullPackageName);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            bool debugMode = true;
            string projectID = "";
            if (null != global_set.PresetProjectInfo.ProjectInfoList && 0 < global_set.PresetProjectInfo.ProjectInfoList.Count)
            {
                debugMode = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].DebugMode;
                projectID = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].ID;
            }

            string patchModulePath = global_set.WorkerFolderPathName + "/patcher/" + global_set.PatchModuleFolderName;
            string[] searchFullPathFileNames = Directory.GetFiles(patchModulePath, "*.smali", SearchOption.AllDirectories);

            string patchPackageNameDot = "com.nexxpace.changkiyoon.methinksandroidpatchersample.MainActivity";
            string patchPackageNameSlash = patchPackageNameDot.Replace('.', '/');


            string smaliFilePathName = global_set.DestinationWorkerFolderPathName + "/" + global_set.WorkFileTitleName + "/" + global_set.SmaliFolderName + "/" + "com/kint/kintframeworkaosaar";
            DirectoryInfo smaliFileDirectoryInfo = new DirectoryInfo(smaliFilePathName);
            if (!smaliFileDirectoryInfo.Exists)
                smaliFileDirectoryInfo.Create();

            for (int countIndex = 0; countIndex < searchFullPathFileNames.Length; ++countIndex)
            {
                if (false == searchFullPathFileNames[countIndex].Contains("MainActivity"))
                    continue;
                 
                string fullFileName = System.IO.Path.GetFileName(searchFullPathFileNames[countIndex]);
                string allSmaliText = File.ReadAllText(searchFullPathFileNames[countIndex]);

                allSmaliText = allSmaliText.Replace(patchPackageNameSlash, global_set.OriginMainActivityFullPackageNameSlash);

                //apply preset
                if (allSmaliText.Contains("integrate_mode"))
                    allSmaliText = allSmaliText.Replace("integrate_mode", "patch_mode");

                if (allSmaliText.Contains("{module:{}}"))
                {
                    /*
                    string presetModule = global_set.PresetModuleInfo.PresetModule;
                    presetModule = presetModule.Trim();
                    presetModule = presetModule.Replace(Environment.NewLine, "");
                    presetModule = presetModule.Replace("\r\n", "");
                    presetModule = presetModule.Replace("\n", "");
                    presetModule = presetModule.Replace("\r", "");
                    presetModule = presetModule.Replace("\"", "\\\"");
                    */                   

                    allSmaliText = allSmaliText.Replace("{module:{}}", _ReplacePreset(global_set.PresetModuleInfo.PresetModule));
                }
                 
                if (allSmaliText.Contains("{project:{}}"))
                {
                    /*
                    string presetProject = global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].PresetProject;
                    presetProject = presetProject.Trim();
                    presetProject = presetProject.Replace(Environment.NewLine, "");
                    presetProject = presetProject.Replace("\r\n", "");
                    presetProject = presetProject.Replace("\n", "");
                    presetProject = presetProject.Replace("\r", "");
                    presetProject = presetProject.Replace("\"", "\\\"");
                    */                   

                    allSmaliText = allSmaliText.Replace("{project:{}}", _ReplacePreset(global_set.PresetProjectInfo.ProjectInfoList[global_set.BuildCount].PresetProject));
                }

                //write file
                File.WriteAllText(smaliFilePathName + "/" + fullFileName, allSmaliText);
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////

            return 0;
        }

        /// <summary>
        /// Replaces the preset.
        /// </summary>
        /// <returns>The preset.</returns>
        /// <param name="originReplaceSet">Origin replace set.</param>
        string _ReplacePreset(string originReplaceSet)
        {
            originReplaceSet = originReplaceSet.Trim();
            originReplaceSet = originReplaceSet.Replace(Environment.NewLine, "");
            originReplaceSet = originReplaceSet.Replace("\r\n", "");
            originReplaceSet = originReplaceSet.Replace("\n", "");
            originReplaceSet = originReplaceSet.Replace("\r", "");
            originReplaceSet = originReplaceSet.Replace("\"", "\\\"");

            return originReplaceSet;
        }

    }
}

