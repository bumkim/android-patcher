﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace methinksPatcherTool
{
    public class execute_command_decompile_apktool : execute_command
    {
        /// <summary>
        /// 
        /// </summary>
        public execute_command_decompile_apktool()
        {
            _processName = "decompile_apktool";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int Run()
        {
            string apktoolJarPath = global_set.WorkerFolderPathName + "/patcher/patcher_tool/apktool_2.3.3.jar";
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "java";
            startInfo.Arguments = "-jar " + apktoolJarPath + " d -r " + global_set.WorkFileFullPathName;

            Process process = null;
            try
            {
                startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process = Process.Start(startInfo);

                bool writeProcessLog = true;
                if (false == writeProcessLog)
                {
                    do
                    {
                        if (!process.HasExited)
                        {
                            process.Refresh();
                        }
                    }
                    while (!process.WaitForExit(1000));               
                }
                else
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        string readLine = process.StandardOutput.ReadLine();
                        Console.WriteLine(readLine);
                        global_set.WorkerProcessLogList.Add(readLine);
                    }
                }
            }
            catch (Exception ex)
            {
                global_set.PatchError = _processName + " - " + ex.Message;
                Console.WriteLine(_processName + " - Exception : " + ex.Message);
                global_set.WorkerProcessLogList.Add("Exception : " + ex.Message);
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }
            }

            Thread.Sleep(5000);
            return 0;
        }

    }
}
