﻿#define _SYNC_2019_0118_1400

using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;

//adb command
//adb shell screencap /sdcard/screen.png
//adb pull /sdcard/screen.png \"C:\\Users\\xxx\\Desktop\\prova\\screen.png\";
//chmod 755 file.ext

namespace methinksPatcherTool
{
    public enum e_patch_device
    { 
        aos,
        ios,

        undefined,
    }

    public class preset_module_info
    {
        public string PresetModule;

        public bool Enable = true;
        public bool SkipYetDate = false;
        public string DebugRTMPServerBaseURL;
        public string ReleaseRTMPServerBaseURL;
        public string DebugServiceServerURL;
        public string ReleaseServiceServerURL;
        public bool RunBackground;
        public int VideoWidth;
        public int VideoHeight;
        public int VideoDpi;
        public int VideoDataRate;
        public int VideoFrameRate;
        public int WaitStartTimeTick = 5000;
    }

    public class preset_project_info
    {
        public string PresetProject;
        public List<project_info> ProjectInfoList = new List<project_info>();
    }


    public class project_info_unit
    {
        public string id { get; set; }
        public string build { get; set; }
        public string debug_mode { get; set; }
    }

    public class project_info
    {
        string _id;
        bool _debugMode;
        bool _skipYetDate;
        string _presetProject;
        int _deviceInstallStep = 1;

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public bool DebugMode
        {
            get
            {
                return _debugMode;
            }
            set
            {
                _debugMode = value;
            }
        }

        public string PresetProject
        {
            get
            {
                return _presetProject;
            }
            set
            {
                _presetProject = value;
            }
        }

        public bool SkipYetDate
        {
            get
            {
                return _skipYetDate;
            }
            set
            {
                _skipYetDate = value;
            }
        }

        public int DeviceInstallStep
        {
            get
            {
                return _deviceInstallStep;
            }
            set
            {
                _deviceInstallStep = value;
            }
        }
    }


    public class global_set
    {
        public static bool ApplyNewLine = true;
        public static e_patch_device PatchDevice = e_patch_device.aos;
        public static string OriginWorkerFolderPathName = AppDomain.CurrentDomain.BaseDirectory;
        public static string DestinationWorkerFolderPathName = AppDomain.CurrentDomain.BaseDirectory;
        public static string WorkerFolderPathName = AppDomain.CurrentDomain.BaseDirectory;
        public static preset_module_info PresetModuleInfo = new preset_module_info();
        public static preset_project_info PresetProjectInfo = new preset_project_info();
        public static string WorkFileFullPathName;
        public static string WorkFileFullName;
        public static string WorkFileTitleName;
        public static android_manifest_binary AndroidManifestBinary;
        public static string OriginMainActivityFullPackageNameDot;
        public static string OriginMainActivityFullPackageNameSlash;
        public static string SmaliFolderName;
        public static string PatchModuleFolderName = "patcher_module_all";
        public static string PatchLibraryFolderName = "patcher_library";
        public static string RecompiledSignedFileFullName;
        public static int BuildCount;
        public static bool StartWithArgument;
        public static string PatchError = "";
        public static string InstallError = "";
        public static string Java8FullPathFileName = "";
        public static List<string> WorkerProcessLogList = new List<string>();
        public static List<string> PermissionList = new List<string>();

        public static bool ApplyModuleApkFile;
        public static string ModuleApkFileName = "methinksAndroidPatcherSample.apk";
        public static string ModuleApkFilePackageName = "com.nexxpace.changkiyoon.methinksandroidpatchersample";
        public static string ModulePackageName = "com.kint.kintframeworkaosaar";

        public static readonly string LogFolderName = "log";

        public global_set()
        {}



        /// <summary>
        /// Sets the start argument.
        /// </summary>
        /// <returns>The start argument.</returns>
        /// <param name="args">Arguments.</param>
        public static string SetStartArgument(string[] args)
        {
            string filePath = "";
            Dictionary<string, string> argumentDictionary = new Dictionary<string, string>();
            if (null != args)
            {
                string startArgument = "Start Argument - ";
                for (int countIndex = 0; countIndex < args.Length; ++countIndex)
                {
                    if (countIndex == args.Length - 1)
                        continue;

                    string lowerArgumentKey = args[countIndex].ToLower();
                    string argumentValue = args[countIndex+1];

                    argumentDictionary.Add(lowerArgumentKey, argumentValue);
                    startArgument += "[" + countIndex + "] : " + args[countIndex];
                    if (countIndex != args.Length - 1)
                        startArgument += " , ";
                }
                Console.WriteLine("[global_set::SetStartArgument] - startArgument : " + startArgument);

                string containKey = "-project_id";
                project_info projectInfo = null;
                if (argumentDictionary.ContainsKey(containKey))
                {
                    projectInfo = new project_info();
                    projectInfo.ID = argumentDictionary[containKey];
                }
                else
                    return "";
                 
                containKey = "-debug_type";
                if (argumentDictionary.ContainsKey(containKey))
                {
                    if (null != projectInfo)
                    {
                        projectInfo.DebugMode = Boolean.Parse(argumentDictionary[containKey]);
                    }
                }
                else
                {
                    if (null != projectInfo)
                    {
                        projectInfo.DebugMode = true;
                    }
                }

                containKey = "-patch_type";
                string patchType = "light";
                if (argumentDictionary.ContainsKey(containKey))
                {
                    patchType = argumentDictionary[containKey];
                    if (null != projectInfo)
                    {
                         
                    }
                }

                containKey = "-install_test";
                if (argumentDictionary.ContainsKey(containKey))
                {
                    if (null != projectInfo)
                    {
                        bool installTest = Boolean.Parse(argumentDictionary[containKey]);
                        if (true == installTest)
                            projectInfo.DeviceInstallStep = 3;
                        else
                            projectInfo.DeviceInstallStep = -1;
                    }
                }
                else
                {
                    if (null != projectInfo)
                    {
                        projectInfo.DeviceInstallStep = -1;
                    }
                }

                containKey = "-file_path";
                if (argumentDictionary.ContainsKey(containKey))
                {
                    filePath = argumentDictionary[containKey];
                    if (null != projectInfo)
                    {

                    }
                }
                else
                    return "";

                if (null != projectInfo)
                {
                    project_info_unit projectInfoUnit = new project_info_unit
                    {
                        id = projectInfo.ID,
                        build = "true",
                        debug_mode = projectInfo.DebugMode.ToString()
                    };

                    projectInfo.PresetProject = JsonConvert.SerializeObject(projectInfoUnit, Formatting.Indented);

                    global_set.PresetProjectInfo.ProjectInfoList.Add(projectInfo);
                    global_set.StartWithArgument = true;
                }
            }

            return filePath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="presetModuleFileName"></param>
        static void __ReadPresetModule(string presetModuleFileName)
        {
            string presetModuleFullPathFileName = global_set.WorkerFolderPathName + @"/" + presetModuleFileName;
            string presetModuleJSON = "";

            try
            {
                presetModuleJSON = File.ReadAllText(presetModuleFullPathFileName);
            }
            catch (IOException e)
            {
                Console.WriteLine("[global_set::__ReadPresetModule] - error : " + e.Message);
                return;
            }

            Console.WriteLine("[global_set::__ReadPresetModule] - json : " + presetModuleJSON);

            //
            JObject presetModuleJsonObject = JObject.Parse(presetModuleJSON);
            JObject moduleJsonObject = JObject.Parse(presetModuleJsonObject["module"].ToString());
            if (null != moduleJsonObject && moduleJsonObject.Count > 0)
            {
                global_set.PresetModuleInfo.PresetModule = presetModuleJSON;

                global_set.PresetModuleInfo.Enable = bool.Parse(moduleJsonObject["enable"].ToString());
                global_set.PresetModuleInfo.SkipYetDate = bool.Parse(moduleJsonObject["skip_yet_date"].ToString());
                global_set.PresetModuleInfo.DebugRTMPServerBaseURL = moduleJsonObject["debug_rtmp_server_base_url"].ToString();
                global_set.PresetModuleInfo.ReleaseRTMPServerBaseURL = moduleJsonObject["release_rtmp_server_base_url"].ToString();
                global_set.PresetModuleInfo.DebugServiceServerURL = moduleJsonObject["debug_service_server_url"].ToString();
                global_set.PresetModuleInfo.ReleaseServiceServerURL = moduleJsonObject["release_service_server_url"].ToString();
                global_set.PresetModuleInfo.RunBackground = bool.Parse(moduleJsonObject["run_background"].ToString());
                global_set.PresetModuleInfo.VideoWidth = int.Parse(moduleJsonObject["video_width"].ToString());
                global_set.PresetModuleInfo.VideoHeight = int.Parse(moduleJsonObject["video_height"].ToString());
                global_set.PresetModuleInfo.VideoDpi = int.Parse(moduleJsonObject["video_dpi"].ToString());
                global_set.PresetModuleInfo.VideoDataRate = int.Parse(moduleJsonObject["video_data_rate"].ToString());
                global_set.PresetModuleInfo.VideoFrameRate = int.Parse(moduleJsonObject["video_frame_rate"].ToString());
                global_set.PresetModuleInfo.WaitStartTimeTick = int.Parse(moduleJsonObject["wait_start_time_tick"].ToString());
            }

            if (true == presetModuleJsonObject.ContainsKey("tool"))
            {
                JObject toolJsonObject = JObject.Parse(presetModuleJsonObject["tool"].ToString());
                JArray toolPermissionJsonArray = JArray.Parse(toolJsonObject["permissions"].ToString());
                foreach (JObject item in toolPermissionJsonArray)
                {
                    string permission = item["permission"].ToString();
                    global_set.PermissionList.Add(permission);
                    Console.WriteLine("[global_set::__ReadPresetModule] - Permission : " + permission);
                }
            } 

            Console.WriteLine("[global_set::__ReadPresetModule] - Finished!!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="presetProjectFileName"></param>
        static void __ReadPresetProject(string presetProjectFileName)
        {
            if(string.IsNullOrEmpty(presetProjectFileName))
            {
                Console.WriteLine("[global_set::__ReadPresetProject] - PresetProjectFileName is null or empty.");
                return;
            }

            string presetProjectFullPathFileName = global_set.WorkerFolderPathName + @"/" + presetProjectFileName;
            string presetProjectJSON = "";

            try
            {
                presetProjectJSON = File.ReadAllText(presetProjectFullPathFileName);
            }
            catch (IOException e)
            {
                Console.WriteLine("[global_set::__ReadPresetProject] - error : " + e.Message);
                return;
            }

            Console.WriteLine("[global_set::__ReadPresetProject] - json : " + presetProjectJSON);
            global_set.PresetProjectInfo.PresetProject = presetProjectJSON;

            //
            JObject presetProjectJsonObject = JObject.Parse(presetProjectJSON);
            JArray presetJsonProjectArray = JArray.Parse(presetProjectJsonObject["project"].ToString());

            global_set.PresetProjectInfo.ProjectInfoList.Clear();
            foreach (JObject item in presetJsonProjectArray)
            {
                bool build = bool.Parse(item["build"].ToString());
                if (false == build)
                    continue;

                project_info projectInfo = new project_info();

                projectInfo.ID = item["id"].ToString();
                projectInfo.DebugMode = bool.Parse(item["debug_mode"].ToString());
                //projectInfo.SkipYetDate = bool.Parse(item["skip_yet_date"].ToString());
                projectInfo.PresetProject = item.ToString();

                global_set.PresetProjectInfo.ProjectInfoList.Add(projectInfo);
            }

            Console.WriteLine("[global_set::__ReadPresetProject] - Finished!!");
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ReadPreset()
        {
            string osVersion = "";
            string [] split = null;
            if (utility.IsWindowsOS())
            {
                osVersion = "windows";
            }
            else if (utility.IsUnixOS())
            {
                osVersion = "unix";

                split = global_set.WorkerFolderPathName.Split("/methinksPatcherTool.app");
                if (null != split && split.Length > 1)
                    global_set.WorkerFolderPathName = split[0];

                global_set.OriginWorkerFolderPathName = global_set.OriginWorkerFolderPathName.TrimEnd('/');

                split = global_set.OriginWorkerFolderPathName.Split("MonoBundle");
                if (null != split && split.Length > 1)
                {
                    global_set.DestinationWorkerFolderPathName = split[0] + "Resources";
                }
            }
            else
            {
                Console.WriteLine("unsupported os version");
                return;
            }

            string presetFullPathFileName = global_set.WorkerFolderPathName + "/"+"preset.txt";
            string presetJSON = "";
            try
            {
                presetJSON = File.ReadAllText(presetFullPathFileName);
            }
            catch (IOException e)
            {
                Console.WriteLine("[global_set::ReadPreset] - Error : " + e.Message);
                return;
            }

            Console.WriteLine("[global_set::ReadPreset] - Json : " + presetJSON);
            JObject presetJsonObject = JObject.Parse(presetJSON);

            int firstSequenceType = int.Parse(presetJsonObject["first_sequence_type"].ToString());
            global_set.ModuleApkFileName = presetJsonObject["module_apk_file_name"].ToString();
            global_set.ApplyModuleApkFile = bool.Parse(presetJsonObject["apply_module_apk_file"].ToString());
            global_set.ModuleApkFilePackageName = presetJsonObject["module_apk_file_package_name"].ToString();
            global_set.ModulePackageName = presetJsonObject["module_package_name"].ToString();

            string presetModuleFileName = presetJsonObject["preset_module_file_name"].ToString();
            string presetProjectFileName = "";
            if (false == global_set.StartWithArgument)
                presetProjectFileName = presetJsonObject["preset_project_file_name"].ToString();


            JArray java8FullPathFileNameJarray = JArray.Parse(presetJsonObject["java8_executable"].ToString());
            foreach (JObject item in java8FullPathFileNameJarray)
            {
                if (item["os_version"].ToString().Equals(osVersion))
                {
                    global_set.Java8FullPathFileName = item["fullpath_filename"].ToString();
                }
            }

            __ReadPresetModule(presetModuleFileName);
            if (false == global_set.StartWithArgument)
                __ReadPresetProject(presetProjectFileName);

            Console.WriteLine("[global_set::ReadPreset] - Finished!!");
        }
    }
}
