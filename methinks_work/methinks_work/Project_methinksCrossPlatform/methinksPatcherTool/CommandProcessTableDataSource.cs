﻿using System;
using AppKit;
using CoreGraphics;
using Foundation;
using System.Collections;
using System.Collections.Generic;

namespace methinksPatcherTool
{
    public class CommandProcessTableDataSource : NSTableViewDataSource
    {
        #region Public Variables
        public List<execute_command> CommandProcessList = new List<execute_command>();
        #endregion

        #region Constructors
        public CommandProcessTableDataSource()
        {
        }
        #endregion

        #region Override Methods
        public override nint GetRowCount(NSTableView tableView)
        {
           return CommandProcessList.Count;
        }
        #endregion
    }
}


