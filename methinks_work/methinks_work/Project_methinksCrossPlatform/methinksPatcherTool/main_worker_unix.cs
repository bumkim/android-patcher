﻿using System;
using AppKit;
using Foundation;


namespace methinksPatcherTool
{
    public class main_worker_unix : main_worker
    {
        public main_worker_unix()
        {}
         
        protected override int _Initialize()
        {
            if(0 != base._Initialize())
            {
                //application quit
                NSApplication.SharedApplication.Terminate(NSApplication.SharedApplication);
            }

            return 0;
        }

        protected override void  _UnInitialize()
        {
            base._UnInitialize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="processLog"></param>
        protected override void _AddProcessLog(string processCommand, string processLog)
        {
            base._AddProcessLog(processCommand,processLog);

            /*
            if (!ProcessFlowListBox.Dispatcher.CheckAccess())
            {
                ProcessFlowListBox.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                {
                    ProcessFlowListBox.Items.Add(processLog);
                }
                ));
            }
            else
            {
                ProcessFlowListBox.Items.Add(processLog);
            }
            */

            //ViewController viewController = Storyboard.InstantiateViewController(nameof(ViewController)) as ViewController;

            NSApplication.SharedApplication.InvokeOnMainThread(() => {
                for (int n = 0; n < NSApplication.SharedApplication.Windows.Length; ++n)
                {
                    var content = NSApplication.SharedApplication.Windows[n].ContentViewController as ViewController;
                    if (content != null)
                    {
                        // Reformat all text
                        content.AddProcessLog(processCommand,processLog);
                        
                    }
                }
            });
        }

        /// <summary>
        /// Quits the application.
        /// </summary>
        protected override void _QuitApplication()
        {
            base._QuitApplication();
            //application quit
            NSApplication.SharedApplication.InvokeOnMainThread(() =>
            {
                NSApplication.SharedApplication.Terminate(NSApplication.SharedApplication);
            });
        }
    }
}
