﻿using System;

using AppKit;
using Foundation;

namespace methinksPatcherTool
{
    public partial class ViewController : NSViewController
    {
        CommandProcessTableDataSource _dataSource;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Do any additional setup after loading the view.
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }

        /// <summary>
        /// Awakes from nib.
        /// </summary>
        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            // Create the Product Table Data Source and populate it
            _dataSource = new CommandProcessTableDataSource();
            // Populate the Product Table
            CommandProcessTable.DataSource = _dataSource;
            CommandProcessTable.Delegate = new CommandProcessTableDelegate(_dataSource);
        }

        /// <summary>
        /// Adds the process log.
        /// </summary>
        /// <param name="processCommand">Process command.</param>
        /// <param name="processLog">Process log.</param>
        public void AddProcessLog(string processCommand,string processLog)
        {
            _dataSource.CommandProcessList.Add(new execute_command(processCommand, processLog));
            CommandProcessTable.ReloadData();
            CommandProcessTable.ScrollRowToVisible(_dataSource.CommandProcessList.Count - 1);
        }

        /// <summary>
        /// Clears the process log.
        /// </summary>
        public void ClearProcessLog()
        {
            _dataSource.CommandProcessList.Clear();
            CommandProcessTable.ReloadData();
        }
    }
}
