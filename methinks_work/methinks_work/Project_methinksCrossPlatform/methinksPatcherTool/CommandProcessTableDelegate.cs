﻿using System;
using AppKit;
using CoreGraphics;
using Foundation;
using System.Collections;
using System.Collections.Generic;

namespace methinksPatcherTool
{
    public class CommandProcessTableDelegate : NSTableViewDelegate
    {
        #region Constants 
        private const string CellIdentifier = "ProdCell";
        #endregion

        #region Private Variables
        private CommandProcessTableDataSource DataSource;
        #endregion

        #region Constructors
        public CommandProcessTableDelegate(CommandProcessTableDataSource datasource)
        {
            this.DataSource = datasource;
        }
        #endregion

        #region Override Methods
        public override NSView GetViewForItem(NSTableView tableView, NSTableColumn tableColumn, nint row)
        {
            // This pattern allows you reuse existing views when they are no-longer in use.
            // If the returned view is null, you instance up a new view
            // If a non-null view is returned, you modify it enough to reflect the new data
            NSTextField view = (NSTextField)tableView.MakeView(CellIdentifier, this);
            if (view == null)
            {
                view = new NSTextField();
                view.Identifier = CellIdentifier;
                view.BackgroundColor = NSColor.Clear;
                view.Bordered = false;
                view.Selectable = false;
                view.Editable = false;
            }

            // Setup view based on the column selected
            switch (tableColumn.Title)
            {
                case "CommandColumn":
                    view.StringValue = DataSource.CommandProcessList[(int)row].CommandName;
                    break;
                case "ProcessColumn":
                    view.StringValue = DataSource.CommandProcessList[(int)row].ProcessName;
                    break;
            }

            return view;
        }
        #endregion
    }
}