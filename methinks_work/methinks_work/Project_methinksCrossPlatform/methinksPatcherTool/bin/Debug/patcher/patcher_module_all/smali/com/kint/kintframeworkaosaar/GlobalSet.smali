.class public Lcom/kint/kintframeworkaosaar/GlobalSet;
.super Ljava/lang/Object;
.source "GlobalSet.java"


# static fields
.field private static _questionInfoList:Ljava/util/ArrayList;

.field private static _questionSequence:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;
    .locals 1
    .param p0, "countIndex"    # I

    .line 17
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    return-object v0

    .line 19
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/QuestionInfo;

    return-object v0
.end method

.method public static GetQuestionInfoCount()I
    .locals 1

    .line 53
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 54
    const/4 v0, 0x0

    return v0

    .line 55
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static GetQuestionSequence()I
    .locals 1

    .line 61
    sget v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionSequence:I

    return v0
.end method

.method public static ParseQuestionInfo(Lorg/json/JSONObject;)V
    .locals 5
    .param p0, "questionInfoJson"    # Lorg/json/JSONObject;

    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionSequence:I

    .line 26
    sget-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 27
    sget-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 31
    :cond_0
    :try_start_0
    const-string v1, "questions"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 32
    .local v1, "questionJsonArray":Lorg/json/JSONArray;
    nop

    .local v0, "countIndex":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 34
    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    .line 37
    :cond_1
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    .line 39
    .local v2, "questionInfoJsonObject":Lorg/json/JSONObject;
    new-instance v3, Lcom/kint/kintframeworkaosaar/QuestionInfo;

    invoke-direct {v3}, Lcom/kint/kintframeworkaosaar/QuestionInfo;-><init>()V

    .line 40
    .local v3, "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    invoke-virtual {v3, v2}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->Parse(Lorg/json/JSONObject;)V

    .line 42
    sget-object v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .end local v2    # "questionInfoJsonObject":Lorg/json/JSONObject;
    .end local v3    # "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    .end local v0    # "countIndex":I
    .end local v1    # "questionJsonArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 45
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 49
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method

.method public static SetQuestionSequence(I)V
    .locals 0
    .param p0, "questionSequence"    # I

    .line 66
    sput p0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionSequence:I

    .line 67
    return-void
.end method
