.class public Lcom/kint/kintframeworkaosaar/YUVUtil;
.super Ljava/lang/Object;
.source "YUVUtil.java"


# static fields
.field private static preAllocatedBufferColor:[B

.field private static preAllocatedBufferRotate:[B

.field private static preAllocatedBufferRotate270:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ARGBtoYUV420SemiPlanar([III)[B
    .locals 18
    .param p0, "argb"    # [I
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 49
    move/from16 v1, p1

    move/from16 v2, p2

    mul-int v3, v1, v2

    .line 50
    .local v3, "frameSize":I
    mul-int v4, v1, v2

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x2

    new-array v4, v4, [B

    .line 51
    .local v4, "yuv420sp":[B
    const/4 v5, 0x0

    .line 52
    .local v5, "yIndex":I
    move v6, v3

    .line 55
    .local v6, "uvIndex":I
    const/4 v7, 0x0

    .line 56
    .local v7, "index":I
    const/4 v8, 0x0

    move v9, v7

    move v7, v6

    move v6, v5

    const/4 v5, 0x0

    .local v5, "j":I
    .local v6, "yIndex":I
    .local v7, "uvIndex":I
    .local v9, "index":I
    :goto_0
    if-ge v5, v2, :cond_8

    .line 57
    move v10, v7

    move v7, v6

    const/4 v6, 0x0

    .local v6, "i":I
    .local v7, "yIndex":I
    .local v10, "uvIndex":I
    :goto_1
    if-ge v6, v1, :cond_7

    .line 59
    aget v11, p0, v9

    const/high16 v12, -0x1000000

    and-int/2addr v11, v12

    shr-int/lit8 v11, v11, 0x18

    .line 60
    .local v11, "a":I
    aget v12, p0, v9

    const/high16 v13, 0xff0000

    and-int/2addr v12, v13

    shr-int/lit8 v12, v12, 0x10

    .line 61
    .local v12, "R":I
    aget v13, p0, v9

    const v14, 0xff00

    and-int/2addr v13, v14

    shr-int/lit8 v13, v13, 0x8

    .line 62
    .local v13, "G":I
    aget v14, p0, v9

    const/16 v15, 0xff

    and-int/2addr v14, v15

    shr-int/2addr v14, v8

    .line 65
    .local v14, "B":I
    mul-int/lit8 v16, v12, 0x42

    mul-int/lit16 v8, v13, 0x81

    add-int v16, v16, v8

    mul-int/lit8 v8, v14, 0x19

    add-int v8, v16, v8

    add-int/lit16 v8, v8, 0x80

    shr-int/lit8 v8, v8, 0x8

    add-int/lit8 v8, v8, 0x10

    .line 66
    .local v8, "Y":I
    mul-int/lit8 v16, v12, -0x26

    mul-int/lit8 v17, v13, 0x4a

    sub-int v16, v16, v17

    mul-int/lit8 v17, v14, 0x70

    add-int v15, v16, v17

    add-int/lit16 v15, v15, 0x80

    shr-int/lit8 v15, v15, 0x8

    add-int/lit16 v15, v15, 0x80

    .line 67
    .local v15, "U":I
    mul-int/lit8 v16, v12, 0x70

    mul-int/lit8 v17, v13, 0x5e

    sub-int v16, v16, v17

    mul-int/lit8 v17, v14, 0x12

    sub-int v0, v16, v17

    add-int/lit16 v0, v0, 0x80

    shr-int/lit8 v0, v0, 0x8

    add-int/lit16 v0, v0, 0x80

    .line 72
    .local v0, "V":I
    add-int/lit8 v16, v7, 0x1

    .local v16, "yIndex":I
    if-gez v8, :cond_0

    const/4 v1, 0x0

    goto :goto_2

    :cond_0
    const/16 v1, 0xff

    if-le v8, v1, :cond_1

    const/16 v1, 0xff

    goto :goto_2

    :cond_1
    move v1, v8

    :goto_2
    int-to-byte v1, v1

    aput-byte v1, v4, v7

    .line 73
    .end local v7    # "yIndex":I
    rem-int/lit8 v1, v5, 0x2

    if-nez v1, :cond_6

    rem-int/lit8 v1, v9, 0x2

    if-nez v1, :cond_6

    .line 74
    add-int/lit8 v1, v10, 0x1

    .local v1, "uvIndex":I
    if-gez v0, :cond_2

    const/4 v7, 0x0

    goto :goto_3

    :cond_2
    const/16 v7, 0xff

    if-le v0, v7, :cond_3

    const/16 v7, 0xff

    goto :goto_3

    :cond_3
    move v7, v0

    :goto_3
    int-to-byte v7, v7

    aput-byte v7, v4, v10

    .line 75
    .end local v10    # "uvIndex":I
    add-int/lit8 v10, v1, 0x1

    .restart local v10    # "uvIndex":I
    if-gez v15, :cond_4

    const/4 v7, 0x0

    goto :goto_4

    :cond_4
    const/16 v7, 0xff

    if-le v15, v7, :cond_5

    goto :goto_4

    :cond_5
    move v7, v15

    :goto_4
    int-to-byte v7, v7

    aput-byte v7, v4, v1

    .line 78
    .end local v1    # "uvIndex":I
    :cond_6
    add-int/lit8 v9, v9, 0x1

    .line 57
    add-int/lit8 v6, v6, 0x1

    move/from16 v7, v16

    move/from16 v1, p1

    const/4 v8, 0x0

    goto/16 :goto_1

    .line 56
    .end local v0    # "V":I
    .end local v6    # "i":I
    .end local v8    # "Y":I
    .end local v11    # "a":I
    .end local v12    # "R":I
    .end local v13    # "G":I
    .end local v14    # "B":I
    .end local v15    # "U":I
    .end local v16    # "yIndex":I
    .restart local v7    # "yIndex":I
    :cond_7
    add-int/lit8 v5, v5, 0x1

    move v6, v7

    move v7, v10

    move/from16 v1, p1

    const/4 v8, 0x0

    goto/16 :goto_0

    .line 81
    .end local v5    # "j":I
    .end local v10    # "uvIndex":I
    .local v6, "yIndex":I
    .local v7, "uvIndex":I
    :cond_8
    return-object v4
.end method

.method public static CropYuv(I[BIIII)[B
    .locals 16
    .param p0, "src_format"    # I
    .param p1, "src_yuv"    # [B
    .param p2, "src_width"    # I
    .param p3, "src_height"    # I
    .param p4, "dst_width"    # I
    .param p5, "dst_height"    # I

    .line 279
    move/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    if-nez v1, :cond_0

    const/4 v6, 0x0

    return-object v6

    .line 281
    :cond_0
    if-ne v2, v4, :cond_1

    if-ne v3, v5, :cond_1

    .line 282
    move-object/from16 v6, p1

    goto/16 :goto_5

    .line 284
    :cond_1
    mul-int v6, v4, v5

    int-to-double v6, v6

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    mul-double v6, v6, v8

    double-to-int v6, v6

    new-array v6, v6, [B

    .line 285
    .local v6, "dst_yuv":[B
    const/16 v7, 0x27

    const/4 v8, 0x0

    if-eq v0, v7, :cond_5

    const v7, 0x7f000100

    if-eq v0, v7, :cond_5

    const v7, 0x7fa30c00

    if-eq v0, v7, :cond_5

    packed-switch v0, :pswitch_data_0

    .line 351
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 290
    :pswitch_0
    const/4 v7, 0x0

    .line 291
    .local v7, "src_yoffset":I
    const/4 v9, 0x0

    .line 292
    .local v9, "dst_yoffset":I
    move v10, v9

    move v9, v7

    const/4 v7, 0x0

    .local v7, "i":I
    .local v9, "src_yoffset":I
    .local v10, "dst_yoffset":I
    :goto_0
    if-ge v7, v5, :cond_2

    .line 293
    invoke-static {v1, v9, v6, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 294
    add-int/2addr v9, v2

    .line 295
    add-int/2addr v10, v4

    .line 292
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 299
    .end local v7    # "i":I
    :cond_2
    const/4 v7, 0x0

    .line 300
    .local v7, "src_uoffset":I
    const/4 v11, 0x0

    .line 301
    .local v11, "dst_uoffset":I
    mul-int v9, v2, v3

    .line 302
    mul-int v10, v4, v5

    .line 303
    move v12, v11

    move v11, v7

    const/4 v7, 0x0

    .local v7, "i":I
    .local v11, "src_uoffset":I
    .local v12, "dst_uoffset":I
    :goto_1
    div-int/lit8 v13, v5, 0x2

    if-ge v7, v13, :cond_3

    .line 304
    add-int v13, v9, v11

    add-int v14, v10, v12

    div-int/lit8 v15, v4, 0x2

    invoke-static {v1, v13, v6, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 306
    div-int/lit8 v13, v2, 0x2

    add-int/2addr v11, v13

    .line 307
    div-int/lit8 v13, v4, 0x2

    add-int/2addr v12, v13

    .line 303
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 311
    .end local v7    # "i":I
    :cond_3
    const/4 v7, 0x0

    .line 312
    .local v7, "src_voffset":I
    const/4 v13, 0x0

    .line 313
    .local v13, "dst_voffset":I
    mul-int v14, v2, v3

    mul-int v15, v2, v3

    div-int/lit8 v15, v15, 0x4

    add-int/2addr v14, v15

    .line 314
    .end local v11    # "src_uoffset":I
    .local v14, "src_uoffset":I
    mul-int v11, v4, v5

    mul-int v15, v4, v5

    div-int/lit8 v15, v15, 0x4

    add-int/2addr v11, v15

    .line 315
    .end local v12    # "dst_uoffset":I
    .local v11, "dst_uoffset":I
    nop

    .local v8, "i":I
    :goto_2
    div-int/lit8 v12, v5, 0x2

    if-ge v8, v12, :cond_4

    .line 316
    add-int v12, v14, v7

    add-int v15, v11, v13

    div-int/lit8 v0, v4, 0x2

    invoke-static {v1, v12, v6, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 318
    div-int/lit8 v0, v2, 0x2

    add-int/2addr v7, v0

    .line 319
    div-int/lit8 v0, v4, 0x2

    add-int/2addr v13, v0

    .line 315
    add-int/lit8 v8, v8, 0x1

    move/from16 v0, p0

    goto :goto_2

    .line 322
    .end local v7    # "src_voffset":I
    .end local v8    # "i":I
    .end local v9    # "src_yoffset":I
    .end local v10    # "dst_yoffset":I
    .end local v11    # "dst_uoffset":I
    .end local v13    # "dst_voffset":I
    .end local v14    # "src_uoffset":I
    :cond_4
    goto :goto_5

    .line 328
    :cond_5
    :pswitch_1
    const/4 v0, 0x0

    .line 329
    .local v0, "src_yoffset":I
    const/4 v7, 0x0

    .line 330
    .local v7, "dst_yoffset":I
    move v9, v7

    move v7, v0

    const/4 v0, 0x0

    .local v0, "i":I
    .local v7, "src_yoffset":I
    .local v9, "dst_yoffset":I
    :goto_3
    if-ge v0, v5, :cond_6

    .line 331
    invoke-static {v1, v7, v6, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 332
    add-int/2addr v7, v2

    .line 333
    add-int/2addr v9, v4

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 337
    .end local v0    # "i":I
    :cond_6
    const/4 v0, 0x0

    .line 338
    .local v0, "src_uoffset":I
    const/4 v10, 0x0

    .line 339
    .local v10, "dst_uoffset":I
    mul-int v7, v2, v3

    .line 340
    mul-int v9, v4, v5

    .line 341
    nop

    .restart local v8    # "i":I
    :goto_4
    div-int/lit8 v11, v5, 0x2

    if-ge v8, v11, :cond_7

    .line 342
    add-int v11, v7, v0

    add-int v12, v9, v10

    invoke-static {v1, v11, v6, v12, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 344
    add-int/2addr v0, v2

    .line 345
    add-int/2addr v10, v4

    .line 341
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 348
    .end local v0    # "src_uoffset":I
    .end local v7    # "src_yoffset":I
    .end local v8    # "i":I
    .end local v9    # "dst_yoffset":I
    .end local v10    # "dst_uoffset":I
    :cond_7
    nop

    .line 356
    :goto_5
    return-object v6

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static NV21toI420([BII)[B
    .locals 6
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 165
    mul-int v0, p1, p2

    .line 166
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 167
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 168
    nop

    .local v3, "i":I
    :goto_0
    move v2, v3

    .end local v3    # "i":I
    .local v2, "i":I
    if-ge v2, v1, :cond_0

    .line 169
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    add-int v4, v0, v2

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 170
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    add-int v4, v0, v2

    add-int/2addr v4, v1

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 168
    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    .line 172
    .end local v2    # "i":I
    :cond_0
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static NV21toNV12([BII)[B
    .locals 6
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 154
    mul-int v0, p1, p2

    .line 155
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 156
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    nop

    .local v3, "i":I
    :goto_0
    move v2, v3

    .end local v3    # "i":I
    .local v2, "i":I
    if-ge v2, v1, :cond_0

    .line 158
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 159
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 157
    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    .line 161
    .end local v2    # "i":I
    :cond_0
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static NV21toYUV420byColor([BIILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)[B
    .locals 2
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "formatVideoEncoder"    # Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 137
    sget-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil$1;->$SwitchMap$com$kint$kintframeworkaosaar$FormatVideoEncoder:[I

    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 148
    const/4 v0, 0x0

    return-object v0

    .line 141
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->NV21toI420([BII)[B

    move-result-object v0

    return-object v0

    .line 143
    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->NV21toNV12([BII)[B

    move-result-object v0

    return-object v0

    .line 146
    :pswitch_2
    return-object p0

    .line 139
    :pswitch_3
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->NV21toI420([BII)[B

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static NV21toYV12([BII)[B
    .locals 6
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 176
    mul-int v0, p1, p2

    .line 177
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 178
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    nop

    .local v3, "i":I
    :goto_0
    move v2, v3

    .end local v3    # "i":I
    .local v2, "i":I
    if-ge v2, v1, :cond_0

    .line 180
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    add-int v4, v0, v2

    add-int/2addr v4, v1

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 181
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    add-int v4, v0, v2

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v5, v0

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 179
    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    .line 183
    .end local v2    # "i":I
    :cond_0
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static YV12toI420([BII)[B
    .locals 4
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 114
    mul-int v0, p1, p2

    .line 115
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 116
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    add-int v2, v0, v1

    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    invoke-static {p0, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    add-int v3, v0, v1

    invoke-static {p0, v0, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static YV12toNV12([BII)[B
    .locals 6
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 103
    mul-int v0, p1, p2

    .line 104
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 105
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    nop

    .local v3, "i":I
    :goto_0
    move v2, v3

    .end local v3    # "i":I
    .local v2, "i":I
    if-ge v2, v1, :cond_0

    .line 107
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    add-int v5, v0, v2

    add-int/2addr v5, v1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 108
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    add-int v5, v0, v2

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 106
    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    .line 110
    .end local v2    # "i":I
    :cond_0
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static YV12toNV21([BII)[B
    .locals 6
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 125
    mul-int v0, p1, p2

    .line 126
    .local v0, "frameSize":I
    div-int/lit8 v1, v0, 0x4

    .line 127
    .local v1, "qFrameSize":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    const/4 v3, 0x0

    invoke-static {p0, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 128
    nop

    .local v3, "i":I
    :goto_0
    move v2, v3

    .end local v3    # "i":I
    .local v2, "i":I
    if-ge v2, v1, :cond_0

    .line 129
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    add-int v5, v0, v2

    add-int/2addr v5, v1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 130
    sget-object v3, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v0

    add-int v5, v0, v2

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 128
    add-int/lit8 v3, v2, 0x1

    goto :goto_0

    .line 132
    .end local v2    # "i":I
    :cond_0
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    return-object v2
.end method

.method public static YV12toYUV420byColor([BIILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)[B
    .locals 2
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "formatVideoEncoder"    # Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 86
    sget-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil$1;->$SwitchMap$com$kint$kintframeworkaosaar$FormatVideoEncoder:[I

    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    const/4 v0, 0x0

    return-object v0

    .line 95
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->YV12toNV21([BII)[B

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->NV21toI420([BII)[B

    move-result-object v0

    return-object v0

    .line 92
    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->YV12toNV12([BII)[B

    move-result-object v0

    return-object v0

    .line 90
    :pswitch_2
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->YV12toNV12([BII)[B

    move-result-object v0

    return-object v0

    .line 88
    :pswitch_3
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->YV12toI420([BII)[B

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static bitmapToNV21(IILandroid/graphics/Bitmap;)[B
    .locals 9
    .param p0, "inputWidth"    # I
    .param p1, "inputHeight"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .line 434
    mul-int v0, p0, p1

    new-array v0, v0, [I

    .line 435
    .local v0, "argb":[I
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, v0

    move v4, p0

    move v7, p0

    move v8, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 436
    invoke-static {v0, p0, p1}, Lcom/kint/kintframeworkaosaar/YUVUtil;->ARGBtoYUV420SemiPlanar([III)[B

    move-result-object v1

    .line 437
    .local v1, "yuv":[B
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 438
    return-object v1
.end method

.method public static getYuvBuffer(II)I
    .locals 6
    .param p0, "width"    # I
    .param p1, "height"    # I

    .line 33
    int-to-double v0, p0

    const-wide/high16 v2, 0x4030000000000000L    # 16.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v0, v0, 0x10

    .line 35
    .local v0, "stride":I
    mul-int v1, v0, p1

    .line 37
    .local v1, "y_size":I
    int-to-double v2, p0

    const-wide/high16 v4, 0x4040000000000000L    # 32.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    mul-int/lit8 v2, v2, 0x10

    .line 39
    .local v2, "c_stride":I
    mul-int v3, v2, p1

    div-int/lit8 v3, v3, 0x2

    .line 41
    .local v3, "c_size":I
    mul-int/lit8 v4, v3, 0x2

    add-int/2addr v4, v1

    return v4
.end method

.method public static mirrorNV21([BII)[B
    .locals 20
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 403
    move-object/from16 v0, p0

    array-length v1, v0

    new-array v1, v1, [B

    .line 405
    .local v1, "output":[B
    const/4 v2, 0x0

    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    move/from16 v4, p1

    if-ge v3, v4, :cond_1

    .line 406
    const/4 v5, 0x0

    .local v5, "y":I
    :goto_1
    move/from16 v6, p2

    if-ge v5, v6, :cond_0

    .line 407
    move v7, v3

    .local v7, "xo":I
    move v8, v5

    .line 408
    .local v8, "yo":I
    move/from16 v9, p1

    .local v9, "w":I
    move/from16 v10, p2

    .line 409
    .local v10, "h":I
    move v11, v7

    .local v11, "xi":I
    move v12, v8

    .line 410
    .local v12, "yi":I
    sub-int v13, v10, v12

    add-int/lit8 v13, v13, -0x1

    .line 411
    .end local v12    # "yi":I
    .local v13, "yi":I
    mul-int v12, v9, v8

    add-int/2addr v12, v7

    mul-int v14, v9, v13

    add-int/2addr v14, v11

    aget-byte v14, v0, v14

    aput-byte v14, v1, v12

    .line 412
    mul-int v12, v9, v10

    .line 413
    .local v12, "fs":I
    shr-int/lit8 v14, v12, 0x2

    .line 414
    .local v14, "qs":I
    shr-int/lit8 v11, v11, 0x1

    .line 415
    shr-int/lit8 v13, v13, 0x1

    .line 416
    shr-int/lit8 v7, v7, 0x1

    .line 417
    shr-int/lit8 v8, v8, 0x1

    .line 418
    shr-int/lit8 v9, v9, 0x1

    .line 419
    shr-int/lit8 v10, v10, 0x1

    .line 421
    mul-int v15, v9, v13

    add-int/2addr v15, v11

    mul-int/lit8 v15, v15, 0x2

    add-int/2addr v15, v12

    .line 422
    .local v15, "ui":I
    mul-int v16, v9, v8

    add-int v16, v16, v7

    mul-int/lit8 v16, v16, 0x2

    add-int v16, v12, v16

    .line 424
    .local v16, "uo":I
    add-int/lit8 v17, v15, 0x1

    .line 425
    .local v17, "vi":I
    add-int/lit8 v18, v16, 0x1

    .line 426
    .local v18, "vo":I
    aget-byte v19, v0, v15

    aput-byte v19, v1, v16

    .line 427
    aget-byte v19, v0, v17

    aput-byte v19, v1, v18

    .line 406
    .end local v7    # "xo":I
    .end local v8    # "yo":I
    .end local v9    # "w":I
    .end local v10    # "h":I
    .end local v11    # "xi":I
    .end local v12    # "fs":I
    .end local v13    # "yi":I
    .end local v14    # "qs":I
    .end local v15    # "ui":I
    .end local v16    # "uo":I
    .end local v17    # "vi":I
    .end local v18    # "vo":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 405
    .end local v5    # "y":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 430
    .end local v3    # "x":I
    :cond_1
    move/from16 v6, p2

    return-object v1
.end method

.method public static preAllocateBuffers(I)V
    .locals 1
    .param p0, "length"    # I

    .line 23
    new-array v0, p0, [B

    sput-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    .line 24
    new-array v0, p0, [B

    sput-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate270:[B

    .line 25
    new-array v0, p0, [B

    sput-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferColor:[B

    .line 26
    return-void
.end method

.method public static rotateNV21([BIII)[B
    .locals 1
    .param p0, "data"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "rotation"    # I

    .line 187
    if-eqz p3, :cond_3

    const/16 v0, 0x5a

    if-eq p3, v0, :cond_2

    const/16 v0, 0xb4

    if-eq p3, v0, :cond_1

    const/16 v0, 0x10e

    if-eq p3, v0, :cond_0

    .line 197
    const/4 v0, 0x0

    return-object v0

    .line 195
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->rotateNV21Degree270([BII)[B

    move-result-object v0

    return-object v0

    .line 193
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->rotateNV21Degree180([BII)[B

    move-result-object v0

    return-object v0

    .line 191
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->rotateNV21Degree90([BII)[B

    move-result-object v0

    return-object v0

    .line 189
    :cond_3
    return-object p0
.end method

.method private static rotateNV21Degree180([BII)[B
    .locals 5
    .param p0, "data"    # [B
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .line 222
    const/4 v0, 0x0

    .line 223
    .local v0, "count":I
    mul-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 224
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    aget-byte v3, p0, v1

    aput-byte v3, v2, v0

    .line 225
    add-int/lit8 v0, v0, 0x1

    .line 223
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 227
    .end local v1    # "i":I
    :cond_0
    mul-int v1, p1, p2

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    .restart local v1    # "i":I
    :goto_1
    mul-int v2, p1, p2

    if-lt v1, v2, :cond_1

    .line 228
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    add-int/lit8 v3, v0, 0x1

    .local v3, "count":I
    add-int/lit8 v4, v1, -0x1

    aget-byte v4, p0, v4

    aput-byte v4, v2, v0

    .line 229
    .end local v0    # "count":I
    sget-object v0, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    add-int/lit8 v2, v3, 0x1

    .local v2, "count":I
    aget-byte v4, p0, v1

    aput-byte v4, v0, v3

    .line 227
    .end local v3    # "count":I
    add-int/lit8 v1, v1, -0x2

    move v0, v2

    goto :goto_1

    .line 231
    .end local v1    # "i":I
    .end local v2    # "count":I
    .restart local v0    # "count":I
    :cond_1
    sget-object v1, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    return-object v1
.end method

.method private static rotateNV21Degree270([BII)[B
    .locals 10
    .param p0, "data"    # [B
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .line 235
    mul-int v0, p1, p2

    .line 236
    .local v0, "wh":I
    shr-int/lit8 v1, p2, 0x1

    .line 239
    .local v1, "uvHeight":I
    const/4 v2, 0x0

    .line 240
    .local v2, "cont":I
    const/4 v3, 0x0

    move v4, v2

    const/4 v2, 0x0

    .local v2, "i":I
    .local v4, "cont":I
    :goto_0
    if-ge v2, p1, :cond_1

    .line 241
    const/4 v5, 0x0

    .line 242
    .local v5, "nPos":I
    move v6, v4

    const/4 v4, 0x0

    .local v4, "j":I
    .local v6, "cont":I
    :goto_1
    if-ge v4, p2, :cond_0

    .line 243
    sget-object v7, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate270:[B

    add-int/lit8 v8, v6, 0x1

    .local v8, "cont":I
    add-int v9, v5, v2

    aget-byte v9, p0, v9

    aput-byte v9, v7, v6

    .line 244
    .end local v6    # "cont":I
    add-int/2addr v5, p1

    .line 242
    add-int/lit8 v4, v4, 0x1

    move v6, v8

    goto :goto_1

    .line 240
    .end local v4    # "j":I
    .end local v5    # "nPos":I
    .end local v8    # "cont":I
    .restart local v6    # "cont":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    move v4, v6

    goto :goto_0

    .line 249
    .end local v2    # "i":I
    .end local v6    # "cont":I
    .local v4, "cont":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, p1, :cond_3

    .line 250
    move v5, v0

    .line 251
    .restart local v5    # "nPos":I
    move v6, v4

    const/4 v4, 0x0

    .local v4, "j":I
    .restart local v6    # "cont":I
    :goto_3
    if-ge v4, v1, :cond_2

    .line 252
    sget-object v7, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate270:[B

    add-int v8, v5, v2

    aget-byte v8, p0, v8

    aput-byte v8, v7, v6

    .line 253
    sget-object v7, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate270:[B

    add-int/lit8 v8, v6, 0x1

    add-int v9, v5, v2

    add-int/lit8 v9, v9, 0x1

    aget-byte v9, p0, v9

    aput-byte v9, v7, v8

    .line 254
    add-int/lit8 v6, v6, 0x2

    .line 255
    add-int/2addr v5, p1

    .line 251
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 249
    .end local v4    # "j":I
    .end local v5    # "nPos":I
    :cond_2
    add-int/lit8 v2, v2, 0x2

    move v4, v6

    goto :goto_2

    .line 258
    .end local v2    # "i":I
    .end local v6    # "cont":I
    .local v4, "cont":I
    :cond_3
    sget-object v2, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate270:[B

    invoke-static {v2, p1, p2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->rotateNV21Degree180([BII)[B

    move-result-object v2

    return-object v2
.end method

.method private static rotateNV21Degree90([BII)[B
    .locals 9
    .param p0, "data"    # [B
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "i":I
    const/4 v1, 0x0

    move v2, v0

    const/4 v0, 0x0

    .local v0, "x":I
    .local v2, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 205
    add-int/lit8 v3, p2, -0x1

    .local v3, "y":I
    :goto_1
    if-ltz v3, :cond_0

    .line 206
    sget-object v4, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    add-int/lit8 v5, v2, 0x1

    .local v5, "i":I
    mul-int v6, v3, p1

    add-int/2addr v6, v0

    aget-byte v6, p0, v6

    aput-byte v6, v4, v2

    .line 205
    .end local v2    # "i":I
    add-int/lit8 v3, v3, -0x1

    move v2, v5

    goto :goto_1

    .line 204
    .end local v3    # "y":I
    .end local v5    # "i":I
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    .end local v0    # "x":I
    :cond_1
    mul-int v0, p1, p2

    .line 211
    .local v0, "size":I
    mul-int/lit8 v3, v0, 0x3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    .line 212
    .end local v2    # "i":I
    .local v3, "i":I
    add-int/lit8 v2, p1, -0x1

    .local v2, "x":I
    :goto_2
    if-lez v2, :cond_3

    .line 213
    move v4, v3

    const/4 v3, 0x0

    .local v3, "y":I
    .local v4, "i":I
    :goto_3
    div-int/lit8 v5, p2, 0x2

    if-ge v3, v5, :cond_2

    .line 214
    sget-object v5, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    add-int/lit8 v6, v4, -0x1

    .local v6, "i":I
    mul-int v7, v3, p1

    add-int/2addr v7, v0

    add-int/2addr v7, v2

    aget-byte v7, p0, v7

    aput-byte v7, v5, v4

    .line 215
    .end local v4    # "i":I
    sget-object v4, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    add-int/lit8 v5, v6, -0x1

    .restart local v5    # "i":I
    mul-int v7, v3, p1

    add-int/2addr v7, v0

    add-int/lit8 v8, v2, -0x1

    add-int/2addr v7, v8

    aget-byte v7, p0, v7

    aput-byte v7, v4, v6

    .line 213
    .end local v6    # "i":I
    add-int/lit8 v3, v3, 0x1

    move v4, v5

    goto :goto_3

    .line 212
    .end local v3    # "y":I
    .end local v5    # "i":I
    .restart local v4    # "i":I
    :cond_2
    add-int/lit8 v2, v2, -0x2

    move v3, v4

    goto :goto_2

    .line 218
    .end local v2    # "x":I
    .end local v4    # "i":I
    .local v3, "i":I
    :cond_3
    sget-object v1, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocatedBufferRotate:[B

    return-object v1
.end method

.method public static rotatePixelsNV21([BIII)[B
    .locals 25
    .param p0, "input"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "rotation"    # I

    .line 360
    move-object/from16 v0, p0

    move/from16 v1, p3

    array-length v2, v0

    new-array v2, v2, [B

    .line 362
    .local v2, "output":[B
    const/16 v3, 0x10e

    const/16 v4, 0x5a

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eq v1, v4, :cond_1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v7, 0x1

    .line 363
    .local v7, "swap":Z
    :goto_1
    const/16 v8, 0xb4

    if-eq v1, v4, :cond_3

    if-ne v1, v8, :cond_2

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v4, 0x1

    .line 364
    .local v4, "yflip":Z
    :goto_3
    if-eq v1, v3, :cond_5

    if-ne v1, v8, :cond_4

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 v3, 0x1

    .line 365
    .local v3, "xflip":Z
    :goto_5
    const/4 v8, 0x0

    .local v8, "x":I
    :goto_6
    move/from16 v9, p1

    if-ge v8, v9, :cond_a

    .line 366
    const/4 v10, 0x0

    .local v10, "y":I
    :goto_7
    move/from16 v11, p2

    if-ge v10, v11, :cond_9

    .line 367
    move v12, v8

    .local v12, "xo":I
    move v13, v10

    .line 368
    .local v13, "yo":I
    move/from16 v14, p1

    .local v14, "w":I
    move/from16 v15, p2

    .line 369
    .local v15, "h":I
    move/from16 v16, v12

    .local v16, "xi":I
    move/from16 v17, v13

    .line 370
    .local v17, "yi":I
    if-eqz v7, :cond_6

    .line 371
    mul-int v18, v14, v13

    div-int v16, v18, v15

    .line 372
    mul-int v18, v15, v12

    div-int v17, v18, v14

    .line 374
    :cond_6
    if-eqz v4, :cond_7

    .line 375
    sub-int v18, v15, v17

    add-int/lit8 v17, v18, -0x1

    .line 377
    :cond_7
    if-eqz v3, :cond_8

    .line 378
    sub-int v18, v14, v16

    add-int/lit8 v16, v18, -0x1

    .line 380
    :cond_8
    mul-int v18, v14, v13

    add-int v18, v18, v12

    mul-int v19, v14, v17

    add-int v19, v19, v16

    aget-byte v19, v0, v19

    aput-byte v19, v2, v18

    .line 381
    mul-int v18, v14, v15

    .line 382
    .local v18, "fs":I
    shr-int/lit8 v19, v18, 0x2

    .line 383
    .local v19, "qs":I
    shr-int/lit8 v16, v16, 0x1

    .line 384
    shr-int/lit8 v17, v17, 0x1

    .line 385
    shr-int/lit8 v12, v12, 0x1

    .line 386
    shr-int/lit8 v13, v13, 0x1

    .line 387
    shr-int/lit8 v14, v14, 0x1

    .line 388
    shr-int/lit8 v15, v15, 0x1

    .line 390
    mul-int v20, v14, v17

    add-int v20, v20, v16

    mul-int/lit8 v20, v20, 0x2

    add-int v20, v18, v20

    .line 391
    .local v20, "ui":I
    mul-int v21, v14, v13

    add-int v21, v21, v12

    mul-int/lit8 v21, v21, 0x2

    add-int v21, v18, v21

    .line 393
    .local v21, "uo":I
    add-int/lit8 v22, v20, 0x1

    .line 394
    .local v22, "vi":I
    add-int/lit8 v23, v21, 0x1

    .line 395
    .local v23, "vo":I
    aget-byte v24, v0, v20

    aput-byte v24, v2, v21

    .line 396
    aget-byte v24, v0, v22

    aput-byte v24, v2, v23

    .line 366
    .end local v12    # "xo":I
    .end local v13    # "yo":I
    .end local v14    # "w":I
    .end local v15    # "h":I
    .end local v16    # "xi":I
    .end local v17    # "yi":I
    .end local v18    # "fs":I
    .end local v19    # "qs":I
    .end local v20    # "ui":I
    .end local v21    # "uo":I
    .end local v22    # "vi":I
    .end local v23    # "vo":I
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 365
    .end local v10    # "y":I
    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 399
    .end local v8    # "x":I
    :cond_a
    move/from16 v11, p2

    return-object v2
.end method


# virtual methods
.method public dumpYUVData([BILjava/lang/String;)V
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "len"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 262
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/tmp/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 267
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 268
    .local v1, "out":Ljava/io/FileOutputStream;
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 269
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 270
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 274
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method
