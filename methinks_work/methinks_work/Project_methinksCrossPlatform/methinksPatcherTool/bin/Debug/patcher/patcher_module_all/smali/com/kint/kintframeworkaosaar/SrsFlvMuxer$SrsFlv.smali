.class Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;
.super Ljava/lang/Object;
.source "SrsFlvMuxer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SrsFlv"
.end annotation


# instance fields
.field private Pps:Ljava/nio/ByteBuffer;

.field private Sps:Ljava/nio/ByteBuffer;

.field private aac_specific_config_got:Z

.field private achannel:I

.field private audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

.field private avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

.field private ipbs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

.field private video_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V
    .locals 2

    .line 644
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    .line 636
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->ipbs:Ljava/util/ArrayList;

    .line 642
    const/4 p1, 0x2

    iput p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->achannel:I

    .line 645
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->reset()V

    .line 646
    return-void
.end method

.method private flvFrameCacheAdd(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V
    .locals 3
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 885
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$200(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 888
    goto :goto_0

    .line 886
    :catch_0
    move-exception v0

    .line 887
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "SrsFlvMuxer"

    const-string v2, "frame discarded"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    return-void
.end method

.method private writeAdtsHeader([BI)V
    .locals 3
    .param p1, "frame"    # [B
    .param p2, "offset"    # I

    .line 743
    const/4 v0, -0x1

    aput-byte v0, p1, p2

    .line 744
    add-int/lit8 v0, p2, 0x1

    const/16 v1, -0x10

    aput-byte v1, p1, v0

    .line 746
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 748
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 750
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 752
    add-int/lit8 v0, p2, 0x2

    const/16 v1, 0x40

    aput-byte v1, p1, v0

    .line 754
    add-int/lit8 v0, p2, 0x2

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 756
    add-int/lit8 v0, p2, 0x2

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 757
    add-int/lit8 v0, p2, 0x3

    const/16 v1, -0x80

    aput-byte v1, p1, v0

    .line 759
    add-int/lit8 v0, p2, 0x3

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 761
    add-int/lit8 v0, p2, 0x3

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 763
    add-int/lit8 v0, p2, 0x3

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 765
    add-int/lit8 v0, p2, 0x3

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 767
    add-int/lit8 v0, p2, 0x3

    aget-byte v1, p1, v0

    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    and-int/lit16 v2, v2, 0x1800

    shr-int/lit8 v2, v2, 0xb

    or-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 768
    add-int/lit8 v0, p2, 0x4

    array-length v1, p1

    add-int/lit8 v1, v1, -0x2

    and-int/lit16 v1, v1, 0x7f8

    shr-int/lit8 v1, v1, 0x3

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 769
    add-int/lit8 v0, p2, 0x5

    array-length v1, p1

    add-int/lit8 v1, v1, -0x2

    and-int/lit8 v1, v1, 0x7

    shl-int/lit8 v1, v1, 0x5

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 771
    add-int/lit8 v0, p2, 0x5

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x1f

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 772
    add-int/lit8 v0, p2, 0x6

    const/4 v1, -0x4

    aput-byte v1, p1, v0

    .line 774
    add-int/lit8 v0, p2, 0x6

    aget-byte v1, p1, v0

    or-int/lit8 v1, v1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 775
    return-void
.end method

.method private writeH264IpbFrame(Ljava/util/ArrayList;II)V
    .locals 7
    .param p2, "frame_type"    # I
    .param p3, "dts"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;",
            ">;II)V"
        }
    .end annotation

    .line 852
    .local p1, "frames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;>;"
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    goto :goto_0

    .line 855
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->muxFlvTag(Ljava/util/ArrayList;II)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->video_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 857
    const/16 v2, 0x9

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->video_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-object v1, p0

    move v3, p3

    move v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeRtmpPacket(IIIILcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V

    .line 858
    return-void

    .line 853
    :cond_1
    :goto_0
    return-void
.end method

.method private writeH264SpsPps(I)V
    .locals 11
    .param p1, "pts"    # I

    .line 829
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1300(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 834
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 835
    .local v0, "frames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;>;"
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2, v3, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->muxSequenceHeader(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/util/ArrayList;)V

    .line 838
    const/4 v1, 0x1

    .line 839
    .local v1, "frame_type":I
    const/4 v2, 0x0

    .line 840
    .local v2, "avc_packet_type":I
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    invoke-virtual {v3, v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->muxFlvTag(Ljava/util/ArrayList;II)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->video_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 842
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v10, 0x1

    invoke-static {v3, v10}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z

    .line 844
    const/16 v5, 0x9

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->video_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-object v4, p0

    move v6, p1

    move v7, v1

    move v8, v2

    invoke-direct/range {v4 .. v9}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeRtmpPacket(IIIILcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V

    .line 845
    const-string v3, "SrsFlvMuxer"

    const-string v4, "flv: h264 sps/pps sent, sps=%dB, pps=%dB"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    .line 846
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    .line 845
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    return-void

    .line 830
    .end local v0    # "frames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;>;"
    .end local v1    # "frame_type":I
    .end local v2    # "avc_packet_type":I
    :cond_1
    :goto_0
    return-void
.end method

.method private writeRtmpPacket(IIIILcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "dts"    # I
    .param p3, "frame_type"    # I
    .param p4, "avc_aac_type"    # I
    .param p5, "tag"    # Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 862
    new-instance v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    .line 863
    .local v0, "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    iput-object p5, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 864
    iput p1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->type:I

    .line 865
    iput p2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->dts:I

    .line 866
    iput p3, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->frame_type:I

    .line 867
    iput p4, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->avc_aac_type:I

    .line 869
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_video()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 870
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1700(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 871
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_keyframe()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 872
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1702(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z

    .line 873
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->flvFrameCacheAdd(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    goto :goto_0

    .line 876
    :cond_0
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->flvFrameCacheAdd(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    goto :goto_0

    .line 878
    :cond_1
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_audio()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 879
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->flvFrameCacheAdd(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    .line 881
    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .line 653
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    .line 654
    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    .line 655
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z

    .line 656
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->aac_specific_config_got:Z

    .line 657
    return-void
.end method

.method public setAchannel(I)V
    .locals 0
    .param p1, "achannel"    # I

    .line 649
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->achannel:I

    .line 650
    return-void
.end method

.method public setSpsPPs(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;

    .line 823
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    .line 824
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    .line 825
    return-void
.end method

.method public writeAudioSample(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 13
    .param p1, "bb"    # Ljava/nio/ByteBuffer;
    .param p2, "bi"    # Landroid/media/MediaCodec$BufferInfo;

    .line 660
    iget-wide v0, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 662
    .local v0, "dts":I
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsAllocator;

    move-result-object v1

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    const/4 v3, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator;->allocate(I)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 663
    const/4 v1, 0x1

    .line 664
    .local v1, "aac_packet_type":B
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->aac_specific_config_got:Z

    const/16 v4, 0x2b11

    const/16 v5, 0x5622

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-nez v2, :cond_5

    .line 669
    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xf8

    int-to-byte v2, v2

    .line 673
    .local v2, "ch":B
    const/4 v8, 0x4

    .line 674
    .local v8, "samplingFrequencyIndex":B
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v9

    if-ne v9, v5, :cond_0

    .line 675
    const/4 v8, 0x7

    goto :goto_0

    .line 676
    :cond_0
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v9

    if-ne v9, v4, :cond_1

    .line 677
    const/16 v8, 0xa

    goto :goto_0

    .line 678
    :cond_1
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v9

    const/16 v10, 0x7d00

    if-ne v9, v10, :cond_2

    .line 679
    const/4 v8, 0x5

    goto :goto_0

    .line 680
    :cond_2
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v9

    const/16 v10, 0x3e80

    if-ne v9, v10, :cond_3

    .line 681
    const/16 v8, 0x8

    .line 683
    :cond_3
    :goto_0
    shr-int/lit8 v9, v8, 0x1

    const/4 v10, 0x7

    and-int/2addr v9, v10

    or-int/2addr v9, v2

    int-to-byte v2, v9

    .line 684
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v9, v2, v3}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(BI)V

    .line 686
    shl-int/lit8 v9, v8, 0x7

    and-int/lit16 v9, v9, 0x80

    int-to-byte v2, v9

    .line 690
    const/4 v9, 0x1

    .line 691
    .local v9, "channelConfiguration":B
    iget v11, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->achannel:I

    if-ne v11, v3, :cond_4

    .line 692
    const/4 v9, 0x2

    .line 694
    :cond_4
    shl-int/lit8 v11, v9, 0x3

    and-int/lit8 v11, v11, 0x78

    or-int/2addr v11, v2

    int-to-byte v2, v11

    .line 702
    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    const/4 v12, 0x3

    invoke-virtual {v11, v2, v12}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(BI)V

    .line 704
    iput-boolean v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->aac_specific_config_got:Z

    .line 705
    const/4 v1, 0x0

    .line 707
    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v11}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v11

    const/4 v12, 0x4

    invoke-direct {p0, v11, v12}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeAdtsHeader([BI)V

    .line 708
    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v11, v10}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->appendOffset(I)V

    .line 709
    .end local v2    # "ch":B
    .end local v8    # "samplingFrequencyIndex":B
    .end local v9    # "channelConfiguration":B
    goto :goto_1

    .line 710
    :cond_5
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v2

    iget v8, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {p1, v2, v3, v8}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 711
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iget v8, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v8, v3

    invoke-virtual {v2, v8}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->appendOffset(I)V

    .line 714
    :goto_1
    const/16 v8, 0xa

    .line 715
    .local v8, "sound_format":B
    const/4 v2, 0x0

    .line 716
    .local v2, "sound_type":B
    iget v9, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->achannel:I

    if-ne v9, v3, :cond_6

    .line 717
    const/4 v2, 0x1

    .line 719
    .end local v2    # "sound_type":B
    .local v9, "sound_type":B
    :cond_6
    move v9, v2

    const/4 v10, 0x1

    .line 720
    .local v10, "sound_size":B
    const/4 v2, 0x3

    .line 721
    .local v2, "sound_rate":B
    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v11}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v11

    if-ne v11, v5, :cond_8

    .line 722
    const/4 v2, 0x2

    .line 730
    .end local v2    # "sound_rate":B
    .local v11, "sound_rate":B
    :cond_7
    :goto_2
    move v11, v2

    goto :goto_3

    .line 723
    .end local v11    # "sound_rate":B
    .restart local v2    # "sound_rate":B
    :cond_8
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I

    move-result v5

    if-ne v5, v4, :cond_7

    .line 724
    const/4 v2, 0x1

    goto :goto_2

    .line 730
    .end local v2    # "sound_rate":B
    .restart local v11    # "sound_rate":B
    :goto_3
    and-int/lit8 v2, v9, 0x1

    int-to-byte v2, v2

    .line 731
    .local v2, "audio_header":B
    shl-int/lit8 v4, v10, 0x1

    and-int/2addr v3, v4

    or-int/2addr v3, v2

    int-to-byte v2, v3

    .line 732
    shl-int/lit8 v3, v11, 0x2

    and-int/lit8 v3, v3, 0xc

    or-int/2addr v3, v2

    int-to-byte v2, v3

    .line 733
    shl-int/lit8 v3, v8, 0x4

    and-int/lit16 v3, v3, 0xf0

    or-int/2addr v3, v2

    int-to-byte v12, v3

    .line 735
    .end local v2    # "audio_header":B
    .local v12, "audio_header":B
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v2, v12, v6}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(BI)V

    .line 736
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v2, v1, v7}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(BI)V

    .line 738
    const/16 v3, 0x8

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->audio_tag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-object v2, p0

    move v4, v0

    move v6, v1

    invoke-direct/range {v2 .. v7}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeRtmpPacket(IIIILcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V

    .line 739
    return-void
.end method

.method public writeVideoSample(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 9
    .param p1, "bb"    # Ljava/nio/ByteBuffer;
    .param p2, "bi"    # Landroid/media/MediaCodec$BufferInfo;

    .line 778
    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    return-void

    .line 780
    :cond_0
    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 781
    .local v0, "pts":I
    const/4 v2, 0x2

    .line 782
    .local v2, "type":I
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    iget v4, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    const/4 v5, 0x1

    invoke-virtual {v3, p1, v4, v5}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->demuxAnnexb(Ljava/nio/ByteBuffer;IZ)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    move-result-object v3

    .line 783
    .local v3, "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    iget-object v4, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit8 v4, v4, 0x1f

    .line 784
    .local v4, "nal_unit_type":I
    const/4 v7, 0x5

    if-ne v4, v7, :cond_1

    .line 785
    const/4 v2, 0x1

    goto :goto_0

    .line 786
    :cond_1
    const/4 v7, 0x7

    if-eq v4, v7, :cond_4

    const/16 v7, 0x8

    if-ne v4, v7, :cond_2

    goto :goto_1

    .line 811
    :cond_2
    if-eq v4, v5, :cond_3

    .line 812
    return-void

    .line 815
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->ipbs:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    invoke-virtual {v5, v3}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->muxNaluHeader(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 816
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->ipbs:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 818
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->ipbs:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeH264IpbFrame(Ljava/util/ArrayList;II)V

    .line 819
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->ipbs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 820
    return-void

    .line 787
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    iget v7, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v5, p1, v7, v6}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->demuxAnnexb(Ljava/nio/ByteBuffer;IZ)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    move-result-object v5

    .line 788
    .local v5, "frame_pps":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    iget v7, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    iget v8, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v1

    iput v7, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 789
    iget-object v1, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 790
    iget v1, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    new-array v1, v1, [B

    .line 791
    .local v1, "sps":[B
    iget-object v7, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 792
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v7, v6}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z

    .line 793
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    iput-object v7, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Sps:Ljava/nio/ByteBuffer;

    .line 796
    .end local v1    # "sps":[B
    :cond_5
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->avc:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;

    iget v7, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v1, p1, v7, v6}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->demuxAnnexb(Ljava/nio/ByteBuffer;IZ)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    move-result-object v1

    .line 797
    .local v1, "frame_sei":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    iget v7, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    if-lez v7, :cond_6

    .line 798
    const/4 v7, 0x6

    iget-object v8, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/lit8 v8, v8, 0x1f

    if-ne v7, v8, :cond_6

    .line 799
    iget v7, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    iget v8, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x3

    iput v7, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 803
    :cond_6
    iget v7, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    if-lez v7, :cond_7

    iget-object v7, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 804
    iget v7, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    new-array v7, v7, [B

    .line 805
    .local v7, "pps":[B
    iget-object v8, v5, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 806
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v8, v6}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z

    .line 807
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    iput-object v6, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->Pps:Ljava/nio/ByteBuffer;

    .line 808
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeH264SpsPps(I)V

    .line 810
    .end local v7    # "pps":[B
    :cond_7
    return-void
.end method
