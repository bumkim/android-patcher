.class public Lcom/kint/kintframeworkaosaar/ScreenCastingService;
.super Landroid/app/Service;
.source "ScreenCastingService.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;
.implements Lcom/kint/kintframeworkaosaar/GetH264Data;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/ScreenCastingService$ScreenCastingServiceBinder;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 62
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    const-string v0, "ScreenCastingService"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/ScreenCastingService;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 0
    .param p1, "h264Buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "info"    # Landroid/media/MediaCodec$BufferInfo;

    .line 317
    return-void
.end method

.method public onAuthErrorRtmp()V
    .locals 0

    .line 265
    return-void
.end method

.method public onAuthSuccessRtmp()V
    .locals 0

    .line 273
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .line 104
    new-instance v0, Lcom/kint/kintframeworkaosaar/ScreenCastingService$ScreenCastingServiceBinder;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/ScreenCastingService$ScreenCastingServiceBinder;-><init>(Lcom/kint/kintframeworkaosaar/ScreenCastingService;)V

    return-object v0
.end method

.method public onConnectionFailedRtmp(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .line 246
    return-void
.end method

.method public onConnectionSuccessRtmp()V
    .locals 0

    .line 237
    return-void
.end method

.method public onCreate()V
    .locals 2

    .line 111
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 112
    const-string v0, "ScreenCastingService Created"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 113
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 130
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 131
    const-string v0, "ScreenCastingService Destroyed"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 132
    return-void
.end method

.method public onDisconnectRtmp()V
    .locals 0

    .line 257
    return-void
.end method

.method public onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;

    .line 289
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public onVideoFormat(Landroid/media/MediaFormat;)V
    .locals 0
    .param p1, "mediaFormat"    # Landroid/media/MediaFormat;

    .line 325
    return-void
.end method
