.class public Lcom/kint/kintframeworkaosaar/Video;
.super Lcom/kint/kintframeworkaosaar/ContentData;
.source "Video.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 20
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->VIDEO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x6

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/ContentData;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 15
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/ContentData;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 16
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 27
    const-string v0, "RTMP Video"

    return-object v0
.end method
