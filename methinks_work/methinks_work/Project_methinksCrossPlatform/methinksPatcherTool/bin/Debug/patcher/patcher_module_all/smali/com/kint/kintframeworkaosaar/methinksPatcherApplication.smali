.class public Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
.super Landroid/app/Application;
.source "methinksPatcherApplication.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;
.implements Lcom/kint/kintframeworkaosaar/GetH264Data;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;,
        Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    }
.end annotation


# static fields
.field public static final INTEGRATE_MODE:I = 0x1

.field public static IsEarphoneOn:Z = false

.field public static final MESSAGE_CONNECTED_FAILURE_RTMP_SERVER:I = 0x1

.field public static final MESSAGE_CONNECTED_SUCCESS_RTMP_SERVER:I = 0x0

.field public static final MESSAGE_DISCONNECTED_RTMP_SERVER:I = 0x3

.field public static final MESSAGE_LOGIN_FAILURE:I = 0x8

.field public static final MESSAGE_LOGIN_PREPARE:I = 0x6

.field public static final MESSAGE_LOGIN_SUCCESS:I = 0x7

.field public static final MESSAGE_NOTIFICATION_BACKGROUND:I = 0xa

.field public static final MESSAGE_NOTIFICATION_FOREGROUND:I = 0x9

.field public static final MESSAGE_ORIENTATION_ROTATE_LANDSCAPE:I = 0x5

.field public static final MESSAGE_ORIENTATION_ROTATE_PORTRAIT:I = 0x4

.field public static final MESSAGE_QUESTION_FINISH:I = 0xb

.field public static final MESSAGE_QUESTION_PREPARE:I = 0x9

.field public static final MESSAGE_QUESTION_START:I = 0xa

.field public static final MESSAGE_START_SEND_STREAM:I = 0x2

.field public static final MESSAGE_TITLE_INVALID_PROJECT:Ljava/lang/String; = "Testing period is over. Thank you for your participation. Please delete this app from your device."

.field public static final MESSAGE_TITLE_INVALID_USER:Ljava/lang/String; = "Invalid user code. Enter your user code again."

.field public static final MESSAGE_TITLE_LOGIN:Ljava/lang/String; = "Enter methinks user code."

.field public static final MESSAGE_TITLE_LOGIN_RETRY:Ljava/lang/String; = "Enter user code from methinks app."

.field public static final MESSAGE_TITLE_PERMISSION_CHECK:Ljava/lang/String; = ""

.field public static final MESSAGE_TITLE_PERMISSION_SETTING:Ljava/lang/String; = "Please allow the required permission in the Setting menu."

.field public static final MESSAGE_TITLE_PHOTO_PERMISSION_CHECK:Ljava/lang/String; = ""

.field public static final MESSAGE_TITLE_RECORDING_START:Ljava/lang/String; = ""

.field public static final MESSAGE_TITLE_SCREENSHOT_ACTION:Ljava/lang/String; = ""

.field public static final MESSAGE_TITLE_SCREENSHOT_BLOCKED:Ljava/lang/String; = ""

.field public static final MESSAGE_TITLE_SCREENSHOT_DETAIL:Ljava/lang/String; = "More details please!"

.field public static final MESSAGE_TITLE_SERVER_ERROR:Ljava/lang/String; = "Can\'t connect to methinks server. Please try again or contact methinks."

.field public static final PATCH_MODE:I = 0x0

.field public static final SCREEN_CAPTURE_REQUEST_CODE:I = 0x3e8

.field public static final SCREEN_OVERRAY_REQUEST_CODE:I = 0x3ea

.field public static final SYSTEM_SETTING_REQUEST_CODE:I = 0x3e9


# instance fields
.field public LoginTryCount:I

.field private final TAG:Ljava/lang/String;

.field private _backgroundMode:Z

.field private _backgroundModeCheckTickTime:I

.field private _callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

.field private _checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _currentActiviy:Landroid/app/Activity;

.field private _detectScreenShotHandler:Landroid/os/Handler;

.field private _detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _dispatchNetworkStatus:I

.field private _enableBackground:Z

.field private _exitStatus:I

.field private _logDelayTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field _loginImageView:Landroid/widget/ImageView;

.field private _loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

.field _loginLayoutView:Landroid/view/View;

.field private _mainActivity:Landroid/app/Activity;

.field private _mediaProjection:Landroid/media/projection/MediaProjection;

.field private _mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

.field private _patchIntegrateMode:I

.field private _processStatus:I

.field private _projectID:Ljava/lang/String;

.field private _projectionIntentData:Landroid/content/Intent;

.field private _projectionResultCode:I

.field private _quitApplication:Z

.field _receivedMessageHandler:Landroid/os/Handler;

.field private _release:Z

.field private _requestMessageList:Ljava/util/ArrayList;

.field private _rtmpServerBaseURL:Ljava/lang/String;

.field private _rtmpServerURL:Ljava/lang/String;

.field private _screenRotationAngle:I

.field private _showDialog:Z

.field private _testMode:Z

.field private _userCode:Ljava/lang/String;

.field _userCodeEditText:Landroid/widget/EditText;

.field private _virtualDisplay:Landroid/hardware/display/VirtualDisplay;

.field private _windowManager:Landroid/view/WindowManager;

.field private connectedRTMPServer:Z

.field private networkStatus:Ljava/lang/String;

.field private paused:Z

.field private rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

.field private running:Z

.field private sendRTMPVideo:Z

.field private videoDPI:I

.field private videoDataRate:I

.field private videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

.field private videoFrameRate:I

.field private videoHeight:I

.field private videoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 387
    const/4 v0, 0x0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->IsEarphoneOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 65
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 287
    const-string v0, "methinksPatcherApp."

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->TAG:Ljava/lang/String;

    .line 323
    const v0, 0x1d4c0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_backgroundModeCheckTickTime:I

    .line 338
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 339
    const/16 v0, 0x500

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    .line 340
    const/16 v0, 0x140

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoDPI:I

    .line 342
    const/16 v0, 0xa

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoFrameRate:I

    .line 344
    const-string v0, "rtmp://221.165.42.119:5391"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerBaseURL:Ljava/lang/String;

    .line 345
    const-string v0, "rtmp://221.165.42.119:5391/screen/bilbo915?projectID=oulj264yvC"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    .line 351
    const-string v0, "Ready"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    .line 354
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_patchIntegrateMode:I

    .line 355
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_screenRotationAngle:I

    .line 356
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_enableBackground:Z

    .line 358
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_backgroundMode:Z

    .line 368
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_dispatchNetworkStatus:I

    .line 369
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_logDelayTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 370
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 371
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 372
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 373
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 374
    new-instance v1, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 383
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_showDialog:Z

    .line 386
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_testMode:Z

    .line 1574
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method static GetScreenBrightness(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 2276
    const/4 v0, -0x1

    .line 2279
    .local v0, "screenBrightness":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 2285
    goto :goto_0

    .line 2281
    :catch_0
    move-exception v1

    .line 2283
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, -0x1

    .line 2284
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2287
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method private _CreateWindowManagerLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 9

    .line 2337
    const/4 v0, 0x0

    .line 2338
    .local v0, "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_0

    .line 2340
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/16 v6, 0x7d2

    const/16 v7, 0x20

    const/4 v8, -0x2

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    move-object v0, v1

    goto :goto_0

    .line 2349
    :cond_0
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/16 v4, 0x7f6

    const/16 v5, 0x20

    const/4 v6, -0x2

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    move-object v0, v7

    .line 2357
    :goto_0
    return-object v0
.end method

.method private _GetLayoutResourceID(Ljava/lang/String;)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 2311
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "layout"

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private _GetResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 2299
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2300
    .local v0, "resourceID":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private _GetResourceID(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "name"    # Ljava/lang/String;

    .line 2305
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2306
    .local v0, "resourceID":I
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private _GetResourceStringValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 2293
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "string"

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2294
    .local v0, "resourceID":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private _GetScreenBrightness()I
    .locals 1

    .line 2271
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetScreenBrightness(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method private _GetXmlResourceParser(Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 2316
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "xml"

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2317
    .local v0, "resourceID":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 2318
    .local v1, "xmlResourceParser":Landroid/content/res/XmlResourceParser;
    return-object v1
.end method

.method private _IsForegrounded()Z
    .locals 3

    .line 885
    new-instance v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    .line 886
    .local v0, "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    invoke-static {v0}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    .line 887
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0x64

    if-eq v1, v2, :cond_1

    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private _ReadUserCode()Ljava/lang/String;
    .locals 3

    .line 1957
    const-string v0, "methinksPatcher"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1959
    .local v0, "pref":Landroid/content/SharedPreferences;
    iget-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    const/4 v2, 0x1

    if-ne v2, v1, :cond_0

    .line 1960
    const-string v1, "UserCode_Release"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1961
    :cond_0
    const-string v1, "UserCode_Debug"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private _RemoveAllUserCode()V
    .locals 3

    .line 1980
    const-string v0, "methinksPatcher"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1981
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1983
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1984
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1986
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1987
    return-void
.end method

.method private _RemoveUserCode()V
    .locals 4

    .line 1966
    const-string v0, "methinksPatcher"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1967
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1969
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    const/4 v3, 0x1

    if-ne v3, v2, :cond_0

    .line 1970
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 1972
    :cond_0
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1974
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1975
    return-void
.end method

.method private _RequestServer(Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 35
    .param p1, "messagePack"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 920
    move-object/from16 v7, p0

    :try_start_0
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[_RequestServer] - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    iget-boolean v1, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    if-nez v1, :cond_0

    .line 924
    const-string v1, "https://apptest-dev.methinks.io/"

    goto :goto_0

    .line 928
    :cond_0
    const-string v1, "https://apptest.methinks.io/"

    .line 930
    .local v1, "fullURL":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    .line 931
    .end local v1    # "fullURL":Ljava/lang/String;
    .local v8, "fullURL":Ljava/lang/String;
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[_RequestServer] - URL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object v9, v1

    .line 934
    .local v9, "url":Ljava/net/URL;
    const/4 v1, 0x0

    .line 936
    .local v1, "httpURLConnection":Ljava/net/HttpURLConnection;
    const/4 v10, 0x0

    .line 937
    .local v10, "outputStream":Ljava/io/OutputStream;
    const/4 v11, 0x0

    .line 938
    .local v11, "inputStream":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 940
    .local v12, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    move-object v13, v2

    .line 941
    .end local v1    # "httpURLConnection":Ljava/net/HttpURLConnection;
    .local v13, "httpURLConnection":Ljava/net/HttpURLConnection;
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 942
    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 943
    const/16 v1, 0x2710

    invoke-virtual {v13, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 944
    const/16 v1, 0x3a98

    invoke-virtual {v13, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 945
    const-string v1, "POST"

    invoke-virtual {v13, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 947
    const-string v1, "project-name"

    iget-object v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectID:Ljava/lang/String;

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    const-string v1, "user-code"

    iget-object v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProjectID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " , UserCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v6, 0x2

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v2, "picCapture"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_1
    const-string v2, "captureDetail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    goto :goto_2

    :sswitch_2
    const-string v2, "capture"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_3
    const-string v2, "picCaptureInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    goto :goto_2

    :sswitch_4
    const-string v2, "login"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_5
    const-string v2, "event"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v2, "log"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_7
    const-string v2, "question"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_8
    const-string v2, "answer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v1, -0x1

    :goto_2
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_3

    .line 1126
    :pswitch_0
    const/4 v1, 0x1

    .line 1127
    .local v1, "isNull":Z
    if-ne v14, v1, :cond_2

    .end local v1    # "isNull":Z
    goto/16 :goto_3

    .line 1131
    .restart local v1    # "isNull":Z
    :cond_2
    const-string v2, "Content-Type"

    const-string v3, "application/json"

    invoke-virtual {v13, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    const-string v2, "Content-Length"

    const-string v3, "1"

    invoke-virtual {v13, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1135
    .local v2, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    move-object v10, v3

    .line 1136
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/io/OutputStream;->write([B)V

    .line 1137
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    goto/16 :goto_3

    .line 1108
    .end local v1    # "isNull":Z
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :pswitch_1
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1112
    .local v1, "jasonObject":Lorg/json/JSONObject;
    const-string v2, "event"

    const-string v3, "test"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1113
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 1114
    .local v2, "jsonBytes":[B
    array-length v3, v2

    .line 1115
    .local v3, "jsonBytesLength":I
    const-string v4, "Content-Length"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    move-object v10, v4

    .line 1117
    invoke-virtual {v10, v2}, Ljava/io/OutputStream;->write([B)V

    .line 1118
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 1120
    .end local v1    # "jasonObject":Lorg/json/JSONObject;
    .end local v2    # "jsonBytes":[B
    .end local v3    # "jsonBytesLength":I
    goto/16 :goto_3

    .line 1094
    :pswitch_2
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    invoke-static {v14}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;

    move-result-object v1

    invoke-virtual {v1, v14}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->Answer(Z)Ljava/lang/String;

    move-result-object v1

    .line 1096
    .local v1, "answer":Ljava/lang/String;
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 1098
    .restart local v2    # "jsonBytes":[B
    const-string v3, "Content-Length"

    array-length v4, v2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    move-object v10, v3

    .line 1101
    invoke-virtual {v10, v2}, Ljava/io/OutputStream;->write([B)V

    .line 1102
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 1104
    .end local v1    # "answer":Ljava/lang/String;
    .end local v2    # "jsonBytes":[B
    goto/16 :goto_3

    .line 999
    :pswitch_3
    sget v1, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    if-eq v14, v1, :cond_3

    goto/16 :goto_3

    .line 1003
    :cond_3
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    const-string v1, "phone"

    invoke-virtual {v7, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 1006
    .local v3, "telephonyManager":Landroid/telephony/TelephonyManager;
    const-string v1, "audio"

    invoke-virtual {v7, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 1007
    .local v1, "audioManager":Landroid/media/AudioManager;
    const-string v2, "connectivity"

    invoke-virtual {v7, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1008
    .local v2, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-direct/range {p0 .. p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_GetScreenBrightness()I

    move-result v4

    const/4 v5, 0x0

    .line 1009
    .local v4, "screenBrightness":I
    iget-object v5, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_windowManager:Landroid/view/WindowManager;

    const/4 v14, -0x1

    const/4 v15, 0x0

    move-object/from16 v6, p0

    invoke-static/range {v1 .. v6}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetDeviceInfoInLog(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1011
    .local v5, "deviceInfoJsonString":Ljava/lang/String;
    const-string v6, "methinksPatcherApp."

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "DeviceInfoJsonString : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v6, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 1014
    .local v6, "jsonBytes":[B
    array-length v14, v6

    .line 1015
    .local v14, "jsonBytesLength":I
    const-string v15, "Content-Length"

    move-object/from16 v18, v1

    .end local v1    # "audioManager":Landroid/media/AudioManager;
    .local v18, "audioManager":Landroid/media/AudioManager;
    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v15, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    move-object v10, v1

    .line 1017
    invoke-virtual {v10, v6}, Ljava/io/OutputStream;->write([B)V

    .line 1018
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 1021
    const/4 v1, 0x0

    sput v1, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    .line 1025
    .end local v2    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v3    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .end local v4    # "screenBrightness":I
    .end local v5    # "deviceInfoJsonString":Ljava/lang/String;
    .end local v6    # "jsonBytes":[B
    .end local v14    # "jsonBytesLength":I
    .end local v18    # "audioManager":Landroid/media/AudioManager;
    goto/16 :goto_3

    .line 973
    :pswitch_4
    const-string v1, "methinksPatcherApp."

    const-string v2, "login"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const-string v1, "phone"

    invoke-virtual {v7, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 979
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    const-string v2, "audio"

    invoke-virtual {v7, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 980
    .local v2, "audioManager":Landroid/media/AudioManager;
    const-string v3, "connectivity"

    invoke-virtual {v7, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 981
    .local v3, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-direct/range {p0 .. p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_GetScreenBrightness()I

    move-result v4

    .line 982
    .restart local v4    # "screenBrightness":I
    invoke-static {v2, v3, v1, v4}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetDeviceInfoInLogin(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;I)Ljava/lang/String;

    move-result-object v5

    .line 984
    .restart local v5    # "deviceInfoJsonString":Ljava/lang/String;
    const-string v6, "methinksPatcherApp."

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "DeviceInfoJsonString : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v6, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 987
    .restart local v6    # "jsonBytes":[B
    array-length v14, v6

    .line 988
    .restart local v14    # "jsonBytesLength":I
    const-string v15, "methinksPatcherApp."

    move-object/from16 v19, v1

    .end local v1    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .local v19, "telephonyManager":Landroid/telephony/TelephonyManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v20, v2

    .end local v2    # "audioManager":Landroid/media/AudioManager;
    .local v20, "audioManager":Landroid/media/AudioManager;
    const-string v2, "jsonBytesLength : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v15, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    const-string v1, "Content-Length"

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    move-object v10, v1

    .line 991
    invoke-virtual {v10, v6}, Ljava/io/OutputStream;->write([B)V

    .line 992
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 993
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V

    .line 995
    .end local v3    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v4    # "screenBrightness":I
    .end local v5    # "deviceInfoJsonString":Ljava/lang/String;
    .end local v6    # "jsonBytes":[B
    .end local v14    # "jsonBytesLength":I
    .end local v19    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .end local v20    # "audioManager":Landroid/media/AudioManager;
    goto :goto_3

    .line 956
    :pswitch_5
    const-string v1, "----------V2ymHFg03ehb4238qgZCaK128O6jy"

    .line 957
    .local v1, "BoundaryConstant":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multipart/form-data; boundary="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 958
    .local v2, "contentType":Ljava/lang/String;
    const-string v3, "Content-Type"

    invoke-virtual {v13, v3, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    .end local v1    # "BoundaryConstant":Ljava/lang/String;
    .end local v2    # "contentType":Ljava/lang/String;
    nop

    .line 1144
    :goto_3
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->connect()V

    .line 1145
    const-string v1, "None"

    .line 1146
    .local v1, "serverResponseMessage":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 1147
    .local v2, "serverResponseCode":I
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "httpURLConnection.getResponseCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_5

    const/16 v3, 0x194

    if-eq v2, v3, :cond_4

    goto :goto_4

    .line 1153
    :cond_4
    const-string v3, "methinksPatcherApp."

    const-string v4, "ResponseCode : HTTP_NOT_FOUND"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    nop

    .line 1312
    :goto_4
    move/from16 v21, v2

    goto/16 :goto_b

    .line 1159
    :cond_5
    const-string v3, "methinksPatcherApp."

    const-string v4, "ResponseCode : HTTP_OK"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    move-object v11, v3

    .line 1162
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v12, v3

    .line 1163
    const/16 v3, 0x800

    new-array v3, v3, [B

    .line 1164
    .local v3, "byteBuffer":[B
    const/4 v4, 0x0

    .line 1165
    .local v4, "byteData":[B
    const/4 v5, 0x0

    .line 1166
    .local v5, "nLength":I
    :goto_5
    array-length v6, v3

    const/4 v14, 0x0

    invoke-virtual {v11, v3, v14, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    move v5, v6

    const/4 v15, -0x1

    if-eq v6, v15, :cond_6

    .line 1168
    invoke-virtual {v12, v3, v14, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_5

    .line 1171
    :cond_6
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    move-object v4, v6

    .line 1172
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([B)V

    move-object v1, v6

    .line 1173
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1174
    .local v6, "responseJSON":Lorg/json/JSONObject;
    const-string v14, "status"

    invoke-virtual {v6, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1175
    .local v14, "status":Ljava/lang/String;
    const-string v15, "methinksPatcherApp."

    move/from16 v21, v2

    .end local v2    # "serverResponseCode":I
    .local v21, "serverResponseCode":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v22, v3

    .end local v3    # "byteBuffer":[B
    .local v22, "byteBuffer":[B
    const-string v3, "status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v15, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v15, -0x5435c042

    if-eq v3, v15, :cond_a

    const v15, 0x1a344

    if-eq v3, v15, :cond_9

    const v15, 0x5c6729a

    if-eq v3, v15, :cond_8

    const v15, 0x625ef69

    if-eq v3, v15, :cond_7

    goto :goto_6

    :cond_7
    const-string v3, "login"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v17, 0x1

    goto :goto_7

    :cond_8
    const-string v3, "event"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v17, 0x2

    goto :goto_7

    :cond_9
    const-string v3, "log"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v17, 0x0

    goto :goto_7

    :cond_a
    const-string v3, "answer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v17, 0x3

    goto :goto_7

    :cond_b
    :goto_6
    const/16 v17, -0x1

    :goto_7
    packed-switch v17, :pswitch_data_1

    .line 1307
    :cond_c
    move-object/from16 v23, v4

    move/from16 v30, v5

    move-object/from16 v31, v6

    .end local v4    # "byteData":[B
    .end local v5    # "nLength":I
    .end local v6    # "responseJSON":Lorg/json/JSONObject;
    .local v23, "byteData":[B
    .local v30, "nLength":I
    .local v31, "responseJSON":Lorg/json/JSONObject;
    goto/16 :goto_a

    .line 1298
    .end local v23    # "byteData":[B
    .end local v30    # "nLength":I
    .end local v31    # "responseJSON":Lorg/json/JSONObject;
    .restart local v4    # "byteData":[B
    .restart local v5    # "nLength":I
    .restart local v6    # "responseJSON":Lorg/json/JSONObject;
    :pswitch_6
    const-string v2, "ok"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1281
    :pswitch_7
    const-string v2, "ok"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1283
    invoke-static {v6}, Lcom/kint/kintframeworkaosaar/GlobalSet;->ParseQuestionInfo(Lorg/json/JSONObject;)V

    .line 1286
    iget-object v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 1287
    .local v2, "msg":Landroid/os/Message;
    const/16 v3, 0x9

    iput v3, v2, Landroid/os/Message;->what:I

    .line 1288
    iget-object v3, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1289
    .end local v2    # "msg":Landroid/os/Message;
    nop

    .line 1307
    .end local v4    # "byteData":[B
    .end local v5    # "nLength":I
    .end local v6    # "responseJSON":Lorg/json/JSONObject;
    .restart local v23    # "byteData":[B
    .restart local v30    # "nLength":I
    .restart local v31    # "responseJSON":Lorg/json/JSONObject;
    :cond_d
    :goto_8
    move-object/from16 v23, v4

    move/from16 v30, v5

    move-object/from16 v31, v6

    goto/16 :goto_a

    .line 1235
    .end local v23    # "byteData":[B
    .end local v30    # "nLength":I
    .end local v31    # "responseJSON":Lorg/json/JSONObject;
    .restart local v4    # "byteData":[B
    .restart local v5    # "nLength":I
    .restart local v6    # "responseJSON":Lorg/json/JSONObject;
    :pswitch_8
    new-instance v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    invoke-direct {v2, v7}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V

    iput-object v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    .line 1236
    iget-object v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    invoke-virtual {v2, v6}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->Parse(Lorg/json/JSONObject;)Landroid/os/Message;

    .line 1240
    goto :goto_8

    .line 1194
    :pswitch_9
    const-string v2, "ok"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1196
    const-string v2, "isScreenCaptureAllowed"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1197
    .local v2, "isScreenCaptureAllowed":Z
    const-string v3, "methinksPatcherApp."

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v23, v4

    .end local v4    # "byteData":[B
    .restart local v23    # "byteData":[B
    const-string v4, "isScreenCaptureAllowed : "

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    const-string v3, "isScreenStreamAllowed"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 1201
    .local v3, "isScreenStreamAllowed":Z
    const-string v4, "methinksPatcherApp."

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v24, v2

    .end local v2    # "isScreenCaptureAllowed":Z
    .local v24, "isScreenCaptureAllowed":Z
    const-string v2, "isScreenStreamAllowed : "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    const-string v2, "isCameraStreamAllowed"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1205
    .local v2, "isCameraStreamAllowed":Z
    const-string v4, "methinksPatcherApp."

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v25, v3

    .end local v3    # "isScreenStreamAllowed":Z
    .local v25, "isScreenStreamAllowed":Z
    const-string v3, "isCameraStreamAllowed : "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1208
    const-string v3, "minimumTestBuildNumber"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1209
    .local v3, "minimumTestBuildNumber":I
    const-string v4, "methinksPatcherApp."

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v26, v2

    .end local v2    # "isCameraStreamAllowed":Z
    .local v26, "isCameraStreamAllowed":Z
    const-string v2, "minimumTestBuildNumber : "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    const-string v2, "sessionTime"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1211
    .local v2, "sessionTimeJSON":Lorg/json/JSONObject;
    const-string v4, "accumulated"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v15

    move-wide/from16 v27, v15

    .line 1212
    .local v27, "accumulated":J
    const-string v4, "methinksPatcherApp."

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v29, v3

    .end local v3    # "minimumTestBuildNumber":I
    .local v29, "minimumTestBuildNumber":I
    const-string v3, "accumulated : "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v30, v5

    move-object/from16 v31, v6

    move-wide/from16 v5, v27

    .end local v6    # "responseJSON":Lorg/json/JSONObject;
    .end local v27    # "accumulated":J
    .local v5, "accumulated":J
    .restart local v30    # "nLength":I
    .restart local v31    # "responseJSON":Lorg/json/JSONObject;
    invoke-virtual {v15, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    const-string v3, "current"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 1214
    .local v3, "current":J
    const-string v15, "methinksPatcherApp."

    move-object/from16 v32, v2

    .end local v2    # "sessionTimeJSON":Lorg/json/JSONObject;
    .local v32, "sessionTimeJSON":Lorg/json/JSONObject;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v33, v5

    .end local v5    # "accumulated":J
    .local v33, "accumulated":J
    const-string v5, "current : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v15, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    sget v2, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    if-nez v2, :cond_f

    .line 1218
    sget-wide v5, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    cmp-long v2, v3, v5

    if-ltz v2, :cond_f

    .line 1220
    iget-boolean v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    const/4 v5, 0x1

    if-ne v5, v2, :cond_e

    .line 1221
    sget-wide v5, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    const-wide/16 v15, 0x258

    add-long/2addr v5, v15

    sput-wide v5, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    goto :goto_9

    .line 1223
    :cond_e
    sget-wide v5, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    const-wide/16 v15, 0x3c

    add-long/2addr v5, v15

    sput-wide v5, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    .line 1226
    :goto_9
    const/4 v2, 0x1

    sput v2, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    .line 1229
    .end local v3    # "current":J
    .end local v24    # "isScreenCaptureAllowed":Z
    .end local v25    # "isScreenStreamAllowed":Z
    .end local v26    # "isCameraStreamAllowed":Z
    .end local v29    # "minimumTestBuildNumber":I
    .end local v32    # "sessionTimeJSON":Lorg/json/JSONObject;
    .end local v33    # "accumulated":J
    :cond_f
    nop

    .line 1307
    :goto_a
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-----------------------------> Server Response - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    .end local v14    # "status":Ljava/lang/String;
    .end local v22    # "byteBuffer":[B
    .end local v23    # "byteData":[B
    .end local v30    # "nLength":I
    .end local v31    # "responseJSON":Lorg/json/JSONObject;
    :goto_b
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 1313
    const/4 v2, 0x2

    iput v2, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_dispatchNetworkStatus:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_c

    .line 1319
    .end local v1    # "serverResponseMessage":Ljava/lang/String;
    .end local v8    # "fullURL":Ljava/lang/String;
    .end local v9    # "url":Ljava/net/URL;
    .end local v10    # "outputStream":Ljava/io/OutputStream;
    .end local v11    # "inputStream":Ljava/io/InputStream;
    .end local v12    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .end local v13    # "httpURLConnection":Ljava/net/HttpURLConnection;
    .end local v21    # "serverResponseCode":I
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1321
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 1315
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 1317
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 1322
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_c
    nop

    .line 1324
    :goto_d
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5435c042 -> :sswitch_8
        -0x457dc41a -> :sswitch_7
        0x1a344 -> :sswitch_6
        0x5c6729a -> :sswitch_5
        0x625ef69 -> :sswitch_4
        0xb94092a -> :sswitch_3
        0x20efc746 -> :sswitch_2
        0x5a81bf37 -> :sswitch_1
        0x606fbe5c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method private _SendCallbackMessage(I)V
    .locals 1
    .param p1, "messageType"    # I

    .line 432
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    invoke-interface {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;->SendMessage(I)V

    .line 434
    :cond_0
    return-void
.end method

.method private _StartNetworkMessageLoop()V
    .locals 5

    .line 1329
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1330
    .local v0, "handler":Landroid/os/Handler;
    const/16 v1, 0x3e8

    .line 1332
    .local v1, "delay":I
    new-instance v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    invoke-direct {v2, p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/os/Handler;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1571
    return-void
.end method

.method private _WriteUserCode(Ljava/lang/String;)V
    .locals 4
    .param p1, "userCode"    # Ljava/lang/String;

    .line 1945
    const-string v0, "methinksPatcher"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1946
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1948
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    const/4 v3, 0x1

    if-ne v3, v2, :cond_0

    .line 1949
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 1951
    :cond_0
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1952
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1953
    return-void
.end method

.method static synthetic access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_RemoveAllUserCode()V

    return-void
.end method

.method static synthetic access$100(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_IsForegrounded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->paused:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Z

    .line 65
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->paused:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/media/projection/MediaProjection;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/media/projection/MediaProjection;)Landroid/media/projection/MediaProjection;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Landroid/media/projection/MediaProjection;

    .line 65
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectionResultCode:I

    return v0
.end method

.method static synthetic access$1800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectionIntentData:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/media/projection/MediaProjectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_requestMessageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_quitApplication:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Z

    .line 65
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_quitApplication:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_showDialog:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_CreateWindowManagerLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_GetLayoutResourceID(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_GetResourceID(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_windowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_GetResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_dispatchNetworkStatus:I

    return v0
.end method

.method static synthetic access$3000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # I

    .line 65
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_SendCallbackMessage(I)V

    return-void
.end method

.method static synthetic access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # I

    .line 65
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_dispatchNetworkStatus:I

    return p1
.end method

.method static synthetic access$3100(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_testMode:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_WriteUserCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 65
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_RequestServer(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    return-void
.end method

.method static synthetic access$500(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_processStatus:I

    return v0
.end method

.method static synthetic access$502(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # I

    .line 65
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_processStatus:I

    return p1
.end method

.method static synthetic access$600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_logDelayTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_enableBackground:Z

    return v0
.end method

.method static synthetic access$800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_backgroundMode:Z

    return v0
.end method

.method static synthetic access$802(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
    .param p1, "x1"    # Z

    .line 65
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_backgroundMode:Z

    return p1
.end method

.method static synthetic access$900(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 65
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static enableShowTouch(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isChecked"    # Z

    .line 2251
    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    .line 2253
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "show_touches"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 2257
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "show_touches"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2259
    :goto_0
    return-void
.end method

.method static getShowTouchCheck(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 2263
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "show_touches"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2264
    .local v0, "showTouchStatus":I
    if-nez v0, :cond_0

    .line 2265
    return v2

    .line 2266
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method static settingsSystemCanWrite(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 2242
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 2244
    invoke-static {p0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    return v0

    .line 2246
    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public CreateScreenCaptureIntent()Landroid/content/Intent;
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    if-nez v0, :cond_0

    .line 407
    const-string v0, "media_projection"

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/projection/MediaProjectionManager;

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    invoke-virtual {v0}, Landroid/media/projection/MediaProjectionManager;->createScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public GetMainActivity()Landroid/app/Activity;
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mainActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public GetProcessStatus()I
    .locals 1

    .line 401
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_processStatus:I

    return v0
.end method

.method public GetReleaseMode()Z
    .locals 1

    .line 485
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    return v0
.end method

.method public Initialize(Ljava/lang/String;Ljava/lang/String;ZIZ)V
    .locals 3
    .param p1, "rtmpServerBaseURL"    # Ljava/lang/String;
    .param p2, "projectID"    # Ljava/lang/String;
    .param p3, "release"    # Z
    .param p4, "cheatKey"    # I
    .param p5, "enableBackground"    # Z

    .line 438
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 440
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerBaseURL:Ljava/lang/String;

    goto :goto_0

    .line 444
    :cond_0
    if-eqz p3, :cond_1

    .line 446
    const-string v0, "rtmp://ec2-18-232-156-45.compute-1.amazonaws.com:5391"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerBaseURL:Ljava/lang/String;

    goto :goto_0

    .line 450
    :cond_1
    const-string v0, "rtmp://ec2-184-72-75-118.compute-1.amazonaws.com:5391"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerBaseURL:Ljava/lang/String;

    .line 454
    :goto_0
    const v0, 0xbe81d

    if-ne v0, p4, :cond_2

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_testMode:Z

    .line 457
    :cond_2
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectID:Ljava/lang/String;

    .line 458
    iput-boolean p3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    .line 459
    iput-boolean p5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_enableBackground:Z

    .line 461
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    if-nez v0, :cond_3

    .line 462
    const-wide/16 v0, 0x3c

    sput-wide v0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    goto :goto_1

    .line 464
    :cond_3
    const-wide/16 v0, 0x258

    sput-wide v0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    .line 465
    :goto_1
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Initialize] - RTMPServerBaseURL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , ProjectID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , Release : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_release:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, " , TestMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_testMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    return-void
.end method

.method public Login()V
    .locals 2

    .line 762
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_processStatus:I

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 764
    return-void

    .line 767
    :cond_0
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_StartNetworkMessageLoop()V

    .line 768
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_ReadUserCode()Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "userCode":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 771
    new-instance v1, Lcom/kint/kintframeworkaosaar/MessagePackLogin;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;-><init>()V

    .line 772
    .local v1, "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;->Prepare()V

    .line 773
    invoke-virtual {p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 774
    .end local v1    # "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    goto :goto_0

    .line 778
    :cond_1
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SetUserCode(Ljava/lang/String;)V

    .line 780
    :goto_0
    return-void
.end method

.method public OnActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 711
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityCreated] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    return-void
.end method

.method public OnActivityDestroyed(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 748
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityDestroyed] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_currentActiviy:Landroid/app/Activity;

    .line 758
    return-void
.end method

.method public OnActivityPaused(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 687
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityPaused] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , PackageName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    return-void
.end method

.method public OnActivityResumed(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 717
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityResumed] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , PackageName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_currentActiviy:Landroid/app/Activity;

    .line 743
    return-void
.end method

.method public OnActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 682
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivitySaveInstanceState] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    return-void
.end method

.method public OnActivityStarted(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 677
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityStarted] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    return-void
.end method

.method public OnActivityStopped(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 671
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onActivityStopped] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    return-void
.end method

.method public Quit()V
    .locals 1

    .line 830
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_currentActiviy:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_currentActiviy:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 832
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 833
    return-void
.end method

.method public SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 1
    .param p1, "requestMessagePack"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 895
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_requestMessageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 896
    return-void
.end method

.method public SendRequestMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 900
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_processStatus:I

    if-nez v0, :cond_0

    .line 902
    const-string v0, "Login Required"

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Tooltip(Ljava/lang/String;)V

    .line 903
    return-void

    .line 906
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 912
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;

    invoke-direct {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/MessagePackEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    .local v0, "messagePackEvent":Lcom/kint/kintframeworkaosaar/MessagePackEvent;
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 914
    return-void
.end method

.method public SetCallbackMessage(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;)V
    .locals 0
    .param p1, "callbackMessage"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    .line 427
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    .line 428
    return-void
.end method

.method public SetMainActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 475
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mainActivity:Landroid/app/Activity;

    .line 476
    return-void
.end method

.method public SetMediaProjection(ILandroid/content/Intent;)V
    .locals 0
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .line 413
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectionResultCode:I

    .line 414
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectionIntentData:Landroid/content/Intent;

    .line 418
    return-void
.end method

.method public SetPatchIntegrateMode(I)V
    .locals 0
    .param p1, "patchIntegrateMode"    # I

    .line 470
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_patchIntegrateMode:I

    .line 471
    return-void
.end method

.method public SetUserCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "userCode"    # Ljava/lang/String;

    .line 784
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    .line 785
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/screen/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "?projectID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_projectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    .line 786
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SetUserCode] - RTMPServerURL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackLogin;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;-><init>()V

    .line 789
    .local v0, "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;->Request()V

    .line 790
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 791
    return-void
.end method

.method public Tooltip(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .line 1939
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1940
    return-void
.end method

.method _DetectScreenShotService()V
    .locals 10

    .line 837
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 838
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 839
    .local v1, "runningServiceInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 841
    .local v3, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    const-string v5, "com.android.systemui:screenshot"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 843
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v6}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "/Screenshots"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$2;

    invoke-direct {v5, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V

    .line 844
    invoke-virtual {v4, v5}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v4

    .line 854
    .local v4, "files":[Ljava/io/File;
    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 855
    array-length v5, v4

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    aget-object v5, v4, v5

    .line 856
    .local v5, "lastFile":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Screenshot captured!! - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 879
    .end local v3    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    .end local v4    # "files":[Ljava/io/File;
    .end local v5    # "lastFile":Ljava/io/File;
    :cond_0
    goto :goto_0

    .line 880
    :cond_1
    return-void
.end method

.method public connectServer(Ljava/lang/String;)V
    .locals 3
    .param p1, "rtmpServerURL"    # Ljava/lang/String;

    .line 2011
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ConnectServer] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2012
    const-string v0, "Ready"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2014
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    .line 2016
    new-instance v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;-><init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 2017
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    iget v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setVideoResolution(II)V

    .line 2018
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoDataRate:I

    iget v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoFrameRate:I

    invoke-virtual {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setVideoRate(II)V

    .line 2026
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_rtmpServerURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->start(Ljava/lang/String;)V

    .line 2027
    new-instance v0, Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;-><init>(Lcom/kint/kintframeworkaosaar/GetH264Data;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2028
    return-void
.end method

.method public getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 7
    .param p1, "h264Buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "info"    # Landroid/media/MediaCodec$BufferInfo;

    .line 2161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    rem-long/2addr v0, v2

    .line 2162
    .local v0, "checkTime":J
    const-wide/16 v2, 0x79e

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 2164
    const-string v4, "methinksPatcherApp."

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[getH264Data] captured : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2167
    :cond_0
    iget-boolean v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectedRTMPServer:Z

    const/4 v5, 0x1

    if-ne v5, v4, :cond_3

    .line 2170
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v4, :cond_3

    .line 2173
    iget-boolean v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->paused:Z

    if-nez v4, :cond_2

    .line 2176
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 2178
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getH264Data] - Sending Stream : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2181
    :cond_1
    const-string v2, "Sending..."

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2182
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-virtual {v2, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sendVideo(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 2184
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->sendRTMPVideo:Z

    if-nez v2, :cond_3

    .line 2186
    const-string v2, "methinksPatcherApp."

    const-string v3, "[getH264Data] - Start Send Stream"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2187
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 2188
    .local v2, "message":Landroid/os/Message;
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->what:I

    .line 2189
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2191
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->sendRTMPVideo:Z

    .line 2192
    .end local v2    # "message":Landroid/os/Message;
    goto :goto_0

    .line 2196
    :cond_2
    const-string v2, "methinksPatcherApp."

    const-string v3, "[getH264Data] - Pause Send Stream"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2197
    const-string v2, "Paused"

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2205
    :cond_3
    :goto_0
    return-void
.end method

.method public getNetworkStatus()Ljava/lang/String;
    .locals 1

    .line 2088
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getShowTouchCheck()Z
    .locals 1

    .line 2217
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getShowTouchCheck(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .line 1992
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    return v0
.end method

.method public onAuthErrorRtmp()V
    .locals 2

    .line 2132
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onAuthErrorRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2133
    const-string v0, "AuthError"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2134
    return-void
.end method

.method public onAuthSuccessRtmp()V
    .locals 2

    .line 2139
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onAuthSuccessRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2140
    const-string v0, "AuthSuccess"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2141
    return-void
.end method

.method public onConnectionFailedRtmp(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 2108
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onConnectionFailedRtmp] - reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConnectionFailed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2111
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2112
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2113
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2114
    return-void
.end method

.method public onConnectionSuccessRtmp()V
    .locals 2

    .line 2095
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onConnectionSuccessRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2096
    const-string v0, "ConnectionSuccess"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2097
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectedRTMPServer:Z

    .line 2098
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->paused:Z

    .line 2100
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 2101
    .local v1, "message":Landroid/os/Message;
    iput v0, v1, Landroid/os/Message;->what:I

    .line 2102
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2103
    return-void
.end method

.method public onCreate()V
    .locals 4

    .line 490
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 492
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 493
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 494
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 495
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 496
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_0

    .line 498
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 522
    :cond_0
    new-instance v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;

    invoke-direct {v1, p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v1, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 626
    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_windowManager:Landroid/view/WindowManager;

    .line 627
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_requestMessageList:Ljava/util/ArrayList;

    .line 628
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_windowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 656
    :pswitch_0
    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 657
    .local v1, "temp":I
    iget v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 658
    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    .line 661
    const/16 v2, 0x5a

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_screenRotationAngle:I

    goto :goto_0

    .line 650
    .end local v1    # "temp":I
    :pswitch_1
    const/16 v1, 0xb4

    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_screenRotationAngle:I

    .line 652
    goto :goto_0

    .line 638
    :pswitch_2
    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 639
    .restart local v1    # "temp":I
    iget v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 640
    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    .line 643
    const/16 v2, -0x5a

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_screenRotationAngle:I

    .line 645
    .end local v1    # "temp":I
    goto :goto_0

    .line 634
    :pswitch_3
    nop

    .line 666
    :goto_0
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onCreate] - Language : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDisconnectRtmp()V
    .locals 2

    .line 2119
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onDisconnectRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2120
    const-string v0, "Disconnect"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectedRTMPServer:Z

    .line 2122
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->sendRTMPVideo:Z

    .line 2124
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2125
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2126
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2127
    return-void
.end method

.method public onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;

    .line 2147
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onSPSandPPS]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2148
    const-string v0, "SPSandPPS"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2149
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectedRTMPServer:Z

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 2151
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v0, :cond_0

    .line 2153
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setSpsPPs(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 2156
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0
    .param p1, "level"    # I

    .line 820
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 821
    nop

    .line 826
    return-void
.end method

.method public onVideoFormat(Landroid/media/MediaFormat;)V
    .locals 3
    .param p1, "mediaFormat"    # Landroid/media/MediaFormat;

    .line 2210
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onVideoFormat] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/media/MediaFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2211
    const-string v0, "VideoFormat"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->networkStatus:Ljava/lang/String;

    .line 2212
    return-void
.end method

.method public setResolution(III)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "dpi"    # I

    .line 1997
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    .line 1998
    iput p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    .line 1999
    iput p3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoDPI:I

    .line 2000
    return-void
.end method

.method public setVideoRate(II)V
    .locals 0
    .param p1, "videoDataRate"    # I
    .param p2, "frameRate"    # I

    .line 2005
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoDataRate:I

    .line 2006
    iput p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoFrameRate:I

    .line 2007
    return-void
.end method

.method public settingsSystemShowTouch()Z
    .locals 3

    .line 2222
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/16 v2, 0x17

    if-lt v0, v2, :cond_1

    .line 2224
    invoke-static {p0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2226
    const/4 v0, 0x0

    return v0

    .line 2230
    :cond_0
    invoke-static {p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->enableShowTouch(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2235
    :cond_1
    invoke-static {p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->enableShowTouch(Landroid/content/Context;Z)V

    .line 2237
    :goto_0
    return v1
.end method

.method public startRecord(Landroid/media/projection/MediaProjection;)Z
    .locals 10
    .param p1, "project"    # Landroid/media/projection/MediaProjection;

    .line 2032
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[StartRecord] - running :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    if-eqz v0, :cond_0

    .line 2034
    const/4 v0, 0x0

    return v0

    .line 2036
    :cond_0
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    .line 2038
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->setWidth(I)V

    .line 2039
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->setHeight(I)V

    .line 2040
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->prepareVideoEncoder()Z

    .line 2042
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    const-string v2, "MainScreen"

    iget v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoWidth:I

    iget v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoHeight:I

    iget v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoDPI:I

    const/16 v6, 0x10

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2043
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getInputSurface()Landroid/view/Surface;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2042
    invoke-virtual/range {v1 .. v9}, Landroid/media/projection/MediaProjection;->createVirtualDisplay(Ljava/lang/String;IIIILandroid/view/Surface;Landroid/hardware/display/VirtualDisplay$Callback;Landroid/os/Handler;)Landroid/hardware/display/VirtualDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 2045
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->start()V

    .line 2047
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    .line 2048
    return v0
.end method

.method public stopRecord()Z
    .locals 3

    .line 2053
    const-string v0, "methinksPatcherApp."

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[StopRecord] - running :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2054
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2055
    return v1

    .line 2057
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 2059
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->stop()V

    .line 2060
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2063
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v0, :cond_2

    .line 2065
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->stop()V

    .line 2066
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 2069
    :cond_2
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->running:Z

    .line 2070
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->paused:Z

    .line 2072
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    if-eqz v0, :cond_3

    .line 2074
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 2075
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 2078
    :cond_3
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    if-eqz v0, :cond_4

    .line 2080
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    invoke-virtual {v0}, Landroid/media/projection/MediaProjection;->stop()V

    .line 2081
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_mediaProjection:Landroid/media/projection/MediaProjection;

    .line 2083
    :cond_4
    const/4 v0, 0x1

    return v0
.end method
