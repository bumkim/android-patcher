.class Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
.super Ljava/lang/Object;
.source "methinksPatcherApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LoginInfo"
.end annotation


# instance fields
.field private _campaignName:Ljava/lang/String;

.field private _error:Ljava/lang/String;

.field private _isCameraStreamAllowed:Z

.field private _isScreenCaptureAllowed:Z

.field private _isScreenStreamAllowed:Z

.field private _minimumTestBuildNumber:I

.field private _notice:Ljava/lang/String;

.field private _permissionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private _screenName:Ljava/lang/String;

.field private _status:Ljava/lang/String;

.field private _statusResultAnnouncementCount:I

.field private _statusResultScreenName:Ljava/lang/String;

.field private _statusResultStartDate:J

.field private _statusResultStatus:Ljava/lang/String;

.field private _statusResultSurveyCount:I

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 67
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public CheckValidData()Z
    .locals 22

    .line 132
    move-object/from16 v0, p0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 133
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 135
    .local v2, "date":Ljava/util/Date;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-long v4, v4

    .line 136
    .local v4, "year":J
    const/4 v6, 0x2

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/2addr v6, v3

    int-to-long v6, v6

    .line 137
    .local v6, "month":J
    const/4 v8, 0x5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    .line 138
    .local v8, "day":J
    const/16 v10, 0xb

    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    int-to-long v10, v10

    .line 139
    .local v10, "hour":J
    const-wide/32 v12, 0xf4240

    mul-long v12, v12, v4

    const-wide/16 v14, 0x2710

    mul-long v14, v14, v6

    add-long/2addr v12, v14

    const-wide/16 v14, 0x64

    mul-long v14, v14, v8

    add-long/2addr v12, v14

    add-long/2addr v12, v10

    .line 141
    .local v12, "nowDateTime":J
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    .line 143
    .local v14, "nowDateTimeExceptTick":J
    const-string v3, "methinksPatcherApp."

    move-object/from16 v18, v1

    .end local v1    # "calendar":Ljava/util/Calendar;
    .local v18, "calendar":Ljava/util/Calendar;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v19, v2

    .end local v2    # "date":Ljava/util/Date;
    .local v19, "date":Ljava/util/Date;
    const-string v2, "[CheckValidate] - nowDataTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " , nowDateTimeExceptTick : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " , StartDateTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v20, v4

    .end local v4    # "year":J
    .local v20, "year":J
    iget-wide v4, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStartDate:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-wide v1, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStartDate:J

    cmp-long v1, v14, v1

    if-ltz v1, :cond_0

    .line 145
    const/4 v1, 0x1

    return v1

    .line 146
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public GetError()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_error:Ljava/lang/String;

    return-object v0
.end method

.method public GetResultStatus()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    return-object v0
.end method

.method public GetStartData()J
    .locals 2

    .line 117
    iget-wide v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStartDate:J

    return-wide v0
.end method

.method public IsCameraStreamAllowed()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isCameraStreamAllowed:Z

    return v0
.end method

.method public IsScreenCaptureAllowed()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenCaptureAllowed:Z

    return v0
.end method

.method public IsScreenStreamAllowed()Z
    .locals 1

    .line 97
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenStreamAllowed:Z

    return v0
.end method

.method public Parse(Lorg/json/JSONObject;)Landroid/os/Message;
    .locals 7
    .param p1, "responseJSON"    # Lorg/json/JSONObject;

    .line 182
    const/4 v0, 0x0

    .line 185
    .local v0, "message":Landroid/os/Message;
    :try_start_0
    const-string v1, "status"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_status:Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_status:Ljava/lang/String;

    const-string v2, "ok"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 188
    const-string v1, "isScreenCaptureAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenCaptureAllowed:Z

    .line 189
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isScreenCaptureAllowed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenCaptureAllowed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const-string v1, "isScreenStreamAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenStreamAllowed:Z

    .line 191
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isScreenStreamAllowed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenStreamAllowed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const-string v1, "isCameraStreamAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isCameraStreamAllowed:Z

    .line 193
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCameraStreamAllowed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isCameraStreamAllowed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v1, "notice"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_notice:Ljava/lang/String;

    .line 195
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notice : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_notice:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v1, "permissions"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 197
    .local v1, "permissionsJsonArray":Lorg/json/JSONArray;
    if-eqz v1, :cond_1

    .line 199
    const/4 v2, 0x0

    .local v2, "countIndex":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 201
    if-nez v2, :cond_0

    .line 202
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_permissionList:Ljava/util/List;

    .line 203
    :cond_0
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_permissionList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    .end local v2    # "countIndex":I
    :cond_1
    const-string v2, "screenName"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_screenName:Ljava/lang/String;

    .line 214
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "screenName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_screenName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const-string v2, "campaignName"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_campaignName:Ljava/lang/String;

    .line 216
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "campaignName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_campaignName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const-string v2, "minimumTestBuildNumber"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_minimumTestBuildNumber:I

    .line 218
    const-string v2, "methinksPatcherApp."

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "minimumTestBuildNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_minimumTestBuildNumber:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const-string v2, "statusResult"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 220
    .local v2, "statusResultJSON":Lorg/json/JSONObject;
    const-string v3, "startDate"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStartDate:J

    .line 221
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "statusResultStartDate : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStartDate:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const-string v3, "screenName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultScreenName:Ljava/lang/String;

    .line 224
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "statusResultScreenName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultScreenName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const-string v3, "status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    .line 226
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "statusResultStatus : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-string v3, "surveyCount"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultSurveyCount:I

    .line 228
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "statusResultSurveyCount : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultSurveyCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const-string v3, "announcementCount"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultAnnouncementCount:I

    .line 230
    const-string v3, "methinksPatcherApp."

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "statusResultAnnouncementCount : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_statusResultAnnouncementCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    move-object v0, v3

    .line 242
    const/4 v3, 0x7

    iput v3, v0, Landroid/os/Message;->what:I

    .line 244
    .end local v1    # "permissionsJsonArray":Lorg/json/JSONArray;
    .end local v2    # "statusResultJSON":Lorg/json/JSONObject;
    goto/16 :goto_1

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_status:Ljava/lang/String;

    const-string v2, "error"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 247
    const-string v1, "error"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_error:Ljava/lang/String;

    .line 248
    const-string v1, "methinksPatcherApp."

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Login Failure : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_error:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_error:Ljava/lang/String;

    const-string v2, "invalidProject"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0x8

    if-eqz v1, :cond_3

    .line 252
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V

    .line 255
    const-string v1, "methinksPatcherApp."

    const-string v3, "Application Quit"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v3, "invalidProject"

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Ljava/lang/String;

    .line 258
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    move-object v0, v1

    .line 259
    iput v2, v0, Landroid/os/Message;->what:I

    goto :goto_1

    .line 262
    :cond_3
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_error:Ljava/lang/String;

    const-string v3, "invalidUserCode"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 264
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V

    .line 265
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v3, "invalidUserCode"

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Ljava/lang/String;

    .line 267
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    move-object v0, v1

    .line 268
    iput v2, v0, Landroid/os/Message;->what:I

    .line 273
    :cond_4
    :goto_1
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 274
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    goto :goto_2

    .line 278
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 283
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_2
    return-object v0
.end method

.method public SetCameraStreamAllowed(Z)V
    .locals 0
    .param p1, "isCameraStreamAllowed"    # Z

    .line 111
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isCameraStreamAllowed:Z

    .line 112
    return-void
.end method

.method public SetScreenCaptureAllowed(Z)V
    .locals 0
    .param p1, "isScreenCaptureAllowed"    # Z

    .line 92
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenCaptureAllowed:Z

    .line 93
    return-void
.end method

.method public SetScreenStreamAllowed(Z)V
    .locals 0
    .param p1, "isScreenStreamAllowed"    # Z

    .line 101
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->_isScreenStreamAllowed:Z

    .line 102
    return-void
.end method
