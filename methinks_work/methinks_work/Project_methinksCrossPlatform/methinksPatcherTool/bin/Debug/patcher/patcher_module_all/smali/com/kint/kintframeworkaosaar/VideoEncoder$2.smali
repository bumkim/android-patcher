.class Lcom/kint/kintframeworkaosaar/VideoEncoder$2;
.super Ljava/lang/Object;
.source "VideoEncoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromSurfaceAPI21()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 347
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 350
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_3

    .line 353
    :goto_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v0

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    .line 354
    .local v0, "outBufferIndex":I
    const/4 v1, -0x2

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    .line 355
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v1

    .line 356
    .local v1, "mediaFormat":Landroid/media/MediaFormat;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onVideoFormat(Landroid/media/MediaFormat;)V

    .line 357
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v3

    const-string v4, "csd-0"

    invoke-virtual {v1, v4}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v4

    const-string v5, "csd-1"

    .line 358
    invoke-virtual {v1, v5}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 357
    invoke-interface {v3, v4, v5}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 359
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3, v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1402(Lcom/kint/kintframeworkaosaar/VideoEncoder;Z)Z

    .line 360
    .end local v1    # "mediaFormat":Landroid/media/MediaFormat;
    goto/16 :goto_1

    :cond_1
    if-ltz v0, :cond_0

    .line 362
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 363
    .local v1, "bb":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    iget v3, v3, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    .line 364
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 365
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 366
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v5

    iget v5, v5, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v3, v4, v5}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1500(Lcom/kint/kintframeworkaosaar/VideoEncoder;Ljava/nio/ByteBuffer;I)Landroid/util/Pair;

    move-result-object v3

    .line 367
    .local v3, "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    if-eqz v3, :cond_2

    .line 368
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v4

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/nio/ByteBuffer;

    iget-object v6, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/nio/ByteBuffer;

    invoke-interface {v4, v5, v6}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 369
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4, v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1402(Lcom/kint/kintframeworkaosaar/VideoEncoder;Z)Z

    .line 373
    .end local v3    # "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    :cond_2
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1600(Lcom/kint/kintframeworkaosaar/VideoEncoder;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    iput-wide v3, v2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 374
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/kint/kintframeworkaosaar/GetH264Data;->getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 375
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 379
    .end local v0    # "outBufferIndex":I
    .end local v1    # "bb":Ljava/nio/ByteBuffer;
    :goto_1
    goto/16 :goto_0

    .line 381
    :cond_3
    return-void
.end method
