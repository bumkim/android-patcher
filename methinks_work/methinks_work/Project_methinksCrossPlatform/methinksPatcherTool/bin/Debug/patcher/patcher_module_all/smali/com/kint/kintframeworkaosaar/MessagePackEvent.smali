.class public Lcom/kint/kintframeworkaosaar/MessagePackEvent;
.super Lcom/kint/kintframeworkaosaar/MessagePack;
.source "MessagePackEvent.java"


# instance fields
.field _eventKey:Ljava/lang/String;

.field _eventValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 9
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;-><init>()V

    .line 10
    const-string v0, "event"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    .line 11
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_key:I

    .line 13
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventKey:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventValue:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public BuildRequest()Ljava/lang/String;
    .locals 1

    .line 27
    const-string v0, ""

    return-object v0
.end method

.method public Request()V
    .locals 1

    .line 20
    const-string v0, "event"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    .line 21
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_key:I

    .line 22
    return-void
.end method
