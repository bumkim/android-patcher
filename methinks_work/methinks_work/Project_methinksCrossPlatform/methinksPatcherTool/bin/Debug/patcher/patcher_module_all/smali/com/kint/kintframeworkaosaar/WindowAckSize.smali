.class public Lcom/kint/kintframeworkaosaar/WindowAckSize;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "WindowAckSize.java"


# instance fields
.field private acknowledgementWindowSize:I


# direct methods
.method public constructor <init>(ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 4
    .param p1, "acknowledgementWindowSize"    # I
    .param p2, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 26
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 27
    invoke-virtual {p2, v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->canReusePrevHeaderTx(Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    :goto_0
    const/4 v2, 0x2

    sget-object v3, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-direct {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    .line 26
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 31
    iput p1, p0, Lcom/kint/kintframeworkaosaar/WindowAckSize;->acknowledgementWindowSize:I

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 22
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAcknowledgementWindowSize()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/kint/kintframeworkaosaar/WindowAckSize;->acknowledgementWindowSize:I

    return v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 44
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/WindowAckSize;->acknowledgementWindowSize:I

    .line 45
    return-void
.end method

.method public setAcknowledgementWindowSize(I)V
    .locals 0
    .param p1, "acknowledgementWindowSize"    # I

    .line 39
    iput p1, p0, Lcom/kint/kintframeworkaosaar/WindowAckSize;->acknowledgementWindowSize:I

    .line 40
    return-void
.end method

.method protected size()I
    .locals 1

    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 64
    const-string v0, "RTMP Window Acknowledgment Size"

    return-object v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 49
    iget v0, p0, Lcom/kint/kintframeworkaosaar/WindowAckSize;->acknowledgementWindowSize:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 50
    return-void
.end method
