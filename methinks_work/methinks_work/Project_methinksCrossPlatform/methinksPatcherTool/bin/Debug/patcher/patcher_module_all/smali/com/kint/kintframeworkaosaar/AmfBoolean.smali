.class public Lcom/kint/kintframeworkaosaar/AmfBoolean;
.super Ljava/lang/Object;
.source "AmfBoolean.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/AmfData;


# instance fields
.field private value:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1, "value"    # Z

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/AmfBoolean;->value:Z

    .line 24
    return-void
.end method

.method public static readBooleanFrom(Ljava/io/InputStream;)Z
    .locals 2
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .line 47
    const/4 v0, 0x2

    return v0
.end method

.method public isValue()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/AmfBoolean;->value:Z

    return v0
.end method

.method public readFrom(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 37
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/AmfBoolean;->value:Z

    .line 38
    return-void
.end method

.method public setValue(Z)V
    .locals 0
    .param p1, "value"    # Z

    .line 19
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/AmfBoolean;->value:Z

    .line 20
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 31
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->BOOLEAN:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 32
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/AmfBoolean;->value:Z

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 33
    return-void
.end method
