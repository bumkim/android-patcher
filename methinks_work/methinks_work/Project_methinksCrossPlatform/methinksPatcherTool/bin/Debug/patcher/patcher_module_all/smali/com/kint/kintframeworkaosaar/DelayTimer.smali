.class public Lcom/kint/kintframeworkaosaar/DelayTimer;
.super Ljava/lang/Object;
.source "DelayTimer.java"


# instance fields
.field private _callCount:I

.field private _delayTickCount:I

.field private _startTime:J

.field private _stopFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_stopFlag:Z

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_delayTickCount:I

    .line 8
    iput v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_callCount:I

    .line 12
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 13
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "startTime"    # J

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_stopFlag:Z

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_delayTickCount:I

    .line 8
    iput v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_callCount:I

    .line 17
    iput-wide p1, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_startTime:J

    .line 18
    return-void
.end method


# virtual methods
.method public ElapsedSeconds()J
    .locals 4

    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_startTime:J

    sub-long/2addr v0, v2

    .line 29
    .local v0, "elapsedTimeMillis":J
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    return-wide v2
.end method

.method public ElapsedTickCount(I)Z
    .locals 4
    .param p1, "iDelayTickCount"    # I

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_startTime:J

    sub-long/2addr v0, v2

    .line 35
    .local v0, "elapsedTimeMillis":J
    int-to-long v2, p1

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 38
    const/4 v2, 0x1

    return v2

    .line 41
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public Reset()V
    .locals 2

    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kint/kintframeworkaosaar/DelayTimer;->_startTime:J

    .line 23
    return-void
.end method
