.class public Lcom/kint/kintframeworkaosaar/Frame;
.super Ljava/lang/Object;
.source "Frame.java"


# instance fields
.field private buffer:[B

.field private flip:Z

.field private format:I


# direct methods
.method public constructor <init>([BZI)V
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "flip"    # Z
    .param p3, "format"    # I

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/Frame;->flip:Z

    .line 14
    const/16 v0, 0x11

    iput v0, p0, Lcom/kint/kintframeworkaosaar/Frame;->format:I

    .line 18
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Frame;->buffer:[B

    .line 19
    iput-boolean p2, p0, Lcom/kint/kintframeworkaosaar/Frame;->flip:Z

    .line 20
    iput p3, p0, Lcom/kint/kintframeworkaosaar/Frame;->format:I

    .line 21
    return-void
.end method


# virtual methods
.method public getBuffer()[B
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Frame;->buffer:[B

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Frame;->format:I

    return v0
.end method

.method public isFlip()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/Frame;->flip:Z

    return v0
.end method

.method public setBuffer([B)V
    .locals 0
    .param p1, "buffer"    # [B

    .line 28
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Frame;->buffer:[B

    .line 29
    return-void
.end method

.method public setFlip(Z)V
    .locals 0
    .param p1, "flip"    # Z

    .line 36
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/Frame;->flip:Z

    .line 37
    return-void
.end method

.method public setFormat(I)V
    .locals 0
    .param p1, "format"    # I

    .line 44
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Frame;->format:I

    .line 45
    return-void
.end method
