.class Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;
.super Ljava/lang/Object;
.source "SrsFlvMuxer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->start(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

.field final synthetic val$rtmpUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 166
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->val$rtmpUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 169
    const/4 v0, -0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 170
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->val$rtmpUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$000(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$100(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    move-result-object v0

    invoke-interface {v0}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionSuccessRtmp()V

    .line 174
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_5

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$200(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 177
    .local v0, "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_sequenceHeader()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_video()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 180
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$300(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    goto :goto_1

    .line 181
    :cond_1
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_audio()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 182
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$502(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 183
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    goto :goto_1

    .line 186
    :cond_2
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_video()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$300(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 187
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    goto :goto_1

    .line 188
    :cond_3
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_audio()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 189
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 192
    .end local v0    # "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$600(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 194
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_4
    :goto_1
    goto :goto_0

    .line 196
    :cond_5
    return-void
.end method
