.class public Lcom/kint/kintframeworkaosaar/Abort;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "Abort.java"


# instance fields
.field private chunkStreamId:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "chunkStreamId"    # I

    .line 24
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_CHUNK_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 26
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Abort;->chunkStreamId:I

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 20
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChunkStreamId()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Abort;->chunkStreamId:I

    return v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 42
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/Abort;->chunkStreamId:I

    .line 43
    return-void
.end method

.method public setChunkStreamId(I)V
    .locals 0
    .param p1, "chunkStreamId"    # I

    .line 36
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Abort;->chunkStreamId:I

    .line 37
    return-void
.end method

.method protected size()I
    .locals 1

    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 57
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Abort;->chunkStreamId:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 58
    return-void
.end method
