.class public final Lcom/kint/kintframeworkaosaar/SrsAllocator;
.super Ljava/lang/Object;
.source "SrsAllocator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    }
.end annotation


# instance fields
.field private availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

.field private volatile availableSentinel:I

.field private final individualAllocationSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "individualAllocationSize"    # I

    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator;-><init>(II)V

    .line 72
    return-void
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "individualAllocationSize"    # I
    .param p2, "initialAllocationCount"    # I

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->individualAllocationSize:I

    .line 83
    add-int/lit8 v0, p2, 0xa

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    .line 84
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 85
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    if-ge v0, v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    new-instance v2, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-direct {v2, p0, p1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;-><init>(Lcom/kint/kintframeworkaosaar/SrsAllocator;I)V

    aput-object v2, v1, v0

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized allocate(I)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    .locals 4
    .param p1, "size"    # I

    monitor-enter p0

    .line 91
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    if-ge v0, v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size()I

    move-result v1

    if-lt v1, p1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    aget-object v1, v1, v0

    .line 94
    .local v1, "ret":Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    const/4 v3, 0x0

    aput-object v3, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-object v1

    .line 91
    .end local v1    # "ret":Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "i":I
    :cond_1
    :try_start_1
    new-instance v0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->individualAllocationSize:I

    if-le p1, v1, :cond_2

    move v1, p1

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->individualAllocationSize:I

    :goto_1
    invoke-direct {v0, p0, v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;-><init>(Lcom/kint/kintframeworkaosaar/SrsAllocator;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 90
    .end local p1    # "size":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized release(Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V
    .locals 3
    .param p1, "allocation"    # Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    monitor-enter p0

    .line 103
    :try_start_0
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->clear()V

    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    if-ge v0, v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    aput-object p1, v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 105
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    .end local v0    # "i":I
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 113
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableAllocations:[Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator;->availableSentinel:I

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 102
    .end local p1    # "allocation":Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
