.class Lcom/kint/kintframeworkaosaar/VideoEncoder$3;
.super Ljava/lang/Object;
.source "VideoEncoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromSurface()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 387
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 390
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_3

    .line 391
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 393
    .local v0, "outputBuffers":[Ljava/nio/ByteBuffer;
    :goto_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v2

    const-wide/16 v3, 0x2710

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    .line 394
    .local v1, "outBufferIndex":I
    const/4 v2, -0x2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    .line 395
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v2

    .line 396
    .local v2, "mediaFormat":Landroid/media/MediaFormat;
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onVideoFormat(Landroid/media/MediaFormat;)V

    .line 397
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v4

    const-string v5, "csd-0"

    invoke-virtual {v2, v5}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v5

    const-string v6, "csd-1"

    .line 398
    invoke-virtual {v2, v6}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 397
    invoke-interface {v4, v5, v6}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 399
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4, v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1402(Lcom/kint/kintframeworkaosaar/VideoEncoder;Z)Z

    .line 400
    .end local v2    # "mediaFormat":Landroid/media/MediaFormat;
    goto :goto_2

    :cond_0
    if-ltz v1, :cond_2

    .line 402
    aget-object v2, v0, v1

    .line 403
    .local v2, "bb":Ljava/nio/ByteBuffer;
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v4

    iget v4, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_1

    .line 404
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 405
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 406
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v5

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v6}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v6

    iget v6, v6, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v4, v5, v6}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1500(Lcom/kint/kintframeworkaosaar/VideoEncoder;Ljava/nio/ByteBuffer;I)Landroid/util/Pair;

    move-result-object v4

    .line 407
    .local v4, "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    if-eqz v4, :cond_1

    .line 408
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v5

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/nio/ByteBuffer;

    iget-object v7, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/nio/ByteBuffer;

    invoke-interface {v5, v6, v7}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 409
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v5, v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1402(Lcom/kint/kintframeworkaosaar/VideoEncoder;Z)Z

    .line 413
    .end local v4    # "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    :cond_1
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v6}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1600(Lcom/kint/kintframeworkaosaar/VideoEncoder;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    iput-wide v4, v3, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 414
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;

    move-result-object v3

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/kint/kintframeworkaosaar/GetH264Data;->getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 415
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 419
    .end local v1    # "outBufferIndex":I
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    :goto_2
    goto/16 :goto_1

    .line 420
    .end local v0    # "outputBuffers":[Ljava/nio/ByteBuffer;
    :cond_2
    goto/16 :goto_0

    .line 421
    :cond_3
    return-void
.end method
