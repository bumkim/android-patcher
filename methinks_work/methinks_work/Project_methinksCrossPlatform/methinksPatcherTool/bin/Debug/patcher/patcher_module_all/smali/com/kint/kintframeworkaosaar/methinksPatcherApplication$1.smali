.class Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;
.super Landroid/content/BroadcastReceiver;
.source "methinksPatcherApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

.field final synthetic val$intentFilter:Landroid/content/IntentFilter;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/content/IntentFilter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 523
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;->val$intentFilter:Landroid/content/IntentFilter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 526
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onCreate] : Screen On"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 540
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onCreate] : Screen Off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 542
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 544
    const-string v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_2

    const/4 v1, 0x1

    nop

    :cond_2
    sput-boolean v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->IsEarphoneOn:Z

    .line 545
    sget-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->IsEarphoneOn:Z

    if-eqz v0, :cond_3

    .line 547
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onCreate] : Earphone is plugged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 551
    :cond_3
    const-string v0, "methinksPatcherApp."

    const-string v1, "[onCreate] : Earphone is unPlugged"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    :cond_4
    :goto_0
    return-void
.end method
