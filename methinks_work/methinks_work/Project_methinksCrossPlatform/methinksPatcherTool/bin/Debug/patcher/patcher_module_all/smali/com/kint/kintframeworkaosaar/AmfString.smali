.class public Lcom/kint/kintframeworkaosaar/AmfString;
.super Ljava/lang/Object;
.source "AmfString.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/AmfData;


# static fields
.field private static final TAG:Ljava/lang/String; = "AmfString"


# instance fields
.field private key:Z

.field private size:I

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;-><init>(Ljava/lang/String;Z)V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "isKey"    # Z

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    .line 28
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    .line 29
    iput-boolean p2, p0, Lcom/kint/kintframeworkaosaar/AmfString;->key:Z

    .line 30
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isKey"    # Z

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    .line 37
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->key:Z

    .line 38
    return-void
.end method

.method public static readStringFrom(Ljava/io/InputStream;Z)Ljava/lang/String;
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "isKey"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 82
    if-nez p1, :cond_0

    .line 84
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    .line 86
    :cond_0
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt16(Ljava/io/InputStream;)I

    move-result v0

    .line 88
    .local v0, "length":I
    new-array v1, v0, [B

    .line 89
    .local v1, "byteValue":[B
    invoke-static {p0, v1}, Lcom/kint/kintframeworkaosaar/Util;->readBytesUntilFull(Ljava/io/InputStream;[B)V

    .line 90
    new-instance v2, Ljava/lang/String;

    const-string v3, "ASCII"

    invoke-direct {v2, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v2
.end method

.method public static sizeOf(Ljava/lang/String;Z)I
    .locals 3
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "isKey"    # Z

    .line 123
    xor-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x2

    :try_start_0
    const-string v1, "ASCII"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    .line 124
    .local v0, "size":I
    return v0

    .line 125
    .end local v0    # "size":I
    :catch_0
    move-exception v0

    .line 126
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v1, "AmfString"

    const-string v2, "AmfString.SizeOf(): caught exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static writeStringTo(Ljava/io/OutputStream;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "isKey"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 96
    const-string v0, "ASCII"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 98
    .local v0, "byteValue":[B
    if-nez p2, :cond_0

    .line 99
    sget-object v1, Lcom/kint/kintframeworkaosaar/AmfType;->STRING:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 102
    :cond_0
    array-length v1, v0

    invoke-static {p0, v1}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt16(Ljava/io/OutputStream;I)V

    .line 104
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 105
    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 111
    :try_start_0
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/AmfString;->isKey()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    const-string v2, "ASCII"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v1, "AmfString"

    const-string v2, "AmfString.getSize(): caught exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 114
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 117
    .end local v0    # "ex":Ljava/io/UnsupportedEncodingException;
    :cond_0
    :goto_0
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    return-object v0
.end method

.method public isKey()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->key:Z

    return v0
.end method

.method public readFrom(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 73
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt16(Ljava/io/InputStream;)I

    move-result v0

    .line 74
    .local v0, "length":I
    add-int/lit8 v1, v0, 0x3

    iput v1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->size:I

    .line 76
    new-array v1, v0, [B

    .line 77
    .local v1, "byteValue":[B
    invoke-static {p1, v1}, Lcom/kint/kintframeworkaosaar/Util;->readBytesUntilFull(Ljava/io/InputStream;[B)V

    .line 78
    new-instance v2, Ljava/lang/String;

    const-string v3, "ASCII"

    invoke-direct {v2, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setKey(Z)V
    .locals 0
    .param p1, "key"    # Z

    .line 53
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->key:Z

    .line 54
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfString;->value:Ljava/lang/String;

    const-string v1, "ASCII"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 61
    .local v0, "byteValue":[B
    iget-boolean v1, p0, Lcom/kint/kintframeworkaosaar/AmfString;->key:Z

    if-nez v1, :cond_0

    .line 62
    sget-object v1, Lcom/kint/kintframeworkaosaar/AmfType;->STRING:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 65
    :cond_0
    array-length v1, v0

    invoke-static {p1, v1}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt16(Ljava/io/OutputStream;I)V

    .line 67
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 68
    return-void
.end method
