.class public Lcom/kint/kintframeworkaosaar/MessagePackLog;
.super Lcom/kint/kintframeworkaosaar/MessagePack;
.source "MessagePackLog.java"


# static fields
.field public static SessionTimeCheck:J

.field public static SessionTimeCheckStatus:I

.field private static __messagePackLog:Lcom/kint/kintframeworkaosaar/MessagePackLog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackLog;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/MessagePackLog;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->__messagePackLog:Lcom/kint/kintframeworkaosaar/MessagePackLog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;-><init>()V

    .line 11
    const-string v0, "log"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_value:Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_key:I

    .line 13
    return-void
.end method

.method public static GetMessagePackLog()Lcom/kint/kintframeworkaosaar/MessagePackLog;
    .locals 1

    .line 31
    sget-object v0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->__messagePackLog:Lcom/kint/kintframeworkaosaar/MessagePackLog;

    return-object v0
.end method


# virtual methods
.method public BuildRequest()Ljava/lang/String;
    .locals 1

    .line 25
    const-string v0, ""

    return-object v0
.end method

.method public Request()V
    .locals 1

    .line 18
    const-string v0, "log"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_value:Ljava/lang/String;

    .line 19
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackLog;->_key:I

    .line 20
    return-void
.end method
