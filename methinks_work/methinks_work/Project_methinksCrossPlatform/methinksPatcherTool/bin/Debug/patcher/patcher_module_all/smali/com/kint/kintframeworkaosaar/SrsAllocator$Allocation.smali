.class public Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
.super Ljava/lang/Object;
.source "SrsAllocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/SrsAllocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Allocation"
.end annotation


# instance fields
.field private data:[B

.field private size:I

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/SrsAllocator;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/SrsAllocator;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/SrsAllocator;
    .param p2, "size"    # I

    .line 13
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->this$0:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-array v0, p2, [B

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->data:[B

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    .line 16
    return-void
.end method


# virtual methods
.method public appendOffset(I)V
    .locals 1
    .param p1, "offset"    # I

    .line 27
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    .line 28
    return-void
.end method

.method public array()[B
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->data:[B

    return-object v0
.end method

.method public clear()V
    .locals 1

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    .line 32
    return-void
.end method

.method public put(B)V
    .locals 3
    .param p1, "b"    # B

    .line 35
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->data:[B

    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    aput-byte p1, v0, v1

    .line 36
    return-void
.end method

.method public put(BI)V
    .locals 2
    .param p1, "b"    # B
    .param p2, "pos"    # I

    .line 39
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->data:[B

    add-int/lit8 v1, p2, 0x1

    .local v1, "pos":I
    aput-byte p1, v0, p2

    .line 40
    .end local p2    # "pos":I
    iget p2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    if-le v1, p2, :cond_0

    move p2, v1

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    :goto_0
    iput p2, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    .line 41
    return-void
.end method

.method public put(I)V
    .locals 1
    .param p1, "i"    # I

    .line 49
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 50
    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 51
    ushr-int/lit8 v0, p1, 0x10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 52
    ushr-int/lit8 v0, p1, 0x18

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 53
    return-void
.end method

.method public put(S)V
    .locals 1
    .param p1, "s"    # S

    .line 44
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 45
    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 46
    return-void
.end method

.method public put([B)V
    .locals 4
    .param p1, "bs"    # [B

    .line 56
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->data:[B

    iget v1, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    array-length v2, p1

    const/4 v3, 0x0

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    .line 58
    return-void
.end method

.method public size()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size:I

    return v0
.end method
