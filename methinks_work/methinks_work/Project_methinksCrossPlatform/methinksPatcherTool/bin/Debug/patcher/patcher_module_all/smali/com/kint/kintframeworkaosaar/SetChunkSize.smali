.class public Lcom/kint/kintframeworkaosaar/SetChunkSize;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "SetChunkSize.java"


# instance fields
.field private chunkSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "chunkSize"    # I

    .line 24
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_CHUNK_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 26
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SetChunkSize;->chunkSize:I

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 20
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChunkSize()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SetChunkSize;->chunkSize:I

    return v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 40
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SetChunkSize;->chunkSize:I

    .line 41
    return-void
.end method

.method public setChunkSize(I)V
    .locals 0
    .param p1, "chunkSize"    # I

    .line 34
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SetChunkSize;->chunkSize:I

    .line 35
    return-void
.end method

.method protected size()I
    .locals 1

    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 45
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SetChunkSize;->chunkSize:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 46
    return-void
.end method
