.class public Lcom/kint/kintframeworkaosaar/Acknowledgement;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "Acknowledgement.java"


# instance fields
.field private sequenceNumber:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "numBytesReadThusFar"    # I

    .line 31
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ACKNOWLEDGEMENT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 34
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 27
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAcknowledgementWindowSize()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    return v0
.end method

.method public getSequenceNumber()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    return v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 53
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    .line 54
    return-void
.end method

.method public setSequenceNumber(I)V
    .locals 0
    .param p1, "numBytesRead"    # I

    .line 48
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    .line 49
    return-void
.end method

.method protected size()I
    .locals 1

    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RTMP Acknowledgment (sequence number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 58
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Acknowledgement;->sequenceNumber:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 59
    return-void
.end method
