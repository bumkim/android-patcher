.class public Lcom/kint/kintframeworkaosaar/MessagePack;
.super Ljava/lang/Object;
.source "MessagePack.java"


# static fields
.field public static final CONNECTION_ANSWER:Ljava/lang/String; = "answer"

.field public static final CONNECTION_CAMERA_CAPTURE:Ljava/lang/String; = "picCapture"

.field public static final CONNECTION_CAMERA_CAPTURE_INFO:Ljava/lang/String; = "picCaptureInfo"

.field public static final CONNECTION_CAPTURE:Ljava/lang/String; = "capture"

.field public static final CONNECTION_CAPTURE_DETAIL:Ljava/lang/String; = "captureDetail"

.field public static final CONNECTION_EVENT:Ljava/lang/String; = "event"

.field public static final CONNECTION_FRONT_CAM_SHOT:Ljava/lang/String; = "frontStillShot"

.field public static final CONNECTION_LOG:Ljava/lang/String; = "log"

.field public static final CONNECTION_LOGIN:Ljava/lang/String; = "login"

.field public static final CONNECTION_NOTICE:Ljava/lang/String; = "notice"

.field public static final CONNECTION_QUESTION:Ljava/lang/String; = "question"

.field public static final PREPARE_LOGIN:Ljava/lang/String; = "prepare_login"

.field public static final PREPARE_QUESTION:Ljava/lang/String; = "prepare_question"


# instance fields
.field protected _key:I

.field protected _value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetDeviceInfoInLog(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;)Ljava/lang/String;
    .locals 38
    .param p0, "audioManager"    # Landroid/media/AudioManager;
    .param p1, "connectivityManager"    # Landroid/net/ConnectivityManager;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p3, "screenBrightness"    # I
    .param p4, "windowManager"    # Landroid/view/WindowManager;
    .param p5, "context"    # Landroid/content/Context;

    .line 423
    move-object/from16 v1, p0

    move-object/from16 v2, p5

    const-string v0, ""

    move-object v3, v0

    .line 427
    .local v3, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 430
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const/4 v4, 0x1

    .line 431
    .local v4, "audioStreamType":I
    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    .line 432
    .local v5, "currentStreamVolume":I
    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v6

    .line 433
    .local v6, "maxStreamVolume":I
    mul-int/lit8 v7, v5, 0x64

    div-int/2addr v7, v6

    .line 435
    .local v7, "volumeLevel":I
    const-string v8, "Built-in speaker on an Android device"

    .line 436
    .local v8, "soundOutputDevice":Ljava/lang/String;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v10, 0x17

    const/4 v12, 0x2

    const/4 v13, 0x1

    if-ge v9, v10, :cond_3

    .line 438
    :try_start_1
    sget-boolean v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->IsEarphoneOn:Z

    if-ne v13, v9, :cond_0

    .line 440
    const-string v9, "Headphones"

    move-object v8, v9

    goto :goto_2

    .line 444
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 446
    const-string v9, "BluetoothA2DP"

    move-object v8, v9

    .line 449
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 451
    const-string v9, "Speakerphone"

    move-object v8, v9

    .line 454
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 456
    const-string v9, "BluetoothSCO"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v8, v9

    goto :goto_2

    .line 613
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "audioStreamType":I
    .end local v5    # "currentStreamVolume":I
    .end local v6    # "maxStreamVolume":I
    .end local v7    # "volumeLevel":I
    .end local v8    # "soundOutputDevice":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object/from16 v20, v3

    goto/16 :goto_7

    .line 464
    .restart local v0    # "jsonObject":Lorg/json/JSONObject;
    .restart local v4    # "audioStreamType":I
    .restart local v5    # "currentStreamVolume":I
    .restart local v6    # "maxStreamVolume":I
    .restart local v7    # "volumeLevel":I
    .restart local v8    # "soundOutputDevice":Ljava/lang/String;
    :cond_3
    :try_start_2
    invoke-virtual {v1, v12}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v9

    .line 465
    .local v9, "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    move-object v10, v8

    const/4 v8, 0x0

    .local v8, "countIndex":I
    .local v10, "soundOutputDevice":Ljava/lang/String;
    :goto_0
    array-length v14, v9
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    if-ge v8, v14, :cond_4

    .line 467
    :try_start_3
    aget-object v14, v9, v8

    .line 468
    .local v14, "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    invoke-virtual {v14}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v15

    packed-switch v15, :pswitch_data_0

    .end local v14    # "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    :pswitch_0
    goto :goto_1

    .line 478
    .restart local v14    # "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    :pswitch_1
    const-string v15, "BluetoothA2DP"

    move-object v10, v15

    .line 480
    goto :goto_1

    .line 484
    :pswitch_2
    const-string v15, "BluetoothSCO"

    move-object v10, v15

    .line 486
    goto :goto_1

    .line 472
    :pswitch_3
    const-string v15, "Headphones"

    move-object v10, v15

    .line 474
    goto :goto_1

    .line 496
    :pswitch_4
    const-string v15, "Built-in speaker on an Android device"

    move-object v10, v15

    goto :goto_1

    .line 490
    :pswitch_5
    const-string v15, "Built in Earpiece"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v10, v15

    .line 492
    nop

    .line 465
    .end local v14    # "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 502
    .end local v8    # "countIndex":I
    .end local v9    # "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    :cond_4
    move-object v8, v10

    .end local v10    # "soundOutputDevice":Ljava/lang/String;
    .local v8, "soundOutputDevice":Ljava/lang/String;
    :cond_5
    :goto_2
    :try_start_4
    const-string v9, "Sound Output Device"

    invoke-virtual {v0, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 505
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v9

    .line 506
    .local v9, "info":Ljava/lang/Runtime;
    invoke-virtual {v9}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v14

    .line 507
    .local v14, "freeSize":J
    invoke-virtual {v9}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v16
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_2

    move-wide/from16 v18, v16

    .line 508
    .local v18, "totalSize":J
    const/4 v10, 0x0

    move-object/from16 v20, v3

    move/from16 v21, v4

    move-wide/from16 v11, v18

    .end local v3    # "jsonString":Ljava/lang/String;
    .end local v4    # "audioStreamType":I
    .end local v18    # "totalSize":J
    .local v11, "totalSize":J
    .local v20, "jsonString":Ljava/lang/String;
    .local v21, "audioStreamType":I
    sub-long v3, v11, v14

    .line 510
    .local v3, "usedSize":J
    :try_start_5
    const-string v10, "Memory Usage"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v1, v3

    move-wide/from16 v22, v3

    .end local v3    # "usedSize":J
    .local v22, "usedSize":J
    long-to-float v3, v11

    div-float/2addr v1, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float v1, v1, v3

    float-to-int v1, v1

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v10, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 511
    const/4 v1, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 512
    .local v1, "batteryStatus":Landroid/content/Intent;
    const-string v4, "plugged"

    const/4 v10, -0x1

    invoke-virtual {v1, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 514
    .local v4, "plugged":I
    const-string v13, "plugged out"

    .line 515
    .local v13, "pluggedStatus":Ljava/lang/String;
    const/4 v3, 0x1

    if-eq v4, v3, :cond_6

    const/4 v3, 0x2

    if-eq v4, v3, :cond_6

    const/4 v3, 0x4

    if-ne v4, v3, :cond_7

    .line 516
    :cond_6
    const-string v3, "plugged in"

    move-object v13, v3

    .line 518
    :cond_7
    const-string v3, "level"

    invoke-virtual {v1, v3, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 519
    .local v3, "level":I
    const-string v10, "scale"

    move/from16 v25, v4

    const/4 v4, -0x1

    .end local v4    # "plugged":I
    .local v25, "plugged":I
    invoke-virtual {v1, v10, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 521
    .local v10, "scale":I
    int-to-float v4, v3

    move-object/from16 v26, v1

    .end local v1    # "batteryStatus":Landroid/content/Intent;
    .local v26, "batteryStatus":Landroid/content/Intent;
    int-to-float v1, v10

    div-float/2addr v4, v1

    .line 522
    .local v4, "batteryPct":F
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float v1, v1, v4

    float-to-int v1, v1

    .line 524
    .local v1, "batteryPercent":I
    move/from16 v27, v3

    .end local v3    # "level":I
    .local v27, "level":I
    const-string v3, "Battery Status"

    move/from16 v28, v4

    .end local v4    # "batteryPct":F
    .local v28, "batteryPct":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v29, v5

    .end local v5    # "currentStreamVolume":I
    .local v29, "currentStreamVolume":I
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 525
    const-string v3, "OS Version"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 526
    const-string v3, "Volume Level(db)"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 528
    invoke-static/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v3

    .line 529
    .local v3, "connectivity":Ljava/lang/String;
    const-string v4, "0.587MB/s"

    .line 530
    .local v4, "currrentConnectionSpeed":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    move/from16 v30, v1

    .end local v1    # "batteryPercent":I
    .local v30, "batteryPercent":I
    const/16 v1, 0x674

    if-eq v5, v1, :cond_b

    const v1, 0x127bd

    if-eq v5, v1, :cond_a

    const v1, 0x2065bd

    if-eq v5, v1, :cond_9

    const v1, 0x292335

    if-eq v5, v1, :cond_8

    goto :goto_3

    :cond_8
    const-string v1, "Wifi"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v24, 0x0

    goto :goto_4

    :cond_9
    const-string v1, "EDGE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v24, 0x1

    goto :goto_4

    :cond_a
    const-string v1, "LTE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x3

    const/16 v24, 0x3

    goto :goto_4

    :cond_b
    const-string v1, "3G"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v24, 0x2

    goto :goto_4

    :cond_c
    :goto_3
    const/16 v24, -0x1

    :goto_4
    packed-switch v24, :pswitch_data_1

    goto :goto_5

    .line 542
    :pswitch_6
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetMobileSpeed(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_5

    .line 534
    :pswitch_7
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetWifiSpeed(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 536
    nop

    .line 547
    :goto_5
    const-string v1, "Current Connection Speed"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 548
    const-string v1, "Device"

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 549
    const-string v1, "Free Space"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalAvailableMemory()J

    move-result-wide v16

    const-wide/32 v18, 0x100000

    move/from16 v31, v6

    move/from16 v32, v7

    .end local v6    # "maxStreamVolume":I
    .end local v7    # "volumeLevel":I
    .local v31, "maxStreamVolume":I
    .local v32, "volumeLevel":I
    div-long v6, v16, v18

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " MB / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalStorageAllMemory()J

    move-result-wide v6

    div-long v6, v6, v18

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " MB "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 552
    const/16 v1, 0xa

    .line 553
    .local v1, "checkCount":I
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 554
    .local v5, "latencyJsonArray":Lorg/json/JSONArray;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 555
    .local v6, "usWestJsonObject":Lorg/json/JSONObject;
    const-string v7, "13.52.0.0"

    invoke-static {v7, v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v7

    .line 556
    .local v7, "averagePingValue":F
    const-string v2, "US_WEST"

    move-object/from16 v33, v8

    move-object/from16 v34, v9

    .end local v8    # "soundOutputDevice":Ljava/lang/String;
    .end local v9    # "info":Ljava/lang/Runtime;
    .local v33, "soundOutputDevice":Ljava/lang/String;
    .local v34, "info":Ljava/lang/Runtime;
    float-to-double v8, v7

    invoke-virtual {v6, v2, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 557
    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 559
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 560
    .local v2, "usEastJsonObject":Lorg/json/JSONObject;
    const-string v8, "23.23.255.255"

    invoke-static {v8, v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v8

    move v7, v8

    .line 561
    const-string v8, "US_EAST"

    move/from16 v35, v10

    .end local v10    # "scale":I
    .local v35, "scale":I
    float-to-double v9, v7

    invoke-virtual {v2, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 562
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 564
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 565
    .local v8, "seoulJsonObject":Lorg/json/JSONObject;
    const-string v9, "13.124.63.251"

    invoke-static {v9, v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v9

    move v7, v9

    .line 566
    const-string v9, "SEOUL"

    move/from16 v36, v1

    move-object/from16 v37, v2

    .end local v1    # "checkCount":I
    .end local v2    # "usEastJsonObject":Lorg/json/JSONObject;
    .local v36, "checkCount":I
    .local v37, "usEastJsonObject":Lorg/json/JSONObject;
    float-to-double v1, v7

    invoke-virtual {v8, v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 567
    invoke-virtual {v5, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 569
    const-string v1, "Current Latency"

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 570
    const-string v1, "Connectivity"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 571
    mul-int/lit8 v2, p3, 0x64

    div-int/lit16 v2, v2, 0xff

    .line 572
    .local v2, "screenBrightnessLevel":I
    const-string v9, "Screen Brightness"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 575
    const-string v1, ""

    .line 576
    .local v1, "screenOrientation":Ljava/lang/String;
    invoke-interface/range {p4 .. p4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getRotation()I

    move-result v9

    packed-switch v9, :pswitch_data_2

    goto :goto_6

    .line 602
    :pswitch_8
    const-string v9, "Device oriented horizontally, home button on the left"

    move-object v1, v9

    goto :goto_6

    .line 595
    :pswitch_9
    const-string v9, "Device oriented vertically, home button on the top"

    move-object v1, v9

    .line 597
    goto :goto_6

    .line 588
    :pswitch_a
    const-string v9, "Device oriented horizontally, home button on the right"

    move-object v1, v9

    .line 590
    goto :goto_6

    .line 581
    :pswitch_b
    const-string v9, "Device oriented vertically, home button on the bottom"

    move-object v1, v9

    .line 583
    nop

    .line 607
    :goto_6
    const-string v9, "Screen Orientation"

    invoke-virtual {v0, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 608
    const-string v9, "Carrier"

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 610
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    move-object v3, v9

    .line 616
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v1    # "screenOrientation":Ljava/lang/String;
    .end local v2    # "screenBrightnessLevel":I
    .end local v4    # "currrentConnectionSpeed":Ljava/lang/String;
    .end local v5    # "latencyJsonArray":Lorg/json/JSONArray;
    .end local v6    # "usWestJsonObject":Lorg/json/JSONObject;
    .end local v7    # "averagePingValue":F
    .end local v8    # "seoulJsonObject":Lorg/json/JSONObject;
    .end local v11    # "totalSize":J
    .end local v13    # "pluggedStatus":Ljava/lang/String;
    .end local v14    # "freeSize":J
    .end local v20    # "jsonString":Ljava/lang/String;
    .end local v21    # "audioStreamType":I
    .end local v22    # "usedSize":J
    .end local v25    # "plugged":I
    .end local v26    # "batteryStatus":Landroid/content/Intent;
    .end local v27    # "level":I
    .end local v28    # "batteryPct":F
    .end local v29    # "currentStreamVolume":I
    .end local v30    # "batteryPercent":I
    .end local v31    # "maxStreamVolume":I
    .end local v32    # "volumeLevel":I
    .end local v33    # "soundOutputDevice":Ljava/lang/String;
    .end local v34    # "info":Ljava/lang/Runtime;
    .end local v35    # "scale":I
    .end local v36    # "checkCount":I
    .end local v37    # "usEastJsonObject":Lorg/json/JSONObject;
    .local v3, "jsonString":Ljava/lang/String;
    goto :goto_8

    .line 613
    .end local v3    # "jsonString":Ljava/lang/String;
    .restart local v20    # "jsonString":Ljava/lang/String;
    :catch_1
    move-exception v0

    goto :goto_7

    .end local v20    # "jsonString":Ljava/lang/String;
    .restart local v3    # "jsonString":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v20, v3

    .line 615
    .end local v3    # "jsonString":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v20    # "jsonString":Ljava/lang/String;
    :goto_7
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 618
    move-object/from16 v3, v20

    .end local v0    # "e":Lorg/json/JSONException;
    .end local v20    # "jsonString":Ljava/lang/String;
    .restart local v3    # "jsonString":Ljava/lang/String;
    :goto_8
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public static GetDeviceInfoInLogin(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;I)Ljava/lang/String;
    .locals 19
    .param p0, "audioManager"    # Landroid/media/AudioManager;
    .param p1, "connectivityManager"    # Landroid/net/ConnectivityManager;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p3, "screenBrightness"    # I

    .line 192
    move-object/from16 v1, p0

    const-string v0, ""

    move-object v2, v0

    .line 195
    .local v2, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 196
    .local v0, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 198
    .local v3, "jsonDeviceInfoArray":Lorg/json/JSONArray;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 199
    .local v4, "deviceJsonObject":Lorg/json/JSONObject;
    const-string v5, "Device"

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 200
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 202
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 203
    .local v5, "osVersionJsonObject":Lorg/json/JSONObject;
    const-string v6, "OS Version"

    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 204
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 206
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 207
    .local v6, "carrierJsonObject":Lorg/json/JSONObject;
    const-string v7, "Carrier"

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 208
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 210
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 211
    .local v7, "connectionTypeJsonObject":Lorg/json/JSONObject;
    const-string v8, "Connectivity"

    invoke-static/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 212
    invoke-virtual {v3, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    .line 214
    const/4 v8, 0x0

    .line 215
    .local v8, "sendPing":Z
    const/4 v9, 0x1

    if-ne v9, v8, :cond_0

    .line 217
    const/16 v9, 0xa

    .line 218
    .local v9, "checkCount":I
    :try_start_1
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 219
    .local v10, "latencyJsonArray":Lorg/json/JSONArray;
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 220
    .local v11, "usWestJsonObject":Lorg/json/JSONObject;
    const-string v12, "13.52.0.0"

    invoke-static {v12, v9}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v12

    .line 221
    .local v12, "averagePingValue":F
    const-string v13, "US_WEST"

    float-to-double v14, v12

    invoke-virtual {v11, v13, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 222
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 224
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 225
    .local v13, "usEastJsonObject":Lorg/json/JSONObject;
    const-string v14, "23.23.255.255"

    invoke-static {v14, v9}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v14

    move v12, v14

    .line 226
    const-string v14, "US_EAST"

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    .end local v4    # "deviceJsonObject":Lorg/json/JSONObject;
    .end local v5    # "osVersionJsonObject":Lorg/json/JSONObject;
    .local v16, "deviceJsonObject":Lorg/json/JSONObject;
    .local v17, "osVersionJsonObject":Lorg/json/JSONObject;
    float-to-double v4, v12

    invoke-virtual {v13, v14, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 227
    invoke-virtual {v10, v13}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 229
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 230
    .local v4, "seoulJsonObject":Lorg/json/JSONObject;
    const-string v5, "13.124.63.251"

    invoke-static {v5, v9}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverage(Ljava/lang/String;I)F

    move-result v5

    .line 231
    .end local v12    # "averagePingValue":F
    .local v5, "averagePingValue":F
    const-string v12, "SEOUL"

    float-to-double v14, v5

    invoke-virtual {v4, v12, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 232
    invoke-virtual {v10, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 234
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 235
    .local v12, "latencyJsonObject":Lorg/json/JSONObject;
    const-string v14, "Current Latency(ms)"

    invoke-virtual {v12, v14, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 236
    invoke-virtual {v3, v12}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 259
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "jsonDeviceInfoArray":Lorg/json/JSONArray;
    .end local v4    # "seoulJsonObject":Lorg/json/JSONObject;
    .end local v5    # "averagePingValue":F
    .end local v6    # "carrierJsonObject":Lorg/json/JSONObject;
    .end local v7    # "connectionTypeJsonObject":Lorg/json/JSONObject;
    .end local v8    # "sendPing":Z
    .end local v9    # "checkCount":I
    .end local v10    # "latencyJsonArray":Lorg/json/JSONArray;
    .end local v11    # "usWestJsonObject":Lorg/json/JSONObject;
    .end local v12    # "latencyJsonObject":Lorg/json/JSONObject;
    .end local v13    # "usEastJsonObject":Lorg/json/JSONObject;
    .end local v16    # "deviceJsonObject":Lorg/json/JSONObject;
    .end local v17    # "osVersionJsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    move-object/from16 v18, v2

    goto :goto_1

    .line 240
    .restart local v0    # "jsonObject":Lorg/json/JSONObject;
    .restart local v3    # "jsonDeviceInfoArray":Lorg/json/JSONArray;
    .local v4, "deviceJsonObject":Lorg/json/JSONObject;
    .local v5, "osVersionJsonObject":Lorg/json/JSONObject;
    .restart local v6    # "carrierJsonObject":Lorg/json/JSONObject;
    .restart local v7    # "connectionTypeJsonObject":Lorg/json/JSONObject;
    .restart local v8    # "sendPing":Z
    :cond_0
    move-object/from16 v16, v4

    move-object/from16 v17, v5

    .end local v4    # "deviceJsonObject":Lorg/json/JSONObject;
    .end local v5    # "osVersionJsonObject":Lorg/json/JSONObject;
    .restart local v16    # "deviceJsonObject":Lorg/json/JSONObject;
    .restart local v17    # "osVersionJsonObject":Lorg/json/JSONObject;
    :goto_0
    const/4 v4, 0x1

    .line 241
    .local v4, "audioStreamType":I
    :try_start_2
    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    .line 242
    .local v5, "currentStreamVolume":I
    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v9

    .line 243
    .local v9, "maxStreamVolume":I
    mul-int/lit8 v10, v5, 0x64

    div-int/2addr v10, v9

    .line 245
    .local v10, "volumeLevel":I
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 246
    .local v11, "volumeLevelJsonObject":Lorg/json/JSONObject;
    const-string v12, "Volume Level(db)"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v14, "%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    invoke-virtual {v3, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 251
    mul-int/lit8 v13, p3, 0x64

    div-int/lit16 v13, v13, 0xff

    .line 252
    .local v13, "screenBrightnessLevel":I
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 253
    .local v14, "brightnessJsonObject":Lorg/json/JSONObject;
    const-string v15, "Screen Brightness"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v18, v2

    .end local v2    # "jsonString":Ljava/lang/String;
    .local v18, "jsonString":Ljava/lang/String;
    :try_start_3
    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v15, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 254
    invoke-virtual {v3, v14}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 256
    const-string v1, "deviceInfo"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 257
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v1

    .line 262
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "jsonDeviceInfoArray":Lorg/json/JSONArray;
    .end local v4    # "audioStreamType":I
    .end local v5    # "currentStreamVolume":I
    .end local v6    # "carrierJsonObject":Lorg/json/JSONObject;
    .end local v7    # "connectionTypeJsonObject":Lorg/json/JSONObject;
    .end local v8    # "sendPing":Z
    .end local v9    # "maxStreamVolume":I
    .end local v10    # "volumeLevel":I
    .end local v11    # "volumeLevelJsonObject":Lorg/json/JSONObject;
    .end local v13    # "screenBrightnessLevel":I
    .end local v14    # "brightnessJsonObject":Lorg/json/JSONObject;
    .end local v16    # "deviceJsonObject":Lorg/json/JSONObject;
    .end local v17    # "osVersionJsonObject":Lorg/json/JSONObject;
    .end local v18    # "jsonString":Ljava/lang/String;
    .restart local v2    # "jsonString":Ljava/lang/String;
    nop

    .line 264
    move-object/from16 v18, v2

    goto :goto_2

    .line 259
    .end local v2    # "jsonString":Ljava/lang/String;
    .restart local v18    # "jsonString":Ljava/lang/String;
    :catch_1
    move-exception v0

    goto :goto_1

    .end local v18    # "jsonString":Ljava/lang/String;
    .restart local v2    # "jsonString":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v18, v2

    .line 261
    .end local v2    # "jsonString":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v18    # "jsonString":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 264
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_2
    return-object v18
.end method

.method private static _GetMobileSpeed(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "withUnit"    # Z

    .line 316
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 317
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    .line 318
    .local v1, "networkType":I
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getMobileNetworkSpeed(I)F

    move-result v2

    .line 320
    .local v2, "netSpeed":F
    const/4 v3, 0x1

    if-ne v3, p1, :cond_0

    .line 321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getMobileNetworkSpeedUnit(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 322
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static _GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;
    .locals 3
    .param p0, "connectivityManager"    # Landroid/net/ConnectivityManager;

    .line 150
    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 152
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 154
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 155
    const-string v1, "Wifi"

    return-object v1

    .line 156
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_2

    .line 158
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    .line 159
    .local v1, "networkType":I
    packed-switch v1, :pswitch_data_0

    .line 183
    :pswitch_0
    const-string v2, "?"

    return-object v2

    .line 181
    :pswitch_1
    const-string v2, "LTE"

    return-object v2

    .line 177
    :pswitch_2
    const-string v2, "3G"

    return-object v2

    .line 166
    :pswitch_3
    const-string v2, "EDGE"

    return-object v2

    .line 186
    .end local v1    # "networkType":I
    :cond_2
    const-string v1, "?"

    return-object v1

    .line 153
    :cond_3
    :goto_0
    const-string v1, "-"

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static _GetWifiSpeed(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "withUnit"    # Z

    .line 289
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;->getWiFiSpeed(Landroid/content/Context;)F

    move-result v0

    .line 290
    .local v0, "speed":F
    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "Mbps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 292
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static _Ping(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "url"    # Ljava/lang/String;

    .line 87
    const-string v0, ""

    .line 90
    .local v0, "str":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ping -c 1 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 91
    .local v1, "process":Ljava/lang/Process;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 93
    .local v2, "reader":Ljava/io/BufferedReader;
    const/16 v3, 0x1000

    new-array v3, v3, [C

    .line 94
    .local v3, "buffer":[C
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 95
    .local v4, "output":Ljava/lang/StringBuffer;
    const/16 v5, 0x40

    new-array v5, v5, [Ljava/lang/String;

    .line 96
    .local v5, "op":[Ljava/lang/String;
    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/String;

    .line 97
    .local v6, "delay":[Ljava/lang/String;
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->read([C)I

    move-result v7

    move v8, v7

    .local v8, "i":I
    if-lez v7, :cond_0

    .line 98
    const/4 v7, 0x0

    invoke-virtual {v4, v3, v7, v8}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 100
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v9, "\n"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    move-object v5, v7

    .line 101
    const-string v7, "methinksPatcherApp."

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ping : output - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    if-eqz v5, :cond_4

    array-length v7, v5

    const/4 v9, 0x2

    if-le v9, v7, :cond_1

    goto :goto_2

    .line 104
    :cond_1
    const-string v7, "methinksPatcherApp."

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Ping : delay - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v11, 0x1

    aget-object v12, v5, v11

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    aget-object v7, v5, v11

    const-string v10, "time="

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 106
    if-eqz v6, :cond_3

    array-length v7, v6

    if-le v9, v7, :cond_2

    goto :goto_1

    .line 110
    :cond_2
    aget-object v7, v6, v11

    move-object v0, v7

    .line 117
    .end local v1    # "process":Ljava/lang/Process;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "buffer":[C
    .end local v4    # "output":Ljava/lang/StringBuffer;
    .end local v5    # "op":[Ljava/lang/String;
    .end local v6    # "delay":[Ljava/lang/String;
    .end local v8    # "i":I
    goto :goto_3

    .line 107
    .restart local v1    # "process":Ljava/lang/Process;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "buffer":[C
    .restart local v4    # "output":Ljava/lang/StringBuffer;
    .restart local v5    # "op":[Ljava/lang/String;
    .restart local v6    # "delay":[Ljava/lang/String;
    .restart local v8    # "i":I
    :cond_3
    :goto_1
    const-string v7, ""

    return-object v7

    .line 103
    :cond_4
    :goto_2
    const-string v7, ""
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v7

    .line 113
    .end local v1    # "process":Ljava/lang/Process;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "buffer":[C
    .end local v4    # "output":Ljava/lang/StringBuffer;
    .end local v5    # "op":[Ljava/lang/String;
    .end local v6    # "delay":[Ljava/lang/String;
    .end local v8    # "i":I
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 118
    .end local v1    # "e":Ljava/io/IOException;
    :goto_3
    return-object v0
.end method

.method private static _PingAverage(Ljava/lang/String;I)F
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "checkCount"    # I

    .line 124
    const-string v0, ""

    .line 125
    .local v0, "ping":Ljava/lang/String;
    const/4 v1, 0x0

    .line 126
    .local v1, "pingTotal":F
    const/4 v2, 0x0

    move v3, v1

    move-object v1, v0

    const/4 v0, 0x0

    .line 128
    .local v0, "countIndex":I
    .local v1, "ping":Ljava/lang/String;
    .local v3, "pingTotal":F
    :cond_0
    :goto_0
    if-ge v0, p1, :cond_2

    .line 130
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;->_Ping(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    const-string v4, ""

    if-eq v4, v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 132
    goto :goto_0

    .line 134
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 136
    const-string v4, "methinksPatcherApp."

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ping : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 139
    .local v4, "split":[Ljava/lang/String;
    aget-object v5, v4, v2

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    add-float/2addr v3, v5

    .line 140
    .end local v4    # "split":[Ljava/lang/String;
    goto :goto_0

    .line 142
    :cond_2
    int-to-float v4, p1

    div-float v4, v3, v4

    .line 143
    .local v4, "pingValue":F
    const-string v5, "%.1f"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 144
    .end local v4    # "pingValue":F
    .local v2, "pingValue":F
    return v2
.end method

.method private static checkInternalAvailableMemory()J
    .locals 7

    .line 279
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 280
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 281
    .local v1, "blockSize":J
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v3

    .line 282
    .local v3, "availableBlocks":J
    mul-long v5, v1, v3

    return-wide v5
.end method

.method private static checkInternalStorageAllMemory()J
    .locals 7

    .line 270
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 271
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 272
    .local v1, "blockSize":J
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v3

    .line 273
    .local v3, "totalBlocks":J
    mul-long v5, v1, v3

    return-wide v5
.end method

.method private static getMobileNetworkSpeed(I)F
    .locals 1
    .param p0, "networkType"    # I

    .line 355
    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    .line 393
    return v0

    .line 391
    :pswitch_0
    return v0

    .line 387
    :pswitch_1
    return v0

    .line 359
    :pswitch_2
    return v0

    .line 385
    :pswitch_3
    const/high16 v0, 0x42280000    # 42.0f

    return v0

    .line 383
    :pswitch_4
    const v0, 0x3fa47ae1    # 1.285f

    return v0

    .line 389
    :pswitch_5
    const/high16 v0, 0x42c80000    # 100.0f

    return v0

    .line 381
    :pswitch_6
    const v0, 0x409ccccd    # 4.9f

    return v0

    .line 367
    :pswitch_7
    const/high16 v0, 0x42700000    # 60.0f

    return v0

    .line 379
    :pswitch_8
    const/high16 v0, 0x41600000    # 14.0f

    return v0

    .line 377
    :pswitch_9
    const v0, 0x40b851ec    # 5.76f

    return v0

    .line 375
    :pswitch_a
    const v0, 0x41accccd    # 21.6f

    return v0

    .line 365
    :pswitch_b
    const/high16 v0, 0x43190000    # 153.0f

    return v0

    .line 373
    :pswitch_c
    const v0, 0x40466666    # 3.1f

    return v0

    .line 371
    :pswitch_d
    const v0, 0x401d70a4    # 2.46f

    return v0

    .line 363
    :pswitch_e
    const/high16 v0, 0x42e60000    # 115.0f

    return v0

    .line 369
    :pswitch_f
    const/high16 v0, 0x43c00000    # 384.0f

    return v0

    .line 361
    :pswitch_10
    const/high16 v0, 0x43940000    # 296.0f

    return v0

    .line 357
    :pswitch_11
    const/high16 v0, 0x42e40000    # 114.0f

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getMobileNetworkSpeedUnit(I)Ljava/lang/String;
    .locals 1
    .param p0, "networkType"    # I

    .line 327
    packed-switch p0, :pswitch_data_0

    .line 349
    const-string v0, "KBps"

    return-object v0

    .line 347
    :pswitch_0
    const-string v0, "MBps"

    return-object v0

    .line 335
    :pswitch_1
    const-string v0, "KBps"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static getWiFiSpeed(Landroid/content/Context;)F
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 298
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 299
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 300
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getWifiNetworkSpeed(Landroid/net/wifi/WifiInfo;)F

    move-result v2

    return v2
.end method

.method private static getWifiNetworkSpeed(Landroid/net/wifi/WifiInfo;)F
    .locals 2
    .param p0, "wifiInfo"    # Landroid/net/wifi/WifiInfo;

    .line 305
    if-nez p0, :cond_0

    .line 307
    const/4 v0, 0x0

    return v0

    .line 309
    :cond_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v0

    .line 310
    .local v0, "linkSpeed":I
    int-to-float v1, v0

    return v1
.end method


# virtual methods
.method public BuildRequest()Ljava/lang/String;
    .locals 1

    .line 66
    const-string v0, ""

    return-object v0
.end method

.method public Finish()V
    .locals 0

    .line 83
    return-void
.end method

.method public GetKey()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/kint/kintframeworkaosaar/MessagePack;->_key:I

    return v0
.end method

.method public GetValue()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePack;->_value:Ljava/lang/String;

    return-object v0
.end method

.method public Prepare()V
    .locals 0

    .line 73
    return-void
.end method

.method public Request()V
    .locals 0

    .line 78
    return-void
.end method
