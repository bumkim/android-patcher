.class public Lcom/kint/kintframeworkaosaar/VideoEncoder;
.super Ljava/lang/Object;
.source "VideoEncoder.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/GetCaptureData;


# instance fields
.field private TAG:Ljava/lang/String;

.field private bitRate:I

.field private blackImage:[B

.field private force:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

.field private formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field private fps:I

.field private getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

.field private hardwareRotation:Z

.field private height:I

.field private iFrameInterval:I

.field private inputSurface:Landroid/view/Surface;

.field private mPresentTimeUs:J

.field private queue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/kint/kintframeworkaosaar/Frame;",
            ">;"
        }
    .end annotation
.end field

.field private rotation:I

.field private running:Z

.field private sendBlackImage:Z

.field private spsPpsSetted:Z

.field private final sync:Ljava/lang/Object;

.field private thread:Ljava/lang/Thread;

.field private videoEncoder:Landroid/media/MediaCodec;

.field private videoInfo:Landroid/media/MediaCodec$BufferInfo;

.field private width:I


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/GetH264Data;)V
    .locals 3
    .param p1, "getH264Data"    # Lcom/kint/kintframeworkaosaar/GetH264Data;

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "VideoEncoder"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    .line 37
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z

    .line 40
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 41
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->hardwareRotation:Z

    .line 46
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->queue:Ljava/util/concurrent/BlockingQueue;

    .line 47
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sync:Ljava/lang/Object;

    .line 50
    sget-object v1, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->FIRST_COMPATIBLE_FOUND:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->force:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    .line 51
    const/16 v1, 0x2d0

    iput v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    .line 52
    const/16 v1, 0x500

    iput v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    .line 53
    const/16 v1, 0x14

    iput v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->fps:I

    .line 54
    const v1, 0x12c000

    iput v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    .line 55
    iput v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    .line 56
    const/4 v1, 0x2

    iput v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->iFrameInterval:I

    .line 57
    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 59
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sendBlackImage:Z

    .line 64
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    return v0
.end method

.method static synthetic access$100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    return v0
.end method

.method static synthetic access$1000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec$BufferInfo;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Landroid/media/MediaCodec;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/GetH264Data;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/kint/kintframeworkaosaar/VideoEncoder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;
    .param p1, "x1"    # Z

    .line 31
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/kint/kintframeworkaosaar/VideoEncoder;Ljava/nio/ByteBuffer;I)Landroid/util/Pair;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;
    .param p1, "x1"    # Ljava/nio/ByteBuffer;
    .param p2, "x2"    # I

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->decodeSpsPpsFromBuffer(Ljava/nio/ByteBuffer;I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/kint/kintframeworkaosaar/VideoEncoder;)J
    .locals 2
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-wide v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->mPresentTimeUs:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->queue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->hardwareRotation:Z

    return v0
.end method

.method static synthetic access$400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    return v0
.end method

.method static synthetic access$500(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sendBlackImage:Z

    return v0
.end method

.method static synthetic access$600(Lcom/kint/kintframeworkaosaar/VideoEncoder;)[B
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->blackImage:[B

    return-object v0
.end method

.method static synthetic access$700(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0
.end method

.method static synthetic access$800(Lcom/kint/kintframeworkaosaar/VideoEncoder;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;
    .param p1, "x1"    # [B

    .line 31
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromEncoderAPI21([B)V

    return-void
.end method

.method static synthetic access$900(Lcom/kint/kintframeworkaosaar/VideoEncoder;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;
    .param p1, "x1"    # [B

    .line 31
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromEncoder([B)V

    return-void
.end method

.method private chooseColorDynamically(Landroid/media/MediaCodecInfo;)Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;
    .locals 5
    .param p1, "mediaCodecInfo"    # Landroid/media/MediaCodecInfo;

    .line 152
    const-string v0, "video/avc"

    invoke-virtual {p1, v0}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    iget-object v0, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget v3, v0, v2

    .line 153
    .local v3, "color":I
    sget-object v4, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 154
    sget-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0

    .line 155
    :cond_0
    sget-object v4, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 156
    sget-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0

    .line 157
    :cond_1
    sget-object v4, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 158
    sget-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0

    .line 152
    .end local v3    # "color":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 161
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method private chooseVideoEncoder(Ljava/lang/String;)Landroid/media/MediaCodecInfo;
    .locals 11
    .param p1, "mime"    # Ljava/lang/String;

    .line 511
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->force:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    sget-object v1, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->HARDWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    if-ne v0, v1, :cond_0

    .line 512
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/CodecUtil;->getAllHardwareEncoders(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->force:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    sget-object v1, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->SOFTWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    if-ne v0, v1, :cond_1

    .line 514
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/CodecUtil;->getAllSoftwareEncoders(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 516
    :cond_1
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/CodecUtil;->getAllEncoders(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 518
    .local v0, "mediaCodecInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/media/MediaCodecInfo;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/MediaCodecInfo;

    .line 519
    .local v2, "mci":Landroid/media/MediaCodecInfo;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v4, "VideoEncoder %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-virtual {v2, p1}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v3

    .line 521
    .local v3, "codecCapabilities":Landroid/media/MediaCodecInfo$CodecCapabilities;
    iget-object v4, v3, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v5, v4

    :goto_2
    if-ge v7, v5, :cond_4

    aget v6, v4, v7

    .line 522
    .local v6, "color":I
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Color supported: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    sget-object v8, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-virtual {v8}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v8

    if-eq v6, v8, :cond_3

    sget-object v8, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 525
    invoke-virtual {v8}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v8

    if-eq v6, v8, :cond_3

    sget-object v8, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 526
    invoke-virtual {v8}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v8

    if-ne v6, v8, :cond_2

    goto :goto_3

    .line 521
    .end local v6    # "color":I
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 527
    .restart local v6    # "color":I
    :cond_3
    :goto_3
    return-object v2

    .line 530
    .end local v2    # "mci":Landroid/media/MediaCodecInfo;
    .end local v3    # "codecCapabilities":Landroid/media/MediaCodecInfo$CodecCapabilities;
    .end local v6    # "color":I
    :cond_4
    goto :goto_1

    .line 531
    :cond_5
    const/4 v1, 0x0

    return-object v1
.end method

.method private decodeSpsPpsFromBuffer(Ljava/nio/ByteBuffer;I)Landroid/util/Pair;
    .locals 10
    .param p1, "outputBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "I)",
            "Landroid/util/Pair<",
            "Ljava/nio/ByteBuffer;",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .line 573
    const/4 v0, 0x0

    .local v0, "mSPS":[B
    const/4 v1, 0x0

    .line 574
    .local v1, "mPPS":[B
    new-array v2, p2, [B

    .line 575
    .local v2, "csd":[B
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 576
    const/4 v4, 0x0

    .line 577
    .local v4, "i":I
    const/4 v5, -0x1

    .line 578
    .local v5, "spsIndex":I
    const/4 v6, -0x1

    move v7, v5

    move v5, v4

    const/4 v4, -0x1

    .line 579
    .local v4, "ppsIndex":I
    .local v5, "i":I
    .local v7, "spsIndex":I
    :goto_0
    add-int/lit8 v8, p2, -0x4

    if-ge v5, v8, :cond_2

    .line 580
    aget-byte v8, v2, v5

    if-nez v8, :cond_1

    add-int/lit8 v8, v5, 0x1

    aget-byte v8, v2, v8

    if-nez v8, :cond_1

    add-int/lit8 v8, v5, 0x2

    aget-byte v8, v2, v8

    if-nez v8, :cond_1

    add-int/lit8 v8, v5, 0x3

    aget-byte v8, v2, v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 581
    if-ne v7, v6, :cond_0

    .line 582
    move v7, v5

    goto :goto_1

    .line 584
    :cond_0
    move v4, v5

    .line 585
    goto :goto_2

    .line 588
    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 590
    :cond_2
    :goto_2
    if-eq v7, v6, :cond_3

    if-eq v4, v6, :cond_3

    .line 591
    new-array v0, v4, [B

    .line 592
    invoke-static {v2, v7, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 593
    sub-int v6, p2, v4

    new-array v1, v6, [B

    .line 594
    sub-int v6, p2, v4

    invoke-static {v2, v4, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 596
    :cond_3
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 597
    new-instance v3, Landroid/util/Pair;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-direct {v3, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 599
    :cond_4
    const/4 v3, 0x0

    return-object v3
.end method

.method private getDataFromEncoder([B)V
    .locals 14
    .param p1, "buffer"    # [B

    .line 465
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 466
    .local v0, "inputBuffers":[Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 468
    .local v1, "outputBuffers":[Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v2

    .line 469
    .local v2, "inBufferIndex":I
    const/4 v3, 0x0

    if-ltz v2, :cond_0

    .line 470
    aget-object v4, v0, v2

    .line 471
    .local v4, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 472
    array-length v5, p1

    invoke-virtual {v4, p1, v3, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 473
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    iget-wide v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->mPresentTimeUs:J

    sub-long v12, v5, v7

    .line 474
    .local v12, "pts":J
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    const/4 v7, 0x0

    array-length v8, p1

    const/4 v11, 0x0

    move v6, v2

    move-wide v9, v12

    invoke-virtual/range {v5 .. v11}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 478
    .end local v4    # "bb":Ljava/nio/ByteBuffer;
    .end local v12    # "pts":J
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v4

    .line 479
    .local v4, "outBufferIndex":I
    const/4 v5, -0x2

    const/4 v6, 0x1

    if-ne v4, v5, :cond_1

    .line 480
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v5}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v5

    .line 481
    .local v5, "mediaFormat":Landroid/media/MediaFormat;
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    invoke-interface {v7, v5}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onVideoFormat(Landroid/media/MediaFormat;)V

    .line 482
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    const-string v8, "csd-0"

    invoke-virtual {v5, v8}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v8

    const-string v9, "csd-1"

    .line 483
    invoke-virtual {v5, v9}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 482
    invoke-interface {v7, v8, v9}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 484
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 485
    .end local v5    # "mediaFormat":Landroid/media/MediaFormat;
    goto :goto_1

    :cond_1
    if-ltz v4, :cond_3

    .line 487
    aget-object v5, v1, v4

    .line 488
    .local v5, "bb":Ljava/nio/ByteBuffer;
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    iget v7, v7, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_2

    .line 489
    iget-boolean v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    if-nez v7, :cond_2

    .line 490
    nop

    .line 491
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v7

    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    iget v8, v8, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-direct {p0, v7, v8}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->decodeSpsPpsFromBuffer(Ljava/nio/ByteBuffer;I)Landroid/util/Pair;

    move-result-object v7

    .line 492
    .local v7, "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    if-eqz v7, :cond_2

    .line 493
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    iget-object v9, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Ljava/nio/ByteBuffer;

    iget-object v10, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/nio/ByteBuffer;

    invoke-interface {v8, v9, v10}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 494
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 498
    .end local v7    # "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    :cond_2
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    invoke-interface {v6, v5, v7}, Lcom/kint/kintframeworkaosaar/GetH264Data;->getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 499
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v6, v4, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 503
    .end local v4    # "outBufferIndex":I
    .end local v5    # "bb":Ljava/nio/ByteBuffer;
    :goto_1
    goto :goto_0

    .line 504
    :cond_3
    return-void
.end method

.method private getDataFromEncoderAPI21([B)V
    .locals 12
    .param p1, "buffer"    # [B
    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x15
    .end annotation

    .line 428
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    .line 429
    .local v0, "inBufferIndex":I
    const/4 v1, 0x0

    if-ltz v0, :cond_0

    .line 430
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v2, v0}, Landroid/media/MediaCodec;->getInputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 431
    .local v2, "bb":Ljava/nio/ByteBuffer;
    array-length v3, p1

    invoke-virtual {v2, p1, v1, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 432
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iget-wide v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->mPresentTimeUs:J

    sub-long v10, v3, v5

    .line 433
    .local v10, "pts":J
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    const/4 v5, 0x0

    array-length v6, p1

    const/4 v9, 0x0

    move v4, v0

    move-wide v7, v10

    invoke-virtual/range {v3 .. v9}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 436
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    .end local v10    # "pts":J
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v2

    .line 437
    .local v2, "outBufferIndex":I
    const/4 v3, -0x2

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    .line 438
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v3

    .line 439
    .local v3, "mediaFormat":Landroid/media/MediaFormat;
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    invoke-interface {v5, v3}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onVideoFormat(Landroid/media/MediaFormat;)V

    .line 440
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    const-string v6, "csd-0"

    invoke-virtual {v3, v6}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v6

    const-string v7, "csd-1"

    .line 441
    invoke-virtual {v3, v7}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 440
    invoke-interface {v5, v6, v7}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 442
    iput-boolean v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 443
    .end local v3    # "mediaFormat":Landroid/media/MediaFormat;
    goto :goto_1

    :cond_1
    if-ltz v2, :cond_3

    .line 445
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v3, v2}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 446
    .local v3, "bb":Ljava/nio/ByteBuffer;
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    iget v5, v5, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_2

    .line 447
    iget-boolean v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    if-nez v5, :cond_2

    .line 448
    nop

    .line 449
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v5

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    iget v6, v6, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-direct {p0, v5, v6}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->decodeSpsPpsFromBuffer(Ljava/nio/ByteBuffer;I)Landroid/util/Pair;

    move-result-object v5

    .line 450
    .local v5, "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    if-eqz v5, :cond_2

    .line 451
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    iget-object v7, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/nio/ByteBuffer;

    iget-object v8, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/nio/ByteBuffer;

    invoke-interface {v6, v7, v8}, Lcom/kint/kintframeworkaosaar/GetH264Data;->onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 452
    iput-boolean v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 456
    .end local v5    # "buffers":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;>;"
    :cond_2
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getH264Data:Lcom/kint/kintframeworkaosaar/GetH264Data;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoInfo:Landroid/media/MediaCodec$BufferInfo;

    invoke-interface {v4, v3, v5}, Lcom/kint/kintframeworkaosaar/GetH264Data;->getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 457
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v4, v2, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 461
    .end local v2    # "outBufferIndex":I
    .end local v3    # "bb":Ljava/nio/ByteBuffer;
    :goto_1
    goto :goto_0

    .line 462
    :cond_3
    return-void
.end method

.method private getDataFromSurface()V
    .locals 2

    .line 387
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;

    invoke-direct {v1, p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder$3;-><init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    .line 423
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 424
    return-void
.end method

.method private getDataFromSurfaceAPI21()V
    .locals 2
    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x15
    .end annotation

    .line 347
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;

    invoke-direct {v1, p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder$2;-><init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    .line 383
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 384
    return-void
.end method

.method private prepareBlackImage()V
    .locals 13

    .line 535
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 536
    .local v0, "b":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 537
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 538
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 539
    .local v2, "x":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    .line 540
    .local v11, "y":I
    mul-int v3, v2, v11

    new-array v12, v3, [I

    .line 541
    .local v12, "data":[I
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v0

    move-object v4, v12

    move v6, v2

    move v9, v2

    move v10, v11

    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 542
    iget v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    iget v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    invoke-static {v12, v3, v4}, Lcom/kint/kintframeworkaosaar/YUVUtil;->ARGBtoYUV420SemiPlanar([III)[B

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->blackImage:[B

    .line 543
    return-void
.end method


# virtual methods
.method public getFps()I
    .locals 1

    .line 226
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->fps:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .line 209
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    return v0
.end method

.method public getInputSurface()Landroid/view/Surface;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->inputSurface:Landroid/view/Surface;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .line 222
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 204
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    return v0
.end method

.method public inputYUVData(Lcom/kint/kintframeworkaosaar/Frame;)V
    .locals 4
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/Frame;

    .line 334
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sync:Ljava/lang/Object;

    monitor-enter v0

    .line 335
    :try_start_0
    iget-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 337
    :try_start_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->queue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    goto :goto_0

    .line 338
    :catch_0
    move-exception v1

    .line 339
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v3, "frame discarded"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_0
    :goto_0
    monitor-exit v0

    .line 343
    return-void

    .line 342
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public isHardwareRotation()Z
    .locals 1

    .line 214
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->hardwareRotation:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .line 218
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z

    return v0
.end method

.method public prepareVideoEncoder()Z
    .locals 9

    .line 168
    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    iget v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    iget v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->fps:I

    iget v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    iget v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    iget v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->iFrameInterval:I

    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->prepareVideoEncoder(IIIIIZILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)Z

    move-result v0

    return v0
.end method

.method public prepareVideoEncoder(IIIIIZILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)Z
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "fps"    # I
    .param p4, "bitRate"    # I
    .param p5, "rotation"    # I
    .param p6, "hardwareRotation"    # Z
    .param p7, "iFrameInterval"    # I
    .param p8, "formatVideoEncoder"    # Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 83
    iput p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    .line 84
    iput p2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    .line 85
    iput p3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->fps:I

    .line 86
    iput p4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    .line 87
    iput p5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    .line 88
    iput-boolean p6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->hardwareRotation:Z

    .line 89
    iput-object p8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 90
    const-string v0, "video/avc"

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->chooseVideoEncoder(Ljava/lang/String;)Landroid/media/MediaCodecInfo;

    move-result-object v0

    .line 93
    .local v0, "encoder":Landroid/media/MediaCodecInfo;
    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 95
    :try_start_0
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    .line 96
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    sget-object v3, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420Dynamical:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    if-ne v2, v3, :cond_0

    .line 98
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->chooseColorDynamically(Landroid/media/MediaCodecInfo;)Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 99
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    if-nez v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v3, "YUV420 dynamical choose failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return v1

    .line 115
    :cond_0
    if-nez p6, :cond_2

    const/16 v2, 0x5a

    if-eq p5, v2, :cond_1

    const/16 v2, 0x10e

    if-ne p5, v2, :cond_2

    .line 117
    :cond_1
    const-string v2, "video/avc"

    invoke-static {v2, p2, p1}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v2

    goto :goto_0

    .line 121
    :cond_2
    const-string v2, "video/avc"

    invoke-static {v2, p1, p2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v2

    .line 123
    .local v2, "videoFormat":Landroid/media/MediaFormat;
    :goto_0
    const-string v3, "color-format"

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 124
    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->getFormatCodec()I

    move-result v4

    .line 123
    invoke-virtual {v2, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 125
    const-string v3, "max-input-size"

    invoke-virtual {v2, v3, v1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 126
    const-string v3, "bitrate"

    invoke-virtual {v2, v3, p4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 127
    const-string v3, "frame-rate"

    invoke-virtual {v2, v3, p3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 128
    const-string v3, "i-frame-interval"

    invoke-virtual {v2, v3, p7}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 129
    if-eqz p6, :cond_3

    .line 131
    const-string v3, "rotation-degrees"

    invoke-virtual {v2, v3, p5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 133
    :cond_3
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5, v5, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 134
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z

    .line 135
    sget-object v3, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    if-ne p8, v3, :cond_4

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v3, v5, :cond_4

    .line 137
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v3}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->inputSurface:Landroid/view/Surface;

    goto :goto_1

    .line 141
    :cond_4
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->prepareBlackImage()V

    .line 144
    :goto_1
    return v4

    .line 145
    .end local v2    # "videoFormat":Landroid/media/MediaFormat;
    :catch_0
    move-exception v2

    goto :goto_2

    .line 108
    :cond_5
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v3, "Valid encoder not found"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    return v1

    .line 145
    :goto_2
    nop

    .line 146
    .local v2, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v4, "create videoEncoder failed."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    return v1
.end method

.method public reset()V
    .locals 10

    .line 324
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sync:Ljava/lang/Object;

    monitor-enter v0

    .line 325
    :try_start_0
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->stop()V

    .line 326
    iget v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    iget v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    iget v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->fps:I

    iget v5, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    iget v6, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    iget-boolean v7, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->hardwareRotation:Z

    iget v8, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->iFrameInterval:I

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->prepareVideoEncoder(IIIIIZILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)Z

    .line 328
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->start(Z)V

    .line 329
    monitor-exit v0

    .line 330
    return-void

    .line 329
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setForce(Lcom/kint/kintframeworkaosaar/CodecUtil$Force;)V
    .locals 0
    .param p1, "force"    # Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    .line 189
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->force:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    .line 190
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .line 74
    iput p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->height:I

    .line 75
    return-void
.end method

.method public setInputSurface(Landroid/view/Surface;)V
    .locals 0
    .param p1, "inputSurface"    # Landroid/view/Surface;

    .line 199
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->inputSurface:Landroid/view/Surface;

    .line 200
    return-void
.end method

.method public setVideoBitrateOnFly(I)V
    .locals 4
    .param p1, "bitrate"    # I
    .annotation build Landroid/support/annotation/RequiresApi;
        api = 0x13
    .end annotation

    .line 174
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iput p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    .line 176
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 177
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "video-bitrate"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    goto :goto_0

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v3, "encoder need be running"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 185
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_0
    :goto_0
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .line 69
    iput p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->width:I

    .line 70
    return-void
.end method

.method public start()V
    .locals 1

    .line 230
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->start(Z)V

    .line 231
    return-void
.end method

.method public start(Z)V
    .locals 5
    .param p1, "resetTs"    # Z

    .line 234
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sync:Ljava/lang/Object;

    monitor-enter v0

    .line 235
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    if-eqz v1, :cond_5

    .line 236
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 237
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    iput-wide v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->mPresentTimeUs:J

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    .line 240
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->formatVideoEncoder:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    if-ne v1, v2, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_2

    .line 243
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 244
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromSurfaceAPI21()V

    goto :goto_1

    .line 246
    :cond_1
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getDataFromSurface()V

    goto :goto_1

    .line 250
    :cond_2
    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    const/16 v2, 0xb4

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->rotation:I

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_3

    goto :goto_0

    .line 251
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "rotation value unsupported, select value 0, 90, 180 or 270"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 254
    :cond_4
    :goto_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;

    invoke-direct {v2, p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;-><init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    .line 290
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 292
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z

    goto :goto_2

    .line 294
    :cond_5
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v2, "VideoEncoder need be prepared, VideoEncoder not enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :goto_2
    monitor-exit v0

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public startSendBlackImage()V
    .locals 4

    .line 546
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sendBlackImage:Z

    .line 547
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 548
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 550
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "video-bitrate"

    const v2, 0x19000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 552
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 556
    goto :goto_0

    .line 553
    :catch_0
    move-exception v1

    .line 554
    .local v1, "e":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->TAG:Ljava/lang/String;

    const-string v3, "encoder need be running"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 559
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_0
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 6

    .line 301
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sync:Ljava/lang/Object;

    monitor-enter v0

    .line 302
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->running:Z

    .line 303
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    :try_start_1
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309
    goto :goto_0

    .line 307
    :catch_0
    move-exception v2

    .line 308
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 310
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_0
    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->thread:Ljava/lang/Thread;

    .line 312
    :cond_0
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    if-eqz v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->stop()V

    .line 314
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->release()V

    .line 315
    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->videoEncoder:Landroid/media/MediaCodec;

    .line 317
    :cond_1
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->queue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 318
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->spsPpsSetted:Z

    .line 319
    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->inputSurface:Landroid/view/Surface;

    .line 320
    monitor-exit v0

    .line 321
    return-void

    .line 320
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public stopSendBlackImage()V
    .locals 2

    .line 563
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->sendBlackImage:Z

    .line 564
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 565
    iget v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder;->bitRate:I

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->setVideoBitrateOnFly(I)V

    .line 567
    :cond_0
    return-void
.end method
