.class public Lcom/kint/kintframeworkaosaar/CreateSSLSocket;
.super Ljava/lang/Object;
.source "CreateSSLSocket.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createSSlSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 3
    .param p0, "host"    # Ljava/lang/String;
    .param p1, "port"    # I

    .line 24
    :try_start_0
    new-instance v0, Lcom/kint/kintframeworkaosaar/TLSSocketFactory;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/TLSSocketFactory;-><init>()V

    .line 25
    .local v0, "socketFactory":Lcom/kint/kintframeworkaosaar/TLSSocketFactory;
    invoke-virtual {v0, p0, p1}, Lcom/kint/kintframeworkaosaar/TLSSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 26
    .end local v0    # "socketFactory":Lcom/kint/kintframeworkaosaar/TLSSocketFactory;
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CreateSSLSocket"

    const-string v2, "Error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 28
    const/4 v1, 0x0

    return-object v1
.end method
