.class public Lcom/kint/kintframeworkaosaar/QuestionInfo;
.super Ljava/lang/Object;
.source "QuestionInfo.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private _answerList:Ljava/util/ArrayList;

.field private _elementList:Ljava/util/ArrayList;

.field private _id:Ljava/lang/String;

.field private _imageURL:Ljava/lang/String;

.field private _type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "QuestionInfo"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public Answer(Z)Ljava/lang/String;
    .locals 7
    .param p1, "test"    # Z

    .line 23
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 27
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "questionId"

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 28
    const-string v1, "questionType"

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 30
    const/4 v1, 0x1

    if-ne v1, p1, :cond_4

    .line 32
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 33
    .local v2, "answerJsonArray":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    const v6, -0x4b4b31cf

    if-eq v5, v6, :cond_2

    const v6, 0x674393d

    if-eq v5, v6, :cond_1

    const v6, 0x1e0a23d1

    if-eq v5, v6, :cond_0

    goto :goto_0

    :cond_0
    const-string v5, "multipleChoice"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_1
    const-string v1, "range"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "openEnd"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 50
    :pswitch_0
    const-string v1, "TestTest"

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    .line 43
    :pswitch_1
    const-string v1, "choice1"

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 44
    const-string v1, "choice2"

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 46
    goto :goto_2

    .line 37
    :pswitch_2
    const-string v1, "1"

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 39
    nop

    .line 54
    :goto_2
    const-string v1, "answer"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    .end local v2    # "answerJsonArray":Lorg/json/JSONArray;
    :cond_4
    goto :goto_3

    .line 57
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 62
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public Parse(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "questionInfoJsonObject"    # Lorg/json/JSONObject;

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 74
    :cond_0
    const-string v0, "questionId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    .line 75
    const-string v0, "questionType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    .line 77
    const-string v0, "QuestionInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Question ID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , Question Type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x4b4b31cf

    const/4 v4, 0x0

    if-eq v2, v3, :cond_3

    const v3, 0x674393d

    if-eq v2, v3, :cond_2

    const v3, 0x1e0a23d1

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "multipleChoice"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const-string v2, "range"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v2, "openEnd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_3

    .line 98
    :pswitch_0
    const-string v0, "choices"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 99
    .local v0, "questionJsonArray":Lorg/json/JSONArray;
    nop

    .local v4, "countIndex":I
    :goto_1
    move v1, v4

    .end local v4    # "countIndex":I
    .local v1, "countIndex":I
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 101
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    if-nez v2, :cond_5

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    .line 104
    :cond_5
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 105
    .local v2, "questionRangeElement":Ljava/lang/String;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v3, "QuestionInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Question Multiple Choice Element["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    .end local v2    # "questionRangeElement":Ljava/lang/String;
    add-int/lit8 v4, v1, 0x1

    goto :goto_1

    .line 110
    .end local v0    # "questionJsonArray":Lorg/json/JSONArray;
    .end local v1    # "countIndex":I
    :cond_6
    goto :goto_3

    .line 82
    :pswitch_1
    const-string v0, "range"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 83
    .restart local v0    # "questionJsonArray":Lorg/json/JSONArray;
    nop

    .restart local v4    # "countIndex":I
    :goto_2
    move v1, v4

    .end local v4    # "countIndex":I
    .restart local v1    # "countIndex":I
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 85
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    if-nez v2, :cond_7

    .line 86
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    .line 88
    :cond_7
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 89
    .restart local v2    # "questionRangeElement":Ljava/lang/String;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_elementList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    const-string v3, "QuestionInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Question Range Element["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    .end local v2    # "questionRangeElement":Ljava/lang/String;
    add-int/lit8 v4, v1, 0x1

    goto :goto_2

    .line 94
    .end local v0    # "questionJsonArray":Lorg/json/JSONArray;
    .end local v1    # "countIndex":I
    :cond_8
    nop

    .line 117
    :goto_3
    const-string v0, "imageURL"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_imageURL:Ljava/lang/String;

    .line 118
    const-string v0, "QuestionInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Question ImageURL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_imageURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    goto :goto_4

    .line 121
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 125
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
