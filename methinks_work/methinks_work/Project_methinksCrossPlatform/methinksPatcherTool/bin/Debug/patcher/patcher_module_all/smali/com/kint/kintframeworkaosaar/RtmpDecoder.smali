.class public Lcom/kint/kintframeworkaosaar/RtmpDecoder;
.super Ljava/lang/Object;
.source "RtmpDecoder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RtmpDecoder"


# instance fields
.field private rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)V
    .locals 0
    .param p1, "rtmpSessionInfo"    # Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 33
    return-void
.end method


# virtual methods
.method public readPacket(Ljava/io/InputStream;)Lcom/kint/kintframeworkaosaar/RtmpPacket;
    .locals 7
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->readHeader(Ljava/io/InputStream;Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v0

    .line 40
    .local v0, "header":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getChunkStreamId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v1

    .line 41
    .local v1, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    invoke-virtual {v1, v0}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->setPrevHeaderRx(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 43
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getPacketLength()I

    move-result v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getRxChunkSize()I

    move-result v3

    const/4 v4, 0x0

    if-le v2, v3, :cond_1

    .line 46
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getRxChunkSize()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->storePacketChunk(Ljava/io/InputStream;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 48
    return-object v4

    .line 51
    :cond_0
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->getStoredPacketInputStream()Ljava/io/ByteArrayInputStream;

    move-result-object p1

    .line 56
    :cond_1
    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpDecoder$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$MessageType:[I

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getMessageType()Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 91
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No packet body implementation for message type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getMessageType()Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 88
    :pswitch_0
    new-instance v2, Lcom/kint/kintframeworkaosaar/Acknowledgement;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Acknowledgement;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 89
    .local v2, "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 85
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_1
    new-instance v2, Lcom/kint/kintframeworkaosaar/Data;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Data;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 86
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 82
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_2
    new-instance v2, Lcom/kint/kintframeworkaosaar/Command;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 83
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 79
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_3
    new-instance v2, Lcom/kint/kintframeworkaosaar/Video;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Video;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 80
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 76
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_4
    new-instance v2, Lcom/kint/kintframeworkaosaar/Audio;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Audio;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 77
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 73
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_5
    new-instance v2, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 74
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 70
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_6
    new-instance v2, Lcom/kint/kintframeworkaosaar/WindowAckSize;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/WindowAckSize;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 71
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 67
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_7
    new-instance v2, Lcom/kint/kintframeworkaosaar/UserControl;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/UserControl;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 68
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    goto :goto_0

    .line 64
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_8
    new-instance v2, Lcom/kint/kintframeworkaosaar/Abort;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/Abort;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 65
    .restart local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    nop

    .line 92
    :goto_0
    nop

    .line 94
    invoke-virtual {v2, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->readBody(Ljava/io/InputStream;)V

    .line 95
    return-object v2

    .line 58
    .end local v2    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :pswitch_9
    new-instance v2, Lcom/kint/kintframeworkaosaar/SetChunkSize;

    invoke-direct {v2, v0}, Lcom/kint/kintframeworkaosaar/SetChunkSize;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 59
    .local v2, "setChunkSize":Lcom/kint/kintframeworkaosaar/SetChunkSize;
    invoke-virtual {v2, p1}, Lcom/kint/kintframeworkaosaar/SetChunkSize;->readBody(Ljava/io/InputStream;)V

    .line 60
    const-string v3, "RtmpDecoder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "readPacket(): Setting chunk size to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SetChunkSize;->getChunkSize()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SetChunkSize;->getChunkSize()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->setRxChunkSize(I)V

    .line 62
    return-object v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
