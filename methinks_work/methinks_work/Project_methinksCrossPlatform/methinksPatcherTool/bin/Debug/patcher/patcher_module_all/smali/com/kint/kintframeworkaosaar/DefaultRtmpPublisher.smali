.class public Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;
.super Ljava/lang/Object;
.source "DefaultRtmpPublisher.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/RtmpPublisher;


# instance fields
.field private rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V
    .locals 1
    .param p1, "connectCheckerRtmp"    # Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-direct {v0, p1}, Lcom/kint/kintframeworkaosaar/RtmpConnection;-><init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    .line 18
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->close()V

    .line 33
    return-void
.end method

.method public connect(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .line 22
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connect(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public publish(Ljava/lang/String;)Z
    .locals 1
    .param p1, "publishType"    # Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publish(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public publishAudioData([BII)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "size"    # I
    .param p3, "dts"    # I

    .line 42
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1, p2, p3}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishAudioData([BII)V

    .line 43
    return-void
.end method

.method public publishVideoData([BII)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "size"    # I
    .param p3, "dts"    # I

    .line 37
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1, p2, p3}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishVideoData([BII)V

    .line 38
    return-void
.end method

.method public setAuthorization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->setAuthorization(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public setVideoRate(II)V
    .locals 1
    .param p1, "videoDataRate"    # I
    .param p2, "frameRate"    # I

    .line 53
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->setVideoRate(II)V

    .line 54
    return-void
.end method

.method public setVideoResolution(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 47
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->rtmpConnection:Lcom/kint/kintframeworkaosaar/RtmpConnection;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->setVideoResolution(II)V

    .line 48
    return-void
.end method
