.class Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;
.super Ljava/lang/Object;
.source "SrsFlvMuxer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SrsRawH264Stream"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SrsFlvMuxer"


# instance fields
.field private annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

.field private nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field private pps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field private pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field private seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field private sps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field private sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;


# direct methods
.method private constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V
    .locals 2

    .line 395
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    .line 399
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 400
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 401
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 402
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 403
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 404
    new-instance p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    return-void
.end method

.method synthetic constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p2, "x1"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;

    .line 395
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V

    return-void
.end method

.method private searchAnnexb(Ljava/nio/ByteBuffer;I)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;
    .locals 4
    .param p1, "bb"    # Ljava/nio/ByteBuffer;
    .param p2, "size"    # I

    .line 583
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 584
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    .line 585
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v1, p2, -0x4

    if-ge v0, v1, :cond_3

    .line 587
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    goto :goto_1

    .line 591
    :cond_0
    add-int/lit8 v1, v0, 0x2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 592
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput-boolean v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 593
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    add-int/lit8 v2, v0, 0x3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    .line 594
    goto :goto_2

    .line 597
    :cond_1
    add-int/lit8 v1, v0, 0x2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, 0x3

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-ne v1, v2, :cond_2

    .line 598
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput-boolean v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 599
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    add-int/lit8 v2, v0, 0x4

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    .line 600
    goto :goto_2

    .line 585
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 603
    .end local v0    # "i":I
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    return-object v0
.end method

.method private searchStartcode(Ljava/nio/ByteBuffer;I)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;
    .locals 5
    .param p1, "bb"    # Ljava/nio/ByteBuffer;
    .param p2, "size"    # I

    .line 566
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 567
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    .line 568
    add-int/lit8 v0, p2, -0x4

    if-lez v0, :cond_1

    .line 569
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v4, :cond_0

    .line 571
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput-boolean v4, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 572
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    const/4 v1, 0x4

    iput v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    goto :goto_0

    .line 573
    :cond_0
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v4, :cond_1

    .line 575
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput-boolean v4, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    .line 576
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    iput v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    .line 579
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->annexb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    return-object v0
.end method


# virtual methods
.method public demuxAnnexb(Ljava/nio/ByteBuffer;IZ)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    .locals 4
    .param p1, "bb"    # Ljava/nio/ByteBuffer;
    .param p2, "size"    # I
    .param p3, "isOnlyChkHeader"    # Z

    .line 607
    new-instance v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V

    .line 608
    .local v0, "tbb":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/lit8 v2, p2, -0x4

    if-ge v1, v2, :cond_4

    .line 611
    if-eqz p3, :cond_0

    .line 612
    invoke-direct {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->searchStartcode(Ljava/nio/ByteBuffer;I)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->searchAnnexb(Ljava/nio/ByteBuffer;I)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;

    move-result-object v1

    .line 614
    .local v1, "tbbsc":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;
    :goto_0
    iget-boolean v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->match:Z

    if-eqz v2, :cond_3

    iget v2, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    goto :goto_2

    .line 618
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v3, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;->nb_start_code:I

    if-ge v2, v3, :cond_2

    .line 619
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    .line 618
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 623
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 624
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    goto :goto_3

    .line 615
    :cond_3
    :goto_2
    const-string v2, "SrsFlvMuxer"

    const-string v3, "annexb not match."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    .end local v1    # "tbbsc":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;
    :cond_4
    :goto_3
    return-object v0
.end method

.method public isPps(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;)Z
    .locals 4
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 411
    iget v0, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    iget-object v0, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    const/4 v1, 0x1

    nop

    :cond_0
    return v1
.end method

.method public isSps(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;)Z
    .locals 4
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 407
    iget v0, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    iget-object v0, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    const/4 v3, 0x7

    if-ne v0, v3, :cond_0

    const/4 v1, 0x1

    nop

    :cond_0
    return v1
.end method

.method public muxFlvTag(Ljava/util/ArrayList;II)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    .locals 9
    .param p2, "frame_type"    # I
    .param p3, "avc_packet_type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;",
            ">;II)",
            "Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;"
        }
    .end annotation

    .line 530
    .local p1, "frames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;>;"
    const/4 v0, 0x5

    .line 531
    .local v0, "size":I
    const/4 v1, 0x0

    move v2, v0

    const/4 v0, 0x0

    .local v0, "i":I
    .local v2, "size":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 532
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget v3, v3, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    add-int/2addr v2, v3

    .line 531
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 534
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1100(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsAllocator;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator;->allocate(I)Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    move-result-object v0

    .line 540
    .local v0, "allocation":Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;
    shl-int/lit8 v3, p2, 0x4

    or-int/lit8 v3, v3, 0x7

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 543
    int-to-byte v3, p3

    invoke-virtual {v0, v3}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 549
    const/4 v3, 0x0

    .line 550
    .local v3, "cts":I
    shr-int/lit8 v4, v3, 0x10

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 551
    shr-int/lit8 v4, v3, 0x8

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 552
    int-to-byte v4, v3

    invoke-virtual {v0, v4}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->put(B)V

    .line 555
    nop

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 556
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 557
    .local v4, "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    iget-object v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 558
    iget-object v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v6

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size()I

    move-result v7

    iget v8, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    invoke-virtual {v5, v6, v7, v8}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 559
    iget v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    invoke-virtual {v0, v5}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->appendOffset(I)V

    .line 555
    .end local v4    # "frame":Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 562
    .end local v1    # "i":I
    :cond_1
    return-object v0
.end method

.method public muxNaluHeader(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;
    .locals 3
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    .line 415
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 417
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iput v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 423
    iget v0, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 428
    .local v0, "NAL_unit_length":I
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 431
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 432
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->nalu_header:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    return-object v1
.end method

.method public muxSequenceHeader(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Ljava/nio/ByteBuffer;",
            "Ljava/util/ArrayList<",
            "Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;",
            ">;)V"
        }
    .end annotation

    .line 451
    .local p3, "frames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;>;"
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 453
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iput v1, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 460
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    .line 462
    .local v1, "profile_idc":B
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    .line 467
    .local v3, "level_idc":B
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 469
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 471
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->access$1000(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 473
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 476
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 479
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 480
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->seq_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    if-nez v4, :cond_1

    .line 484
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    iput-object v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 485
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iput v2, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 487
    :cond_1
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 490
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 492
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    array-length v5, v5

    int-to-short v5, v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 494
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 495
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    array-length v5, v5

    iput v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 499
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v5

    iput-object v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 500
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->sps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    if-nez v4, :cond_2

    .line 504
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    iput-object v5, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 505
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iput v2, v4, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 507
    :cond_2
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 510
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 512
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    array-length v2, v2

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 514
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 515
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_hdr:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    array-length v2, v2

    iput v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->size:I

    .line 519
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;->data:Ljava/nio/ByteBuffer;

    .line 520
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;->pps_bb:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    return-void
.end method
