.class Lcom/kint/kintframeworkaosaar/VideoEncoder$1;
.super Ljava/lang/Object;
.source "VideoEncoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/VideoEncoder;->start(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/VideoEncoder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 255
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 259
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v0

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v1

    mul-int v0, v0, v1

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/YUVUtil;->preAllocateBuffers(I)V

    .line 260
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_7

    .line 262
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$200(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/Frame;

    .line 263
    .local v0, "frame":Lcom/kint/kintframeworkaosaar/Frame;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Frame;->getBuffer()[B

    move-result-object v1

    .line 264
    .local v1, "buffer":[B
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Frame;->getFormat()I

    move-result v2

    const v3, 0x32315659

    if-ne v2, v3, :cond_0

    .line 265
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/kint/kintframeworkaosaar/YUVUtil;->YV12toNV21([BII)[B

    move-result-object v2

    move-object v1, v2

    .line 267
    :cond_0
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$300(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 269
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Frame;->isFlip()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v2

    add-int/lit16 v2, v2, 0xb4

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$400(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v2

    .line 270
    .local v2, "orientation":I
    :goto_1
    const/16 v3, 0x168

    if-lt v2, v3, :cond_2

    add-int/lit16 v2, v2, -0x168

    .line 271
    :cond_2
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v3

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v4

    invoke-static {v1, v3, v4, v2}, Lcom/kint/kintframeworkaosaar/YUVUtil;->rotateNV21([BIII)[B

    move-result-object v3

    move-object v1, v3

    .line 273
    .end local v2    # "orientation":I
    :cond_3
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$500(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$600(Lcom/kint/kintframeworkaosaar/VideoEncoder;)[B

    move-result-object v2

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 274
    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$100(Lcom/kint/kintframeworkaosaar/VideoEncoder;)I

    move-result v3

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$700(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/kint/kintframeworkaosaar/YUVUtil;->NV21toYUV420byColor([BIILcom/kint/kintframeworkaosaar/FormatVideoEncoder;)[B

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 275
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_5

    .line 276
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$800(Lcom/kint/kintframeworkaosaar/VideoEncoder;[B)V

    goto :goto_3

    .line 280
    :cond_5
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$900(Lcom/kint/kintframeworkaosaar/VideoEncoder;[B)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 283
    .end local v0    # "frame":Lcom/kint/kintframeworkaosaar/Frame;
    .end local v1    # "buffer":[B
    :catch_0
    move-exception v0

    .line 285
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Ljava/lang/Thread;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/VideoEncoder$1;->this$0:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->access$1000(Lcom/kint/kintframeworkaosaar/VideoEncoder;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 286
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_6
    :goto_3
    goto/16 :goto_0

    .line 288
    :cond_7
    return-void
.end method
