.class public Lcom/kint/kintframeworkaosaar/Crypto;
.super Ljava/lang/Object;
.source "Crypto.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Crypto"


# instance fields
.field private hmacSHA256:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    :try_start_0
    const-string v0, "HmacSHA256"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/Crypto;->hmacSHA256:Ljavax/crypto/Mac;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v1, "Crypto"

    const-string v2, "HMAC SHA256 does not exist"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 29
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "Crypto"

    const-string v2, "Security exception when getting HMAC"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 33
    .end local v0    # "e":Ljava/lang/SecurityException;
    :goto_0
    nop

    .line 34
    :goto_1
    return-void
.end method


# virtual methods
.method public calculateHmacSHA256([B[B)[B
    .locals 4
    .param p1, "input"    # [B
    .param p2, "key"    # [B

    .line 42
    const/4 v0, 0x0

    .line 44
    .local v0, "output":[B
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Crypto;->hmacSHA256:Ljavax/crypto/Mac;

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA256"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 45
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Crypto;->hmacSHA256:Ljavax/crypto/Mac;

    invoke-virtual {v1, p1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 48
    goto :goto_0

    .line 46
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Ljava/security/InvalidKeyException;
    const-string v2, "Crypto"

    const-string v3, "Invalid key"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 49
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :goto_0
    return-object v0
.end method

.method public calculateHmacSHA256([B[BI)[B
    .locals 5
    .param p1, "input"    # [B
    .param p2, "key"    # [B
    .param p3, "length"    # I

    .line 58
    const/4 v0, 0x0

    .line 60
    .local v0, "output":[B
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Crypto;->hmacSHA256:Ljavax/crypto/Mac;

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v3, 0x0

    const-string v4, "HmacSHA256"

    invoke-direct {v2, p2, v3, p3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 61
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Crypto;->hmacSHA256:Ljavax/crypto/Mac;

    invoke-virtual {v1, p1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 64
    goto :goto_0

    .line 62
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/security/InvalidKeyException;
    const-string v2, "Crypto"

    const-string v3, "Invalid key"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 65
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :goto_0
    return-object v0
.end method
