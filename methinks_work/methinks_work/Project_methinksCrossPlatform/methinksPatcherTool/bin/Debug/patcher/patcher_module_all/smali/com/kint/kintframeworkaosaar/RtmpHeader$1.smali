.class synthetic Lcom/kint/kintframeworkaosaar/RtmpHeader$1;
.super Ljava/lang/Object;
.source "RtmpHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/RtmpHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 226
    invoke-static {}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->values()[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    :try_start_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    :try_start_1
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    :try_start_2
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    :goto_2
    :try_start_3
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_3_RELATIVE_SINGLE_BYTE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    :goto_3
    return-void
.end method
