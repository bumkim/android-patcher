.class Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;
.super Landroid/os/Handler;
.source "methinksPatcherApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 1575
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .line 1578
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_3

    .line 1806
    :pswitch_1
    goto/16 :goto_3

    .line 1744
    :pswitch_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1745
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v0, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1746
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1747
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v0, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1749
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    .line 1750
    .local v0, "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Tooltip(Ljava/lang/String;)V

    .line 1752
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetError()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    const v6, -0x5bb96091

    if-eq v5, v6, :cond_3

    const v6, -0x2565e2de

    if-eq v5, v6, :cond_2

    goto :goto_0

    :cond_2
    const-string v5, "invalidProject"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string v5, "invalidUserCode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_0
    packed-switch v1, :pswitch_data_1

    .end local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    goto :goto_1

    .line 1764
    .restart local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    :pswitch_3
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget v4, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->LoginTryCount:I

    add-int/2addr v4, v3

    iput v4, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->LoginTryCount:I

    .line 1765
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v3, "Invalid user code. Enter your user code again."

    invoke-virtual {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Tooltip(Ljava/lang/String;)V

    .line 1766
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$3000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)V

    .line 1768
    goto :goto_1

    .line 1756
    :pswitch_4
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v2, "Testing period is over. Thank you for your participation. Please delete this app from your device."

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Tooltip(Ljava/lang/String;)V

    .line 1757
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 1758
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z

    .line 1760
    nop

    .line 1782
    .end local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    :goto_1
    goto/16 :goto_3

    .line 1681
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;

    .line 1682
    .restart local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    const-string v1, "TAG"

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1683
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Tooltip(Ljava/lang/String;)V

    .line 1685
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$3100(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1687
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->CheckValidData()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1689
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gig has not started yet"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1691
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 1692
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z

    goto/16 :goto_2

    .line 1697
    :cond_5
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$3200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)V

    .line 1698
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$502(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I

    .line 1700
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 1701
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1702
    :cond_6
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    if-eqz v1, :cond_7

    .line 1703
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1705
    :cond_7
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->IsScreenStreamAllowed()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1707
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$1900(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/media/projection/MediaProjectionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$1700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)I

    move-result v3

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$1800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/projection/MediaProjectionManager;->getMediaProjection(ILandroid/content/Intent;)Landroid/media/projection/MediaProjection;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$1602(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/media/projection/MediaProjection;)Landroid/media/projection/MediaProjection;

    .line 1708
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectServer(Ljava/lang/String;)V

    goto :goto_2

    .line 1714
    :cond_8
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gig has not started yet"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1716
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 1717
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2102(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Z)Z

    goto :goto_2

    .line 1724
    :cond_9
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$3200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)V

    .line 1725
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$502(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I

    .line 1727
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_a

    .line 1728
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1729
    :cond_a
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    if-eqz v1, :cond_b

    .line 1730
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1732
    :cond_b
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;->IsScreenStreamAllowed()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1734
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->connectServer(Ljava/lang/String;)V

    .line 1740
    .end local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$LoginInfo;
    :cond_c
    :goto_2
    goto/16 :goto_3

    .line 1582
    :pswitch_6
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2300(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1584
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1585
    .local v0, "density":F
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1586
    .local v2, "width":I
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1588
    .local v3, "height":I
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 1589
    .local v4, "windowManagerLayoutParams2":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x33

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1590
    const/16 v6, 0x100

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 1591
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1594
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1595
    .local v6, "loginLayoutInflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v8, "login_layout"

    invoke-static {v7, v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)I

    move-result v7

    .line 1596
    .local v7, "resourceID":I
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    .line 1597
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v8, v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1599
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v8, v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    const-string v9, "button_login"

    invoke-static {v1, v8, v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1600
    .local v1, "loginButton":Landroid/widget/Button;
    if-eqz v1, :cond_d

    .line 1601
    new-instance v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$1;

    invoke-direct {v8, p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;Landroid/widget/Button;)V

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1622
    :cond_d
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    const-string v11, "editText_user_code"

    invoke-static {v9, v10, v11}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    iput-object v9, v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCodeEditText:Landroid/widget/EditText;

    .line 1623
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v8, v8, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCodeEditText:Landroid/widget/EditText;

    if-eqz v8, :cond_e

    .line 1627
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v9, "input_method"

    invoke-virtual {v8, v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    .line 1628
    .local v8, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCodeEditText:Landroid/widget/EditText;

    new-instance v10, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$2;

    invoke-direct {v10, p0, v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1640
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_userCodeEditText:Landroid/widget/EditText;

    new-instance v10, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$3;

    invoke-direct {v10, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;)V

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1653
    .end local v8    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_e
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v8

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v8, v9, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1657
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 1658
    .local v8, "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    iput v5, v8, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1659
    const/4 v5, -0x2

    iput v5, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1660
    iput v5, v8, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1662
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const-string v9, "img_methinks_logo"

    invoke-static {v5, v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1663
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    sub-int v9, v2, v9

    div-int/lit8 v9, v9, 0x2

    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1664
    const/high16 v9, 0x42c80000    # 100.0f

    mul-float v9, v9, v0

    float-to-int v9, v9

    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1666
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    new-instance v10, Landroid/widget/ImageView;

    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-direct {v10, v11}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v10, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    .line 1667
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    invoke-virtual {v9, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1669
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/view/WindowManager;

    move-result-object v9

    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v9, v10, v8}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1670
    .end local v0    # "density":F
    .end local v1    # "loginButton":Landroid/widget/Button;
    .end local v2    # "width":I
    .end local v3    # "height":I
    .end local v4    # "windowManagerLayoutParams2":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "loginLayoutInflater":Landroid/view/LayoutInflater;
    .end local v7    # "resourceID":I
    .end local v8    # "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    goto :goto_3

    .line 1673
    :cond_f
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$3000(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)V

    .line 1677
    goto :goto_3

    .line 1800
    :pswitch_7
    goto :goto_3

    .line 1794
    :pswitch_8
    goto :goto_3

    .line 1786
    :pswitch_9
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$1600(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Landroid/media/projection/MediaProjection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->startRecord(Landroid/media/projection/MediaProjection;)Z

    .line 1788
    nop

    .line 1816
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
