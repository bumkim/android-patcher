.class public final Lcom/kint/kintframeworkaosaar/Handshake;
.super Ljava/lang/Object;
.source "Handshake.java"


# static fields
.field private static final DIGEST_OFFSET_INDICATOR_POS:I = 0x304

.field private static final GENUINE_FP_KEY:[B

.field private static final HANDSHAKE_SIZE:I = 0x600

.field private static final PROTOCOL_VERSION:I = 0x3

.field private static final SHA256_DIGEST_SIZE:I = 0x20

.field private static final TAG:Ljava/lang/String; = "Handshake"


# instance fields
.field private s1:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    const/16 v0, 0x3e

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/kint/kintframeworkaosaar/Handshake;->GENUINE_FP_KEY:[B

    return-void

    :array_0
    .array-data 1
        0x47t
        0x65t
        0x6et
        0x75t
        0x69t
        0x6et
        0x65t
        0x20t
        0x41t
        0x64t
        0x6ft
        0x62t
        0x65t
        0x20t
        0x46t
        0x6ct
        0x61t
        0x73t
        0x68t
        0x20t
        0x50t
        0x6ct
        0x61t
        0x79t
        0x65t
        0x72t
        0x20t
        0x30t
        0x30t
        0x31t
        -0x10t
        -0x12t
        -0x3et
        0x4at
        -0x80t
        0x68t
        -0x42t
        -0x18t
        0x2et
        0x0t
        -0x30t
        -0x2ft
        0x2t
        -0x62t
        0x7et
        0x57t
        0x6et
        -0x14t
        0x5dt
        0x2dt
        0x29t
        -0x80t
        0x6ft
        -0x55t
        -0x6dt
        -0x48t
        -0x1at
        0x36t
        -0x31t
        -0x15t
        0x31t
        -0x52t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final readS0(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 51
    const-string v0, "Handshake"

    const-string v1, "readS0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    .line 53
    .local v0, "s0":B
    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 54
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 55
    new-instance v1, Ljava/io/IOException;

    const-string v2, "InputStream closed"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid RTMP protocol version; expected 3, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :cond_1
    return-void
.end method

.method public final readS1(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    const-string v0, "Handshake"

    const-string v1, "readS1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const/16 v0, 0x600

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/Handshake;->s1:[B

    .line 142
    const/4 v1, 0x0

    .line 145
    .local v1, "totalBytesRead":I
    :cond_0
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/Handshake;->s1:[B

    rsub-int v3, v1, 0x600

    invoke-virtual {p1, v2, v1, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 146
    .local v2, "read":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 147
    add-int/2addr v1, v2

    .line 149
    :cond_1
    if-lt v1, v0, :cond_0

    .line 151
    if-ne v1, v0, :cond_2

    .line 158
    const-string v0, "Handshake"

    const-string v3, "readS1(): S1 total bytes read OK"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void

    .line 152
    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected EOF while reading S1, expected 1536 bytes, but only read "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final readS2(Ljava/io/InputStream;)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    const-string v0, "Handshake"

    const-string v1, "readS2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const/4 v0, 0x4

    new-array v1, v0, [B

    .line 176
    .local v1, "sr_serverTime":[B
    new-array v2, v0, [B

    .line 177
    .local v2, "s2_serverVersion":[B
    const/16 v3, 0x5f8

    new-array v4, v3, [B

    .line 180
    .local v4, "s2_rest":[B
    const/4 v5, 0x0

    .line 183
    .local v5, "totalBytesRead":I
    :cond_0
    rsub-int/lit8 v6, v5, 0x4

    invoke-virtual {p1, v1, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 184
    .local v6, "read":I
    const/4 v7, -0x1

    if-eq v6, v7, :cond_7

    .line 188
    add-int/2addr v5, v6

    .line 190
    if-lt v5, v0, :cond_0

    .line 193
    const/4 v5, 0x0

    .line 195
    :cond_1
    rsub-int/lit8 v8, v5, 0x4

    invoke-virtual {p1, v2, v5, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 196
    if-eq v6, v7, :cond_6

    .line 200
    add-int/2addr v5, v6

    .line 202
    if-lt v5, v0, :cond_1

    .line 205
    const/16 v0, 0x5f8

    .line 206
    .local v0, "remainingBytes":I
    const/4 v5, 0x0

    .line 208
    :cond_2
    rsub-int v8, v5, 0x5f8

    invoke-virtual {p1, v4, v5, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 209
    if-eq v6, v7, :cond_3

    .line 210
    add-int/2addr v5, v6

    .line 212
    :cond_3
    if-ge v5, v3, :cond_4

    if-ne v6, v7, :cond_2

    .line 214
    :cond_4
    if-ne v5, v3, :cond_5

    .line 221
    const-string v3, "Handshake"

    const-string v7, "readS2(): S2 total bytes read OK"

    invoke-static {v3, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void

    .line 215
    :cond_5
    new-instance v3, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected EOF while reading remainder of S2, expected 1528 bytes, but only read "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, " bytes"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 198
    .end local v0    # "remainingBytes":I
    :cond_6
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Unexpected EOF while reading S2 bytes 4-7"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_7
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Unexpected EOF while reading S2 bytes 0-3"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final writeC0(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 46
    const-string v0, "Handshake"

    const-string v1, "writeC0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 48
    return-void
.end method

.method public final writeC1(Ljava/io/OutputStream;)V
    .locals 14
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 65
    const-string v0, "Handshake"

    const-string v1, "writeC1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v0, "Handshake"

    const-string v1, "writeC1(): Calculating digest offset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 70
    .local v0, "random":Ljava/util/Random;
    const/16 v1, 0x2d0

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 76
    .local v1, "digestOffset":I
    rem-int/lit16 v2, v1, 0x2d8

    const/16 v3, 0x304

    add-int/2addr v2, v3

    const/4 v4, 0x4

    add-int/2addr v2, v4

    .line 77
    .local v2, "absoluteDigestOffset":I
    const-string v5, "Handshake"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeC1(): (real value of) digestOffset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v5, "Handshake"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeC1(): recalculated digestOffset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    move v5, v1

    .line 82
    .local v5, "remaining":I
    new-array v6, v4, [B

    .line 83
    .local v6, "digestOffsetBytes":[B
    const/4 v7, 0x3

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_1

    .line 84
    const/16 v8, 0xff

    if-le v5, v8, :cond_0

    .line 85
    const/4 v8, -0x1

    aput-byte v8, v6, v7

    .line 86
    add-int/lit16 v5, v5, -0xff

    goto :goto_1

    .line 88
    :cond_0
    int-to-byte v8, v5

    aput-byte v8, v6, v7

    .line 89
    sub-int/2addr v5, v5

    .line 83
    :goto_1
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 95
    .end local v7    # "i":I
    :cond_1
    const-string v7, "Handshake"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "writeC1(): digestOffsetBytes: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6}, Lcom/kint/kintframeworkaosaar/Util;->toHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-array v7, v2, [B

    .line 100
    .local v7, "partBeforeDigest":[B
    const-string v8, "Handshake"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "partBeforeDigest(): size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v10, v7

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v0, v7}, Ljava/util/Random;->nextBytes([B)V

    .line 103
    const-string v8, "Handshake"

    const-string v9, "writeC1(): Writing timestamp and Flash Player version"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v8, v8

    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/Util;->unsignedInt32ToByteArray(I)[B

    move-result-object v8

    .line 105
    .local v8, "timeStamp":[B
    const/4 v9, 0x0

    invoke-static {v8, v9, v7, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    new-array v10, v4, [B

    fill-array-data v10, :array_0

    invoke-static {v10, v9, v7, v4, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    rsub-int v10, v2, 0x600

    add-int/lit8 v10, v10, -0x20

    new-array v10, v10, [B

    .line 114
    .local v10, "partAfterDigest":[B
    const-string v11, "Handshake"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "partAfterDigest(): size: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v13, v10

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v0, v10}, Ljava/util/Random;->nextBytes([B)V

    .line 118
    const-string v11, "Handshake"

    const-string v12, "copying digest offset bytes in partBeforeDigest"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-static {v6, v9, v7, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    const-string v3, "Handshake"

    const-string v4, "writeC1(): Calculating digest"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/16 v3, 0x5e0

    new-array v3, v3, [B

    .line 122
    .local v3, "tempBuffer":[B
    array-length v4, v7

    invoke-static {v7, v9, v3, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    array-length v4, v7

    array-length v11, v10

    invoke-static {v10, v9, v3, v4, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    new-instance v4, Lcom/kint/kintframeworkaosaar/Crypto;

    invoke-direct {v4}, Lcom/kint/kintframeworkaosaar/Crypto;-><init>()V

    .line 127
    .local v4, "crypto":Lcom/kint/kintframeworkaosaar/Crypto;
    sget-object v9, Lcom/kint/kintframeworkaosaar/Handshake;->GENUINE_FP_KEY:[B

    const/16 v11, 0x1e

    invoke-virtual {v4, v3, v9, v11}, Lcom/kint/kintframeworkaosaar/Crypto;->calculateHmacSHA256([B[BI)[B

    move-result-object v9

    .line 130
    .local v9, "digest":[B
    const-string v11, "Handshake"

    const-string v12, "writeC1(): writing C1 packet"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-virtual {p1, v7}, Ljava/io/OutputStream;->write([B)V

    .line 132
    invoke-virtual {p1, v9}, Ljava/io/OutputStream;->write([B)V

    .line 133
    invoke-virtual {p1, v10}, Ljava/io/OutputStream;->write([B)V

    .line 134
    return-void

    :array_0
    .array-data 1
        -0x80t
        0x0t
        0x7t
        0x2t
    .end array-data
.end method

.method public final writeC2(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    const-string v0, "Handshake"

    const-string v1, "writeC2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Handshake;->s1:[B

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Handshake;->s1:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 170
    return-void

    .line 167
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "C2 cannot be written without S1 being read first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
