.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 215
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public SendMessage(I)V
    .locals 9
    .param p1, "messageType"    # I

    .line 218
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    .line 360
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 361
    .local v0, "popDialog":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/widget/SeekBar;

    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 362
    .local v1, "seekBar":Landroid/widget/SeekBar;
    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 363
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setKeyProgressIncrement(I)V

    .line 365
    const-string v2, "Please Select Into Your Desired Brightness "

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 366
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 369
    new-instance v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$9;

    invoke-direct {v2, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$9;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;)V

    .line 389
    .local v2, "onSeekBarChangeListener":Landroid/widget/SeekBar$OnSeekBarChangeListener;
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 392
    .end local v0    # "popDialog":Landroid/app/AlertDialog$Builder;
    .end local v1    # "seekBar":Landroid/widget/SeekBar;
    .end local v2    # "onSeekBarChangeListener":Landroid/widget/SeekBar$OnSeekBarChangeListener;
    goto/16 :goto_1

    .line 307
    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v0, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "IT/Computer"

    const-string v2, "Game"

    const-string v3, "Fashion"

    const-string v4, "VR"

    const-string v5, "Kidult"

    const-string v6, "Sports"

    const-string v7, "Music"

    const-string v8, "Movie"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "items":[Ljava/lang/String;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 311
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    const-string v3, "Select favorite things"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/16 v4, 0x8

    new-array v4, v4, [Z

    fill-array-data v4, :array_0

    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$8;

    invoke-direct {v5, p0, v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$8;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;Ljava/util/List;[Ljava/lang/String;)V

    .line 312
    invoke-virtual {v3, v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "Confirm"

    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;

    invoke-direct {v5, p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;Ljava/util/List;)V

    .line 330
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 353
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 356
    .end local v0    # "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "items":[Ljava/lang/String;
    .end local v2    # "dialog":Landroid/app/AlertDialog$Builder;
    goto/16 :goto_1

    .line 267
    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetMainActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 268
    .local v1, "alertDialog":Landroid/app/AlertDialog$Builder;
    const-string v2, "methinks Login"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 269
    const-string v2, "Enter methinks user code."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 270
    new-instance v2, Landroid/widget/EditText;

    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetMainActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 271
    .local v2, "editText":Landroid/widget/EditText;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 273
    const-string v3, "Confirm"

    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$4;

    invoke-direct {v4, p0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$4;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 283
    const-string v3, "Cancel"

    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$5;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$5;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 293
    new-instance v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$6;

    invoke-direct {v3, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$6;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 300
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 301
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 303
    .end local v1    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v2    # "editText":Landroid/widget/EditText;
    goto :goto_1

    .line 222
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetMainActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 223
    .restart local v1    # "alertDialog":Landroid/app/AlertDialog$Builder;
    const-string v2, "methinks Login"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 225
    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->LoginTryCount:I

    if-nez v2, :cond_1

    .line 226
    const-string v2, "Enter methinks user code."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 228
    :cond_1
    const-string v2, "Enter user code from methinks app."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 230
    :goto_0
    new-instance v2, Landroid/widget/EditText;

    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetMainActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 231
    .restart local v2    # "editText":Landroid/widget/EditText;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 233
    const-string v3, "Confirm"

    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$1;

    invoke-direct {v4, p0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 243
    const-string v3, "Cancel"

    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$2;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 253
    new-instance v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$3;

    invoke-direct {v3, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 261
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 263
    .end local v1    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v2    # "editText":Landroid/widget/EditText;
    nop

    .line 397
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method
