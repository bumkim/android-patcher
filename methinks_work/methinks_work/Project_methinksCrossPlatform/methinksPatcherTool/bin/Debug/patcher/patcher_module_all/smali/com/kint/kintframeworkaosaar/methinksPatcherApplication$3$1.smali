.class Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;
.super Ljava/lang/Thread;
.source "methinksPatcherApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    .line 1373
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 1376
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 1377
    .local v0, "messagePack":Lcom/kint/kintframeworkaosaar/MessagePack;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x7d9de7e2

    const/4 v5, 0x1

    if-eq v3, v4, :cond_1

    const v4, 0x5c163e31

    if-eq v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "prepare_login"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_1
    const-string v1, "prepare_question"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, -0x1

    :goto_1
    const/4 v2, 0x2

    packed-switch v1, :pswitch_data_0

    .line 1438
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$400(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;Lcom/kint/kintframeworkaosaar/MessagePack;)V

    goto :goto_3

    .line 1411
    :pswitch_0
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v3, "http://221.165.42.119/methinksPatcher/question_range.png"

    invoke-direct {v1, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1414
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    .line 1415
    .local v3, "httpURLConnection":Ljava/net/HttpURLConnection;
    invoke-virtual {v3, v5}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 1416
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 1417
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 1418
    .local v4, "inputStream":Ljava/io/InputStream;
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1420
    .local v5, "loginBitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v6, v6, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v6, v6, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 1421
    .local v6, "msg":Landroid/os/Message;
    const/16 v7, 0x9

    iput v7, v6, Landroid/os/Message;->what:I

    .line 1422
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v7, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v7, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1424
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1430
    .end local v1    # "url":Ljava/net/URL;
    .end local v3    # "httpURLConnection":Ljava/net/HttpURLConnection;
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "loginBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "msg":Landroid/os/Message;
    goto :goto_2

    .line 1426
    :catch_0
    move-exception v1

    .line 1428
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "methinksPatcherApp."

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1429
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1432
    .end local v1    # "e":Ljava/io/IOException;
    :goto_2
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I

    .line 1434
    goto :goto_3

    .line 1393
    :pswitch_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1394
    .local v1, "msg":Landroid/os/Message;
    const/4 v3, 0x6

    iput v3, v1, Landroid/os/Message;->what:I

    .line 1395
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1403
    .end local v1    # "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;I)I

    .line 1405
    nop

    .line 1442
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
