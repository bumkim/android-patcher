.class public Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field public static ApplicationContext:Landroid/content/Context;

.field private static __instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

.field public static patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private _callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

.field private _gameObjectName:Ljava/lang/String;

.field private _sendToMethodName:Ljava/lang/String;

.field public _viewDeveloperMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "methinksPatcherBridge"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->TAG:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    .line 214
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    return-void
.end method

.method public static Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .locals 1

    .line 41
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 43
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    return-object v0
.end method


# virtual methods
.method public FirstInitialize(Landroid/app/Activity;)V
    .locals 1
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 49
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    .line 51
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SetMainActivity(Landroid/app/Activity;)V

    .line 53
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 54
    return-void
.end method

.method public Initialize(Ljava/lang/String;Ljava/lang/String;ZZIZ)V
    .locals 6
    .param p1, "rtmpServerBaseURL"    # Ljava/lang/String;
    .param p2, "projectID"    # Ljava/lang/String;
    .param p3, "release"    # Z
    .param p4, "disable"    # Z
    .param p5, "cheatKey"    # I
    .param p6, "enableBackground"    # Z

    .line 181
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Initialize(Ljava/lang/String;Ljava/lang/String;ZIZ)V

    .line 182
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SetCallbackMessage(Lcom/kint/kintframeworkaosaar/methinksPatcherApplication$CallbackMessage;)V

    .line 184
    if-nez p4, :cond_0

    .line 185
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetMainActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnCreate(Landroid/app/Activity;)V

    .line 186
    :cond_0
    return-void
.end method

.method public OnActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 4
    .param p1, "mainActivity"    # Landroid/app/Activity;
    .param p2, "requestCode"    # I
    .param p3, "resultCode"    # I
    .param p4, "data"    # Landroid/content/Intent;

    .line 103
    const/16 v0, 0x3e8

    const/4 v1, -0x1

    if-ne v1, p3, :cond_0

    .line 105
    const-string v1, "methinksPatcherBridge"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult -  resultCode : RESULT_OK , requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 110
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "settingsSystemIntent":Landroid/content/Intent;
    const/16 v1, 0x3e9

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 113
    .end local v0    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_0

    .line 117
    :pswitch_1
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v1

    .line 118
    .local v1, "captureIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 120
    .end local v1    # "captureIntent":Landroid/content/Intent;
    goto :goto_0

    .line 124
    :pswitch_2
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p3, p4}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SetMediaProjection(ILandroid/content/Intent;)V

    .line 125
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->Login()V

    .line 127
    :goto_0
    goto :goto_3

    .line 132
    :cond_0
    const-string v1, "methinksPatcherBridge"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult -  resultCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " , requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-wide/16 v1, 0x7d0

    const/4 v3, 0x0

    packed-switch p2, :pswitch_data_1

    goto :goto_3

    .line 137
    :pswitch_3
    const-string v0, "Draw Overlay Permission Required. Restart App."

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 140
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    goto :goto_1

    .line 142
    :catch_0
    move-exception v0

    .line 144
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 147
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :goto_1
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 148
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 150
    goto :goto_3

    .line 171
    :pswitch_4
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v1

    .line 172
    .restart local v1    # "captureIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 154
    .end local v1    # "captureIntent":Landroid/content/Intent;
    :pswitch_5
    const-string v0, "Screen Capture Permission Required. Restart App."

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 157
    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 162
    goto :goto_2

    .line 159
    :catch_1
    move-exception v0

    .line 161
    .restart local v0    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 163
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :goto_2
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 164
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 166
    nop

    .line 177
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public OnCreate(Landroid/app/Activity;)V
    .locals 5
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 59
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->GetProcessStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 60
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 63
    .local v0, "firstRequestOverlayPermission":Z
    const/16 v2, 0x3e9

    if-ne v1, v0, :cond_3

    .line 65
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v1, v3, :cond_2

    .line 67
    invoke-static {p1}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 68
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 69
    .local v1, "intent":Landroid/content/Intent;
    const/16 v2, 0x3ea

    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 71
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v1, "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 73
    .end local v1    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_1

    .line 77
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    .restart local v1    # "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 79
    .end local v1    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_1

    .line 83
    :cond_3
    iget v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    if-nez v3, :cond_5

    .line 85
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->settingsSystemShowTouch()Z

    move-result v3

    if-nez v3, :cond_4

    .line 87
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    .local v3, "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v3, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 89
    .end local v3    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_0

    .line 92
    :cond_4
    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v2

    .line 93
    .local v2, "captureIntent":Landroid/content/Intent;
    const/16 v3, 0x3e8

    invoke-virtual {p1, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95
    .end local v2    # "captureIntent":Landroid/content/Intent;
    :goto_0
    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    .line 98
    :cond_5
    :goto_1
    return-void
.end method

.method public Preset4Unity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "gameObjectName"    # Ljava/lang/String;
    .param p2, "sendToMethodName"    # Ljava/lang/String;

    .line 190
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SetPatchIntegrateMode(I)V

    .line 191
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_gameObjectName:Ljava/lang/String;

    .line 192
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_sendToMethodName:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public SendMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 202
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->SendRequestMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method public Tooltip(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "echo"    # Z

    .line 207
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 208
    nop

    .line 212
    return-void
.end method

.method public Uninitialize()V
    .locals 0

    .line 198
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 428
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 429
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 441
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityDestroyed(Landroid/app/Activity;)V

    .line 442
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 422
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityPaused(Landroid/app/Activity;)V

    .line 423
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 435
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityResumed(Landroid/app/Activity;)V

    .line 436
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 416
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 417
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 410
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityStarted(Landroid/app/Activity;)V

    .line 411
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 404
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherApplication;->OnActivityStopped(Landroid/app/Activity;)V

    .line 405
    return-void
.end method
