.class public Lcom/kint/kintframeworkaosaar/methinksMainActivity;
.super Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;
.source "methinksMainActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 56
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 57
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 16
    invoke-super/range {p0 .. p1}, Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/4 v0, 0x0

    .line 19
    .local v0, "releaseMode":Z
    const-string v1, "debug_mode"

    .line 20
    .local v1, "debugModeString":Ljava/lang/String;
    const-string v2, "release_mode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 21
    const/4 v0, 0x1

    .line 23
    :cond_0
    const/4 v2, 0x0

    .line 24
    .local v2, "disable":Z
    const-string v9, "enable"

    .line 25
    .local v9, "enableString":Ljava/lang/String;
    const-string v3, "disable"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 26
    const/4 v2, 0x1

    .line 28
    .end local v2    # "disable":Z
    .local v10, "disable":Z
    :cond_1
    move v10, v2

    const/4 v2, 0x0

    .line 29
    .local v2, "patchMode":Z
    const-string v11, "patch_mode"

    .line 30
    .local v11, "patchModeString":Ljava/lang/String;
    const-string v3, "integrate_mode"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 31
    const/4 v2, 0x1

    .line 33
    .end local v2    # "patchMode":Z
    .local v12, "patchMode":Z
    :cond_2
    move v12, v2

    const/4 v2, 0x0

    .line 34
    .local v2, "enableBackground":Z
    const-string v13, "disable_background"

    .line 35
    .local v13, "enableBackgroundString":Ljava/lang/String;
    const-string v3, "enable_background"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 36
    const/4 v2, 0x1

    .line 38
    .end local v2    # "enableBackground":Z
    .local v14, "enableBackground":Z
    :cond_3
    move v14, v2

    const v15, 0x1fa548

    .line 39
    .local v15, "cheatKey":I
    const/16 v16, -0x3e8

    .line 41
    .local v16, "backgroundTimeoutTickCount":I
    const-string v2, "methinksMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "releaseMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, " , disable : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, " , patchMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, " , enableBackground : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v2

    move-object/from16 v8, p0

    invoke-virtual {v2, v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->FirstInitialize(Landroid/app/Activity;)V

    .line 47
    const/4 v2, 0x1

    if-ne v2, v12, :cond_4

    .line 49
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v2

    const-string v3, ""

    const-string v4, "projectID"

    move v5, v0

    move v6, v10

    move v7, v15

    move v8, v14

    invoke-virtual/range {v2 .. v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Initialize(Ljava/lang/String;Ljava/lang/String;ZZIZ)V

    .line 51
    :cond_4
    return-void
.end method
