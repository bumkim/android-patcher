.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->SendMessage(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;

.field final synthetic val$selectedItems:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;

    .line 331
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;->val$selectedItems:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 335
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 341
    :cond_0
    const-string v0, ""

    .line 342
    .local v0, "items":Ljava/lang/String;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 344
    .local v2, "selectedItem":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 345
    .end local v2    # "selectedItem":Ljava/lang/String;
    goto :goto_0

    .line 347
    :cond_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 349
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 352
    .end local v0    # "items":Ljava/lang/String;
    :goto_1
    return-void
.end method
