.class public final enum Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
.super Ljava/lang/Enum;
.source "RtmpHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/RtmpHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum ABORT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum ACKNOWLEDGEMENT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum AGGREGATE_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum AUDIO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum COMMAND_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum DATA_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum DATA_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum SET_CHUNK_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum SET_PEER_BANDWIDTH:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum SHARED_OBJECT_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum SHARED_OBJECT_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum USER_CONTROL_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum VIDEO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field public static final enum WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field private static final quickLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Byte;",
            "Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private value:B


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 36
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SET_CHUNK_SIZE"

    invoke-direct {v0, v3, v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_CHUNK_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 42
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x2

    const-string v4, "ABORT"

    invoke-direct {v0, v4, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ABORT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 49
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v4, 0x3

    const-string v5, "ACKNOWLEDGEMENT"

    invoke-direct {v0, v5, v3, v4}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ACKNOWLEDGEMENT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 56
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v5, 0x4

    const-string v6, "USER_CONTROL_MESSAGE"

    invoke-direct {v0, v6, v4, v5}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->USER_CONTROL_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 62
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v6, 0x5

    const-string v7, "WINDOW_ACKNOWLEDGEMENT_SIZE"

    invoke-direct {v0, v7, v5, v6}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 69
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v7, 0x6

    const-string v8, "SET_PEER_BANDWIDTH"

    invoke-direct {v0, v8, v6, v7}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_PEER_BANDWIDTH:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 73
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v8, 0x8

    const-string v9, "AUDIO"

    invoke-direct {v0, v9, v7, v8}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->AUDIO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 77
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v9, 0x7

    const/16 v10, 0x9

    const-string v11, "VIDEO"

    invoke-direct {v0, v11, v9, v10}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->VIDEO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 84
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v11, 0xf

    const-string v12, "DATA_AMF3"

    invoke-direct {v0, v12, v8, v11}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->DATA_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 91
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const-string v12, "SHARED_OBJECT_AMF3"

    const/16 v13, 0x10

    invoke-direct {v0, v12, v10, v13}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SHARED_OBJECT_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 99
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v12, 0xa

    const-string v13, "COMMAND_AMF3"

    const/16 v14, 0x11

    invoke-direct {v0, v13, v12, v14}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 106
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v13, 0xb

    const-string v14, "DATA_AMF0"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v13, v15}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->DATA_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 114
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v14, 0xc

    const-string v15, "COMMAND_AMF0"

    const/16 v13, 0x14

    invoke-direct {v0, v15, v14, v13}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 121
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v13, 0xd

    const-string v15, "SHARED_OBJECT_AMF0"

    const/16 v14, 0x13

    invoke-direct {v0, v15, v13, v14}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SHARED_OBJECT_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 125
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const-string v14, "AGGREGATE_MESSAGE"

    const/16 v15, 0xe

    const/16 v13, 0x16

    invoke-direct {v0, v14, v15, v13}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->AGGREGATE_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 30
    new-array v0, v11, [Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    sget-object v11, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_CHUNK_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v11, v0, v1

    sget-object v11, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ABORT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v11, v0, v2

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ACKNOWLEDGEMENT:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->USER_CONTROL_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v4

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v5

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_PEER_BANDWIDTH:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v6

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->AUDIO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v7

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->VIDEO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v9

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->DATA_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v8

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SHARED_OBJECT_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v10

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF3:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    aput-object v2, v0, v12

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->DATA_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v3, 0xb

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v3, 0xc

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SHARED_OBJECT_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v3, 0xd

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->AGGREGATE_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/16 v3, 0xe

    aput-object v2, v0, v3

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->$VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->quickLookupMap:Ljava/util/Map;

    .line 130
    invoke-static {}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->values()[Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 131
    .local v3, "messageTypId":Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    sget-object v4, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->quickLookupMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->getValue()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    .end local v3    # "messageTypId":Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    int-to-byte p1, p3

    iput-byte p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->value:B

    .line 137
    return-void
.end method

.method public static valueOf(B)Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    .locals 3
    .param p0, "messageTypeId"    # B

    .line 145
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    return-object v0

    .line 148
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type byte: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/Util;->toHexString(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 30
    const-class v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    .locals 1

    .line 30
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->$VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    return-object v0
.end method


# virtual methods
.method public getValue()B
    .locals 1

    .line 141
    iget-byte v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->value:B

    return v0
.end method
