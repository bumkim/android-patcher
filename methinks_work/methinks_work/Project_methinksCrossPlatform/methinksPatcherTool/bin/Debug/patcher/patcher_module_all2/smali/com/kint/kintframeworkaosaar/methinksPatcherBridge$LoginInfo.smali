.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LoginInfo"
.end annotation


# instance fields
.field private _campaignName:Ljava/lang/String;

.field private _error:Ljava/lang/String;

.field private _isCameraStreamAllowed:Z

.field private _isScreenCaptureAllowed:Z

.field private _isScreenStreamAllowed:Z

.field private _minimumTestBuildNumber:I

.field private _notice:Ljava/lang/String;

.field private _permissionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private _screenName:Ljava/lang/String;

.field private _status:Ljava/lang/String;

.field private _statusResultAnnouncementCount:I

.field private _statusResultScreenName:Ljava/lang/String;

.field private _statusResultStartDate:J

.field private _statusResultStatus:Ljava/lang/String;

.field private _statusResultSurveyCount:I

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 709
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public CheckValidData()Z
    .locals 19

    .line 774
    move-object/from16 v0, p0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 775
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 777
    .local v2, "date":Ljava/util/Date;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-long v4, v4

    .line 778
    .local v4, "year":J
    const/4 v6, 0x2

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/2addr v6, v3

    int-to-long v6, v6

    .line 779
    .local v6, "month":J
    const/4 v8, 0x5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    .line 780
    .local v8, "day":J
    const/16 v10, 0xb

    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    int-to-long v10, v10

    .line 781
    .local v10, "hour":J
    const-wide/32 v12, 0xf4240

    mul-long v12, v12, v4

    const-wide/16 v14, 0x2710

    mul-long v14, v14, v6

    add-long/2addr v12, v14

    const-wide/16 v14, 0x64

    mul-long v14, v14, v8

    add-long/2addr v12, v14

    add-long/2addr v12, v10

    .line 783
    .local v12, "nowDateTime":J
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    .line 785
    .local v14, "nowDateTimeExceptTick":J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v1

    .end local v1    # "calendar":Ljava/util/Calendar;
    .local v17, "calendar":Ljava/util/Calendar;
    const-string v1, "[CheckValidate] - nowDataTime : "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " , nowDateTimeExceptTick : "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " , StartDateTime : "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v18, v2

    .end local v2    # "date":Ljava/util/Date;
    .local v18, "date":Ljava/util/Date;
    iget-wide v1, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStartDate:J

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "methinksPatcherBridge"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    iget-wide v1, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStartDate:J

    cmp-long v3, v14, v1

    if-ltz v3, :cond_0

    .line 787
    const/4 v1, 0x1

    return v1

    .line 788
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public GetError()Ljava/lang/String;
    .locals 1

    .line 769
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_error:Ljava/lang/String;

    return-object v0
.end method

.method public GetResultStatus()Ljava/lang/String;
    .locals 1

    .line 764
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    return-object v0
.end method

.method public GetStartData()J
    .locals 2

    .line 759
    iget-wide v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStartDate:J

    return-wide v0
.end method

.method public IsCameraStreamAllowed()Z
    .locals 1

    .line 748
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isCameraStreamAllowed:Z

    return v0
.end method

.method public IsScreenCaptureAllowed()Z
    .locals 1

    .line 729
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenCaptureAllowed:Z

    return v0
.end method

.method public IsScreenStreamAllowed()Z
    .locals 1

    .line 739
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenStreamAllowed:Z

    return v0
.end method

.method public Parse(Lorg/json/JSONObject;)Landroid/os/Message;
    .locals 10
    .param p1, "responseJSON"    # Lorg/json/JSONObject;

    .line 824
    const-string v0, "screenName"

    const-string v1, "invalidUserCode"

    const-string v2, "invalidProject"

    const-string v3, "error"

    const-string v4, "status"

    const/4 v5, 0x0

    .line 827
    .local v5, "message":Landroid/os/Message;
    :try_start_0
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_status:Ljava/lang/String;

    .line 828
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_status:Ljava/lang/String;

    const-string v7, "ok"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v7, "methinksPatcherBridge"

    if-eqz v6, :cond_2

    .line 830
    :try_start_1
    const-string v1, "isScreenCaptureAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenCaptureAllowed:Z

    .line 831
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isScreenCaptureAllowed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenCaptureAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    const-string v1, "isScreenStreamAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenStreamAllowed:Z

    .line 833
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isScreenStreamAllowed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenStreamAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    const-string v1, "isCameraStreamAllowed"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isCameraStreamAllowed:Z

    .line 835
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCameraStreamAllowed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isCameraStreamAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    const-string v1, "notice"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_notice:Ljava/lang/String;

    .line 837
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notice : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_notice:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    const-string v1, "permissions"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 839
    .local v1, "permissionsJsonArray":Lorg/json/JSONArray;
    if-eqz v1, :cond_1

    .line 841
    const/4 v2, 0x0

    .local v2, "countIndex":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 843
    if-nez v2, :cond_0

    .line 844
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_permissionList:Ljava/util/List;

    .line 845
    :cond_0
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_permissionList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 841
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 855
    .end local v2    # "countIndex":I
    :cond_1
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_screenName:Ljava/lang/String;

    .line 856
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "screenName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_screenName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    const-string v2, "campaignName"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_campaignName:Ljava/lang/String;

    .line 858
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "campaignName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_campaignName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    const-string v2, "minimumTestBuildNumber"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_minimumTestBuildNumber:I

    .line 860
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "minimumTestBuildNumber : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_minimumTestBuildNumber:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    const-string v2, "statusResult"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 862
    .local v2, "statusResultJSON":Lorg/json/JSONObject;
    const-string v3, "startDate"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStartDate:J

    .line 863
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "statusResultStartDate : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStartDate:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultScreenName:Ljava/lang/String;

    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "statusResultScreenName : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultScreenName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    .line 868
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "statusResultStatus : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultStatus:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    const-string v0, "surveyCount"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultSurveyCount:I

    .line 870
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "statusResultSurveyCount : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultSurveyCount:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    const-string v0, "announcementCount"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultAnnouncementCount:I

    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "statusResultAnnouncementCount : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_statusResultAnnouncementCount:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    move-object v5, v0

    .line 884
    const/4 v0, 0x7

    iput v0, v5, Landroid/os/Message;->what:I

    .line 886
    .end local v1    # "permissionsJsonArray":Lorg/json/JSONArray;
    .end local v2    # "statusResultJSON":Lorg/json/JSONObject;
    goto :goto_1

    .line 887
    :cond_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_status:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 889
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_error:Ljava/lang/String;

    .line 890
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Login Failure : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_error:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_error:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/16 v3, 0x8

    if-eqz v0, :cond_3

    .line 894
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    .line 897
    const-string v0, "Application Quit"

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Ljava/lang/String;

    .line 900
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    move-object v5, v0

    .line 901
    iput v3, v5, Landroid/os/Message;->what:I

    goto :goto_1

    .line 904
    :cond_3
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_error:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 906
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    .line 907
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Ljava/lang/String;

    .line 909
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    move-object v5, v0

    .line 910
    iput v3, v5, Landroid/os/Message;->what:I

    .line 915
    :cond_4
    :goto_1
    iput-object p0, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 916
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 923
    goto :goto_2

    .line 920
    :catch_0
    move-exception v0

    .line 922
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 925
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_2
    return-object v5
.end method

.method public SetCameraStreamAllowed(Z)V
    .locals 0
    .param p1, "isCameraStreamAllowed"    # Z

    .line 753
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isCameraStreamAllowed:Z

    .line 754
    return-void
.end method

.method public SetScreenCaptureAllowed(Z)V
    .locals 0
    .param p1, "isScreenCaptureAllowed"    # Z

    .line 734
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenCaptureAllowed:Z

    .line 735
    return-void
.end method

.method public SetScreenStreamAllowed(Z)V
    .locals 0
    .param p1, "isScreenStreamAllowed"    # Z

    .line 743
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->_isScreenStreamAllowed:Z

    .line 744
    return-void
.end method
