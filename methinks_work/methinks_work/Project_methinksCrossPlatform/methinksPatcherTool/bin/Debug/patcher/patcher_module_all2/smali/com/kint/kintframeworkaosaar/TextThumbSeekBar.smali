.class public Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;
.super Landroid/widget/SeekBar;
.source "TextThumbSeekBar.java"


# instance fields
.field private mTextPaint:Landroid/text/TextPaint;

.field private mThumbSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 25
    const v0, 0x101007b

    invoke-direct {p0, p1, p2, v0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/16 v0, 0xc8

    iput v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mThumbSize:I

    .line 34
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    .line 35
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 36
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 37
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 38
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    monitor-enter p0

    .line 45
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 47
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getProgress()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "progressText":Ljava/lang/String;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 49
    .local v1, "bounds":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 51
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getThumbOffset()I

    move-result v3

    sub-int/2addr v2, v3

    .line 52
    .local v2, "leftPadding":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getThumbOffset()I

    move-result v4

    sub-int/2addr v3, v4

    .line 53
    .local v3, "rightPadding":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getWidth()I

    move-result v4

    sub-int/2addr v4, v2

    sub-int/2addr v4, v3

    .line 54
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getProgress()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getMax()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 55
    .local v5, "progressRatio":F
    iget v6, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mThumbSize:I

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    sub-float/2addr v7, v5

    mul-float v6, v6, v7

    .line 56
    .local v6, "thumbOffset":F
    int-to-float v7, v4

    mul-float v7, v7, v5

    int-to-float v8, v2

    add-float/2addr v7, v8

    add-float/2addr v7, v6

    .line 57
    .local v7, "thumbX":F
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->getHeight()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v10, v9

    add-float/2addr v8, v10

    .line 60
    .local v8, "thumbY":F
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 44
    .end local v0    # "progressText":Ljava/lang/String;
    .end local v1    # "bounds":Landroid/graphics/Rect;
    .end local v2    # "leftPadding":I
    .end local v3    # "rightPadding":I
    .end local v4    # "width":I
    .end local v5    # "progressRatio":F
    .end local v6    # "thumbOffset":F
    .end local v7    # "thumbX":F
    .end local v8    # "thumbY":F
    .end local p0    # "this":Lcom/kint/kintframeworkaosaar/TextThumbSeekBar;
    .end local p1    # "canvas":Landroid/graphics/Canvas;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
