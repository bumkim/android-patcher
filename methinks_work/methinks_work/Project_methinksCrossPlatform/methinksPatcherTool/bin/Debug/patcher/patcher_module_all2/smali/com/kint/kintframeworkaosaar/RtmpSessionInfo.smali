.class public Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;
.super Ljava/lang/Object;
.source "RtmpSessionInfo.java"


# instance fields
.field private acknowledgementWindowSize:I

.field private chunkChannels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;",
            ">;"
        }
    .end annotation
.end field

.field private invokedMethods:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rxChunkSize:I

.field private totalBytesRead:I

.field private txChunkSize:I

.field private windowBytesRead:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const v0, 0x7fffffff

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->acknowledgementWindowSize:I

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->totalBytesRead:I

    .line 22
    const/16 v0, 0x80

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->rxChunkSize:I

    .line 23
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->txChunkSize:I

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->chunkChannels:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->invokedMethods:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addInvokedCommand(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "transactionId"    # I
    .param p2, "commandName"    # Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->invokedMethods:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final addToWindowBytesRead(ILcom/kint/kintframeworkaosaar/RtmpPacket;)V
    .locals 2
    .param p1, "numBytes"    # I
    .param p2, "packet"    # Lcom/kint/kintframeworkaosaar/RtmpPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/kint/kintframeworkaosaar/WindowAckRequired;
        }
    .end annotation

    .line 79
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->windowBytesRead:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->windowBytesRead:I

    .line 80
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->totalBytesRead:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->totalBytesRead:I

    .line 81
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->windowBytesRead:I

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->acknowledgementWindowSize:I

    if-ge v0, v1, :cond_0

    .line 85
    return-void

    .line 82
    :cond_0
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->windowBytesRead:I

    .line 83
    new-instance v0, Lcom/kint/kintframeworkaosaar/WindowAckRequired;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->totalBytesRead:I

    invoke-direct {v0, v1, p2}, Lcom/kint/kintframeworkaosaar/WindowAckRequired;-><init>(ILcom/kint/kintframeworkaosaar/RtmpPacket;)V

    throw v0
.end method

.method public getAcknowledgementWindowSize()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->acknowledgementWindowSize:I

    return v0
.end method

.method public getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .locals 3
    .param p1, "chunkStreamId"    # I

    .line 29
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->chunkChannels:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 30
    .local v0, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    if-nez v0, :cond_0

    .line 32
    new-instance v1, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;-><init>()V

    move-object v0, v1

    .line 33
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->chunkChannels:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    :cond_0
    return-object v0
.end method

.method public getRxChunkSize()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->rxChunkSize:I

    return v0
.end method

.method public getTxChunkSize()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->txChunkSize:I

    return v0
.end method

.method public setAcknowledgmentWindowSize(I)V
    .locals 0
    .param p1, "acknowledgementWindowSize"    # I

    .line 68
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->acknowledgementWindowSize:I

    .line 69
    return-void
.end method

.method public setRxChunkSize(I)V
    .locals 0
    .param p1, "chunkSize"    # I

    .line 52
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->rxChunkSize:I

    .line 53
    return-void
.end method

.method public setTxChunkSize(I)V
    .locals 0
    .param p1, "chunkSize"    # I

    .line 60
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->txChunkSize:I

    .line 61
    return-void
.end method

.method public takeInvokedCommand(I)Ljava/lang/String;
    .locals 2
    .param p1, "transactionId"    # I

    .line 40
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->invokedMethods:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
