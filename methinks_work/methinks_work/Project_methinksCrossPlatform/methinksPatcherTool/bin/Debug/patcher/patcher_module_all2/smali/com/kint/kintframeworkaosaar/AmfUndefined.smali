.class public Lcom/kint/kintframeworkaosaar/AmfUndefined;
.super Ljava/lang/Object;
.source "AmfUndefined.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/AmfData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static writeUndefinedTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p0, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 27
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->UNDEFINED:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 28
    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method public readFrom(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .line 24
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 18
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->UNDEFINED:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 19
    return-void
.end method
