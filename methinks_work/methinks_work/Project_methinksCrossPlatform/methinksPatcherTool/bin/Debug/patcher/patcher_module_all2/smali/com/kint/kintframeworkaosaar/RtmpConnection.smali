.class public Lcom/kint/kintframeworkaosaar/RtmpConnection;
.super Ljava/lang/Object;
.source "RtmpConnection.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/RtmpPublisher;


# static fields
.field private static final TAG:Ljava/lang/String; = "RtmpConnection"

.field private static final rtmpUrlPattern:Ljava/util/regex/Pattern;

.field private static final rtmpsUrlPattern:Ljava/util/regex/Pattern;


# instance fields
.field private appName:Ljava/lang/String;

.field private challenge:Ljava/lang/String;

.field private connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

.field private volatile connected:Z

.field private final connectingLock:Ljava/lang/Object;

.field private currentStreamId:I

.field private host:Ljava/lang/String;

.field private inputStream:Ljava/io/BufferedInputStream;

.field private onAuth:Z

.field private opaque:Ljava/lang/String;

.field private outputStream:Ljava/io/BufferedOutputStream;

.field private pageUrl:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private port:I

.field private final publishLock:Ljava/lang/Object;

.field private volatile publishPermitted:Z

.field private publishType:Ljava/lang/String;

.field private rtmpDecoder:Lcom/kint/kintframeworkaosaar/RtmpDecoder;

.field private rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

.field private rxPacketHandler:Ljava/lang/Thread;

.field private salt:Ljava/lang/String;

.field private socket:Ljava/net/Socket;

.field private socketExceptionCause:Ljava/lang/String;

.field private streamName:Ljava/lang/String;

.field private swfUrl:Ljava/lang/String;

.field private tcUrl:Ljava/lang/String;

.field private tlsEnabled:Z

.field private transactionIdCounter:I

.field private user:Ljava/lang/String;

.field private videoDataRate:I

.field private videoFrameRate:I

.field private videoHeight:I

.field private videoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    nop

    .line 46
    const-string v0, "^rtmp://([^/:]+)(:(\\d+))*/([^/]+)(/(.*))*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpUrlPattern:Ljava/util/regex/Pattern;

    .line 47
    nop

    .line 48
    const-string v0, "^rtmps://([^/:]+)(:(\\d+))*/([^/]+)(/(.*))*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpsUrlPattern:Ljava/util/regex/Pattern;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V
    .locals 2
    .param p1, "connectCheckerRtmp"    # Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socketExceptionCause:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 66
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    .line 67
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    .line 68
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishLock:Ljava/lang/Object;

    .line 69
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    .line 70
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    .line 71
    const/16 v1, 0x2d0

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoWidth:I

    .line 72
    const/16 v1, 0x500

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoHeight:I

    .line 73
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoDataRate:I

    .line 74
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoFrameRate:I

    .line 79
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    .line 80
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->salt:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->challenge:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->opaque:Ljava/lang/String;

    .line 84
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->onAuth:Z

    .line 88
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/kint/kintframeworkaosaar/RtmpConnection;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/RtmpConnection;

    .line 42
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->handleRxPacketLoop()V

    return-void
.end method

.method private closeStream()V
    .locals 3

    .line 423
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    const-string v1, "RtmpConnection"

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 428
    :cond_0
    const-string v0, "closeStream(): setting current stream ID to 0"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    new-instance v0, Lcom/kint/kintframeworkaosaar/Command;

    const/4 v1, 0x0

    const-string v2, "closeStream"

    invoke-direct {v0, v2, v1}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;I)V

    .line 430
    .local v0, "closeStream":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setChunkStreamId(I)V

    .line 431
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 432
    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 433
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 434
    return-void

    .line 425
    .end local v0    # "closeStream":Lcom/kint/kintframeworkaosaar/Command;
    :cond_1
    :goto_0
    const-string v0, "closeStream failed"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    return-void
.end method

.method private createStream()Z
    .locals 9

    .line 320
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 327
    :cond_0
    const-string v0, "RtmpConnection"

    const-string v1, "createStream(): Sending releaseStream command..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    new-instance v0, Lcom/kint/kintframeworkaosaar/Command;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v3, "releaseStream"

    invoke-direct {v0, v3, v1}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;I)V

    .line 330
    .local v0, "releaseStream":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setChunkStreamId(I)V

    .line 331
    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 332
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Ljava/lang/String;)V

    .line 333
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 335
    const-string v1, "RtmpConnection"

    const-string v4, "createStream(): Sending FCPublish command..."

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    new-instance v1, Lcom/kint/kintframeworkaosaar/Command;

    iget v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v5, "FCPublish"

    invoke-direct {v1, v5, v4}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;I)V

    .line 338
    .local v1, "FCPublish":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setChunkStreamId(I)V

    .line 339
    new-instance v3, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v3}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    invoke-virtual {v1, v3}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 340
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/kint/kintframeworkaosaar/Command;->addData(Ljava/lang/String;)V

    .line 341
    invoke-direct {p0, v1}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 343
    const-string v3, "RtmpConnection"

    const-string v4, "createStream(): Sending createStream command..."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    const/4 v4, 0x3

    .line 345
    invoke-virtual {v3, v4}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v3

    .line 347
    .local v3, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    new-instance v4, Lcom/kint/kintframeworkaosaar/Command;

    iget v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v6, "createStream"

    invoke-direct {v4, v6, v5, v3}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 348
    .local v4, "createStream":Lcom/kint/kintframeworkaosaar/Command;
    new-instance v5, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v5}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    invoke-virtual {v4, v5}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 349
    invoke-direct {p0, v4}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 352
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishLock:Ljava/lang/Object;

    monitor-enter v5

    .line 354
    :try_start_0
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishLock:Ljava/lang/Object;

    const-wide/16 v7, 0x1388

    invoke-virtual {v6, v7, v8}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    goto :goto_0

    .line 358
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 355
    :catch_0
    move-exception v6

    .line 358
    :goto_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359
    iget-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    if-nez v5, :cond_1

    .line 360
    invoke-direct {p0, v2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->shutdown(Z)V

    .line 361
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    const-string v5, "Error configure stream, publish permitted failed"

    invoke-interface {v2, v5}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 363
    :cond_1
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    return v2

    .line 358
    :goto_1
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 322
    .end local v0    # "releaseStream":Lcom/kint/kintframeworkaosaar/Command;
    .end local v1    # "FCPublish":Lcom/kint/kintframeworkaosaar/Command;
    .end local v3    # "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .end local v4    # "createStream":Lcom/kint/kintframeworkaosaar/Command;
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create stream failed, connected= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", StreamId= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 324
    const/4 v0, 0x0

    return v0
.end method

.method private fmlePublish()V
    .locals 3

    .line 368
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    const-string v1, "RtmpConnection"

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-nez v0, :cond_0

    goto :goto_0

    .line 374
    :cond_0
    const-string v0, "fmlePublish(): Sending publish command..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    new-instance v0, Lcom/kint/kintframeworkaosaar/Command;

    const/4 v1, 0x0

    const-string v2, "publish"

    invoke-direct {v0, v2, v1}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;I)V

    .line 376
    .local v0, "publish":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setChunkStreamId(I)V

    .line 377
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 378
    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 379
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Command;->addData(Ljava/lang/String;)V

    .line 381
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 382
    return-void

    .line 370
    .end local v0    # "publish":Lcom/kint/kintframeworkaosaar/Command;
    :cond_1
    :goto_0
    const-string v0, "fmlePublish failed"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    return-void
.end method

.method private handleRxInvoke(Lcom/kint/kintframeworkaosaar/Command;)V
    .locals 12
    .param p1, "invoke"    # Lcom/kint/kintframeworkaosaar/Command;

    .line 658
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/Command;->getCommandName()Ljava/lang/String;

    move-result-object v0

    .line 659
    .local v0, "commandName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "_result"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "onStatus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_2
    const-string v1, "onBWDone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "_error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_4
    const-string v1, "onFCPublish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    if-eqz v1, :cond_a

    if-eq v1, v6, :cond_4

    if-eq v1, v4, :cond_3

    if-eq v1, v3, :cond_2

    if-eq v1, v2, :cond_1

    .line 782
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleRxInvoke(): Unknown/unhandled server invoke: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RtmpConnection"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 767
    :cond_1
    nop

    .line 768
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/Command;->getData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/kint/kintframeworkaosaar/AmfObject;

    const-string v2, "code"

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/AmfObject;->getProperty(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/AmfData;

    move-result-object v1

    check-cast v1, Lcom/kint/kintframeworkaosaar/AmfString;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/AmfString;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 769
    .local v1, "code":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRxInvoke(): onStatus "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RtmpConnection"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const-string v2, "NetStream.Publish.Start"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 772
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->onMetaData()V

    .line 774
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    .line 775
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishLock:Ljava/lang/Object;

    monitor-enter v2

    .line 777
    :try_start_0
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 778
    monitor-exit v2

    goto/16 :goto_5

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 764
    .end local v1    # "code":Ljava/lang/String;
    :cond_2
    const-string v1, "RtmpConnection"

    const-string v2, "handleRxInvoke(): \'onFCPublish\'"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    goto/16 :goto_5

    .line 761
    :cond_3
    const-string v1, "RtmpConnection"

    const-string v2, "handleRxInvoke(): \'onBWDone\'"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    goto/16 :goto_5

    .line 731
    :cond_4
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/Command;->getTransactionId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->takeInvokedCommand(I)Ljava/lang/String;

    move-result-object v1

    .line 733
    .local v1, "method":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRxInvoke: Got result for invoked method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RtmpConnection"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const-string v2, "connect"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 735
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->onAuth:Z

    if-eqz v2, :cond_5

    .line 736
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    invoke-interface {v2}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onAuthSuccessRtmp()V

    .line 737
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->onAuth:Z

    .line 741
    :cond_5
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 742
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v2

    .line 743
    :try_start_1
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 744
    monitor-exit v2

    goto/16 :goto_5

    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v3

    .line 745
    :cond_6
    const-string v2, "createStream"

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 747
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/Command;->getData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/kint/kintframeworkaosaar/AmfNumber;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/AmfNumber;->getValue()D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    .line 748
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRxInvoke(): Stream ID to publish: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RtmpConnection"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishType:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 750
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->fmlePublish()V

    goto/16 :goto_5

    .line 752
    :cond_7
    const-string v2, "releaseStream"

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 753
    const-string v2, "RtmpConnection"

    const-string v3, "handleRxInvoke(): \'releaseStream\'"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 754
    :cond_8
    const-string v2, "FCPublish"

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 755
    const-string v2, "RtmpConnection"

    const-string v3, "handleRxInvoke(): \'FCPublish\'"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 757
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRxInvoke(): \'_result\' message received for unknown method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RtmpConnection"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    goto/16 :goto_5

    .line 664
    .end local v1    # "method":Ljava/lang/String;
    :cond_a
    :try_start_2
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/Command;->getData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/kint/kintframeworkaosaar/AmfObject;

    const-string v2, "description"

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/AmfObject;->getProperty(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/AmfData;

    move-result-object v1

    check-cast v1, Lcom/kint/kintframeworkaosaar/AmfString;

    .line 665
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/AmfString;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 666
    .local v1, "description":Ljava/lang/String;
    const-string v2, "RtmpConnection"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    const-string v2, "reason=authfailed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 668
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    invoke-interface {v2}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onAuthErrorRtmp()V

    .line 669
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 670
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 671
    :try_start_3
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 672
    monitor-exit v2

    goto/16 :goto_4

    :catchall_2
    move-exception v3

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .end local v0    # "commandName":Ljava/lang/String;
    .end local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :try_start_4
    throw v3

    .line 673
    .restart local v0    # "commandName":Ljava/lang/String;
    .restart local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :cond_b
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    if-eqz v2, :cond_e

    const-string v2, "challenge="

    .line 675
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "salt="

    .line 676
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 677
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->onAuth:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 679
    :try_start_5
    invoke-direct {p0, v5}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->shutdown(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 682
    goto :goto_2

    .line 680
    :catch_0
    move-exception v2

    .line 681
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 683
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    new-instance v2, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;-><init>()V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 684
    new-instance v2, Lcom/kint/kintframeworkaosaar/RtmpDecoder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-direct {v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpDecoder;-><init>(Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpDecoder:Lcom/kint/kintframeworkaosaar/RtmpDecoder;

    .line 685
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tlsEnabled:Z

    if-nez v2, :cond_c

    .line 686
    new-instance v2, Ljava/net/Socket;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    iget v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    invoke-direct {v2, v3, v4}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    goto :goto_3

    .line 688
    :cond_c
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    invoke-static {v2, v3}, Lcom/kint/kintframeworkaosaar/CreateSSLSocket;->createSSlSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    .line 689
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    if-eqz v2, :cond_d

    .line 691
    :goto_3
    new-instance v2, Ljava/io/BufferedInputStream;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->inputStream:Ljava/io/BufferedInputStream;

    .line 692
    new-instance v2, Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    .line 693
    const-string v2, "RtmpConnection"

    const-string v3, "connect(): socket connection established, doing handshake..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/Util;->getSalt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->salt:Ljava/lang/String;

    .line 695
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/Util;->getChallenge(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->challenge:Ljava/lang/String;

    .line 696
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/Util;->getOpaque(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->opaque:Ljava/lang/String;

    .line 697
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->inputStream:Ljava/io/BufferedInputStream;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    invoke-direct {p0, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->handshake(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 698
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/kint/kintframeworkaosaar/RtmpConnection$2;

    invoke-direct {v3, p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection$2;-><init>(Lcom/kint/kintframeworkaosaar/RtmpConnection;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    .line 704
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 705
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->salt:Ljava/lang/String;

    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->challenge:Ljava/lang/String;

    iget-object v11, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->opaque:Ljava/lang/String;

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendConnectAuthPacketFinal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 689
    :cond_d
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Socket creation failed"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .end local v0    # "commandName":Ljava/lang/String;
    .end local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    throw v2

    .line 706
    .restart local v0    # "commandName":Ljava/lang/String;
    .restart local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :cond_e
    const-string v2, "code=403"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    if-eqz v2, :cond_10

    :cond_f
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 707
    :cond_10
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    invoke-interface {v2}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onAuthErrorRtmp()V

    .line 708
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 709
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 710
    :try_start_7
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 711
    monitor-exit v2

    goto :goto_4

    :catchall_3
    move-exception v3

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .end local v0    # "commandName":Ljava/lang/String;
    .end local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :try_start_8
    throw v3

    .line 713
    .restart local v0    # "commandName":Ljava/lang/String;
    .restart local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :cond_11
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    invoke-interface {v2, v1}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 714
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 715
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 716
    :try_start_9
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 717
    monitor-exit v2

    .line 727
    .end local v1    # "description":Ljava/lang/String;
    :goto_4
    goto :goto_5

    .line 717
    .restart local v1    # "description":Ljava/lang/String;
    :catchall_4
    move-exception v3

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .end local v0    # "commandName":Ljava/lang/String;
    .end local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :try_start_a
    throw v3
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    .line 720
    .end local v1    # "description":Ljava/lang/String;
    .restart local v0    # "commandName":Ljava/lang/String;
    .restart local p1    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    :catch_1
    move-exception v1

    .line 722
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 723
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 724
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v2

    .line 725
    :try_start_b
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 726
    monitor-exit v2

    .line 728
    .end local v1    # "e":Ljava/lang/Exception;
    nop

    .line 785
    :cond_12
    :goto_5
    return-void

    .line 726
    .restart local v1    # "e":Ljava/lang/Exception;
    :catchall_5
    move-exception v3

    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6971956d -> :sswitch_4
        -0x581ecaf7 -> :sswitch_3
        0x3b1a5816 -> :sswitch_2
        0x59c2a6b1 -> :sswitch_1
        0x69bdc53c -> :sswitch_0
    .end sparse-switch
.end method

.method private handleRxPacketLoop()V
    .locals 8

    .line 579
    const-string v0, "RtmpConnection"

    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 584
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpDecoder:Lcom/kint/kintframeworkaosaar/RtmpDecoder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->inputStream:Ljava/io/BufferedInputStream;

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpDecoder;->readPacket(Ljava/io/InputStream;)Lcom/kint/kintframeworkaosaar/RtmpPacket;

    move-result-object v1

    .line 585
    .local v1, "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    if-eqz v1, :cond_8

    .line 588
    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpConnection$3;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$MessageType:[I

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getMessageType()Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-eq v2, v5, :cond_3

    if-eq v2, v4, :cond_2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    .line 637
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRxPacketLoop(): Not handling unimplemented/unknown packet of type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getMessageType()Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 637
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 634
    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/kint/kintframeworkaosaar/Command;

    invoke-direct {p0, v2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->handleRxInvoke(Lcom/kint/kintframeworkaosaar/Command;)V

    .line 635
    goto/16 :goto_1

    .line 622
    :cond_1
    move-object v2, v1

    check-cast v2, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;

    .line 623
    .local v2, "bw":Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->getAcknowledgementWindowSize()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->setAcknowledgmentWindowSize(I)V

    .line 624
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getAcknowledgementWindowSize()I

    move-result v3

    .line 625
    .local v3, "acknowledgementWindowsize":I
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 626
    invoke-virtual {v4, v5}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v4

    .line 627
    .local v4, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleRxPacketLoop(): Send acknowledgement window size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    new-instance v5, Lcom/kint/kintframeworkaosaar/WindowAckSize;

    invoke-direct {v5, v3, v4}, Lcom/kint/kintframeworkaosaar/WindowAckSize;-><init>(ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    invoke-direct {p0, v5}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 631
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v5, v3}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 632
    goto/16 :goto_1

    .line 616
    .end local v2    # "bw":Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;
    .end local v3    # "acknowledgementWindowsize":I
    .end local v4    # "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    :cond_2
    move-object v2, v1

    check-cast v2, Lcom/kint/kintframeworkaosaar/WindowAckSize;

    .line 617
    .local v2, "windowAckSize":Lcom/kint/kintframeworkaosaar/WindowAckSize;
    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/WindowAckSize;->getAcknowledgementWindowSize()I

    move-result v3

    .line 618
    .local v3, "size":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleRxPacketLoop(): Setting acknowledgement window size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v4, v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->setAcknowledgmentWindowSize(I)V

    .line 620
    goto/16 :goto_1

    .line 595
    .end local v2    # "windowAckSize":Lcom/kint/kintframeworkaosaar/WindowAckSize;
    .end local v3    # "size":I
    :cond_3
    move-object v2, v1

    check-cast v2, Lcom/kint/kintframeworkaosaar/UserControl;

    .line 596
    .local v2, "user":Lcom/kint/kintframeworkaosaar/UserControl;
    sget-object v6, Lcom/kint/kintframeworkaosaar/RtmpConnection$3;->$SwitchMap$com$kint$kintframeworkaosaar$UserControl$Type:[I

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/UserControl;->getType()Lcom/kint/kintframeworkaosaar/UserControl$Type;

    move-result-object v7

    invoke-virtual {v7}, Lcom/kint/kintframeworkaosaar/UserControl$Type;->ordinal()I

    move-result v7

    aget v6, v6, v7

    if-eq v6, v3, :cond_6

    if-eq v6, v5, :cond_5

    if-eq v6, v4, :cond_4

    .line 612
    goto :goto_1

    .line 608
    :cond_4
    const-string v3, "handleRxPacketLoop(): Stream EOF reached, closing RTMP writer..."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    goto :goto_1

    .line 601
    :cond_5
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 602
    invoke-virtual {v3, v5}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v3

    .line 603
    .local v3, "channelInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    const-string v4, "handleRxPacketLoop(): Sending PONG reply.."

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    new-instance v4, Lcom/kint/kintframeworkaosaar/UserControl;

    invoke-direct {v4, v2, v3}, Lcom/kint/kintframeworkaosaar/UserControl;-><init>(Lcom/kint/kintframeworkaosaar/UserControl;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 605
    .local v4, "pong":Lcom/kint/kintframeworkaosaar/UserControl;
    invoke-direct {p0, v4}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 606
    goto :goto_1

    .line 599
    .end local v3    # "channelInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .end local v4    # "pong":Lcom/kint/kintframeworkaosaar/UserControl;
    :cond_6
    goto :goto_1

    .line 591
    .end local v2    # "user":Lcom/kint/kintframeworkaosaar/UserControl;
    :cond_7
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    move-object v3, v1

    check-cast v3, Lcom/kint/kintframeworkaosaar/Abort;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/Abort;->getChunkStreamId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v2

    .line 592
    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->clearStoredChunks()V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 593
    goto :goto_1

    .line 647
    .end local v1    # "rtmpPacket":Lcom/kint/kintframeworkaosaar/RtmpPacket;
    :catch_0
    move-exception v1

    .line 649
    .local v1, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reading packet: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 650
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caught SocketException while reading/decoding packet, shutting down: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 650
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v1    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 643
    :catch_1
    move-exception v1

    .line 645
    .local v1, "eof":Ljava/io/EOFException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 652
    .end local v1    # "eof":Ljava/io/EOFException;
    :cond_8
    :goto_1
    goto/16 :goto_0

    .line 654
    :cond_9
    return-void
.end method

.method private handshake(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 93
    new-instance v0, Lcom/kint/kintframeworkaosaar/Handshake;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/Handshake;-><init>()V

    .line 94
    .local v0, "handshake":Lcom/kint/kintframeworkaosaar/Handshake;
    invoke-virtual {v0, p2}, Lcom/kint/kintframeworkaosaar/Handshake;->writeC0(Ljava/io/OutputStream;)V

    .line 95
    invoke-virtual {v0, p2}, Lcom/kint/kintframeworkaosaar/Handshake;->writeC1(Ljava/io/OutputStream;)V

    .line 96
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 97
    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/Handshake;->readS0(Ljava/io/InputStream;)V

    .line 98
    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/Handshake;->readS1(Ljava/io/InputStream;)V

    .line 99
    invoke-virtual {v0, p2}, Lcom/kint/kintframeworkaosaar/Handshake;->writeC2(Ljava/io/OutputStream;)V

    .line 100
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 101
    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/Handshake;->readS2(Ljava/io/InputStream;)V

    .line 102
    return-void
.end method

.method private onMetaData()V
    .locals 5

    .line 386
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    const-string v1, "RtmpConnection"

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-nez v0, :cond_0

    goto :goto_0

    .line 392
    :cond_0
    const-string v0, "onMetaData(): Sending empty onMetaData..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    new-instance v0, Lcom/kint/kintframeworkaosaar/Data;

    const-string v1, "@setDataFrame"

    invoke-direct {v0, v1}, Lcom/kint/kintframeworkaosaar/Data;-><init>(Ljava/lang/String;)V

    .line 394
    .local v0, "metadata":Lcom/kint/kintframeworkaosaar/Data;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Data;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 395
    const-string v1, "onMetaData"

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Data;->addData(Ljava/lang/String;)V

    .line 396
    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfMap;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/AmfMap;-><init>()V

    .line 397
    .local v1, "ecmaArray":Lcom/kint/kintframeworkaosaar/AmfMap;
    const/4 v2, 0x0

    const-string v3, "duration"

    invoke-virtual {v1, v3, v2}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 398
    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoWidth:I

    const-string v4, "width"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 399
    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoHeight:I

    const-string v4, "height"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 400
    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoDataRate:I

    const-string v4, "videodatarate"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 401
    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoFrameRate:I

    const-string v4, "framerate"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 402
    const-string v3, "audiodatarate"

    invoke-virtual {v1, v3, v2}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 403
    const v3, 0xac44

    const-string v4, "audiosamplerate"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 404
    const/16 v3, 0x10

    const-string v4, "audiosamplesize"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 405
    const/4 v3, 0x1

    const-string v4, "stereo"

    invoke-virtual {v1, v4, v3}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;Z)V

    .line 406
    const-string v3, "filesize"

    invoke-virtual {v1, v3, v2}, Lcom/kint/kintframeworkaosaar/AmfMap;->setProperty(Ljava/lang/String;I)V

    .line 407
    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/Data;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 408
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 409
    return-void

    .line 388
    .end local v0    # "metadata":Lcom/kint/kintframeworkaosaar/Data;
    .end local v1    # "ecmaArray":Lcom/kint/kintframeworkaosaar/AmfMap;
    :cond_1
    :goto_0
    const-string v0, "onMetaData failed"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    return-void
.end method

.method private reset()V
    .locals 2

    .line 484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    .line 485
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    .line 486
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tcUrl:Ljava/lang/String;

    .line 487
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->swfUrl:Ljava/lang/String;

    .line 488
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->pageUrl:Ljava/lang/String;

    .line 489
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    .line 490
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    .line 491
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishType:Ljava/lang/String;

    .line 492
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    .line 493
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    .line 494
    const-string v0, ""

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socketExceptionCause:Ljava/lang/String;

    .line 495
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    .line 496
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 497
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    .line 498
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    .line 499
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->salt:Ljava/lang/String;

    .line 500
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->challenge:Ljava/lang/String;

    .line 501
    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->opaque:Ljava/lang/String;

    .line 502
    return-void
.end method

.method private rtmpConnect()Z
    .locals 7

    .line 190
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    const-string v2, "Already connected"

    invoke-interface {v0, v2}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 193
    return v1

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 198
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendConnectAuthPacketUser(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_1
    invoke-static {}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markSessionTimestampTx()V

    .line 204
    const-string v0, "RtmpConnection"

    const-string v3, "rtmpConnect(): Building \'connect\' invoke packet"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    const/4 v3, 0x3

    .line 206
    invoke-virtual {v0, v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v0

    .line 207
    .local v0, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    new-instance v3, Lcom/kint/kintframeworkaosaar/Command;

    iget v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v5, "connect"

    invoke-direct {v3, v5, v4, v0}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 208
    .local v3, "invoke":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 209
    new-instance v4, Lcom/kint/kintframeworkaosaar/AmfObject;

    invoke-direct {v4}, Lcom/kint/kintframeworkaosaar/AmfObject;-><init>()V

    .line 210
    .local v4, "args":Lcom/kint/kintframeworkaosaar/AmfObject;
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    const-string v6, "app"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v5, "flashVer"

    const-string v6, "FMLE/3.0 (compatible; Lavf57.56.101)"

    invoke-virtual {v4, v5, v6}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->swfUrl:Ljava/lang/String;

    const-string v6, "swfUrl"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tcUrl:Ljava/lang/String;

    const-string v6, "tcUrl"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v5, "fpad"

    invoke-virtual {v4, v5, v1}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Z)V

    .line 215
    const/16 v5, 0xef

    const-string v6, "capabilities"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 216
    const/16 v5, 0xdf7

    const-string v6, "audioCodecs"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 217
    const/16 v5, 0xfc

    const-string v6, "videoCodecs"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 218
    const-string v5, "videoFunction"

    invoke-virtual {v4, v5, v2}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 219
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->pageUrl:Ljava/lang/String;

    const-string v6, "pageUrl"

    invoke-virtual {v4, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v5, "objectEncoding"

    invoke-virtual {v4, v5, v1}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 221
    invoke-virtual {v3, v4}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 222
    invoke-direct {p0, v3}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 224
    .end local v0    # "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .end local v3    # "invoke":Lcom/kint/kintframeworkaosaar/Command;
    .end local v4    # "args":Lcom/kint/kintframeworkaosaar/AmfObject;
    :goto_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    monitor-enter v0

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectingLock:Ljava/lang/Object;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    goto :goto_1

    .line 234
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 230
    :catch_0
    move-exception v1

    .line 234
    :goto_1
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    if-nez v0, :cond_2

    .line 237
    invoke-direct {p0, v2}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->shutdown(Z)V

    .line 238
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    const-string v1, "Fail to connect, time out"

    invoke-interface {v0, v1}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 240
    :cond_2
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    return v0

    .line 234
    :goto_2
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private sendConnectAuthPacketFinal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "salt"    # Ljava/lang/String;
    .param p4, "challenge"    # Ljava/lang/String;
    .param p5, "opaque"    # Ljava/lang/String;

    .line 270
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "%08x"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "challenge2":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/Util;->stringToMD5BASE64(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 272
    .local v2, "response":Ljava/lang/String;
    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 273
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 274
    :cond_0
    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 277
    :cond_1
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/Util;->stringToMD5BASE64(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 278
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "?authmod=adobe&user="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&challenge="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&response="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 280
    .local v4, "result":Ljava/lang/String;
    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 281
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "&opaque="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 284
    :cond_2
    invoke-static {}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markSessionTimestampTx()V

    .line 285
    const-string v5, "RtmpConnection"

    const-string v6, "rtmpConnect(): Building \'connect\' invoke packet"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    const/4 v6, 0x5

    .line 287
    invoke-virtual {v5, v6}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v5

    .line 288
    .local v5, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    new-instance v6, Lcom/kint/kintframeworkaosaar/Command;

    iget v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    add-int/2addr v7, v0

    iput v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v8, "connect"

    invoke-direct {v6, v8, v7, v5}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 289
    .local v6, "invoke":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v6}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 290
    new-instance v7, Lcom/kint/kintframeworkaosaar/AmfObject;

    invoke-direct {v7}, Lcom/kint/kintframeworkaosaar/AmfObject;-><init>()V

    .line 291
    .local v7, "args":Lcom/kint/kintframeworkaosaar/AmfObject;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "app"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v8, "flashVer"

    const-string v9, "FMLE/3.0 (compatible; Lavf57.56.101)"

    invoke-virtual {v7, v8, v9}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->swfUrl:Ljava/lang/String;

    const-string v9, "swfUrl"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tcUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "tcUrl"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v8, "fpad"

    invoke-virtual {v7, v8, v3}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Z)V

    .line 296
    const/16 v8, 0xef

    const-string v9, "capabilities"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 297
    const/16 v8, 0xdf7

    const-string v9, "audioCodecs"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 298
    const/16 v8, 0xfc

    const-string v9, "videoCodecs"

    invoke-virtual {v7, v9, v8}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 299
    const-string v8, "videoFunction"

    invoke-virtual {v7, v8, v0}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 300
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->pageUrl:Ljava/lang/String;

    const-string v8, "pageUrl"

    invoke-virtual {v7, v8, v0}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v0, "objectEncoding"

    invoke-virtual {v7, v0, v3}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 302
    invoke-virtual {v6, v7}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 303
    invoke-direct {p0, v6}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 304
    return-void
.end method

.method private sendConnectAuthPacketUser(Ljava/lang/String;)V
    .locals 8
    .param p1, "user"    # Ljava/lang/String;

    .line 245
    invoke-static {}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markSessionTimestampTx()V

    .line 246
    const-string v0, "RtmpConnection"

    const-string v1, "rtmpConnect(): Building \'connect\' invoke packet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 248
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v0

    .line 249
    .local v0, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    new-instance v1, Lcom/kint/kintframeworkaosaar/Command;

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->transactionIdCounter:I

    const-string v4, "connect"

    invoke-direct {v1, v4, v2, v0}, Lcom/kint/kintframeworkaosaar/Command;-><init>(Ljava/lang/String;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 250
    .local v1, "invoke":Lcom/kint/kintframeworkaosaar/Command;
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/Command;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 251
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfObject;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfObject;-><init>()V

    .line 252
    .local v2, "args":Lcom/kint/kintframeworkaosaar/AmfObject;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "?authmod=adobe&user="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "app"

    invoke-virtual {v2, v7, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v5, "flashVer"

    const-string v7, "FMLE/3.0 (compatible; Lavf57.56.101)"

    invoke-virtual {v2, v5, v7}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->swfUrl:Ljava/lang/String;

    const-string v7, "swfUrl"

    invoke-virtual {v2, v7, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tcUrl:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "tcUrl"

    invoke-virtual {v2, v6, v5}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v5, "fpad"

    invoke-virtual {v2, v5, v4}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Z)V

    .line 257
    const-string v5, "capabilities"

    const/16 v6, 0xef

    invoke-virtual {v2, v5, v6}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 258
    const-string v5, "audioCodecs"

    const/16 v6, 0xdf7

    invoke-virtual {v2, v5, v6}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 259
    const-string v5, "videoCodecs"

    const/16 v6, 0xfc

    invoke-virtual {v2, v5, v6}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 260
    const-string v5, "videoFunction"

    invoke-virtual {v2, v5, v3}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 261
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->pageUrl:Ljava/lang/String;

    const-string v5, "pageUrl"

    invoke-virtual {v2, v5, v3}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v3, "objectEncoding"

    invoke-virtual {v2, v3, v4}, Lcom/kint/kintframeworkaosaar/AmfObject;->setProperty(Ljava/lang/String;I)V

    .line 263
    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/Command;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 264
    invoke-direct {p0, v1}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 265
    return-void
.end method

.method private sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V
    .locals 5
    .param p1, "rtmpPacket"    # Lcom/kint/kintframeworkaosaar/RtmpPacket;

    .line 546
    const-string v0, "RtmpConnection"

    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 547
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getChunkStreamId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v1

    .line 548
    .local v1, "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->setPrevHeaderTx(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 549
    instance-of v2, p1, Lcom/kint/kintframeworkaosaar/Video;

    if-nez v2, :cond_0

    instance-of v2, p1, Lcom/kint/kintframeworkaosaar/Audio;

    if-nez v2, :cond_0

    .line 551
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v2

    .line 552
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markAbsoluteTimestampTx()J

    move-result-wide v3

    long-to-int v4, v3

    invoke-virtual {v2, v4}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setAbsoluteTimestamp(I)V

    .line 554
    :cond_0
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getTxChunkSize()I

    move-result v3

    invoke-virtual {p1, v2, v3, v1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->writeTo(Ljava/io/OutputStream;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 555
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wrote packet: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getPacketLength()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 555
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    instance-of v2, p1, Lcom/kint/kintframeworkaosaar/Command;

    if-eqz v2, :cond_1

    .line 559
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    move-object v3, p1

    check-cast v3, Lcom/kint/kintframeworkaosaar/Command;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/Command;->getTransactionId()I

    move-result v3

    move-object v4, p1

    check-cast v4, Lcom/kint/kintframeworkaosaar/Command;

    .line 560
    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/Command;->getCommandName()Ljava/lang/String;

    move-result-object v4

    .line 559
    invoke-virtual {v2, v3, v4}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->addInvokedCommand(ILjava/lang/String;)Ljava/lang/String;

    .line 562
    :cond_1
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "chunkStreamInfo":Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    goto :goto_0

    .line 571
    :catch_0
    move-exception v1

    .line 572
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caught IOException during write loop, shutting down: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 563
    .end local v1    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 566
    .local v1, "se":Ljava/net/SocketException;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socketExceptionCause:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 567
    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socketExceptionCause:Ljava/lang/String;

    .line 568
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error send packet: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 569
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caught SocketException during write loop, shutting down: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    .end local v1    # "se":Ljava/net/SocketException;
    :cond_2
    :goto_0
    nop

    .line 574
    :goto_1
    return-void
.end method

.method private shutdown(Z)V
    .locals 4
    .param p1, "r"    # Z

    .line 438
    const-string v0, "RtmpConnection"

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    if-eqz v1, :cond_1

    .line 443
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->shutdownInput()V

    .line 445
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    goto :goto_1

    .line 447
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    .line 449
    .local v1, "e":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 455
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 458
    :try_start_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 463
    goto :goto_2

    .line 460
    :catch_2
    move-exception v1

    .line 462
    .local v1, "ie":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 464
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    .line 469
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 470
    const-string v1, "socket closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 473
    goto :goto_3

    .line 471
    :catch_3
    move-exception v1

    .line 472
    .local v1, "ex":Ljava/io/IOException;
    const-string v2, "shutdown(): failed to close socket"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 476
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_1
    :goto_3
    if-eqz p1, :cond_2

    .line 478
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->reset()V

    .line 480
    :cond_2
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 414
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 416
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->closeStream()V

    .line 418
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->shutdown(Z)V

    .line 419
    return-void
.end method

.method public connect(Ljava/lang/String;)Z
    .locals 9
    .param p1, "url"    # Ljava/lang/String;

    .line 107
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpUrlPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 108
    .local v0, "rtmpMatcher":Ljava/util/regex/Matcher;
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpsUrlPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 109
    .local v1, "rtmpsMatcher":Ljava/util/regex/Matcher;
    move-object v2, v0

    .line 110
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    .line 112
    move-object v2, v0

    .line 113
    iput-boolean v5, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tlsEnabled:Z

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 117
    move-object v2, v1

    .line 118
    iput-boolean v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tlsEnabled:Z

    .line 129
    :cond_1
    :goto_0
    const/16 v3, 0x2f

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tcUrl:Ljava/lang/String;

    .line 130
    const-string v3, ""

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->swfUrl:Ljava/lang/String;

    .line 131
    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->pageUrl:Ljava/lang/String;

    .line 132
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    .line 133
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "portStr":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    :cond_2
    const/16 v4, 0x78f

    :goto_1
    iput v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    .line 135
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    .line 136
    const/4 v4, 0x6

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    .line 139
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connect() called. Host: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", port: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ", appName: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->appName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", publishPath: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->streamName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "RtmpConnection"

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    new-instance v4, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-direct {v4}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;-><init>()V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    .line 148
    new-instance v4, Lcom/kint/kintframeworkaosaar/RtmpDecoder;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpSessionInfo:Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;

    invoke-direct {v4, v7}, Lcom/kint/kintframeworkaosaar/RtmpDecoder;-><init>(Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpDecoder:Lcom/kint/kintframeworkaosaar/RtmpDecoder;

    .line 150
    :try_start_0
    iget-boolean v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->tlsEnabled:Z

    if-nez v4, :cond_3

    .line 151
    new-instance v4, Ljava/net/Socket;

    invoke-direct {v4}, Ljava/net/Socket;-><init>()V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    .line 152
    new-instance v4, Ljava/net/InetSocketAddress;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    iget v8, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    invoke-direct {v4, v7, v8}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 153
    .local v4, "socketAddress":Ljava/net/SocketAddress;
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    const/16 v8, 0x1388

    invoke-virtual {v7, v4, v8}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 154
    .end local v4    # "socketAddress":Ljava/net/SocketAddress;
    goto :goto_2

    .line 157
    :cond_3
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->host:Ljava/lang/String;

    iget v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->port:I

    invoke-static {v4, v7}, Lcom/kint/kintframeworkaosaar/CreateSSLSocket;->createSSlSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v4

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    .line 158
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    if-eqz v4, :cond_4

    .line 161
    :goto_2
    new-instance v4, Ljava/io/BufferedInputStream;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->inputStream:Ljava/io/BufferedInputStream;

    .line 162
    new-instance v4, Ljava/io/BufferedOutputStream;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->socket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    .line 163
    const-string v4, "connect(): socket connection established, doing handhake..."

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->inputStream:Ljava/io/BufferedInputStream;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->outputStream:Ljava/io/BufferedOutputStream;

    invoke-direct {p0, v4, v7}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->handshake(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 165
    const-string v4, "connect(): handshake done"

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    nop

    .line 175
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/kint/kintframeworkaosaar/RtmpConnection$1;

    invoke-direct {v5, p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection$1;-><init>(Lcom/kint/kintframeworkaosaar/RtmpConnection;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    .line 184
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rxPacketHandler:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 185
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->rtmpConnect()Z

    move-result v4

    return v4

    .line 159
    :cond_4
    :try_start_1
    new-instance v4, Ljava/io/IOException;

    const-string v7, "Socket creation failed"

    invoke-direct {v4, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .end local v0    # "rtmpMatcher":Ljava/util/regex/Matcher;
    .end local v1    # "rtmpsMatcher":Ljava/util/regex/Matcher;
    .end local v2    # "matcher":Ljava/util/regex/Matcher;
    .end local v3    # "portStr":Ljava/lang/String;
    .end local p1    # "url":Ljava/lang/String;
    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 167
    .restart local v0    # "rtmpMatcher":Ljava/util/regex/Matcher;
    .restart local v1    # "rtmpsMatcher":Ljava/util/regex/Matcher;
    .restart local v2    # "matcher":Ljava/util/regex/Matcher;
    .restart local v3    # "portStr":Ljava/lang/String;
    .restart local p1    # "url":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 169
    .local v4, "e":Ljava/io/IOException;
    const-string v7, "Error"

    invoke-static {v6, v7, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Connect error, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 171
    return v5
.end method

.method public publish(Ljava/lang/String;)Z
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .line 309
    if-nez p1, :cond_0

    .line 311
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    const-string v1, "Null publish type"

    invoke-interface {v0, v1}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onConnectionFailedRtmp(Ljava/lang/String;)V

    .line 312
    const/4 v0, 0x0

    return v0

    .line 314
    :cond_0
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishType:Ljava/lang/String;

    .line 315
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->createStream()Z

    move-result v0

    return v0
.end method

.method public publishAudioData([BII)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "size"    # I
    .param p3, "dts"    # I

    .line 507
    if-eqz p1, :cond_1

    array-length v0, p1

    if-eqz v0, :cond_1

    if-ltz p3, :cond_1

    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 516
    :cond_0
    new-instance v0, Lcom/kint/kintframeworkaosaar/Audio;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/Audio;-><init>()V

    .line 517
    .local v0, "audio":Lcom/kint/kintframeworkaosaar/Audio;
    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/Audio;->setData([BI)V

    .line 518
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Audio;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setAbsoluteTimestamp(I)V

    .line 519
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Audio;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 520
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 521
    return-void

    .line 514
    .end local v0    # "audio":Lcom/kint/kintframeworkaosaar/Audio;
    :cond_1
    :goto_0
    return-void
.end method

.method public publishVideoData([BII)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "size"    # I
    .param p3, "dts"    # I

    .line 526
    if-eqz p1, :cond_1

    array-length v0, p1

    if-eqz v0, :cond_1

    if-ltz p3, :cond_1

    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->connected:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->publishPermitted:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 535
    :cond_0
    new-instance v0, Lcom/kint/kintframeworkaosaar/Video;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/Video;-><init>()V

    .line 536
    .local v0, "video":Lcom/kint/kintframeworkaosaar/Video;
    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/Video;->setData([BI)V

    .line 537
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Video;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setAbsoluteTimestamp(I)V

    .line 538
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/Video;->getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->currentStreamId:I

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setMessageStreamId(I)V

    .line 539
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpConnection;->sendRtmpPacket(Lcom/kint/kintframeworkaosaar/RtmpPacket;)V

    .line 540
    return-void

    .line 533
    .end local v0    # "video":Lcom/kint/kintframeworkaosaar/Video;
    :cond_1
    :goto_0
    return-void
.end method

.method public setAuthorization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .line 806
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->user:Ljava/lang/String;

    .line 807
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->password:Ljava/lang/String;

    .line 808
    return-void
.end method

.method public setVideoRate(II)V
    .locals 0
    .param p1, "videoDataRate"    # I
    .param p2, "frameRate"    # I

    .line 798
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoDataRate:I

    .line 799
    iput p2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoFrameRate:I

    .line 800
    return-void
.end method

.method public setVideoResolution(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 790
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoWidth:I

    .line 791
    iput p2, p0, Lcom/kint/kintframeworkaosaar/RtmpConnection;->videoHeight:I

    .line 792
    return-void
.end method
