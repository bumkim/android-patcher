.class public Lcom/kint/kintframeworkaosaar/WindowAckRequired;
.super Ljava/lang/Exception;
.source "WindowAckRequired.java"


# instance fields
.field private bytesRead:I

.field private rtmpPacket:Lcom/kint/kintframeworkaosaar/RtmpPacket;


# direct methods
.method public constructor <init>(ILcom/kint/kintframeworkaosaar/RtmpPacket;)V
    .locals 0
    .param p1, "bytesReadThusFar"    # I
    .param p2, "rtmpPacket"    # Lcom/kint/kintframeworkaosaar/RtmpPacket;

    .line 25
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/WindowAckRequired;->rtmpPacket:Lcom/kint/kintframeworkaosaar/RtmpPacket;

    .line 27
    iput p1, p0, Lcom/kint/kintframeworkaosaar/WindowAckRequired;->bytesRead:I

    .line 28
    return-void
.end method


# virtual methods
.method public getBytesRead()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/kint/kintframeworkaosaar/WindowAckRequired;->bytesRead:I

    return v0
.end method

.method public getRtmpPacket()Lcom/kint/kintframeworkaosaar/RtmpPacket;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/WindowAckRequired;->rtmpPacket:Lcom/kint/kintframeworkaosaar/RtmpPacket;

    return-object v0
.end method
