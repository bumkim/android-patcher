.class public abstract Lcom/kint/kintframeworkaosaar/RtmpPacket;
.super Ljava/lang/Object;
.source "RtmpPacket.java"


# instance fields
.field protected header:Lcom/kint/kintframeworkaosaar/RtmpHeader;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract array()[B
.end method

.method public getHeader()Lcom/kint/kintframeworkaosaar/RtmpHeader;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    return-object v0
.end method

.method public abstract readBody(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract size()I
.end method

.method protected abstract writeBody(Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeTo(Ljava/io/OutputStream;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "chunkSize"    # I
    .param p3, "chunkStreamInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 35
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 36
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->writeBody(Ljava/io/OutputStream;)V

    .line 37
    instance-of v1, p0, Lcom/kint/kintframeworkaosaar/ContentData;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->array()[B

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 38
    .local v1, "body":[B
    :goto_0
    instance-of v2, p0, Lcom/kint/kintframeworkaosaar/ContentData;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;->size()I

    move-result v2

    goto :goto_1

    :cond_1
    array-length v2, v1

    .line 39
    .local v2, "length":I
    :goto_1
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    invoke-virtual {v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->setPacketLength(I)V

    .line 41
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/RtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v4, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v3, p1, v4, p3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->writeTo(Ljava/io/OutputStream;Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 42
    const/4 v3, 0x0

    .line 43
    .local v3, "pos":I
    :goto_2
    if-le v2, p2, :cond_2

    .line 45
    invoke-virtual {p1, v1, v3, p2}, Ljava/io/OutputStream;->write([BII)V

    .line 46
    sub-int/2addr v2, p2

    .line 47
    add-int/2addr v3, p2

    .line 49
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/RtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v5, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_3_RELATIVE_SINGLE_BYTE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v4, p1, v5, p3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->writeTo(Ljava/io/OutputStream;Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    goto :goto_2

    .line 51
    :cond_2
    invoke-virtual {p1, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 52
    return-void
.end method
