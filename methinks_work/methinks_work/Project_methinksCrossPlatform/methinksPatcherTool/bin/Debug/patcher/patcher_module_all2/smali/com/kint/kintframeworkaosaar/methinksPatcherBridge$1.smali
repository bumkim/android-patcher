.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;
.super Landroid/content/BroadcastReceiver;
.source "methinksPatcherBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnCreate(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

.field final synthetic val$intentFilter:Landroid/content/IntentFilter;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/content/IntentFilter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 222
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->val$intentFilter:Landroid/content/IntentFilter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 225
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "methinksPatcherBridge"

    if-eqz v0, :cond_0

    .line 227
    const-string v0, "[onCreate] : Screen On"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    const-string v0, "[onCreate] : Screen Off"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;->val$intentFilter:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 243
    const/4 v0, 0x0

    const-string v2, "state"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    sput-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->IsEarphoneOn:Z

    .line 244
    sget-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->IsEarphoneOn:Z

    if-eqz v0, :cond_3

    .line 246
    const-string v0, "[onCreate] : Earphone is plugged"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 250
    :cond_3
    const-string v0, "[onCreate] : Earphone is unPlugged"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :cond_4
    :goto_0
    return-void
.end method
