.class Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
.super Ljava/lang/Object;
.source "SrsFlvMuxer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SrsFlvFrame"
.end annotation


# instance fields
.field public avc_aac_type:I

.field public dts:I

.field public flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

.field public frame_type:I

.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

.field public type:I


# direct methods
.method private constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->this$0:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p2, "x1"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;

    .line 363
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V

    return-void
.end method


# virtual methods
.method public is_audio()Z
    .locals 2

    .line 388
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->type:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public is_keyframe()Z
    .locals 2

    .line 376
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_video()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->frame_type:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public is_sequenceHeader()Z
    .locals 1

    .line 380
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->avc_aac_type:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public is_video()Z
    .locals 2

    .line 384
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->type:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
