.class public Lcom/kint/kintframeworkaosaar/MessagePack;
.super Ljava/lang/Object;
.source "MessagePack.java"


# static fields
.field public static final CONNECTION_ANSWER:Ljava/lang/String; = "answer"

.field public static final CONNECTION_CAMERA_CAPTURE:Ljava/lang/String; = "picCapture"

.field public static final CONNECTION_CAMERA_CAPTURE_INFO:Ljava/lang/String; = "picCaptureInfo"

.field public static final CONNECTION_CAPTURE:Ljava/lang/String; = "capture"

.field public static final CONNECTION_CAPTURE_DETAIL:Ljava/lang/String; = "captureDetail"

.field public static final CONNECTION_EVENT:Ljava/lang/String; = "event"

.field public static final CONNECTION_FRONT_CAM_SHOT:Ljava/lang/String; = "frontStillShot"

.field public static final CONNECTION_LOG:Ljava/lang/String; = "log"

.field public static final CONNECTION_LOGIN:Ljava/lang/String; = "login"

.field public static final CONNECTION_NOTICE:Ljava/lang/String; = "notice"

.field public static final CONNECTION_QUESTION:Ljava/lang/String; = "question"

.field public static final PREPARE_LOGIN:Ljava/lang/String; = "prepare_login"

.field public static final PREPARE_QUESTION:Ljava/lang/String; = "prepare_question"


# instance fields
.field protected _key:I

.field protected _value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetDeviceInfoInLog(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;Landroid/app/Activity;)Ljava/lang/String;
    .locals 35
    .param p0, "audioManager"    # Landroid/media/AudioManager;
    .param p1, "connectivityManager"    # Landroid/net/ConnectivityManager;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p3, "screenBrightness"    # I
    .param p4, "windowManager"    # Landroid/view/WindowManager;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "currentActivity"    # Landroid/app/Activity;

    .line 672
    move-object/from16 v1, p0

    move-object/from16 v2, p5

    const-string v0, "Built-in speaker on an Android device"

    const-string v3, "%"

    const-string v4, ""

    .line 675
    .local v4, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 676
    .local v5, "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 679
    .local v6, "jsonObject":Lorg/json/JSONObject;
    const/4 v7, 0x1

    .line 680
    .local v7, "audioStreamType":I
    invoke-virtual {v1, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v8

    .line 681
    .local v8, "currentStreamVolume":I
    invoke-virtual {v1, v7}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v9

    .line 682
    .local v9, "maxStreamVolume":I
    mul-int/lit8 v10, v8, 0x64

    div-int/2addr v10, v9

    .line 684
    .local v10, "volumeLevel":I
    move-object v11, v0

    .line 685
    .local v11, "soundOutputDevice":Ljava/lang/String;
    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v13, 0x17

    const-string v14, "BluetoothSCO"

    const-string v16, "BluetoothA2DP"

    const-string v17, "Headphones"

    const/16 v18, 0x0

    const/4 v15, 0x1

    if-ge v12, v13, :cond_4

    .line 687
    :try_start_1
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    sget-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->IsEarphoneOn:Z

    if-ne v15, v0, :cond_0

    .line 689
    move-object/from16 v0, v17

    move-object/from16 v20, v4

    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .local v0, "soundOutputDevice":Ljava/lang/String;
    goto/16 :goto_3

    .line 693
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 695
    move-object/from16 v11, v16

    .line 698
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 700
    const-string v0, "Speakerphone"

    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    goto :goto_0

    .line 698
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_2
    move-object v0, v11

    .line 703
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v11
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v11, :cond_3

    .line 705
    move-object v0, v14

    move-object/from16 v20, v4

    goto :goto_3

    .line 703
    :cond_3
    move-object/from16 v20, v4

    goto :goto_3

    .line 893
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .end local v5    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v7    # "audioStreamType":I
    .end local v8    # "currentStreamVolume":I
    .end local v9    # "maxStreamVolume":I
    .end local v10    # "volumeLevel":I
    :catch_0
    move-exception v0

    move-object/from16 v20, v4

    goto/16 :goto_8

    .line 713
    .restart local v5    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "audioStreamType":I
    .restart local v8    # "currentStreamVolume":I
    .restart local v9    # "maxStreamVolume":I
    .restart local v10    # "volumeLevel":I
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_4
    const/4 v12, 0x2

    :try_start_2
    invoke-virtual {v1, v12}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v13

    move-object v12, v13

    .line 714
    .local v12, "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    move/from16 v13, v18

    .local v13, "countIndex":I
    :goto_1
    array-length v15, v12

    if-ge v13, v15, :cond_a

    .line 716
    aget-object v15, v12, v13

    .line 717
    .local v15, "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    invoke-virtual {v15}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v20, v4

    const/4 v4, 0x1

    .end local v4    # "jsonString":Ljava/lang/String;
    .local v20, "jsonString":Ljava/lang/String;
    if-eq v1, v4, :cond_9

    const/4 v4, 0x2

    if-eq v1, v4, :cond_8

    const/4 v4, 0x4

    if-eq v1, v4, :cond_7

    const/4 v4, 0x7

    if-eq v1, v4, :cond_6

    const/16 v4, 0x8

    if-eq v1, v4, :cond_5

    goto :goto_2

    .line 727
    :cond_5
    move-object/from16 v1, v16

    .line 729
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .local v1, "soundOutputDevice":Ljava/lang/String;
    move-object v11, v1

    goto :goto_2

    .line 733
    .end local v1    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_6
    move-object v1, v14

    .line 735
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v1    # "soundOutputDevice":Ljava/lang/String;
    move-object v11, v1

    goto :goto_2

    .line 721
    .end local v1    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_7
    move-object/from16 v1, v17

    .line 723
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v1    # "soundOutputDevice":Ljava/lang/String;
    move-object v11, v1

    goto :goto_2

    .line 745
    .end local v1    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_8
    move-object v1, v0

    move-object v11, v1

    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v1    # "soundOutputDevice":Ljava/lang/String;
    goto :goto_2

    .line 739
    .end local v1    # "soundOutputDevice":Ljava/lang/String;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :cond_9
    :try_start_3
    const-string v1, "Built in Earpiece"

    .line 741
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .restart local v1    # "soundOutputDevice":Ljava/lang/String;
    move-object v11, v1

    .line 714
    .end local v1    # "soundOutputDevice":Ljava/lang/String;
    .end local v15    # "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    .restart local v11    # "soundOutputDevice":Ljava/lang/String;
    :goto_2
    add-int/lit8 v13, v13, 0x1

    const/4 v15, 0x1

    move-object/from16 v1, p0

    move-object/from16 v4, v20

    goto :goto_1

    .end local v20    # "jsonString":Ljava/lang/String;
    .restart local v4    # "jsonString":Ljava/lang/String;
    :cond_a
    move-object/from16 v20, v4

    .end local v4    # "jsonString":Ljava/lang/String;
    .restart local v20    # "jsonString":Ljava/lang/String;
    move-object v0, v11

    .line 751
    .end local v11    # "soundOutputDevice":Ljava/lang/String;
    .end local v12    # "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    .end local v13    # "countIndex":I
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    :goto_3
    const-string v1, "Sound Output Device"

    invoke-virtual {v6, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 752
    const-string v1, "ScreenSize"

    invoke-static/range {p6 .. p6}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetScreenInches(Landroid/app/Activity;)D

    move-result-wide v11

    invoke-virtual {v6, v1, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 754
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    .line 755
    .local v1, "info":Ljava/lang/Runtime;
    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v11

    .line 756
    .local v11, "freeSize":J
    invoke-virtual {v1}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v13

    .line 757
    .local v13, "totalSize":J
    move-object v4, v0

    move-object v15, v1

    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .end local v1    # "info":Ljava/lang/Runtime;
    .local v4, "soundOutputDevice":Ljava/lang/String;
    .local v15, "info":Ljava/lang/Runtime;
    sub-long v0, v13, v11

    .line 759
    .local v0, "usedSize":J
    move-object/from16 v16, v4

    .end local v4    # "soundOutputDevice":Ljava/lang/String;
    .local v16, "soundOutputDevice":Ljava/lang/String;
    const-string v4, "Memory Usage"

    move/from16 v17, v7

    .end local v7    # "audioStreamType":I
    .local v17, "audioStreamType":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v21, v8

    .end local v8    # "currentStreamVolume":I
    .local v21, "currentStreamVolume":I
    long-to-float v8, v0

    move-wide/from16 v22, v0

    .end local v0    # "usedSize":J
    .local v22, "usedSize":J
    long-to-float v0, v13

    div-float/2addr v8, v0

    const/high16 v0, 0x42c80000    # 100.0f

    mul-float v8, v8, v0

    float-to-int v1, v8

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 760
    const/4 v1, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 761
    .local v1, "batteryStatus":Landroid/content/Intent;
    const-string v4, "plugged"

    const/4 v7, -0x1

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 763
    .local v4, "plugged":I
    const-string v8, "plugged out"

    .line 764
    .local v8, "pluggedStatus":Ljava/lang/String;
    const/4 v0, 0x1

    if-eq v4, v0, :cond_b

    const/4 v0, 0x2

    if-eq v4, v0, :cond_b

    const/4 v0, 0x4

    if-ne v4, v0, :cond_c

    .line 765
    :cond_b
    const-string v0, "plugged in"

    move-object v8, v0

    .line 767
    :cond_c
    const-string v0, "level"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 768
    .local v0, "level":I
    const-string v7, "scale"

    move/from16 v25, v4

    const/4 v4, -0x1

    .end local v4    # "plugged":I
    .local v25, "plugged":I
    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 770
    .local v7, "scale":I
    int-to-float v4, v0

    move/from16 v26, v0

    .end local v0    # "level":I
    .local v26, "level":I
    int-to-float v0, v7

    div-float/2addr v4, v0

    .line 771
    .local v4, "batteryPct":F
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float v0, v0, v4

    float-to-int v0, v0

    .line 773
    .local v0, "batteryPercent":I
    move-object/from16 v24, v1

    .end local v1    # "batteryStatus":Landroid/content/Intent;
    .local v24, "batteryStatus":Landroid/content/Intent;
    const-string v1, "Battery Status"

    move/from16 v27, v4

    .end local v4    # "batteryPct":F
    .local v27, "batteryPct":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v28, v7

    .end local v7    # "scale":I
    .local v28, "scale":I
    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 774
    const-string v1, "OS Version"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v6, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 775
    const-string v1, "Volume Level(db)"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 777
    invoke-static/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v1

    .line 778
    .local v1, "connectivity":Ljava/lang/String;
    const-string v4, "0.587MB/s"

    .line 779
    .local v4, "currrentConnectionSpeed":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v7

    move/from16 v29, v0

    .end local v0    # "batteryPercent":I
    .local v29, "batteryPercent":I
    const/16 v0, 0x674

    move-object/from16 v30, v4

    .end local v4    # "currrentConnectionSpeed":Ljava/lang/String;
    .local v30, "currrentConnectionSpeed":Ljava/lang/String;
    const/4 v4, 0x3

    if-eq v7, v0, :cond_11

    const v0, 0x127bd

    if-eq v7, v0, :cond_10

    const v0, 0x2065bd

    if-eq v7, v0, :cond_f

    const v0, 0x292335

    if-eq v7, v0, :cond_e

    :cond_d
    goto :goto_4

    :cond_e
    const-string v0, "Wifi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    goto :goto_5

    :cond_f
    const-string v0, "EDGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    goto :goto_5

    :cond_10
    const-string v0, "LTE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x3

    goto :goto_5

    :cond_11
    const-string v0, "3G"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x2

    goto :goto_5

    :goto_4
    const/4 v0, -0x1

    :goto_5
    if-eqz v0, :cond_13

    const/4 v7, 0x1

    if-eq v0, v7, :cond_12

    const/4 v7, 0x2

    if-eq v0, v7, :cond_12

    if-eq v0, v4, :cond_12

    move-object/from16 v0, v30

    goto :goto_6

    .line 791
    :cond_12
    const/4 v0, 0x1

    invoke-static {v2, v0}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetMobileSpeed(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    .end local v30    # "currrentConnectionSpeed":Ljava/lang/String;
    .local v7, "currrentConnectionSpeed":Ljava/lang/String;
    goto :goto_6

    .line 783
    .end local v7    # "currrentConnectionSpeed":Ljava/lang/String;
    .restart local v30    # "currrentConnectionSpeed":Ljava/lang/String;
    :cond_13
    const/4 v0, 0x1

    invoke-static {v2, v0}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetWifiSpeed(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    .line 785
    .end local v30    # "currrentConnectionSpeed":Ljava/lang/String;
    .local v0, "currrentConnectionSpeed":Ljava/lang/String;
    nop

    .line 796
    :goto_6
    const-string v7, "Current Connection Speed"

    invoke-virtual {v6, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 797
    const-string v7, "Device"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v6, v7, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 798
    const-string v4, "Free Space"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalAvailableMemory()J

    move-result-wide v30

    const-wide/32 v32, 0x100000

    move-object/from16 v34, v8

    move/from16 v19, v9

    .end local v8    # "pluggedStatus":Ljava/lang/String;
    .end local v9    # "maxStreamVolume":I
    .local v19, "maxStreamVolume":I
    .local v34, "pluggedStatus":Ljava/lang/String;
    div-long v8, v30, v32

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, " MB / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalStorageAllMemory()J

    move-result-wide v8

    div-long v8, v8, v32

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, " MB "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 801
    const/16 v4, 0xa

    .line 824
    .local v4, "checkCount":I
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 826
    .local v7, "latencyJsonObject":Lorg/json/JSONObject;
    const-string v8, "13.52.0.0"

    invoke-static {v8, v4}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverageString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 827
    .local v8, "averagePingValue":Ljava/lang/String;
    const-string v9, "US_WEST"

    invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 833
    const-string v9, "23.23.255.255"

    invoke-static {v9, v4}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverageString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    move-object v8, v9

    .line 834
    const-string v9, "US_EAST"

    invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 839
    const-string v9, "13.124.63.251"

    invoke-static {v9, v4}, Lcom/kint/kintframeworkaosaar/MessagePack;->_PingAverageString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    move-object v8, v9

    .line 840
    const-string v9, "SEOUL"

    invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 844
    const-string v9, "Current Latency"

    invoke-virtual {v6, v9, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 847
    const-string v9, "Connectivity"

    invoke-virtual {v6, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 848
    mul-int/lit8 v9, p3, 0x64

    div-int/lit16 v9, v9, 0xff

    .line 849
    .local v9, "screenBrightnessLevel":I
    move-object/from16 v30, v0

    .end local v0    # "currrentConnectionSpeed":Ljava/lang/String;
    .restart local v30    # "currrentConnectionSpeed":Ljava/lang/String;
    const-string v0, "Screen Brightness"

    move-object/from16 v31, v1

    .end local v1    # "connectivity":Ljava/lang/String;
    .local v31, "connectivity":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 852
    const-string v0, ""

    .line 853
    .local v0, "screenOrientation":Ljava/lang/String;
    invoke-interface/range {p4 .. p4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-eqz v1, :cond_17

    const/4 v3, 0x1

    if-eq v1, v3, :cond_16

    const/4 v3, 0x2

    if-eq v1, v3, :cond_15

    const/4 v3, 0x3

    if-eq v1, v3, :cond_14

    goto :goto_7

    .line 879
    :cond_14
    const-string v1, "Device oriented horizontally, home button on the left"

    move-object v0, v1

    goto :goto_7

    .line 872
    :cond_15
    const-string v1, "Device oriented vertically, home button on the top"

    move-object v0, v1

    .line 874
    goto :goto_7

    .line 865
    :cond_16
    const-string v1, "Device oriented horizontally, home button on the right"

    move-object v0, v1

    .line 867
    goto :goto_7

    .line 858
    :cond_17
    const-string v1, "Device oriented vertically, home button on the bottom"

    move-object v0, v1

    .line 860
    nop

    .line 884
    :goto_7
    const-string v1, "Screen Orientation"

    invoke-virtual {v6, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 885
    const-string v1, "Carrier"

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 887
    const-string v1, "deviceInfo"

    invoke-virtual {v5, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 890
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v4, v1

    .line 896
    .end local v0    # "screenOrientation":Ljava/lang/String;
    .end local v5    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v7    # "latencyJsonObject":Lorg/json/JSONObject;
    .end local v8    # "averagePingValue":Ljava/lang/String;
    .end local v9    # "screenBrightnessLevel":I
    .end local v10    # "volumeLevel":I
    .end local v11    # "freeSize":J
    .end local v13    # "totalSize":J
    .end local v15    # "info":Ljava/lang/Runtime;
    .end local v16    # "soundOutputDevice":Ljava/lang/String;
    .end local v17    # "audioStreamType":I
    .end local v19    # "maxStreamVolume":I
    .end local v20    # "jsonString":Ljava/lang/String;
    .end local v21    # "currentStreamVolume":I
    .end local v22    # "usedSize":J
    .end local v24    # "batteryStatus":Landroid/content/Intent;
    .end local v25    # "plugged":I
    .end local v26    # "level":I
    .end local v27    # "batteryPct":F
    .end local v28    # "scale":I
    .end local v29    # "batteryPercent":I
    .end local v30    # "currrentConnectionSpeed":Ljava/lang/String;
    .end local v31    # "connectivity":Ljava/lang/String;
    .end local v34    # "pluggedStatus":Ljava/lang/String;
    .local v4, "jsonString":Ljava/lang/String;
    goto :goto_9

    .line 893
    .end local v4    # "jsonString":Ljava/lang/String;
    .restart local v20    # "jsonString":Ljava/lang/String;
    :catch_1
    move-exception v0

    goto :goto_8

    .end local v20    # "jsonString":Ljava/lang/String;
    .restart local v4    # "jsonString":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v20, v4

    .line 895
    .end local v4    # "jsonString":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v20    # "jsonString":Ljava/lang/String;
    :goto_8
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object/from16 v4, v20

    .line 898
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v20    # "jsonString":Ljava/lang/String;
    .restart local v4    # "jsonString":Ljava/lang/String;
    :goto_9
    return-object v4
.end method

.method public static GetDeviceInfoInLogin(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;Landroid/app/Activity;)Ljava/lang/String;
    .locals 31
    .param p0, "audioManager"    # Landroid/media/AudioManager;
    .param p1, "connectivityManager"    # Landroid/net/ConnectivityManager;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p3, "screenBrightness"    # I
    .param p4, "windowManager"    # Landroid/view/WindowManager;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "currentActivity"    # Landroid/app/Activity;

    .line 324
    move-object/from16 v1, p0

    const-string v0, "Built-in speaker on an Android device"

    const-string v2, "%"

    const-string v3, ""

    .line 327
    .local v3, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 328
    .local v4, "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 330
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "Device"

    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 331
    const-string v6, "OS Version"

    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 332
    const-string v6, "Carrier"

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 333
    const-string v6, "Connectivity"

    invoke-static/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->_GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 334
    const-string v6, "ScreenSize"

    invoke-static/range {p6 .. p6}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetScreenInches(Landroid/app/Activity;)D

    move-result-wide v7

    invoke-virtual {v5, v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 337
    move-object v6, v0

    .line 338
    .local v6, "soundOutputDevice":Ljava/lang/String;
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    const/16 v8, 0x17

    const-string v9, "BluetoothSCO"

    const-string v11, "BluetoothA2DP"

    const-string v12, "Headphones"

    const/4 v13, 0x2

    const/4 v14, 0x1

    if-ge v7, v8, :cond_3

    .line 340
    :try_start_1
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    sget-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->IsEarphoneOn:Z

    if-ne v14, v0, :cond_0

    .line 342
    move-object v0, v12

    .end local v6    # "soundOutputDevice":Ljava/lang/String;
    .local v0, "soundOutputDevice":Ljava/lang/String;
    goto :goto_3

    .line 346
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .restart local v6    # "soundOutputDevice":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    move-object v6, v11

    .line 351
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353
    const-string v0, "Speakerphone"

    .end local v6    # "soundOutputDevice":Ljava/lang/String;
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    goto :goto_0

    .line 351
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .restart local v6    # "soundOutputDevice":Ljava/lang/String;
    :cond_2
    move-object v0, v6

    .line 356
    .end local v6    # "soundOutputDevice":Ljava/lang/String;
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v6
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v6, :cond_a

    .line 358
    move-object v0, v9

    goto :goto_3

    .line 508
    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .end local v4    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    move-object/from16 v15, p5

    move-object/from16 v16, v3

    goto/16 :goto_8

    .line 366
    .restart local v4    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "soundOutputDevice":Ljava/lang/String;
    :cond_3
    :try_start_2
    invoke-virtual {v1, v13}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v7

    .line 367
    .local v7, "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    const/4 v8, 0x0

    .local v8, "countIndex":I
    :goto_1
    array-length v15, v7
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    if-ge v8, v15, :cond_9

    .line 369
    :try_start_3
    aget-object v15, v7, v8

    .line 370
    .local v15, "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    invoke-virtual {v15}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v10

    if-eq v10, v14, :cond_8

    if-eq v10, v13, :cond_7

    const/4 v13, 0x4

    if-eq v10, v13, :cond_6

    const/4 v13, 0x7

    if-eq v10, v13, :cond_5

    const/16 v13, 0x8

    if-eq v10, v13, :cond_4

    goto :goto_2

    .line 380
    :cond_4
    move-object v6, v11

    .line 382
    goto :goto_2

    .line 386
    :cond_5
    move-object v6, v9

    .line 388
    goto :goto_2

    .line 374
    :cond_6
    move-object v6, v12

    .line 376
    goto :goto_2

    .line 398
    :cond_7
    move-object v6, v0

    goto :goto_2

    .line 392
    :cond_8
    const-string v10, "Built in Earpiece"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v6, v10

    .line 394
    nop

    .line 367
    .end local v15    # "audioDeviceInfo":Landroid/media/AudioDeviceInfo;
    :goto_2
    add-int/lit8 v8, v8, 0x1

    const/4 v13, 0x2

    goto :goto_1

    :cond_9
    move-object v0, v6

    .line 404
    .end local v6    # "soundOutputDevice":Ljava/lang/String;
    .end local v7    # "audioDeviceInfos":[Landroid/media/AudioDeviceInfo;
    .end local v8    # "countIndex":I
    .restart local v0    # "soundOutputDevice":Ljava/lang/String;
    :cond_a
    :goto_3
    :try_start_4
    const-string v6, "Sound Output Device"

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 406
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    .line 407
    .local v6, "info":Ljava/lang/Runtime;
    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    .line 408
    .local v7, "freeSize":J
    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v9

    .line 409
    .local v9, "totalSize":J
    sub-long v11, v9, v7

    .line 411
    .local v11, "usedSize":J
    const-string v13, "Memory Usage"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v14, v11

    move-object/from16 v19, v0

    .end local v0    # "soundOutputDevice":Ljava/lang/String;
    .local v19, "soundOutputDevice":Ljava/lang/String;
    long-to-float v0, v9

    div-float/2addr v14, v0

    const/high16 v0, 0x42c80000    # 100.0f

    mul-float v14, v14, v0

    float-to-int v14, v14

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 412
    const/4 v13, 0x0

    new-instance v14, Landroid/content/IntentFilter;

    const-string v15, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v14, v15}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3

    move-object/from16 v15, p5

    :try_start_5
    invoke-virtual {v15, v13, v14}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v13

    .line 413
    .local v13, "batteryStatus":Landroid/content/Intent;
    const-string v14, "plugged"

    const/4 v0, -0x1

    invoke-virtual {v13, v14, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 415
    .local v14, "plugged":I
    const-string v21, "plugged out"

    .line 416
    .local v21, "pluggedStatus":Ljava/lang/String;
    const/4 v0, 0x1

    if-eq v14, v0, :cond_c

    const/4 v0, 0x2

    if-eq v14, v0, :cond_c

    const/4 v0, 0x4

    if-ne v14, v0, :cond_b

    goto :goto_4

    :cond_b
    move-object/from16 v0, v21

    goto :goto_5

    .line 417
    :cond_c
    :goto_4
    const-string v0, "plugged in"
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    .line 419
    .end local v21    # "pluggedStatus":Ljava/lang/String;
    .local v0, "pluggedStatus":Ljava/lang/String;
    :goto_5
    move-object/from16 v16, v3

    .end local v3    # "jsonString":Ljava/lang/String;
    .local v16, "jsonString":Ljava/lang/String;
    :try_start_6
    const-string v3, "level"

    move-object/from16 v21, v6

    const/4 v6, -0x1

    .end local v6    # "info":Ljava/lang/Runtime;
    .local v21, "info":Ljava/lang/Runtime;
    invoke-virtual {v13, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 420
    .local v3, "level":I
    const-string v6, "scale"

    move-wide/from16 v23, v7

    const/4 v7, -0x1

    .end local v7    # "freeSize":J
    .local v23, "freeSize":J
    invoke-virtual {v13, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 422
    .local v6, "scale":I
    int-to-float v7, v3

    int-to-float v8, v6

    div-float/2addr v7, v8

    .line 423
    .local v7, "batteryPct":F
    const/high16 v8, 0x42c80000    # 100.0f

    mul-float v8, v8, v7

    float-to-int v8, v8

    .line 425
    .local v8, "batteryPercent":I
    move/from16 v20, v3

    .end local v3    # "level":I
    .local v20, "level":I
    const-string v3, "Battery Status"

    move/from16 v22, v6

    .end local v6    # "scale":I
    .local v22, "scale":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v25, v0

    .end local v0    # "pluggedStatus":Ljava/lang/String;
    .local v25, "pluggedStatus":Ljava/lang/String;
    const-string v0, ", "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 427
    const-string v0, "Free Space"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalAvailableMemory()J

    move-result-wide v26

    const-wide/32 v28, 0x100000

    move/from16 v30, v7

    .end local v7    # "batteryPct":F
    .local v30, "batteryPct":F
    div-long v6, v26, v28

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " MB / "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/kint/kintframeworkaosaar/MessagePack;->checkInternalStorageAllMemory()J

    move-result-wide v6

    div-long v6, v6, v28

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " MB "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 430
    const-string v0, ""

    .line 431
    .local v0, "screenOrientation":Ljava/lang/String;
    invoke-interface/range {p4 .. p4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    if-eqz v3, :cond_10

    const/4 v6, 0x1

    if-eq v3, v6, :cond_f

    const/4 v6, 0x2

    if-eq v3, v6, :cond_e

    const/4 v6, 0x3

    if-eq v3, v6, :cond_d

    goto :goto_6

    .line 457
    :cond_d
    const-string v3, "Device oriented horizontally, home button on the left"

    move-object v0, v3

    goto :goto_6

    .line 450
    :cond_e
    const-string v3, "Device oriented vertically, home button on the top"

    move-object v0, v3

    .line 452
    goto :goto_6

    .line 443
    :cond_f
    const-string v3, "Device oriented horizontally, home button on the right"

    move-object v0, v3

    .line 445
    goto :goto_6

    .line 436
    :cond_10
    const-string v3, "Device oriented vertically, home button on the bottom"

    move-object v0, v3

    .line 438
    nop

    .line 462
    :goto_6
    const-string v3, "Screen Orientation"

    invoke-virtual {v5, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 492
    const/4 v3, 0x1

    .line 493
    .local v3, "audioStreamType":I
    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v6

    .line 494
    .local v6, "currentStreamVolume":I
    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v7

    .line 495
    .local v7, "maxStreamVolume":I
    mul-int/lit8 v17, v6, 0x64

    div-int v17, v17, v7

    move/from16 v18, v17

    .line 497
    .local v18, "volumeLevel":I
    move-object/from16 v17, v0

    .end local v0    # "screenOrientation":Ljava/lang/String;
    .local v17, "screenOrientation":Ljava/lang/String;
    const-string v0, "Volume Level(db)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v26, v3

    move/from16 v3, v18

    .end local v18    # "volumeLevel":I
    .local v3, "volumeLevel":I
    .local v26, "audioStreamType":I
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 500
    mul-int/lit8 v0, p3, 0x64

    div-int/lit16 v0, v0, 0xff

    .line 501
    .local v0, "screenBrightnessLevel":I
    const-string v1, "Screen Brightness"

    move/from16 v18, v3

    .end local v3    # "volumeLevel":I
    .restart local v18    # "volumeLevel":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 503
    const-string v1, "deviceInfo"

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 506
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1

    move-object v3, v1

    .line 511
    .end local v0    # "screenBrightnessLevel":I
    .end local v4    # "jsonObjectDeviceInfo":Lorg/json/JSONObject;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "currentStreamVolume":I
    .end local v7    # "maxStreamVolume":I
    .end local v8    # "batteryPercent":I
    .end local v9    # "totalSize":J
    .end local v11    # "usedSize":J
    .end local v13    # "batteryStatus":Landroid/content/Intent;
    .end local v14    # "plugged":I
    .end local v16    # "jsonString":Ljava/lang/String;
    .end local v17    # "screenOrientation":Ljava/lang/String;
    .end local v18    # "volumeLevel":I
    .end local v19    # "soundOutputDevice":Ljava/lang/String;
    .end local v20    # "level":I
    .end local v21    # "info":Ljava/lang/Runtime;
    .end local v22    # "scale":I
    .end local v23    # "freeSize":J
    .end local v25    # "pluggedStatus":Ljava/lang/String;
    .end local v26    # "audioStreamType":I
    .end local v30    # "batteryPct":F
    .local v3, "jsonString":Ljava/lang/String;
    move-object/from16 v16, v3

    goto :goto_9

    .line 508
    .end local v3    # "jsonString":Ljava/lang/String;
    .restart local v16    # "jsonString":Ljava/lang/String;
    :catch_1
    move-exception v0

    goto :goto_8

    .end local v16    # "jsonString":Ljava/lang/String;
    .restart local v3    # "jsonString":Ljava/lang/String;
    :catch_2
    move-exception v0

    goto :goto_7

    :catch_3
    move-exception v0

    move-object/from16 v15, p5

    :goto_7
    move-object/from16 v16, v3

    .line 510
    .end local v3    # "jsonString":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v16    # "jsonString":Ljava/lang/String;
    :goto_8
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 513
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_9
    return-object v16
.end method

.method public static GetScreenInches(Landroid/app/Activity;)D
    .locals 17
    .param p0, "currentActivity"    # Landroid/app/Activity;

    .line 298
    if-nez p0, :cond_0

    .line 299
    const-wide/16 v0, 0x0

    return-wide v0

    .line 301
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 302
    .local v0, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 303
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 304
    .local v1, "width":I
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 305
    .local v2, "height":I
    int-to-double v3, v1

    iget v5, v0, Landroid/util/DisplayMetrics;->xdpi:F

    float-to-double v5, v5

    div-double/2addr v3, v5

    .line 306
    .local v3, "wi":D
    int-to-double v5, v2

    iget v7, v0, Landroid/util/DisplayMetrics;->ydpi:F

    float-to-double v7, v7

    div-double/2addr v5, v7

    .line 307
    .local v5, "hi":D
    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    .line 308
    .local v9, "x":D
    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    .line 309
    .local v7, "y":D
    add-double v11, v9, v7

    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v11

    .line 312
    .local v11, "screenInches":D
    const-wide/high16 v13, 0x4024000000000000L    # 10.0

    mul-double v11, v11, v13

    .line 313
    move-object v15, v0

    move/from16 v16, v1

    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    .end local v1    # "width":I
    .local v15, "dm":Landroid/util/DisplayMetrics;
    .local v16, "width":I
    invoke-static {v11, v12}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v13

    .line 315
    .end local v11    # "screenInches":D
    .local v0, "screenInches":D
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "GetScreenInches - ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v12, "] inches"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "methinksPatcher"

    invoke-static {v12, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    return-wide v0
.end method

.method private static _GetMobileSpeed(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "withUnit"    # Z

    .line 564
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 565
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    .line 566
    .local v1, "networkType":I
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getMobileNetworkSpeed(I)F

    move-result v2

    .line 568
    .local v2, "netSpeed":F
    const-string v3, ""

    const/4 v4, 0x1

    if-ne v4, p1, :cond_0

    .line 569
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getMobileNetworkSpeedUnit(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 570
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static _GetNetworkClass(Landroid/net/ConnectivityManager;)Ljava/lang/String;
    .locals 3
    .param p0, "connectivityManager"    # Landroid/net/ConnectivityManager;

    .line 177
    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 179
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 181
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 182
    const-string v1, "Wifi"

    return-object v1

    .line 183
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const-string v2, "?"

    if-nez v1, :cond_2

    .line 185
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    .line 186
    .local v1, "networkType":I
    packed-switch v1, :pswitch_data_0

    .line 210
    :pswitch_0
    return-object v2

    .line 208
    :pswitch_1
    const-string v2, "LTE"

    return-object v2

    .line 204
    :pswitch_2
    const-string v2, "3G"

    return-object v2

    .line 193
    :pswitch_3
    const-string v2, "EDGE"

    return-object v2

    .line 213
    .end local v1    # "networkType":I
    :cond_2
    return-object v2

    .line 180
    :cond_3
    :goto_0
    const-string v1, "-"

    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static _GetWifiSpeed(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "withUnit"    # Z

    .line 537
    const/high16 v0, 0x3f000000    # 0.5f

    .line 538
    .local v0, "speed":F
    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    .line 539
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "Mbps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 540
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static _Ping(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "url"    # Ljava/lang/String;

    .line 89
    const-string v0, "methinksPatcherApp."

    const-string v1, ""

    .line 92
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ping -c 1 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 93
    .local v2, "process":Ljava/lang/Process;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 95
    .local v3, "reader":Ljava/io/BufferedReader;
    const/16 v4, 0x1000

    new-array v4, v4, [C

    .line 96
    .local v4, "buffer":[C
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 97
    .local v5, "output":Ljava/lang/StringBuffer;
    const/16 v6, 0x40

    new-array v6, v6, [Ljava/lang/String;

    .line 98
    .local v6, "op":[Ljava/lang/String;
    const/16 v7, 0x8

    new-array v7, v7, [Ljava/lang/String;

    .line 99
    .local v7, "delay":[Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v4}, Ljava/io/BufferedReader;->read([C)I

    move-result v8

    move v9, v8

    .local v9, "i":I
    if-lez v8, :cond_0

    .line 100
    const/4 v8, 0x0

    invoke-virtual {v5, v4, v8, v9}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 101
    :cond_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 102
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v10, "\n"

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    move-object v6, v8

    .line 103
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ping : output - "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    const-string v8, ""

    if-eqz v6, :cond_4

    :try_start_1
    array-length v10, v6

    const/4 v11, 0x2

    if-le v11, v10, :cond_1

    goto :goto_2

    .line 106
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ping : delay - "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v12, 0x1

    aget-object v13, v6, v12

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    aget-object v0, v6, v12

    const-string v10, "time="

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 108
    .end local v7    # "delay":[Ljava/lang/String;
    .local v0, "delay":[Ljava/lang/String;
    if-eqz v0, :cond_3

    array-length v7, v0

    if-le v11, v7, :cond_2

    goto :goto_1

    .line 112
    :cond_2
    aget-object v7, v0, v12
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v7

    .line 119
    .end local v0    # "delay":[Ljava/lang/String;
    .end local v2    # "process":Ljava/lang/Process;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "buffer":[C
    .end local v5    # "output":Ljava/lang/StringBuffer;
    .end local v6    # "op":[Ljava/lang/String;
    .end local v9    # "i":I
    goto :goto_3

    .line 109
    .restart local v0    # "delay":[Ljava/lang/String;
    .restart local v2    # "process":Ljava/lang/Process;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "buffer":[C
    .restart local v5    # "output":Ljava/lang/StringBuffer;
    .restart local v6    # "op":[Ljava/lang/String;
    .restart local v9    # "i":I
    :cond_3
    :goto_1
    return-object v8

    .line 105
    .end local v0    # "delay":[Ljava/lang/String;
    .restart local v7    # "delay":[Ljava/lang/String;
    :cond_4
    :goto_2
    return-object v8

    .line 115
    .end local v2    # "process":Ljava/lang/Process;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "buffer":[C
    .end local v5    # "output":Ljava/lang/StringBuffer;
    .end local v6    # "op":[Ljava/lang/String;
    .end local v7    # "delay":[Ljava/lang/String;
    .end local v9    # "i":I
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 120
    .end local v0    # "e":Ljava/io/IOException;
    :goto_3
    return-object v1
.end method

.method private static _PingAverageFloat(Ljava/lang/String;I)F
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "checkCount"    # I

    .line 126
    const-string v0, ""

    .line 127
    .local v0, "ping":Ljava/lang/String;
    const/4 v1, 0x0

    .line 128
    .local v1, "pingTotal":F
    const/4 v2, 0x0

    .line 130
    .local v2, "countIndex":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    if-ge v2, p1, :cond_2

    .line 132
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;->_Ping(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    const-string v4, ""

    if-eq v4, v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 134
    goto :goto_0

    .line 136
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ping : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "methinksPatcherApp."

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "split":[Ljava/lang/String;
    aget-object v3, v4, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    add-float/2addr v1, v3

    .line 142
    .end local v4    # "split":[Ljava/lang/String;
    goto :goto_0

    .line 144
    :cond_2
    int-to-float v4, p1

    div-float v4, v1, v4

    .line 145
    .local v4, "pingValue":F
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v3

    const-string v3, "%.1f"

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 146
    .end local v4    # "pingValue":F
    .local v3, "pingValue":F
    return v3
.end method

.method private static _PingAverageString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "checkCount"    # I

    .line 152
    const-string v0, ""

    .line 153
    .local v0, "ping":Ljava/lang/String;
    const/4 v1, 0x0

    .line 154
    .local v1, "pingTotal":F
    const/4 v2, 0x0

    .line 156
    .local v2, "countIndex":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    if-ge v2, p1, :cond_2

    .line 158
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;->_Ping(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v4, ""

    if-eq v4, v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 160
    goto :goto_0

    .line 162
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 164
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ping : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "methinksPatcherApp."

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 167
    .local v4, "split":[Ljava/lang/String;
    aget-object v3, v4, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    add-float/2addr v1, v3

    .line 168
    .end local v4    # "split":[Ljava/lang/String;
    goto :goto_0

    .line 170
    :cond_2
    int-to-float v4, p1

    div-float v4, v1, v4

    .line 171
    .local v4, "pingValue":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v3

    const-string v3, "%.1f"

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " ms"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static checkInternalAvailableMemory()J
    .locals 7

    .line 527
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 528
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 529
    .local v1, "blockSize":J
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v3

    .line 530
    .local v3, "availableBlocks":J
    mul-long v5, v1, v3

    return-wide v5
.end method

.method private static checkInternalStorageAllMemory()J
    .locals 7

    .line 518
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 519
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 520
    .local v1, "blockSize":J
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v3

    .line 521
    .local v3, "totalBlocks":J
    mul-long v5, v1, v3

    return-wide v5
.end method

.method private static getMobileNetworkSpeed(I)F
    .locals 1
    .param p0, "networkType"    # I

    .line 603
    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    .line 641
    return v0

    .line 639
    :pswitch_0
    return v0

    .line 635
    :pswitch_1
    return v0

    .line 607
    :pswitch_2
    return v0

    .line 633
    :pswitch_3
    const/high16 v0, 0x42280000    # 42.0f

    return v0

    .line 631
    :pswitch_4
    const v0, 0x3fa47ae1    # 1.285f

    return v0

    .line 637
    :pswitch_5
    const/high16 v0, 0x42c80000    # 100.0f

    return v0

    .line 629
    :pswitch_6
    const v0, 0x409ccccd    # 4.9f

    return v0

    .line 615
    :pswitch_7
    const/high16 v0, 0x42700000    # 60.0f

    return v0

    .line 627
    :pswitch_8
    const/high16 v0, 0x41600000    # 14.0f

    return v0

    .line 625
    :pswitch_9
    const v0, 0x40b851ec    # 5.76f

    return v0

    .line 623
    :pswitch_a
    const v0, 0x41accccd    # 21.6f

    return v0

    .line 613
    :pswitch_b
    const/high16 v0, 0x43190000    # 153.0f

    return v0

    .line 621
    :pswitch_c
    const v0, 0x40466666    # 3.1f

    return v0

    .line 619
    :pswitch_d
    const v0, 0x401d70a4    # 2.46f

    return v0

    .line 611
    :pswitch_e
    const/high16 v0, 0x42e60000    # 115.0f

    return v0

    .line 617
    :pswitch_f
    const/high16 v0, 0x43c00000    # 384.0f

    return v0

    .line 609
    :pswitch_10
    const/high16 v0, 0x43940000    # 296.0f

    return v0

    .line 605
    :pswitch_11
    const/high16 v0, 0x42e40000    # 114.0f

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getMobileNetworkSpeedUnit(I)Ljava/lang/String;
    .locals 1
    .param p0, "networkType"    # I

    .line 575
    const-string v0, "KBps"

    packed-switch p0, :pswitch_data_0

    .line 597
    return-object v0

    .line 595
    :pswitch_0
    const-string v0, "MBps"

    return-object v0

    .line 583
    :pswitch_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static getWiFiSpeed(Landroid/content/Context;)F
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 546
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 547
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 548
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/MessagePack;->getWifiNetworkSpeed(Landroid/net/wifi/WifiInfo;)F

    move-result v2

    return v2
.end method

.method private static getWifiNetworkSpeed(Landroid/net/wifi/WifiInfo;)F
    .locals 2
    .param p0, "wifiInfo"    # Landroid/net/wifi/WifiInfo;

    .line 553
    if-nez p0, :cond_0

    .line 555
    const/4 v0, 0x0

    return v0

    .line 557
    :cond_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v0

    .line 558
    .local v0, "linkSpeed":I
    int-to-float v1, v0

    return v1
.end method


# virtual methods
.method public BuildRequest()Ljava/lang/String;
    .locals 1

    .line 68
    const-string v0, ""

    return-object v0
.end method

.method public Finish()V
    .locals 0

    .line 85
    return-void
.end method

.method public GetKey()I
    .locals 1

    .line 58
    iget v0, p0, Lcom/kint/kintframeworkaosaar/MessagePack;->_key:I

    return v0
.end method

.method public GetValue()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePack;->_value:Ljava/lang/String;

    return-object v0
.end method

.method public Prepare()V
    .locals 0

    .line 75
    return-void
.end method

.method public Request()V
    .locals 0

    .line 80
    return-void
.end method
