.class public Lcom/kint/kintframeworkaosaar/MessagePackEvent;
.super Lcom/kint/kintframeworkaosaar/MessagePack;
.source "MessagePackEvent.java"


# instance fields
.field _eventKey:Ljava/lang/String;

.field _eventValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 9
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/MessagePack;-><init>()V

    .line 10
    const-string v0, "event"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    .line 11
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_key:I

    .line 13
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventKey:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventValue:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public BuildRequest()Ljava/lang/String;
    .locals 1

    .line 37
    const-string v0, ""

    return-object v0
.end method

.method public GetEventKey()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventKey:Ljava/lang/String;

    return-object v0
.end method

.method public GetEventValue()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_eventValue:Ljava/lang/String;

    return-object v0
.end method

.method public Request()V
    .locals 1

    .line 30
    const-string v0, "event"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->_key:I

    .line 32
    return-void
.end method
