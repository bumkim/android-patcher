.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;

.field final synthetic val$selectedItems:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;

    .line 2424
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->val$selectedItems:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 2428
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 2434
    :cond_0
    const-string v0, ""

    .line 2435
    .local v0, "items":Ljava/lang/String;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2437
    .local v2, "selectedItem":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2438
    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->AddAnswer(ZLjava/lang/String;)V

    .line 2439
    .end local v2    # "selectedItem":Ljava/lang/String;
    goto :goto_0

    .line 2441
    .end local v0    # "items":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->val$selectedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2443
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 2448
    :goto_1
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackAnswer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/MessagePackAnswer;-><init>()V

    .line 2449
    .local v0, "messagePackAnswer":Lcom/kint/kintframeworkaosaar/MessagePackAnswer;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/MessagePackAnswer;->Request()V

    .line 2450
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v1, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 2452
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2453
    return-void
.end method
