.class public final enum Lcom/kint/kintframeworkaosaar/AmfType;
.super Ljava/lang/Enum;
.source "AmfType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/AmfType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum BOOLEAN:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum ECMA_MAP:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum NULL:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum NUMBER:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum OBJECT:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum STRICT_ARRAY:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum STRING:Lcom/kint/kintframeworkaosaar/AmfType;

.field public static final enum UNDEFINED:Lcom/kint/kintframeworkaosaar/AmfType;

.field private static final quickLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Byte;",
            "Lcom/kint/kintframeworkaosaar/AmfType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private value:B


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 14
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v1, 0x0

    const-string v2, "NUMBER"

    invoke-direct {v0, v2, v1, v1}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->NUMBER:Lcom/kint/kintframeworkaosaar/AmfType;

    .line 15
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v2, 0x1

    const-string v3, "BOOLEAN"

    invoke-direct {v0, v3, v2, v2}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->BOOLEAN:Lcom/kint/kintframeworkaosaar/AmfType;

    .line 16
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v3, 0x2

    const-string v4, "STRING"

    invoke-direct {v0, v4, v3, v3}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->STRING:Lcom/kint/kintframeworkaosaar/AmfType;

    .line 17
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v4, 0x3

    const-string v5, "OBJECT"

    invoke-direct {v0, v5, v4, v4}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->OBJECT:Lcom/kint/kintframeworkaosaar/AmfType;

    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v5, 0x4

    const/4 v6, 0x5

    const-string v7, "NULL"

    invoke-direct {v0, v7, v5, v6}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->NULL:Lcom/kint/kintframeworkaosaar/AmfType;

    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v7, 0x6

    const-string v8, "UNDEFINED"

    invoke-direct {v0, v8, v6, v7}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->UNDEFINED:Lcom/kint/kintframeworkaosaar/AmfType;

    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/16 v8, 0x8

    const-string v9, "ECMA_MAP"

    invoke-direct {v0, v9, v7, v8}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->ECMA_MAP:Lcom/kint/kintframeworkaosaar/AmfType;

    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfType;

    const/4 v9, 0x7

    const-string v10, "STRICT_ARRAY"

    const/16 v11, 0xa

    invoke-direct {v0, v10, v9, v11}, Lcom/kint/kintframeworkaosaar/AmfType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->STRICT_ARRAY:Lcom/kint/kintframeworkaosaar/AmfType;

    .line 11
    new-array v0, v8, [Lcom/kint/kintframeworkaosaar/AmfType;

    sget-object v8, Lcom/kint/kintframeworkaosaar/AmfType;->NUMBER:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v8, v0, v1

    sget-object v8, Lcom/kint/kintframeworkaosaar/AmfType;->BOOLEAN:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v8, v0, v2

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->STRING:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->OBJECT:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v4

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->NULL:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v5

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->UNDEFINED:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v6

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->ECMA_MAP:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v7

    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfType;->STRICT_ARRAY:Lcom/kint/kintframeworkaosaar/AmfType;

    aput-object v2, v0, v9

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->$VALUES:[Lcom/kint/kintframeworkaosaar/AmfType;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->quickLookupMap:Ljava/util/Map;

    .line 23
    invoke-static {}, Lcom/kint/kintframeworkaosaar/AmfType;->values()[Lcom/kint/kintframeworkaosaar/AmfType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 24
    .local v3, "amfType":Lcom/kint/kintframeworkaosaar/AmfType;
    sget-object v4, Lcom/kint/kintframeworkaosaar/AmfType;->quickLookupMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    .end local v3    # "amfType":Lcom/kint/kintframeworkaosaar/AmfType;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    int-to-byte p1, p3

    iput-byte p1, p0, Lcom/kint/kintframeworkaosaar/AmfType;->value:B

    .line 30
    return-void
.end method

.method public static valueOf(B)Lcom/kint/kintframeworkaosaar/AmfType;
    .locals 2
    .param p0, "amfTypeByte"    # B

    .line 37
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/AmfType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/AmfType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 11
    const-class v0, Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/AmfType;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/AmfType;
    .locals 1

    .line 11
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->$VALUES:[Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/AmfType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/AmfType;

    return-object v0
.end method


# virtual methods
.method public getValue()B
    .locals 1

    .line 33
    iget-byte v0, p0, Lcom/kint/kintframeworkaosaar/AmfType;->value:B

    return v0
.end method
