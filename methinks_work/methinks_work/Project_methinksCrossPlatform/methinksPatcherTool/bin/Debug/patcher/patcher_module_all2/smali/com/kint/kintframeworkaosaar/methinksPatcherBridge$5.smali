.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;
.super Landroid/os/Handler;
.source "methinksPatcherBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 2054
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .line 2057
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_1a

    const/4 v1, 0x1

    if-eq v0, v1, :cond_19

    const/4 v2, 0x3

    if-eq v0, v2, :cond_18

    const/16 v3, 0xe

    const/4 v4, 0x2

    if-eq v0, v3, :cond_17

    const/4 v3, 0x4

    const/4 v5, -0x1

    const/4 v6, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_7

    .line 2276
    :pswitch_0
    invoke-static {v6}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;

    move-result-object v0

    .line 2278
    .local v0, "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->GetType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    const v9, -0x4b4b31cf

    if-eq v8, v9, :cond_3

    const v9, 0x674393d

    if-eq v8, v9, :cond_2

    const v9, 0x1e0a23d1

    if-eq v8, v9, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const-string v8, "multipleChoice"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const-string v8, "range"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    const-string v8, "openEnd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x2

    goto :goto_1

    :goto_0
    const/4 v7, -0x1

    :goto_1
    const/16 v8, 0xd

    if-eqz v7, :cond_8

    if-eq v7, v1, :cond_5

    if-eq v7, v4, :cond_4

    goto/16 :goto_3

    .line 2515
    :cond_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2516
    .local v1, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->GetText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2518
    new-instance v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2519
    .local v2, "editText":Landroid/widget/EditText;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2521
    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v3

    .line 2522
    .local v3, "confirm":Ljava/lang/String;
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$13;

    invoke-direct {v4, p0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$13;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2540
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$14;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$14;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 2547
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2548
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_3

    .line 2393
    .end local v1    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v2    # "editText":Landroid/widget/EditText;
    .end local v3    # "confirm":Ljava/lang/String;
    :cond_5
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->IsMultipleChoice()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2396
    .local v1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->ChoiceList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->ChoiceList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 2397
    .local v2, "items":[Ljava/lang/String;
    array-length v3, v2

    new-array v3, v3, [Z

    .line 2398
    .local v3, "checked":[Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v5, v3

    if-ge v4, v5, :cond_6

    .line 2399
    aput-boolean v6, v3, v4

    .line 2398
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2401
    .end local v4    # "i":I
    :cond_6
    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v4

    .line 2403
    .local v4, "confirm":Ljava/lang/String;
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v7}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2404
    .local v5, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->GetText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$8;

    invoke-direct {v8, p0, v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$8;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Ljava/util/List;[Ljava/lang/String;)V

    .line 2405
    invoke-virtual {v7, v2, v3, v8}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;

    invoke-direct {v8, p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$7;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Ljava/util/List;)V

    .line 2423
    invoke-virtual {v7, v4, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2457
    new-instance v7, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$9;

    invoke-direct {v7, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$9;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 2464
    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2466
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    .line 2467
    .local v6, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 2468
    .end local v1    # "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "items":[Ljava/lang/String;
    .end local v3    # "checked":[Z
    .end local v4    # "confirm":Ljava/lang/String;
    .end local v5    # "dialog":Landroid/app/AlertDialog$Builder;
    .end local v6    # "alert":Landroid/app/AlertDialog;
    goto/16 :goto_3

    .line 2471
    :cond_7
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->ChoiceList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->ChoiceList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    .line 2473
    .local v1, "items":[Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2474
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->GetText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2475
    new-instance v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$10;

    invoke-direct {v3, p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$10;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;[Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1, v5, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2484
    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v3

    .line 2485
    .local v3, "confirm":Ljava/lang/String;
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$11;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$11;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2498
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$12;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$12;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 2505
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2507
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 2508
    .local v4, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 2511
    .end local v1    # "items":[Ljava/lang/CharSequence;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "confirm":Ljava/lang/String;
    .end local v4    # "alert":Landroid/app/AlertDialog;
    goto :goto_3

    .line 2283
    :cond_8
    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/CharSequence;

    const/16 v9, 0xf

    .line 2284
    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v6

    const/16 v9, 0x10

    .line 2285
    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v1

    const/16 v1, 0x11

    .line 2286
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v4

    const/16 v1, 0x12

    .line 2287
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v2

    const/16 v1, 0x13

    .line 2288
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v3

    move-object v1, v7

    .line 2292
    .restart local v1    # "items":[Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2293
    .restart local v2    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->GetText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2294
    new-instance v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$4;

    invoke-direct {v3, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$4;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v2, v1, v5, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2304
    invoke-static {v8}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v3

    .line 2305
    .restart local v3    # "confirm":Ljava/lang/String;
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$5;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$5;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2318
    new-instance v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$6;

    invoke-direct {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$6;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 2325
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2327
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 2328
    .restart local v4    # "alert":Landroid/app/AlertDialog;
    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 2389
    .end local v1    # "items":[Ljava/lang/CharSequence;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "confirm":Ljava/lang/String;
    .end local v4    # "alert":Landroid/app/AlertDialog;
    nop

    .line 2553
    .end local v0    # "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    :goto_3
    goto/16 :goto_7

    .line 2266
    :pswitch_1
    goto/16 :goto_7

    .line 2272
    :pswitch_2
    goto/16 :goto_7

    .line 2204
    :pswitch_3
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    .line 2205
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v0, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2206
    :cond_9
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 2207
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v0, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2209
    :cond_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    .line 2210
    .local v0, "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 2212
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->GetError()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v7

    const v8, -0x5bb96091

    if-eq v7, v8, :cond_d

    const v8, -0x2565e2de

    if-eq v7, v8, :cond_c

    :cond_b
    goto :goto_4

    :cond_c
    const-string v7, "invalidProject"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v5, 0x0

    goto :goto_4

    :cond_d
    const-string v7, "invalidUserCode"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_f

    if-eq v5, v1, :cond_e

    goto :goto_5

    .line 2225
    :cond_e
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget v4, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->LoginTryCount:I

    add-int/2addr v4, v1

    iput v4, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->LoginTryCount:I

    .line 2227
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 2228
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v6}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)V

    .line 2230
    goto :goto_5

    .line 2217
    :cond_f
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 2218
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 2219
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1902(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 2221
    nop

    .line 2242
    .end local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
    :goto_5
    goto/16 :goto_7

    .line 2160
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    .line 2161
    .restart local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TAG"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2162
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->GetResultStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 2165
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->CheckValidData()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2175
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)V

    .line 2176
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I

    .line 2178
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_10

    .line 2179
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2180
    :cond_10
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    if-eqz v1, :cond_11

    .line 2181
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2183
    :cond_11
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->IsScreenStreamAllowed()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2185
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjectionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v3

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/media/projection/MediaProjectionManager;->getMediaProjection(ILandroid/content/Intent;)Landroid/media/projection/MediaProjection;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1402(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/media/projection/MediaProjection;)Landroid/media/projection/MediaProjection;

    .line 2186
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectServer(Ljava/lang/String;)V

    goto :goto_6

    .line 2194
    :cond_12
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 2195
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1902(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 2200
    .end local v0    # "loginInfo":Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
    :cond_13
    :goto_6
    goto/16 :goto_7

    .line 2061
    :pswitch_5
    sget v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->ShowDialog:I

    if-ne v5, v0, :cond_16

    .line 2063
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2064
    .local v0, "density":F
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2065
    .local v1, "width":I
    sget-object v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2067
    .local v2, "height":I
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 2068
    .local v3, "windowManagerLayoutParams2":Landroid/view/WindowManager$LayoutParams;
    const/16 v6, 0x33

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2069
    const/16 v7, 0x100

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 2070
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v8, 0x20000

    or-int/2addr v7, v8

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2073
    sget-object v7, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 2074
    .local v7, "loginLayoutInflater":Landroid/view/LayoutInflater;
    iget-object v8, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    const-string v9, "login_layout"

    invoke-static {v8, v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)I

    move-result v8

    .line 2075
    .local v8, "resourceID":I
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    iput-object v10, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    .line 2076
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    invoke-virtual {v9, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2078
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v9, v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    const-string v10, "button_login"

    invoke-static {v5, v9, v10}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 2079
    .local v5, "loginButton":Landroid/widget/Button;
    if-eqz v5, :cond_14

    .line 2080
    new-instance v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$1;

    invoke-direct {v9, p0, v5}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Landroid/widget/Button;)V

    invoke-virtual {v5, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2101
    :cond_14
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v10, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    const-string v11, "editText_user_code"

    invoke-static {v9, v10, v11}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCodeEditText:Landroid/widget/EditText;

    .line 2102
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v9, v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCodeEditText:Landroid/widget/EditText;

    if-eqz v9, :cond_15

    .line 2106
    sget-object v9, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v10, "input_method"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/inputmethod/InputMethodManager;

    .line 2107
    .local v9, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCodeEditText:Landroid/widget/EditText;

    new-instance v11, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$2;

    invoke-direct {v11, p0, v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2119
    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCodeEditText:Landroid/widget/EditText;

    new-instance v11, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$3;

    invoke-direct {v11, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;)V

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2132
    .end local v9    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_15
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v9

    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginLayoutView:Landroid/view/View;

    invoke-interface {v9, v10, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2136
    iget-object v9, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v9}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v9

    .line 2137
    .local v9, "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    iput v6, v9, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2138
    const/4 v6, -0x2

    iput v6, v9, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2139
    iput v6, v9, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2141
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    const-string v10, "img_methinks_logo"

    invoke-static {v6, v10}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2142
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int v10, v1, v10

    div-int/2addr v10, v4

    iput v10, v9, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2143
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float v4, v4, v0

    float-to-int v4, v4

    iput v4, v9, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2145
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    new-instance v10, Landroid/widget/ImageView;

    sget-object v11, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-direct {v10, v11}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v10, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    .line 2146
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v4, v4, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2148
    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;

    move-result-object v4

    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v10, v10, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginImageView:Landroid/widget/ImageView;

    invoke-interface {v4, v10, v9}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2149
    .end local v0    # "density":F
    .end local v1    # "width":I
    .end local v2    # "height":I
    .end local v3    # "windowManagerLayoutParams2":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "loginButton":Landroid/widget/Button;
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "loginLayoutInflater":Landroid/view/LayoutInflater;
    .end local v8    # "resourceID":I
    .end local v9    # "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    goto :goto_7

    .line 2150
    :cond_16
    sget v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->ShowDialog:I

    if-ne v1, v0, :cond_1b

    .line 2152
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v6}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)V

    goto :goto_7

    .line 2557
    :cond_17
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 2558
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 2559
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1902(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    goto :goto_7

    .line 2260
    :cond_18
    goto :goto_7

    .line 2254
    :cond_19
    goto :goto_7

    .line 2246
    :cond_1a
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->startRecord(Landroid/media/projection/MediaProjection;)Z

    .line 2248
    nop

    .line 2564
    :cond_1b
    :goto_7
    return-void

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
