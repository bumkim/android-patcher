.class public interface abstract Lcom/kint/kintframeworkaosaar/RtmpPublisher;
.super Ljava/lang/Object;
.source "RtmpPublisher.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract connect(Ljava/lang/String;)Z
.end method

.method public abstract publish(Ljava/lang/String;)Z
.end method

.method public abstract publishAudioData([BII)V
.end method

.method public abstract publishVideoData([BII)V
.end method

.method public abstract setAuthorization(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setVideoRate(II)V
.end method

.method public abstract setVideoResolution(II)V
.end method
