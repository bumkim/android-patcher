.class public final enum Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;
.super Ljava/lang/Enum;
.source "FormatVideoEncoder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420Dynamical:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV422FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV422PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV422PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV422PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV422SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV444FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

.field public static final enum YUV444INTERLEAVED:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v1, 0x0

    const-string v2, "YUV420FLEXIBLE"

    invoke-direct {v0, v2, v1}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v2, 0x1

    const-string v3, "YUV420PLANAR"

    invoke-direct {v0, v3, v2}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v3, 0x2

    const-string v4, "YUV420SEMIPLANAR"

    invoke-direct {v0, v4, v3}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v4, 0x3

    const-string v5, "YUV420PACKEDPLANAR"

    invoke-direct {v0, v5, v4}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v5, 0x4

    const-string v6, "YUV420PACKEDSEMIPLANAR"

    invoke-direct {v0, v6, v5}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 12
    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v6, 0x5

    const-string v7, "YUV422FLEXIBLE"

    invoke-direct {v0, v7, v6}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v7, 0x6

    const-string v8, "YUV422PLANAR"

    invoke-direct {v0, v8, v7}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/4 v8, 0x7

    const-string v9, "YUV422SEMIPLANAR"

    invoke-direct {v0, v9, v8}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v9, 0x8

    const-string v10, "YUV422PACKEDPLANAR"

    invoke-direct {v0, v10, v9}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v10, 0x9

    const-string v11, "YUV422PACKEDSEMIPLANAR"

    invoke-direct {v0, v11, v10}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 13
    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v11, 0xa

    const-string v12, "YUV444FLEXIBLE"

    invoke-direct {v0, v12, v11}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV444FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v12, 0xb

    const-string v13, "YUV444INTERLEAVED"

    invoke-direct {v0, v13, v12}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV444INTERLEAVED:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v13, 0xc

    const-string v14, "SURFACE"

    invoke-direct {v0, v14, v13}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 15
    new-instance v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    const/16 v14, 0xd

    const-string v15, "YUV420Dynamical"

    invoke-direct {v0, v15, v14}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420Dynamical:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    .line 9
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    sget-object v15, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v15, v0, v1

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v2

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v4

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v5

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v6

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v7

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422SEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v8

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PACKEDPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v9

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV422PACKEDSEMIPLANAR:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v10

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV444FLEXIBLE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v11

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV444INTERLEAVED:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v12

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->SURFACE:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v13

    sget-object v1, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->YUV420Dynamical:Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    aput-object v1, v0, v14

    sput-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->$VALUES:[Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 9
    const-class v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;
    .locals 1

    .line 9
    sget-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->$VALUES:[Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;

    return-object v0
.end method


# virtual methods
.method public getFormatCodec()I
    .locals 2

    .line 18
    sget-object v0, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder$1;->$SwitchMap$com$kint$kintframeworkaosaar$FormatVideoEncoder:[I

    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/FormatVideoEncoder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    const/4 v0, -0x1

    return v0

    .line 44
    :pswitch_0
    const v0, 0x7f000789

    return v0

    .line 42
    :pswitch_1
    const/16 v0, 0x1d

    return v0

    .line 40
    :pswitch_2
    const v0, 0x7f444888

    return v0

    .line 38
    :pswitch_3
    const/16 v0, 0x28

    return v0

    .line 36
    :pswitch_4
    const/16 v0, 0x17

    return v0

    .line 34
    :pswitch_5
    const/16 v0, 0x18

    return v0

    .line 32
    :pswitch_6
    const/16 v0, 0x16

    return v0

    .line 30
    :pswitch_7
    const v0, 0x7f422888

    return v0

    .line 28
    :pswitch_8
    const/16 v0, 0x27

    return v0

    .line 26
    :pswitch_9
    const/16 v0, 0x14

    return v0

    .line 24
    :pswitch_a
    const/16 v0, 0x15

    return v0

    .line 22
    :pswitch_b
    const/16 v0, 0x13

    return v0

    .line 20
    :pswitch_c
    const v0, 0x7f420888

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
