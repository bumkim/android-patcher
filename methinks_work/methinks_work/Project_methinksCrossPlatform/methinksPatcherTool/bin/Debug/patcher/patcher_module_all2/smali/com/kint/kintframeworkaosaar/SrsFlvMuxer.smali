.class public Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
.super Ljava/lang/Object;
.source "SrsFlvMuxer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsRawH264Stream;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrameBytes;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAnnexbSearch;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAvcNaluType;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsAacObjectType;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsCodecVideo;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsCodecAudioSampleRate;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsCodecFlvTag;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsCodecVideoAVCType;,
        Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsCodecVideoAVCFrame;
    }
.end annotation


# static fields
.field private static final AUDIO_ALLOC_SIZE:I = 0x1000

.field private static final TAG:Ljava/lang/String; = "SrsFlvMuxer"

.field private static final VIDEO_ALLOC_SIZE:I = 0x20000


# instance fields
.field private connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

.field private volatile connected:Z

.field private flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

.field private isPpsSpsSend:Z

.field private mAudioAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

.field private mAudioSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

.field private mFlvTagCache:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

.field private mVideoSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

.field private needToFindKeyFrame:Z

.field private profileIop:B

.field private publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

.field private sampleRate:I

.field private worker:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V
    .locals 3
    .param p1, "connectCheckerRtmp"    # Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    .line 55
    new-instance v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-direct {v1, p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    .line 56
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->needToFindKeyFrame:Z

    .line 59
    new-instance v1, Lcom/kint/kintframeworkaosaar/SrsAllocator;

    const/high16 v2, 0x20000

    invoke-direct {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator;-><init>(I)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    .line 60
    new-instance v1, Lcom/kint/kintframeworkaosaar/SrsAllocator;

    const/16 v2, 0x1000

    invoke-direct {v1, v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator;-><init>(I)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    .line 61
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mFlvTagCache:Ljava/util/concurrent/BlockingQueue;

    .line 63
    const v1, 0xac44

    iput v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sampleRate:I

    .line 64
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->isPpsSpsSend:Z

    .line 65
    iput-byte v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->profileIop:B

    .line 72
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 73
    new-instance v0, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    invoke-direct {v0, p1}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;-><init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connect(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connectCheckerRtmp:Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)B
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-byte v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->profileIop:B

    return v0
.end method

.method static synthetic access$1100(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsAllocator;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->isPpsSpsSend:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Z

    .line 45
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->isPpsSpsSend:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsAllocator;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sampleRate:I

    return v0
.end method

.method static synthetic access$1700(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->needToFindKeyFrame:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Z

    .line 45
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->needToFindKeyFrame:Z

    return p1
.end method

.method static synthetic access$200(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mFlvTagCache:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    return-object v0
.end method

.method static synthetic access$302(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 45
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    return-object p1
.end method

.method static synthetic access$400(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 45
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sendFlvTag(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V

    return-void
.end method

.method static synthetic access$500(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    return-object v0
.end method

.method static synthetic access$502(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 45
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    return-object p1
.end method

.method static synthetic access$600(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 45
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$700(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 45
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->disconnect(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V

    return-void
.end method

.method private connect(Ljava/lang/String;)Z
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .line 132
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    if-nez v0, :cond_1

    .line 133
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "worker: connecting to RTMP server by url=%s\n"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SrsFlvMuxer"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    invoke-virtual {v0, p1}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->connect(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    const-string v1, "live"

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->publish(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    .line 137
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 138
    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 140
    :cond_1
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    return v0
.end method

.method private disconnect(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V
    .locals 2
    .param p1, "connectChecker"    # Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .line 124
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 126
    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioSequenceHeader:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 127
    invoke-interface {p1}, Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;->onDisconnectRtmp()V

    .line 128
    const-string v0, "SrsFlvMuxer"

    const-string v1, "worker: disconnect ok."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return-void
.end method

.method private sendFlvTag(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;)V
    .locals 4
    .param p1, "frame"    # Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;

    .line 144
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->connected:Z

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    goto :goto_1

    .line 148
    :cond_0
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_video()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_keyframe()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->type:I

    .line 151
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->dts:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    .line 152
    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v2

    array-length v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 151
    const-string v1, "worker: send frame type=%d, dts=%d, size=%dB"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 150
    const-string v1, "SrsFlvMuxer"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    iget-object v1, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v1

    iget-object v2, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size()I

    move-result v2

    iget v3, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->dts:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->publishVideoData([BII)V

    .line 155
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mVideoAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    iget-object v1, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator;->release(Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->is_audio()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    iget-object v1, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->array()[B

    move-result-object v1

    iget-object v2, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;->size()I

    move-result v2

    iget v3, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->dts:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->publishAudioData([BII)V

    .line 158
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mAudioAllocator:Lcom/kint/kintframeworkaosaar/SrsAllocator;

    iget-object v1, p1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlvFrame;->flvTag:Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsAllocator;->release(Lcom/kint/kintframeworkaosaar/SrsAllocator$Allocation;)V

    .line 160
    :cond_3
    :goto_0
    return-void

    .line 145
    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public sendAudio(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 1
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "bufferInfo"    # Landroid/media/MediaCodec$BufferInfo;

    .line 232
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeAudioSample(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 233
    return-void
.end method

.method public sendVideo(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 1
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "bufferInfo"    # Landroid/media/MediaCodec$BufferInfo;

    .line 228
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->writeVideoSample(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 229
    return-void
.end method

.method public setAuthorization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->setAuthorization(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public setIsStereo(Z)V
    .locals 2
    .param p1, "isStereo"    # Z

    .line 90
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 91
    .local v0, "channel":I
    :goto_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-virtual {v1, v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->setAchannel(I)V

    .line 92
    return-void
.end method

.method public setProfileIop(B)V
    .locals 0
    .param p1, "profileIop"    # B

    .line 77
    iput-byte p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->profileIop:B

    .line 78
    return-void
.end method

.method public setSampleRate(I)V
    .locals 0
    .param p1, "sampleRate"    # I

    .line 85
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sampleRate:I

    .line 86
    return-void
.end method

.method public setSpsPPs(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;

    .line 81
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->setSpsPPs(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 82
    return-void
.end method

.method public setVideoRate(II)V
    .locals 1
    .param p1, "videoDataRate"    # I
    .param p2, "frameRate"    # I

    .line 113
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->setVideoRate(II)V

    .line 116
    :cond_0
    return-void
.end method

.method public setVideoResolution(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 106
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->publisher:Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/DefaultRtmpPublisher;->setVideoResolution(II)V

    .line 109
    :cond_0
    return-void
.end method

.method public start(Ljava/lang/String;)V
    .locals 2
    .param p1, "rtmpUrl"    # Ljava/lang/String;

    .line 166
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;

    invoke-direct {v1, p0, p1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$1;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    .line 198
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 199
    return-void
.end method

.method public stop()V
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 208
    :try_start_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 212
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->worker:Ljava/lang/Thread;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->mFlvTagCache:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 215
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->flv:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$SrsFlv;->reset()V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->needToFindKeyFrame:Z

    .line 217
    const-string v0, "SrsFlvMuxer"

    const-string v1, "SrsFlvMuxer closed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$2;

    invoke-direct {v1, p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer$2;-><init>(Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 224
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 225
    return-void
.end method
