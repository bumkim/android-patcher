.class public Lcom/kint/kintframeworkaosaar/AmfDecoder;
.super Ljava/lang/Object;
.source "AmfDecoder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFrom(Ljava/io/InputStream;)Lcom/kint/kintframeworkaosaar/AmfData;
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 13
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    .line 14
    .local v0, "amfTypeByte":B
    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/AmfType;->valueOf(B)Lcom/kint/kintframeworkaosaar/AmfType;

    move-result-object v1

    .line 17
    .local v1, "amfType":Lcom/kint/kintframeworkaosaar/AmfType;
    sget-object v2, Lcom/kint/kintframeworkaosaar/AmfDecoder$1;->$SwitchMap$com$kint$kintframeworkaosaar$AmfType:[I

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/AmfType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 41
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown/unimplemented AMF data type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 38
    :pswitch_0
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfArray;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfArray;-><init>()V

    .line 39
    .local v2, "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    .line 35
    .end local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    :pswitch_1
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfMap;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfMap;-><init>()V

    .line 36
    .restart local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    .line 33
    .end local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    :pswitch_2
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfUndefined;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfUndefined;-><init>()V

    return-object v2

    .line 31
    :pswitch_3
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    return-object v2

    .line 28
    :pswitch_4
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfObject;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfObject;-><init>()V

    .line 29
    .restart local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    .line 25
    .end local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    :pswitch_5
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfString;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfString;-><init>()V

    .line 26
    .restart local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    .line 22
    .end local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    :pswitch_6
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfBoolean;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfBoolean;-><init>()V

    .line 23
    .restart local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    .line 19
    .end local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    :pswitch_7
    new-instance v2, Lcom/kint/kintframeworkaosaar/AmfNumber;

    invoke-direct {v2}, Lcom/kint/kintframeworkaosaar/AmfNumber;-><init>()V

    .line 20
    .restart local v2    # "amfData":Lcom/kint/kintframeworkaosaar/AmfData;
    nop

    .line 44
    :goto_0
    invoke-interface {v2, p0}, Lcom/kint/kintframeworkaosaar/AmfData;->readFrom(Ljava/io/InputStream;)V

    .line 45
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
