.class public Lcom/kint/kintframeworkaosaar/Audio;
.super Lcom/kint/kintframeworkaosaar/ContentData;
.source "Audio.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 17
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->AUDIO:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x7

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/ContentData;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 13
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/ContentData;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 14
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 23
    const-string v0, "RTMP Audio"

    return-object v0
.end method
