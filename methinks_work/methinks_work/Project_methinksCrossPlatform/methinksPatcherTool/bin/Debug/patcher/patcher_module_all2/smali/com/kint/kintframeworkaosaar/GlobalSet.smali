.class public Lcom/kint/kintframeworkaosaar/GlobalSet;
.super Ljava/lang/Object;
.source "GlobalSet.java"


# static fields
.field public static DebugMode:Z = false

.field public static DebugRTMPServerBaseURL:Ljava/lang/String; = null

.field public static DebugServiceServerURL:Ljava/lang/String; = null

.field public static EnableModule:Z = false

.field public static PatchMode:Z = false

.field public static ProjectID:Ljava/lang/String; = null

.field public static ReleaseRTMPServerBaseURL:Ljava/lang/String; = null

.field public static ReleaseServiceServerURL:Ljava/lang/String; = null

.field public static RunBackground:Z = false

.field public static ShowDialog:I = 0x0

.field public static SkipYetDate:Z = false

.field private static final TAG:Ljava/lang/String; = "methinksGlobalSet"

.field public static VideoDataRate:I

.field public static VideoDpi:I

.field public static VideoFrameRate:I

.field public static VideoHeight:I

.field public static VideoWidth:I

.field private static _questionInfoList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    const/4 v0, 0x1

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->EnableModule:Z

    .line 17
    const/4 v0, 0x0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->PatchMode:Z

    .line 18
    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->SkipYetDate:Z

    .line 20
    const-string v1, "rtmp://ec2-184-72-75-118.compute-1.amazonaws.com:5391"

    sput-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugRTMPServerBaseURL:Ljava/lang/String;

    .line 21
    const-string v1, "rtmp://ec2-54-162-108-19.compute-1.amazonaws.com:5391"

    sput-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseRTMPServerBaseURL:Ljava/lang/String;

    .line 22
    const-string v1, "https://apptest-dev.methinks.io"

    sput-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugServiceServerURL:Ljava/lang/String;

    .line 23
    const-string v1, "https://apptest.methinks.io"

    sput-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseServiceServerURL:Ljava/lang/String;

    .line 24
    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->RunBackground:Z

    .line 27
    const/16 v1, 0x1e0

    sput v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 28
    const/16 v1, 0x356

    sput v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    .line 29
    const/16 v1, 0x140

    sput v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDpi:I

    .line 30
    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDataRate:I

    .line 31
    const/16 v1, 0xa

    sput v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoFrameRate:I

    .line 34
    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;
    .locals 1
    .param p0, "countIndex"    # I

    .line 41
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 43
    :cond_0
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/QuestionInfo;

    return-object v0
.end method

.method public static GetQuestionInfoCount()I
    .locals 1

    .line 79
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    return v0

    .line 81
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static ParsePresetModule(Ljava/lang/String;)V
    .locals 19
    .param p0, "presetModule"    # Ljava/lang/String;

    .line 93
    move-object/from16 v1, p0

    const-string v0, "show_dialog"

    const-string v2, "video_frame_rate"

    const-string v3, "video_data_rate"

    const-string v4, "video_dpi"

    const-string v5, "video_height"

    const-string v6, "video_width"

    const-string v7, "run_background"

    const-string v8, "release_service_server_url"

    const-string v9, "debug_service_server_url"

    const-string v10, "release_rtmp_server_base_url"

    const-string v11, "debug_rtmp_server_base_url"

    const-string v12, "skip_yet_date"

    const-string v13, "patch_mode"

    const-string v14, "enable"

    const-string v15, "methinksGlobalSet"

    if-eqz v1, :cond_10

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v16

    if-nez v16, :cond_0

    goto/16 :goto_1

    .line 101
    :cond_0
    move-object/from16 v16, v0

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "presetModuleJSON":Lorg/json/JSONObject;
    const-string v1, "module"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 103
    .local v1, "moduleJSON":Lorg/json/JSONObject;
    if-nez v1, :cond_1

    .line 105
    const-string v2, "[ParsePresetModule] - null == moduleJSON"

    invoke-static {v15, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void

    .line 110
    :cond_1
    move-object/from16 v17, v0

    .end local v0    # "presetModuleJSON":Lorg/json/JSONObject;
    .local v17, "presetModuleJSON":Lorg/json/JSONObject;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v18, v2

    const-string v2, "[ParsePresetModule] - moduleJSON : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->EnableModule:Z

    .line 116
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - enable : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->EnableModule:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->PatchMode:Z

    .line 122
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - PatchMode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->PatchMode:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 126
    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->SkipYetDate:Z

    .line 128
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - SkipYetDate : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->SkipYetDate:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 132
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugRTMPServerBaseURL:Ljava/lang/String;

    .line 134
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - DebugRTMPServerBaseURL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugRTMPServerBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 139
    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseRTMPServerBaseURL:Ljava/lang/String;

    .line 141
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - ReleaseRTMPServerBaseURL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseRTMPServerBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 145
    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugServiceServerURL:Ljava/lang/String;

    .line 147
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - DebugServiceServerURL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugServiceServerURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 151
    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseServiceServerURL:Ljava/lang/String;

    .line 153
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - ReleaseServiceServerURL : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseServiceServerURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 157
    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->RunBackground:Z

    .line 159
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - RunBackground : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->RunBackground:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 163
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 165
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - videoWidth : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 170
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    .line 172
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - videoHeight : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 177
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDpi:I

    .line 179
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - videoDpi : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDpi:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 184
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDataRate:I

    .line 186
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - videoDataRate : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDataRate:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 191
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoFrameRate:I

    .line 193
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - videoFrameRate : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoFrameRate:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 198
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->ShowDialog:I

    .line 200
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ParsePresetModule] - ShowDialog : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->ShowDialog:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    nop

    .end local v1    # "moduleJSON":Lorg/json/JSONObject;
    .end local v17    # "presetModuleJSON":Lorg/json/JSONObject;
    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 207
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_0
    return-void

    .line 95
    :cond_10
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ParsePresetModule] - presetModule : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method

.method public static ParsePresetProject(Ljava/lang/String;)V
    .locals 6
    .param p0, "presetProject"    # Ljava/lang/String;

    .line 212
    const-string v0, "debug_mode"

    const-string v1, "id"

    const-string v2, "methinksGlobalSet"

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 220
    :cond_0
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 221
    .local v3, "presetProjectJSON":Lorg/json/JSONObject;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ParsePresetProject] - projectJSON : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 225
    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    .line 227
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[ParsePresetProject] - ProjectID : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 231
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    .line 233
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ParsePresetProject] - DebugMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    nop

    .end local v3    # "presetProjectJSON":Lorg/json/JSONObject;
    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 241
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_0
    return-void

    .line 214
    :cond_3
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PresetModule] - presetProject : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    return-void
.end method

.method public static ParseQuestionInfo(Lorg/json/JSONObject;)V
    .locals 5
    .param p0, "questionInfoJson"    # Lorg/json/JSONObject;

    .line 49
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 54
    :cond_0
    :try_start_0
    const-string v0, "questions"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 55
    .local v0, "questionJsonArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_2

    .line 57
    const/4 v1, 0x0

    .local v1, "countIndex":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 59
    sget-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 60
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    .line 62
    :cond_1
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    .line 64
    .local v2, "questionInfoJsonObject":Lorg/json/JSONObject;
    new-instance v3, Lcom/kint/kintframeworkaosaar/QuestionInfo;

    invoke-direct {v3}, Lcom/kint/kintframeworkaosaar/QuestionInfo;-><init>()V

    .line 65
    .local v3, "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    invoke-virtual {v3, v2}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->Parse(Lorg/json/JSONObject;)V

    .line 67
    sget-object v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    nop

    .end local v2    # "questionInfoJsonObject":Lorg/json/JSONObject;
    .end local v3    # "questionInfo":Lcom/kint/kintframeworkaosaar/QuestionInfo;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    .end local v0    # "questionJsonArray":Lorg/json/JSONArray;
    .end local v1    # "countIndex":I
    :cond_2
    goto :goto_1

    .line 71
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 75
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method

.method public static RemoveAtQuestionInfo(I)V
    .locals 1
    .param p0, "countIndex"    # I

    .line 87
    sget-object v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->_questionInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 88
    return-void
.end method
