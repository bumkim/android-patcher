.class public final enum Lcom/kint/kintframeworkaosaar/CodecUtil$Force;
.super Ljava/lang/Enum;
.source "CodecUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/CodecUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Force"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/CodecUtil$Force;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

.field public static final enum FIRST_COMPATIBLE_FOUND:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

.field public static final enum HARDWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

.field public static final enum SOFTWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 25
    new-instance v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    const/4 v1, 0x0

    const-string v2, "FIRST_COMPATIBLE_FOUND"

    invoke-direct {v0, v2, v1}, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->FIRST_COMPATIBLE_FOUND:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    new-instance v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    const/4 v2, 0x1

    const-string v3, "SOFTWARE"

    invoke-direct {v0, v3, v2}, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->SOFTWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    new-instance v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    const/4 v3, 0x2

    const-string v4, "HARDWARE"

    invoke-direct {v0, v4, v3}, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->HARDWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    sget-object v4, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->FIRST_COMPATIBLE_FOUND:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    aput-object v4, v0, v1

    sget-object v1, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->SOFTWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    aput-object v1, v0, v2

    sget-object v1, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->HARDWARE:Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    aput-object v1, v0, v3

    sput-object v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->$VALUES:[Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/CodecUtil$Force;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 24
    const-class v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/CodecUtil$Force;
    .locals 1

    .line 24
    sget-object v0, Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->$VALUES:[Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/CodecUtil$Force;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/CodecUtil$Force;

    return-object v0
.end method
