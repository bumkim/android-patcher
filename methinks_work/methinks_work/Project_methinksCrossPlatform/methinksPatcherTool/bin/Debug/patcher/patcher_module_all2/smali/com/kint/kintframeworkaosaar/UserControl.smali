.class public Lcom/kint/kintframeworkaosaar/UserControl;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "UserControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/UserControl$Type;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private eventData:[I

.field private type:Lcom/kint/kintframeworkaosaar/UserControl$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 4
    .param p1, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 152
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->USER_CONTROL_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 153
    invoke-virtual {p1, v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->canReusePrevHeaderTx(Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    :goto_0
    const/4 v2, 0x2

    sget-object v3, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->USER_CONTROL_MESSAGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-direct {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    .line 152
    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 148
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 149
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/UserControl$Type;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 0
    .param p1, "type"    # Lcom/kint/kintframeworkaosaar/UserControl$Type;
    .param p2, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 166
    invoke-direct {p0, p2}, Lcom/kint/kintframeworkaosaar/UserControl;-><init>(Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 167
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 168
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/UserControl;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 1
    .param p1, "replyToPing"    # Lcom/kint/kintframeworkaosaar/UserControl;
    .param p2, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 161
    sget-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->PONG_REPLY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-direct {p0, v0, p2}, Lcom/kint/kintframeworkaosaar/UserControl;-><init>(Lcom/kint/kintframeworkaosaar/UserControl$Type;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V

    .line 162
    iget-object v0, p1, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    .line 163
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 240
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEventData()[I
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    return-object v0
.end method

.method public getFirstEventData()I
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getType()Lcom/kint/kintframeworkaosaar/UserControl$Type;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    return-object v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 213
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt16(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/UserControl$Type;->valueOf(I)Lcom/kint/kintframeworkaosaar/UserControl$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 214
    const/4 v0, 0x2

    .line 216
    .local v0, "bytesRead":I
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    if-ne v1, v2, :cond_0

    .line 217
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/kint/kintframeworkaosaar/UserControl;->setEventData(II)V

    .line 218
    add-int/lit8 v0, v0, 0x8

    goto :goto_0

    .line 220
    :cond_0
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/kint/kintframeworkaosaar/UserControl;->setEventData(I)V

    .line 221
    add-int/lit8 v0, v0, 0x4

    .line 224
    :goto_0
    nop

    .line 225
    return-void
.end method

.method public setEventData(I)V
    .locals 2
    .param p1, "eventData"    # I

    .line 193
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    sget-object v1, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    if-eq v0, v1, :cond_0

    .line 197
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    .line 198
    return-void

    .line 194
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SET_BUFFER_LENGTH requires two event data values; use setEventData(int, int) instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setEventData(II)V
    .locals 3
    .param p1, "streamId"    # I
    .param p2, "bufferLength"    # I

    .line 202
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    sget-object v1, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    if-ne v0, v1, :cond_0

    .line 207
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    .line 208
    return-void

    .line 203
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User control type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " requires only one event data value; use setEventData(int) instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setType(Lcom/kint/kintframeworkaosaar/UserControl$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 175
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 176
    return-void
.end method

.method protected size()I
    .locals 1

    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RTMP User Control (type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", event data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 230
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/UserControl$Type;->getIntValue()I

    move-result v0

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt16(Ljava/io/OutputStream;I)V

    .line 232
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 233
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->type:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    sget-object v1, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    if-ne v0, v1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/UserControl;->eventData:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 236
    :cond_0
    return-void
.end method
