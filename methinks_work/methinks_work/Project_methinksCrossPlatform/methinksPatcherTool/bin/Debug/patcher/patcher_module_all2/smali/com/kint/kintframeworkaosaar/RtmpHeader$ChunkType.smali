.class public final enum Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
.super Ljava/lang/Enum;
.source "RtmpHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/RtmpHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChunkType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field public static final enum TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field public static final enum TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field public static final enum TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field public static final enum TYPE_3_RELATIVE_SINGLE_BYTE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field private static final quickLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Byte;",
            "Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private value:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 157
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    const/4 v1, 0x0

    const-string v2, "TYPE_0_FULL"

    invoke-direct {v0, v2, v1, v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 158
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    const/4 v2, 0x1

    const-string v3, "TYPE_1_RELATIVE_LARGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 159
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    const/4 v3, 0x2

    const-string v4, "TYPE_2_RELATIVE_TIMESTAMP_ONLY"

    invoke-direct {v0, v4, v3, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 161
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    const/4 v4, 0x3

    const-string v5, "TYPE_3_RELATIVE_SINGLE_BYTE"

    invoke-direct {v0, v5, v4, v4}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_3_RELATIVE_SINGLE_BYTE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 154
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v5, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    aput-object v5, v0, v1

    sget-object v5, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    aput-object v5, v0, v2

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_3_RELATIVE_SINGLE_BYTE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    aput-object v2, v0, v4

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->$VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->quickLookupMap:Ljava/util/Map;

    .line 168
    invoke-static {}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->values()[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 169
    .local v3, "messageTypId":Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    sget-object v4, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->quickLookupMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->getValue()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    .end local v3    # "messageTypId":Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "byteValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 174
    int-to-byte p1, p3

    iput-byte p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->value:B

    .line 175
    return-void
.end method

.method public static valueOf(B)Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .locals 3
    .param p0, "chunkHeaderType"    # B

    .line 183
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    return-object v0

    .line 186
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown chunk header type byte: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-static {p0}, Lcom/kint/kintframeworkaosaar/Util;->toHexString(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 154
    const-class v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .locals 1

    .line 154
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->$VALUES:[Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    return-object v0
.end method


# virtual methods
.method public getValue()B
    .locals 1

    .line 179
    iget-byte v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->value:B

    return v0
.end method
