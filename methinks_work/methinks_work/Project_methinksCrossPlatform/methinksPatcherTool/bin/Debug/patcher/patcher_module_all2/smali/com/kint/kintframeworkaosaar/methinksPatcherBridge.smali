.class public Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;
.implements Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;
.implements Lcom/kint/kintframeworkaosaar/GetH264Data;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;,
        Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;,
        Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;
    }
.end annotation


# static fields
.field public static ApplicationContext:Landroid/content/Context; = null

.field public static IsEarphoneOn:Z = false

.field public static final MESSAGE_CONNECTED_FAILURE_RTMP_SERVER:I = 0x1

.field public static final MESSAGE_CONNECTED_SUCCESS_RTMP_SERVER:I = 0x0

.field public static final MESSAGE_CONNECT_SERVER_FAILURE:I = 0xe

.field public static final MESSAGE_DISCONNECTED_RTMP_SERVER:I = 0x3

.field public static final MESSAGE_LOGIN_FAILURE:I = 0x8

.field public static final MESSAGE_LOGIN_PREPARE:I = 0x6

.field public static final MESSAGE_LOGIN_SUCCESS:I = 0x7

.field public static final MESSAGE_NOTIFICATION_BACKGROUND:I = 0xa

.field public static final MESSAGE_NOTIFICATION_FOREGROUND:I = 0x9

.field public static final MESSAGE_ORIENTATION_ROTATE_LANDSCAPE:I = 0x5

.field public static final MESSAGE_ORIENTATION_ROTATE_PORTRAIT:I = 0x4

.field public static final MESSAGE_QUESTION_FINISH:I = 0xd

.field public static final MESSAGE_QUESTION_PREPARE:I = 0xb

.field public static final MESSAGE_QUESTION_START:I = 0xc

.field public static final MESSAGE_START_SEND_STREAM:I = 0x2

.field public static final SCREEN_CAPTURE_REQUEST_CODE:I = 0x3e8

.field public static final SCREEN_OVERRAY_REQUEST_CODE:I = 0x3ea

.field public static final SYSTEM_SETTING_REQUEST_CODE:I = 0x3e9

.field private static __instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

.field public static patcherApplication:Landroid/app/Application;


# instance fields
.field public LoginTryCount:I

.field private final TAG:Ljava/lang/String;

.field private _backgroundMode:Z

.field private _backgroundModeCheckTickTime:I

.field private _callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

.field private _callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

.field private _checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _currentActivity:Landroid/app/Activity;

.field private _detectScreenShotHandler:Landroid/os/Handler;

.field private _detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field private _dispatchNetworkStatus:I

.field private _logDelayTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

.field _loginImageView:Landroid/widget/ImageView;

.field private _loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

.field _loginLayoutView:Landroid/view/View;

.field private _mainActivity:Landroid/app/Activity;

.field private _mediaProjection:Landroid/media/projection/MediaProjection;

.field private _mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

.field private _processStatus:I

.field private _projectionIntentData:Landroid/content/Intent;

.field private _projectionResultCode:I

.field private _questionStatus:I

.field private _quitApplication:Z

.field _receivedMessageHandler:Landroid/os/Handler;

.field private _requestMessageList:Ljava/util/ArrayList;

.field private _rtmpServerURL:Ljava/lang/String;

.field private _screenRotationAngle:I

.field private _sendToUnityGameObjectName:Ljava/lang/String;

.field private _sendToUnityMethodName:Ljava/lang/String;

.field private _threadUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private _uncaughtExceptionHandler:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;

.field private _userCode:Ljava/lang/String;

.field _userCodeEditText:Landroid/widget/EditText;

.field public _viewDeveloperMode:I

.field private _virtualDisplay:Landroid/hardware/display/VirtualDisplay;

.field private _windowManager:Landroid/view/WindowManager;

.field private connectedRTMPServer:Z

.field private networkStatus:Ljava/lang/String;

.field private paused:Z

.field private rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

.field private running:Z

.field private sendRTMPVideo:Z

.field private videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1013
    const/4 v0, 0x0

    sput-boolean v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->IsEarphoneOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const-string v0, "methinksPatcherBridge"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->TAG:Ljava/lang/String;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    .line 97
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_questionStatus:I

    .line 461
    new-instance v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;

    invoke-direct {v1, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

    .line 968
    const v1, 0x1d4c0

    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_backgroundModeCheckTickTime:I

    .line 980
    const-string v1, "rtmp://221.165.42.119:5391/screen/bilbo915?projectID=oulj264yvC"

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    .line 986
    const-string v1, "Ready"

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 988
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_screenRotationAngle:I

    .line 989
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_backgroundMode:Z

    .line 997
    iput v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_dispatchNetworkStatus:I

    .line 998
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_logDelayTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 999
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 1000
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 1001
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 1002
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 1003
    new-instance v0, Lcom/kint/kintframeworkaosaar/DelayTimer;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/DelayTimer;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    .line 2053
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$5;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method static GetScreenBrightness(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 3023
    const/4 v0, -0x1

    .line 3026
    .local v0, "screenBrightness":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 3032
    goto :goto_0

    .line 3028
    :catch_0
    move-exception v1

    .line 3030
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, -0x1

    .line 3031
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 3034
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method public static Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .locals 1

    .line 106
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 108
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->__instance:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    return-object v0
.end method

.method private _CreateWindowManagerLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 9

    .line 3084
    const/4 v0, 0x0

    .line 3085
    .local v0, "windowManagerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_0

    .line 3087
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/16 v6, 0x7d2

    const/16 v7, 0x20

    const/4 v8, -0x2

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    move-object v0, v1

    goto :goto_0

    .line 3096
    :cond_0
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/16 v4, 0x7f6

    const/16 v5, 0x20

    const/4 v6, -0x2

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    move-object v0, v7

    .line 3104
    :goto_0
    return-object v0
.end method

.method private _GetLayoutResourceID(Ljava/lang/String;)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 3058
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "layout"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private _GetResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 3046
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawable"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3047
    .local v0, "resourceID":I
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private _GetResourceID(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "name"    # Ljava/lang/String;

    .line 3052
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v0, p2, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3053
    .local v0, "resourceID":I
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private _GetResourceStringValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 3040
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "string"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3041
    .local v0, "resourceID":I
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private _GetScreenBrightness()I
    .locals 1

    .line 3018
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->GetScreenBrightness(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method private _GetXmlResourceParser(Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 3063
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "xml"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3064
    .local v0, "resourceID":I
    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 3065
    .local v1, "xmlResourceParser":Landroid/content/res/XmlResourceParser;
    return-object v1
.end method

.method private _IsForegrounded()Z
    .locals 3

    .line 1273
    new-instance v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    .line 1274
    .local v0, "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    invoke-static {v0}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    .line 1275
    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0x64

    if-eq v1, v2, :cond_1

    iget v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private _ReadUserCode()Ljava/lang/String;
    .locals 3

    .line 2703
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "methinksPatcher"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2705
    .local v0, "pref":Landroid/content/SharedPreferences;
    sget-boolean v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    const-string v2, ""

    if-nez v1, :cond_0

    .line 2706
    const-string v1, "UserCode_Release"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2707
    :cond_0
    const-string v1, "UserCode_Debug"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private _RemoveAllUserCode()V
    .locals 3

    .line 2726
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "methinksPatcher"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2727
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2729
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2730
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2732
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2733
    return-void
.end method

.method private _RemoveUserCode()V
    .locals 3

    .line 2712
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "methinksPatcher"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2713
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2715
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    if-nez v2, :cond_0

    .line 2716
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2718
    :cond_0
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2720
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2721
    return-void
.end method

.method private _RequestServer(Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 30
    .param p1, "messagePack"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 1321
    move-object/from16 v1, p0

    const-string v2, "methinksPatcherBridge"

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[_RequestServer] - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1323
    sget-boolean v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "/"

    const/4 v6, 0x1

    if-ne v6, v4, :cond_0

    .line 1325
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugServiceServerURL:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .local v4, "fullURL":Ljava/lang/String;
    goto :goto_0

    .line 1329
    .end local v4    # "fullURL":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseServiceServerURL:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1332
    .restart local v4    # "fullURL":Ljava/lang/String;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 1333
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[_RequestServer] - URL : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1336
    .local v5, "url":Ljava/net/URL;
    const/4 v7, 0x0

    .line 1338
    .local v7, "httpURLConnection":Ljava/net/HttpURLConnection;
    const/4 v8, 0x0

    .line 1339
    .local v8, "outputStream":Ljava/io/OutputStream;
    const/4 v9, 0x0

    .line 1340
    .local v9, "inputStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 1342
    .local v10, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v11

    check-cast v11, Ljava/net/HttpURLConnection;

    move-object v7, v11

    .line 1343
    invoke-virtual {v7, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 1344
    invoke-virtual {v7, v6}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 1345
    const/16 v11, 0x2710

    invoke-virtual {v7, v11}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 1346
    const/16 v11, 0x3a98

    invoke-virtual {v7, v11}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 1347
    const-string v11, "POST"

    invoke-virtual {v7, v11}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1349
    const-string v11, "project-name"

    sget-object v12, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    invoke-virtual {v7, v11, v12}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    const-string v11, "user-code"

    iget-object v12, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    invoke-virtual {v7, v11, v12}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProjectID : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v12, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " , UserCode : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v12
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v13, "log"

    const-string v14, "answer"

    const-string v15, "login"

    sparse-switch v12, :sswitch_data_0

    :cond_1
    goto :goto_1

    :sswitch_0
    :try_start_2
    const-string v12, "picCapture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x1

    goto :goto_2

    :sswitch_1
    const-string v12, "captureDetail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/16 v11, 0x8

    goto :goto_2

    :sswitch_2
    const-string v12, "capture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x0

    goto :goto_2

    :sswitch_3
    const-string v12, "picCaptureInfo"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x7

    goto :goto_2

    :sswitch_4
    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x2

    goto :goto_2

    :sswitch_5
    const-string v12, "event"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x5

    goto :goto_2

    :sswitch_6
    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x3

    goto :goto_2

    :sswitch_7
    const-string v12, "question"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x6

    goto :goto_2

    :sswitch_8
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v11, :cond_1

    const/4 v11, 0x4

    goto :goto_2

    :goto_1
    const/4 v11, -0x1

    :goto_2
    const-string v12, "Content-Length"

    const-string v3, "application/json"

    const-string v6, "Content-Type"

    packed-switch v11, :pswitch_data_0

    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .local v19, "fullURL":Ljava/lang/String;
    goto/16 :goto_3

    .line 1532
    .end local v19    # "fullURL":Ljava/lang/String;
    .restart local v4    # "fullURL":Ljava/lang/String;
    :pswitch_0
    const/4 v11, 0x1

    .line 1533
    .local v11, "isNull":Z
    move-object/from16 v19, v4

    const/4 v4, 0x1

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    if-ne v4, v11, :cond_2

    goto/16 :goto_3

    .line 1537
    :cond_2
    :try_start_3
    invoke-virtual {v7, v6, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    const-string v3, "1"

    invoke-virtual {v7, v12, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1539
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1541
    .local v3, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    move-object v8, v4

    .line 1542
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 1543
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    goto/16 :goto_3

    .line 1513
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v11    # "isNull":Z
    .end local v19    # "fullURL":Ljava/lang/String;
    .restart local v4    # "fullURL":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    invoke-virtual {v7, v6, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1517
    .local v3, "jasonObject":Lorg/json/JSONObject;
    move-object/from16 v4, p1

    check-cast v4, Lcom/kint/kintframeworkaosaar/MessagePackEvent;

    .line 1518
    .local v4, "messagePackEvent":Lcom/kint/kintframeworkaosaar/MessagePackEvent;
    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->GetEventKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/MessagePackEvent;->GetEventValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v6, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1519
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 1520
    .local v6, "jsonBytes":[B
    array-length v11, v6

    .line 1521
    .local v11, "jsonBytesLength":I
    move-object/from16 v20, v3

    .end local v3    # "jasonObject":Lorg/json/JSONObject;
    .local v20, "jasonObject":Lorg/json/JSONObject;
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v12, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    move-object v8, v3

    .line 1523
    invoke-virtual {v8, v6}, Ljava/io/OutputStream;->write([B)V

    .line 1524
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 1526
    .end local v4    # "messagePackEvent":Lcom/kint/kintframeworkaosaar/MessagePackEvent;
    .end local v6    # "jsonBytes":[B
    .end local v11    # "jsonBytesLength":I
    .end local v20    # "jasonObject":Lorg/json/JSONObject;
    goto/16 :goto_3

    .line 1499
    .end local v19    # "fullURL":Ljava/lang/String;
    .local v4, "fullURL":Ljava/lang/String;
    :pswitch_2
    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    invoke-virtual {v7, v6, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfo(I)Lcom/kint/kintframeworkaosaar/QuestionInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/QuestionInfo;->Answer()Ljava/lang/String;

    move-result-object v3

    .line 1501
    .local v3, "answer":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 1503
    .local v4, "jsonBytes":[B
    array-length v6, v4

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v12, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    move-object v8, v6

    .line 1506
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 1507
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 1509
    .end local v3    # "answer":Ljava/lang/String;
    .end local v4    # "jsonBytes":[B
    goto/16 :goto_3

    .line 1404
    .end local v19    # "fullURL":Ljava/lang/String;
    .local v4, "fullURL":Ljava/lang/String;
    :pswitch_3
    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    sget v4, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    const/4 v11, 0x1

    if-eq v11, v4, :cond_3

    goto/16 :goto_3

    .line 1408
    :cond_3
    invoke-virtual {v7, v6, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v22, v3

    check-cast v22, Landroid/telephony/TelephonyManager;

    .line 1411
    .local v22, "telephonyManager":Landroid/telephony/TelephonyManager;
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v20, v3

    check-cast v20, Landroid/media/AudioManager;

    .line 1412
    .local v20, "audioManager":Landroid/media/AudioManager;
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v21, v3

    check-cast v21, Landroid/net/ConnectivityManager;

    .line 1413
    .local v21, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-direct/range {p0 .. p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_GetScreenBrightness()I

    move-result v23

    .line 1414
    .local v23, "screenBrightness":I
    iget-object v3, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_windowManager:Landroid/view/WindowManager;

    sget-object v25, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    iget-object v4, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_currentActivity:Landroid/app/Activity;

    move-object/from16 v24, v3

    move-object/from16 v26, v4

    invoke-static/range {v20 .. v26}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetDeviceInfoInLog(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    .line 1416
    .local v3, "deviceInfoJsonString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DeviceInfoJsonString : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 1419
    .local v4, "jsonBytes":[B
    array-length v6, v4

    .line 1420
    .local v6, "jsonBytesLength":I
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v12, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    move-object v8, v11

    .line 1422
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 1423
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 1424
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 1426
    const/4 v11, 0x0

    sput v11, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    .line 1430
    .end local v3    # "deviceInfoJsonString":Ljava/lang/String;
    .end local v4    # "jsonBytes":[B
    .end local v6    # "jsonBytesLength":I
    .end local v20    # "audioManager":Landroid/media/AudioManager;
    .end local v21    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v22    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .end local v23    # "screenBrightness":I
    goto/16 :goto_3

    .line 1375
    .end local v19    # "fullURL":Ljava/lang/String;
    .local v4, "fullURL":Ljava/lang/String;
    :pswitch_4
    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    invoke-static {v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    invoke-virtual {v7, v6, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v22, v3

    check-cast v22, Landroid/telephony/TelephonyManager;

    .line 1381
    .restart local v22    # "telephonyManager":Landroid/telephony/TelephonyManager;
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v20, v3

    check-cast v20, Landroid/media/AudioManager;

    .line 1382
    .restart local v20    # "audioManager":Landroid/media/AudioManager;
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v21, v3

    check-cast v21, Landroid/net/ConnectivityManager;

    .line 1383
    .restart local v21    # "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-direct/range {p0 .. p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_GetScreenBrightness()I

    move-result v23

    .line 1384
    .restart local v23    # "screenBrightness":I
    iget-object v3, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_windowManager:Landroid/view/WindowManager;

    sget-object v25, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    iget-object v4, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_currentActivity:Landroid/app/Activity;

    .line 1385
    move-object/from16 v24, v3

    move-object/from16 v26, v4

    invoke-static/range {v20 .. v26}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetDeviceInfoInLogin(Landroid/media/AudioManager;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;ILandroid/view/WindowManager;Landroid/content/Context;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    .line 1387
    .restart local v3    # "deviceInfoJsonString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DeviceInfoJsonString : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 1390
    .local v4, "jsonBytes":[B
    array-length v6, v4

    .line 1391
    .restart local v6    # "jsonBytesLength":I
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v24, v3

    .end local v3    # "deviceInfoJsonString":Ljava/lang/String;
    .local v24, "deviceInfoJsonString":Ljava/lang/String;
    const-string v3, "jsonBytesLength : "

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1392
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v12, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    move-object v8, v3

    .line 1394
    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    .line 1395
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 1396
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 1400
    .end local v4    # "jsonBytes":[B
    .end local v6    # "jsonBytesLength":I
    .end local v20    # "audioManager":Landroid/media/AudioManager;
    .end local v21    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v22    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .end local v23    # "screenBrightness":I
    .end local v24    # "deviceInfoJsonString":Ljava/lang/String;
    goto :goto_3

    .line 1358
    .end local v19    # "fullURL":Ljava/lang/String;
    .local v4, "fullURL":Ljava/lang/String;
    :pswitch_5
    move-object/from16 v19, v4

    .end local v4    # "fullURL":Ljava/lang/String;
    .restart local v19    # "fullURL":Ljava/lang/String;
    const-string v3, "----------V2ymHFg03ehb4238qgZCaK128O6jy"

    .line 1359
    .local v3, "BoundaryConstant":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multipart/form-data; boundary="

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1360
    .local v4, "contentType":Ljava/lang/String;
    invoke-virtual {v7, v6, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1371
    .end local v3    # "BoundaryConstant":Ljava/lang/String;
    .end local v4    # "contentType":Ljava/lang/String;
    nop

    .line 1550
    :goto_3
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->connect()V

    .line 1551
    const-string v3, "None"

    .line 1552
    .local v3, "serverResponseMessage":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    .line 1553
    .local v4, "serverResponseCode":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "httpURLConnection.getResponseCode : "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    const/16 v6, 0xc8

    if-eq v4, v6, :cond_4

    .line 1738
    const-string v6, "ResponseCode : Not HTTP_Error - QuitApplication"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1741
    iget-object v2, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 1742
    .local v2, "msg":Landroid/os/Message;
    const/16 v6, 0xe

    iput v6, v2, Landroid/os/Message;->what:I

    .line 1743
    iget-object v6, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move/from16 v21, v4

    move-object/from16 v22, v5

    goto/16 :goto_9

    .line 1559
    .end local v2    # "msg":Landroid/os/Message;
    :cond_4
    const-string v6, "ResponseCode : HTTP_OK"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    move-object v9, v6

    .line 1562
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v10, v6

    .line 1563
    const/16 v6, 0x800

    new-array v6, v6, [B

    .line 1564
    .local v6, "byteBuffer":[B
    const/4 v11, 0x0

    .line 1565
    .local v11, "byteData":[B
    const/4 v12, 0x0

    move/from16 v18, v12

    .line 1566
    .local v18, "nLength":I
    :goto_4
    array-length v12, v6

    move-object/from16 v21, v3

    const/4 v3, 0x0

    .end local v3    # "serverResponseMessage":Ljava/lang/String;
    .local v21, "serverResponseMessage":Ljava/lang/String;
    invoke-virtual {v9, v6, v3, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v12

    move/from16 v20, v12

    const/4 v3, -0x1

    .end local v18    # "nLength":I
    .local v20, "nLength":I
    if-eq v12, v3, :cond_5

    .line 1568
    move/from16 v12, v20

    const/4 v3, 0x0

    .end local v20    # "nLength":I
    .local v12, "nLength":I
    invoke-virtual {v10, v6, v3, v12}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move/from16 v18, v12

    move-object/from16 v3, v21

    const/4 v12, 0x0

    goto :goto_4

    .line 1571
    .end local v12    # "nLength":I
    .restart local v20    # "nLength":I
    :cond_5
    move/from16 v12, v20

    .end local v20    # "nLength":I
    .restart local v12    # "nLength":I
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 1572
    .end local v11    # "byteData":[B
    .local v3, "byteData":[B
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v3}, Ljava/lang/String;-><init>([B)V

    .line 1573
    .end local v21    # "serverResponseMessage":Ljava/lang/String;
    .local v11, "serverResponseMessage":Ljava/lang/String;
    move-object/from16 v20, v3

    .end local v3    # "byteData":[B
    .local v20, "byteData":[B
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1574
    .local v3, "responseJSON":Lorg/json/JSONObject;
    move/from16 v21, v4

    .end local v4    # "serverResponseCode":I
    .local v21, "serverResponseCode":I
    const-string v4, "status"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1575
    .local v4, "status":Ljava/lang/String;
    move-object/from16 v22, v5

    .end local v5    # "url":Ljava/net/URL;
    .local v22, "url":Ljava/net/URL;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v23, v6

    .end local v6    # "byteBuffer":[B
    .local v23, "byteBuffer":[B
    const-string v6, "status : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1577
    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_1

    :cond_6
    goto :goto_5

    :sswitch_9
    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    goto :goto_6

    :sswitch_a
    const-string v6, "event"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x2

    goto :goto_6

    :sswitch_b
    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    goto :goto_6

    :sswitch_c
    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz v5, :cond_6

    const/4 v5, 0x3

    goto :goto_6

    :goto_5
    const/4 v5, -0x1

    :goto_6
    const-string v6, "ok"

    if-eqz v5, :cond_d

    const/4 v13, 0x1

    if-eq v5, v13, :cond_c

    const/4 v13, 0x2

    if-eq v5, v13, :cond_9

    const/4 v13, 0x3

    if-eq v5, v13, :cond_7

    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1702
    :cond_7
    :try_start_4
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1711
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/GlobalSet;->RemoveAtQuestionInfo(I)V

    .line 1712
    invoke-static {}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfoCount()I

    move-result v5

    if-lez v5, :cond_8

    .line 1714
    iget-object v5, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    .line 1715
    .local v5, "msg":Landroid/os/Message;
    const/16 v6, 0xb

    iput v6, v5, Landroid/os/Message;->what:I

    .line 1716
    iget-object v6, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v6, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1712
    .end local v5    # "msg":Landroid/os/Message;
    :cond_8
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1680
    :cond_9
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1682
    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/GlobalSet;->ParseQuestionInfo(Lorg/json/JSONObject;)V

    .line 1683
    invoke-static {}, Lcom/kint/kintframeworkaosaar/GlobalSet;->GetQuestionInfoCount()I

    move-result v5

    if-lez v5, :cond_a

    .line 1685
    iget-object v5, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    .line 1686
    .restart local v5    # "msg":Landroid/os/Message;
    const/16 v6, 0xb

    iput v6, v5, Landroid/os/Message;->what:I

    .line 1687
    iget-object v6, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v6, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1688
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    .end local v5    # "msg":Landroid/os/Message;
    goto/16 :goto_8

    .line 1683
    :cond_a
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1680
    :cond_b
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1634
    :cond_c
    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    invoke-direct {v5, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    iput-object v5, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    .line 1635
    iget-object v5, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    invoke-virtual {v5, v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->Parse(Lorg/json/JSONObject;)Landroid/os/Message;

    .line 1639
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    goto/16 :goto_8

    .line 1594
    :cond_d
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1596
    const-string v5, "isScreenCaptureAllowed"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1597
    .local v5, "isScreenCaptureAllowed":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "isScreenCaptureAllowed : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1600
    const-string v6, "isScreenStreamAllowed"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 1601
    .local v6, "isScreenStreamAllowed":Z
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "isScreenStreamAllowed : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1604
    const-string v13, "isCameraStreamAllowed"

    invoke-virtual {v3, v13}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    .line 1605
    .local v13, "isCameraStreamAllowed":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isCameraStreamAllowed : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1608
    const-string v14, "minimumTestBuildNumber"

    invoke-virtual {v3, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 1609
    .local v14, "minimumTestBuildNumber":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v4

    .end local v4    # "status":Ljava/lang/String;
    .local v16, "status":Ljava/lang/String;
    const-string v4, "minimumTestBuildNumber : "

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    const-string v4, "sessionTime"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1611
    .local v4, "sessionTimeJSON":Lorg/json/JSONObject;
    const-string v15, "accumulated"

    invoke-virtual {v4, v15}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    move-wide/from16 v26, v24

    .line 1612
    .local v26, "accumulated":J
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v3

    .end local v3    # "responseJSON":Lorg/json/JSONObject;
    .local v17, "responseJSON":Lorg/json/JSONObject;
    const-string v3, "accumulated : "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v5

    move/from16 v18, v6

    move-wide/from16 v5, v26

    .end local v6    # "isScreenStreamAllowed":Z
    .end local v26    # "accumulated":J
    .local v3, "isScreenCaptureAllowed":Z
    .local v5, "accumulated":J
    .local v18, "isScreenStreamAllowed":Z
    invoke-virtual {v15, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1613
    const-string v15, "current"

    invoke-virtual {v4, v15}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    move-wide/from16 v26, v24

    .line 1614
    .local v26, "current":J
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v24, v3

    .end local v3    # "isScreenCaptureAllowed":Z
    .local v24, "isScreenCaptureAllowed":Z
    const-string v3, "current : "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v25, v4

    move-wide/from16 v3, v26

    .end local v4    # "sessionTimeJSON":Lorg/json/JSONObject;
    .end local v26    # "current":J
    .local v3, "current":J
    .local v25, "sessionTimeJSON":Lorg/json/JSONObject;
    invoke-virtual {v15, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    sget v15, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    if-nez v15, :cond_f

    .line 1618
    sget-wide v26, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    cmp-long v15, v3, v26

    if-ltz v15, :cond_f

    .line 1620
    sget-boolean v15, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    if-nez v15, :cond_e

    .line 1621
    sget-wide v26, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    const-wide/16 v28, 0x258

    add-long v26, v26, v28

    sput-wide v26, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    goto :goto_7

    .line 1623
    :cond_e
    sget-wide v26, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    const-wide/16 v28, 0x3c

    add-long v26, v26, v28

    sput-wide v26, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    .line 1625
    :goto_7
    const/4 v15, 0x1

    sput v15, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheckStatus:I

    .line 1628
    .end local v3    # "current":J
    .end local v5    # "accumulated":J
    .end local v13    # "isCameraStreamAllowed":Z
    .end local v14    # "minimumTestBuildNumber":I
    .end local v18    # "isScreenStreamAllowed":Z
    .end local v24    # "isScreenCaptureAllowed":Z
    .end local v25    # "sessionTimeJSON":Lorg/json/JSONObject;
    :cond_f
    goto :goto_8

    .line 1594
    .end local v16    # "status":Ljava/lang/String;
    .end local v17    # "responseJSON":Lorg/json/JSONObject;
    .local v3, "responseJSON":Lorg/json/JSONObject;
    .local v4, "status":Ljava/lang/String;
    :cond_10
    move-object/from16 v17, v3

    move-object/from16 v16, v4

    .line 1732
    .end local v3    # "responseJSON":Lorg/json/JSONObject;
    .end local v4    # "status":Ljava/lang/String;
    .restart local v16    # "status":Ljava/lang/String;
    .restart local v17    # "responseJSON":Lorg/json/JSONObject;
    :goto_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-----------------------------> Server Response - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    move-object v3, v11

    .line 1748
    .end local v11    # "serverResponseMessage":Ljava/lang/String;
    .end local v12    # "nLength":I
    .end local v16    # "status":Ljava/lang/String;
    .end local v17    # "responseJSON":Lorg/json/JSONObject;
    .end local v20    # "byteData":[B
    .end local v23    # "byteBuffer":[B
    .local v3, "serverResponseMessage":Ljava/lang/String;
    :goto_9
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 1749
    const/4 v2, 0x2

    iput v2, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_dispatchNetworkStatus:I
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .end local v3    # "serverResponseMessage":Ljava/lang/String;
    .end local v7    # "httpURLConnection":Ljava/net/HttpURLConnection;
    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .end local v9    # "inputStream":Ljava/io/InputStream;
    .end local v10    # "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    .end local v19    # "fullURL":Ljava/lang/String;
    .end local v21    # "serverResponseCode":I
    .end local v22    # "url":Ljava/net/URL;
    goto :goto_c

    .line 1755
    :catch_0
    move-exception v0

    move-object v2, v0

    goto :goto_a

    .line 1751
    :catch_1
    move-exception v0

    move-object v2, v0

    goto :goto_b

    .line 1757
    .local v2, "e":Ljava/io/IOException;
    :goto_a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1760
    iget-object v3, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 1761
    .local v3, "msg":Landroid/os/Message;
    const/16 v4, 0xe

    iput v4, v3, Landroid/os/Message;->what:I

    .line 1762
    iget-object v4, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_d

    .line 1753
    .end local v3    # "msg":Landroid/os/Message;
    .local v2, "e":Lorg/json/JSONException;
    :goto_b
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 1763
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_c
    nop

    .line 1764
    :goto_d
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5435c042 -> :sswitch_8
        -0x457dc41a -> :sswitch_7
        0x1a344 -> :sswitch_6
        0x5c6729a -> :sswitch_5
        0x625ef69 -> :sswitch_4
        0xb94092a -> :sswitch_3
        0x20efc746 -> :sswitch_2
        0x5a81bf37 -> :sswitch_1
        0x606fbe5c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x5435c042 -> :sswitch_c
        0x1a344 -> :sswitch_b
        0x5c6729a -> :sswitch_a
        0x625ef69 -> :sswitch_9
    .end sparse-switch
.end method

.method private _SendCallbackMessage(I)V
    .locals 1
    .param p1, "messageType"    # I

    .line 1059
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

    if-eqz v0, :cond_0

    .line 1060
    invoke-interface {v0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;->SendMessage(I)V

    .line 1061
    :cond_0
    return-void
.end method

.method private _StartNetworkMessageLoop()V
    .locals 5

    .line 1769
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1770
    .local v0, "handler":Landroid/os/Handler;
    const/16 v1, 0x3e8

    .line 1772
    .local v1, "delay":I
    new-instance v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    invoke-direct {v2, p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/os/Handler;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2050
    return-void
.end method

.method private _WriteUserCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "userCode"    # Ljava/lang/String;

    .line 2691
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "methinksPatcher"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2692
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2694
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    sget-boolean v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    if-nez v2, :cond_0

    .line 2695
    const-string v2, "UserCode_Release"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2697
    :cond_0
    const-string v2, "UserCode_Debug"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2698
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2699
    return-void
.end method

.method static synthetic access$000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_RemoveAllUserCode()V

    return-void
.end method

.method static synthetic access$100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer3:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_processStatus:I

    return v0
.end method

.method static synthetic access$1102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # I

    .line 85
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_processStatus:I

    return p1
.end method

.method static synthetic access$1200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_loginInfo:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->paused:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Z

    .line 85
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->paused:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjection;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/media/projection/MediaProjection;)Landroid/media/projection/MediaProjection;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Landroid/media/projection/MediaProjection;

    .line 85
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_projectionResultCode:I

    return v0
.end method

.method static synthetic access$1600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_projectionIntentData:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_quitApplication:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Z

    .line 85
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_quitApplication:Z

    return p1
.end method

.method static synthetic access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_requestMessageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer2:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_CreateWindowManagerLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .line 85
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_GetLayoutResourceID(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/String;

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_GetResourceID(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_windowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .line 85
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_GetResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # I

    .line 85
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_SendCallbackMessage(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .line 85
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_WriteUserCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_currentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_dispatchNetworkStatus:I

    return v0
.end method

.method static synthetic access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # I

    .line 85
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_dispatchNetworkStatus:I

    return p1
.end method

.method static synthetic access$400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 85
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_RequestServer(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    return-void
.end method

.method static synthetic access$500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_detectScreenShotTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkForegroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_IsForegrounded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_backgroundMode:Z

    return v0
.end method

.method static synthetic access$802(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
    .param p1, "x1"    # Z

    .line 85
    iput-boolean p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_backgroundMode:Z

    return p1
.end method

.method static synthetic access$900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;
    .locals 1
    .param p0, "x0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_checkBackgroundTimer:Lcom/kint/kintframeworkaosaar/DelayTimer;

    return-object v0
.end method

.method static enableShowTouch(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isChecked"    # Z

    .line 2998
    const-string v0, "show_touches"

    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    .line 3000
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 3004
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 3006
    :goto_0
    return-void
.end method

.method static getShowTouchCheck(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 3010
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "show_touches"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 3011
    .local v0, "showTouchStatus":I
    if-nez v0, :cond_0

    .line 3012
    return v1

    .line 3013
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method static settingsSystemCanWrite(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 2989
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 2991
    invoke-static {p0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    return v0

    .line 2993
    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public CreateScreenCaptureIntent()Landroid/content/Intent;
    .locals 2

    .line 1033
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    if-nez v0, :cond_0

    .line 1034
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "media_projection"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/projection/MediaProjectionManager;

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    .line 1035
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjectionManager:Landroid/media/projection/MediaProjectionManager;

    invoke-virtual {v0}, Landroid/media/projection/MediaProjectionManager;->createScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public FirstInitialize(Landroid/app/Activity;)V
    .locals 2
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 114
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[FirstInitialize]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Landroid/app/Application;

    .line 117
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    .line 118
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SetMainActivity(Landroid/app/Activity;)V

    .line 120
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->patcherApplication:Landroid/app/Application;

    invoke-virtual {v0, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 123
    return-void
.end method

.method public GetMainActivity()Landroid/app/Activity;
    .locals 1

    .line 1070
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mainActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public GetProcessStatus()I
    .locals 1

    .line 1028
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_processStatus:I

    return v0
.end method

.method public Initialize(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "presetModule"    # Ljava/lang/String;
    .param p2, "presetProject"    # Ljava/lang/String;

    .line 420
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/GlobalSet;->ParsePresetModule(Ljava/lang/String;)V

    .line 421
    invoke-static {p2}, Lcom/kint/kintframeworkaosaar/GlobalSet;->ParsePresetProject(Ljava/lang/String;)V

    .line 423
    sget-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 424
    const-wide/16 v2, 0x3c

    sput-wide v2, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    goto :goto_0

    .line 426
    :cond_0
    const-wide/16 v2, 0x258

    sput-wide v2, Lcom/kint/kintframeworkaosaar/MessagePackLog;->SessionTimeCheck:J

    .line 428
    :goto_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callBackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SetCallbackMessage(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;)V

    .line 430
    sget-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->EnableModule:Z

    if-ne v1, v0, :cond_1

    .line 431
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->GetMainActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnCreate(Landroid/app/Activity;)V

    .line 432
    :cond_1
    return-void
.end method

.method public InitialzeExceptionHandler()V
    .locals 1

    .line 129
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_threadUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 130
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_uncaughtExceptionHandler:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;

    .line 131
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_uncaughtExceptionHandler:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$UncaughtExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 132
    return-void
.end method

.method public Login()V
    .locals 2

    .line 1169
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_processStatus:I

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 1171
    return-void

    .line 1174
    :cond_0
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_StartNetworkMessageLoop()V

    .line 1175
    invoke-direct {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_ReadUserCode()Ljava/lang/String;

    move-result-object v0

    .line 1176
    .local v0, "userCode":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1178
    new-instance v1, Lcom/kint/kintframeworkaosaar/MessagePackLogin;

    invoke-direct {v1}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;-><init>()V

    .line 1179
    .local v1, "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;->Prepare()V

    .line 1180
    invoke-virtual {p0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 1181
    .end local v1    # "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    goto :goto_0

    .line 1185
    :cond_1
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SetUserCode(Ljava/lang/String;)V

    .line 1187
    :goto_0
    return-void
.end method

.method public OnActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 1115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityCreated] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    return-void
.end method

.method public OnActivityDestroyed(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 1155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityDestroyed] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    return-void
.end method

.method public OnActivityPaused(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 1091
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityPaused] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , PackageName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    return-void
.end method

.method public OnActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 4
    .param p1, "mainActivity"    # Landroid/app/Activity;
    .param p2, "requestCode"    # I
    .param p3, "resultCode"    # I
    .param p4, "data"    # Landroid/content/Intent;

    .line 341
    const/16 v0, 0x3e8

    const-string v1, "methinksPatcherBridge"

    const/4 v2, -0x1

    if-ne v2, p3, :cond_0

    .line 343
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult -  resultCode : RESULT_OK , requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 348
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    .local v0, "settingsSystemIntent":Landroid/content/Intent;
    const/16 v1, 0x3e9

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 351
    .end local v0    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_0

    .line 355
    :pswitch_1
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v1

    .line 356
    .local v1, "captureIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 358
    .end local v1    # "captureIntent":Landroid/content/Intent;
    goto :goto_0

    .line 362
    :pswitch_2
    invoke-virtual {p0, p3, p4}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SetMediaProjection(ILandroid/content/Intent;)V

    .line 363
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Login()V

    .line 365
    :goto_0
    goto :goto_3

    .line 370
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult -  resultCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " , requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-wide/16 v1, 0x7d0

    const/4 v3, 0x0

    packed-switch p2, :pswitch_data_1

    goto :goto_3

    .line 375
    :pswitch_3
    const-string v0, "Draw Overlay Permission Required. Restart App."

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 378
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    goto :goto_1

    .line 380
    :catch_0
    move-exception v0

    .line 382
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 385
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :goto_1
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 386
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 388
    goto :goto_3

    .line 409
    :pswitch_4
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v1

    .line 410
    .restart local v1    # "captureIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 392
    .end local v1    # "captureIntent":Landroid/content/Intent;
    :pswitch_5
    const-string v0, "Screen Capture Permission Required. Restart App."

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 395
    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 400
    goto :goto_2

    .line 397
    :catch_1
    move-exception v0

    .line 399
    .restart local v0    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 401
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :goto_2
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 402
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 404
    nop

    .line 415
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public OnActivityResumed(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 1121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityResumed] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , PackageName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_currentActivity:Landroid/app/Activity;

    .line 1150
    return-void
.end method

.method public OnActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 1086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivitySaveInstanceState] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    return-void
.end method

.method public OnActivityStarted(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityStarted] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    return-void
.end method

.method public OnActivityStopped(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 1076
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityStopped] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    return-void
.end method

.method public OnCreate(Landroid/app/Activity;)V
    .locals 6
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 186
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->GetProcessStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 187
    return-void

    .line 189
    :cond_0
    const-string v0, "methinksPatcherBridge"

    const-string v2, "[onCreate] - GetProcessStatus Next"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 192
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 193
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 194
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 195
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v3, v4, :cond_1

    .line 197
    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 221
    :cond_1
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;

    invoke-direct {v5, p0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/content/IntentFilter;)V

    invoke-virtual {v3, v5, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 258
    sget-object v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_windowManager:Landroid/view/WindowManager;

    .line 259
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_requestMessageList:Ljava/util/ArrayList;

    .line 260
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_windowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    if-eqz v3, :cond_5

    if-eq v3, v1, :cond_4

    const/4 v5, 0x2

    if-eq v3, v5, :cond_3

    const/4 v5, 0x3

    if-eq v3, v5, :cond_2

    goto :goto_0

    .line 288
    :cond_2
    sget v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 289
    .local v3, "temp":I
    sget v5, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    sput v5, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 290
    sput v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    .line 293
    const/16 v5, 0x5a

    iput v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_screenRotationAngle:I

    goto :goto_0

    .line 282
    .end local v3    # "temp":I
    :cond_3
    const/16 v3, 0xb4

    iput v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_screenRotationAngle:I

    .line 284
    goto :goto_0

    .line 270
    :cond_4
    sget v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 271
    .restart local v3    # "temp":I
    sget v5, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    sput v5, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 272
    sput v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    .line 275
    const/16 v5, -0x5a

    iput v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_screenRotationAngle:I

    .line 277
    .end local v3    # "temp":I
    goto :goto_0

    .line 266
    :cond_5
    nop

    .line 298
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[onCreate] - Language : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const/4 v0, 0x0

    .line 301
    .local v0, "firstRequestOverlayPermission":Z
    const/16 v3, 0x3e9

    const-string v5, "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"

    if-ne v1, v0, :cond_8

    .line 303
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_7

    .line 305
    invoke-static {p1}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 306
    new-instance v1, Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    invoke-direct {v1, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 307
    .local v1, "intent":Landroid/content/Intent;
    const/16 v3, 0x3ea

    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 308
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_2

    .line 309
    :cond_6
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 310
    .local v1, "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 311
    .end local v1    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_2

    .line 315
    :cond_7
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 316
    .restart local v1    # "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 317
    .end local v1    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_2

    .line 321
    :cond_8
    iget v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    if-nez v4, :cond_a

    .line 323
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->settingsSystemShowTouch()Z

    move-result v4

    if-nez v4, :cond_9

    .line 325
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .local v4, "settingsSystemIntent":Landroid/content/Intent;
    invoke-virtual {p1, v4, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 327
    .end local v4    # "settingsSystemIntent":Landroid/content/Intent;
    goto :goto_1

    .line 330
    :cond_9
    invoke-virtual {p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->CreateScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v3

    .line 331
    .local v3, "captureIntent":Landroid/content/Intent;
    const/16 v4, 0x3e8

    invoke-virtual {p1, v3, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 333
    .end local v3    # "captureIntent":Landroid/content/Intent;
    :goto_1
    iput v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_viewDeveloperMode:I

    .line 336
    :cond_a
    :goto_2
    return-void
.end method

.method public Preset4Unity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "sendToUnityGameObjectName"    # Ljava/lang/String;
    .param p2, "sendToMethodName"    # Ljava/lang/String;

    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Preset4Unity] - sendToUnityGameObjectName :  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " , sendToMethodName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_sendToUnityGameObjectName:Ljava/lang/String;

    .line 438
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_sendToUnityMethodName:Ljava/lang/String;

    .line 439
    return-void
.end method

.method public Quit()V
    .locals 1

    .line 1211
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_currentActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1212
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1213
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 1214
    return-void
.end method

.method public SendMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 448
    invoke-virtual {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SendRequestMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V
    .locals 2
    .param p1, "requestMessagePack"    # Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 1282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendRequestMessage - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_requestMessageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1284
    return-void
.end method

.method public SendRequestMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "eventKey"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .line 1288
    iget v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_processStatus:I

    if-nez v0, :cond_0

    .line 1290
    const-string v0, "Login Required"

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Tooltip(Ljava/lang/String;)V

    .line 1291
    return-void

    .line 1294
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x360802

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq v0, v1, :cond_2

    :cond_1
    goto :goto_0

    :cond_2
    const-string v0, "stop"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_3

    goto :goto_4

    .line 1298
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x47319dfb

    if-eq v0, v1, :cond_5

    :cond_4
    goto :goto_2

    :cond_5
    const-string v0, "stream_log"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_3

    :goto_2
    const/4 v2, -0x1

    :goto_3
    if-eqz v2, :cond_6

    .line 1307
    nop

    .line 1313
    :goto_4
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackEvent;

    invoke-direct {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/MessagePackEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    .local v0, "messagePackEvent":Lcom/kint/kintframeworkaosaar/MessagePackEvent;
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 1315
    return-void

    .line 1304
    .end local v0    # "messagePackEvent":Lcom/kint/kintframeworkaosaar/MessagePackEvent;
    :cond_6
    return-void
.end method

.method public SetCallbackMessage(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;)V
    .locals 0
    .param p1, "callbackMessage"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

    .line 1054
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_callbackMessage:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;

    .line 1055
    return-void
.end method

.method public SetMainActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .line 1065
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mainActivity:Landroid/app/Activity;

    .line 1066
    return-void
.end method

.method public SetMediaProjection(ILandroid/content/Intent;)V
    .locals 0
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .line 1040
    iput p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_projectionResultCode:I

    .line 1041
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_projectionIntentData:Landroid/content/Intent;

    .line 1045
    return-void
.end method

.method public SetUserCode(Ljava/lang/String;)V
    .locals 4
    .param p1, "userCode"    # Ljava/lang/String;

    .line 1191
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    .line 1193
    sget-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugMode:Z

    const-string v1, "?projectID="

    const-string v2, "/screen/"

    const/4 v3, 0x1

    if-ne v3, v0, :cond_0

    .line 1195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->DebugRTMPServerBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    goto :goto_0

    .line 1199
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->ReleaseRTMPServerBaseURL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_userCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->ProjectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    .line 1202
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[SetUserCode] - RTMPServerURL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    new-instance v0, Lcom/kint/kintframeworkaosaar/MessagePackLogin;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;-><init>()V

    .line 1205
    .local v0, "messagePackLogin":Lcom/kint/kintframeworkaosaar/MessagePackLogin;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/MessagePackLogin;->Request()V

    .line 1206
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->SendRequestMessage(Lcom/kint/kintframeworkaosaar/MessagePack;)V

    .line 1207
    return-void
.end method

.method public Tooltip(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .line 2685
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2686
    return-void
.end method

.method public Tooltip(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "echo"    # Z

    .line 453
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 454
    nop

    .line 459
    return-void
.end method

.method public Uninitialize()V
    .locals 0

    .line 444
    return-void
.end method

.method _DetectScreenShotService()V
    .locals 9

    .line 1218
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1219
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 1220
    .local v1, "runningServiceInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1222
    .local v3, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    const-string v5, "com.android.systemui:screenshot"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1224
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v6}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "/Screenshots"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$3;

    invoke-direct {v5, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V

    .line 1225
    invoke-virtual {v4, v5}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v4

    .line 1235
    .local v4, "files":[Ljava/io/File;
    if-eqz v4, :cond_0

    array-length v5, v4

    if-nez v5, :cond_1

    .line 1236
    goto :goto_0

    .line 1238
    :cond_1
    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 1239
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v4, v5

    .line 1241
    .local v5, "lastFile":Ljava/io/File;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Screenshot captured!! - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1243
    .local v6, "screenCaptureMessage":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[_DetectScreenShotService] - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "methinksPatcherBridge"

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    .end local v3    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    .end local v4    # "files":[Ljava/io/File;
    .end local v5    # "lastFile":Ljava/io/File;
    .end local v6    # "screenCaptureMessage":Ljava/lang/String;
    :cond_2
    goto/16 :goto_0

    .line 1268
    :cond_3
    return-void
.end method

.method public connectServer(Ljava/lang/String;)V
    .locals 3
    .param p1, "rtmpServerURL"    # Ljava/lang/String;

    .line 2757
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ConnectServer] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2758
    const-string v0, "Ready"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2760
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    .line 2762
    new-instance v0, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;-><init>(Lcom/kint/kintframeworkaosaar/ConnectCheckerRtmp;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 2763
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    sget v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setVideoResolution(II)V

    .line 2764
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    sget v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDataRate:I

    sget v2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoFrameRate:I

    invoke-virtual {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setVideoRate(II)V

    .line 2772
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_rtmpServerURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->start(Ljava/lang/String;)V

    .line 2773
    new-instance v0, Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;-><init>(Lcom/kint/kintframeworkaosaar/GetH264Data;)V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2774
    return-void
.end method

.method public getH264Data(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 7
    .param p1, "h264Buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "info"    # Landroid/media/MediaCodec$BufferInfo;

    .line 2908
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    rem-long/2addr v0, v2

    .line 2909
    .local v0, "checkTime":J
    const-wide/16 v2, 0x79e

    const-string v4, "methinksPatcherBridge"

    cmp-long v5, v0, v2

    if-lez v5, :cond_0

    .line 2911
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[getH264Data] captured : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2914
    :cond_0
    iget-boolean v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectedRTMPServer:Z

    const/4 v6, 0x1

    if-ne v6, v5, :cond_3

    .line 2917
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v5, :cond_3

    .line 2920
    iget-boolean v5, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->paused:Z

    if-nez v5, :cond_2

    .line 2923
    cmp-long v5, v0, v2

    if-lez v5, :cond_1

    .line 2925
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[getH264Data] - Sending Stream : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2928
    :cond_1
    const-string v2, "Sending..."

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2929
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    invoke-virtual {v2, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->sendVideo(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 2931
    iget-boolean v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->sendRTMPVideo:Z

    if-nez v2, :cond_3

    .line 2933
    const-string v2, "[getH264Data] - Start Send Stream"

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2934
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 2935
    .local v2, "message":Landroid/os/Message;
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->what:I

    .line 2936
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2938
    iput-boolean v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->sendRTMPVideo:Z

    .line 2939
    .end local v2    # "message":Landroid/os/Message;
    goto :goto_0

    .line 2943
    :cond_2
    const-string v2, "[getH264Data] - Pause Send Stream"

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2944
    const-string v2, "Paused"

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2952
    :cond_3
    :goto_0
    return-void
.end method

.method public getNetworkStatus()Ljava/lang/String;
    .locals 1

    .line 2835
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getShowTouchCheck()Z
    .locals 1

    .line 2964
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->getShowTouchCheck(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .line 2738
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    return v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 691
    invoke-virtual {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 692
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 704
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityDestroyed(Landroid/app/Activity;)V

    .line 705
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 685
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityPaused(Landroid/app/Activity;)V

    .line 686
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 698
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityResumed(Landroid/app/Activity;)V

    .line 699
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .line 679
    invoke-virtual {p0, p1, p2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 680
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 673
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityStarted(Landroid/app/Activity;)V

    .line 674
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 667
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityStopped(Landroid/app/Activity;)V

    .line 668
    return-void
.end method

.method public onAuthErrorRtmp()V
    .locals 2

    .line 2879
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[onAuthErrorRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2880
    const-string v0, "AuthError"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2881
    return-void
.end method

.method public onAuthSuccessRtmp()V
    .locals 2

    .line 2886
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[onAuthSuccessRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2887
    const-string v0, "AuthSuccess"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2888
    return-void
.end method

.method public onConnectionFailedRtmp(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 2855
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onConnectionFailedRtmp] - reason : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2856
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConnectionFailed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2858
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2859
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2860
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2861
    return-void
.end method

.method public onConnectionSuccessRtmp()V
    .locals 2

    .line 2842
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[onConnectionSuccessRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2843
    const-string v0, "ConnectionSuccess"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2844
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectedRTMPServer:Z

    .line 2845
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->paused:Z

    .line 2847
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 2848
    .local v1, "message":Landroid/os/Message;
    iput v0, v1, Landroid/os/Message;->what:I

    .line 2849
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2850
    return-void
.end method

.method public onDisconnectRtmp()V
    .locals 2

    .line 2866
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[onDisconnectRtmp]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2867
    const-string v0, "Disconnect"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2868
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectedRTMPServer:Z

    .line 2869
    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->sendRTMPVideo:Z

    .line 2871
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2872
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2873
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2874
    return-void
.end method

.method public onSPSandPPS(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "sps"    # Ljava/nio/ByteBuffer;
    .param p2, "pps"    # Ljava/nio/ByteBuffer;

    .line 2894
    const-string v0, "methinksPatcherBridge"

    const-string v1, "[onSPSandPPS]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2895
    const-string v0, "SPSandPPS"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2896
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectedRTMPServer:Z

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 2898
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v0, :cond_0

    .line 2900
    invoke-virtual {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->setSpsPPs(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 2903
    :cond_0
    return-void
.end method

.method public onVideoFormat(Landroid/media/MediaFormat;)V
    .locals 2
    .param p1, "mediaFormat"    # Landroid/media/MediaFormat;

    .line 2957
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onVideoFormat] - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/media/MediaFormat;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2958
    const-string v0, "VideoFormat"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->networkStatus:Ljava/lang/String;

    .line 2959
    return-void
.end method

.method public setResolution(III)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "dpi"    # I

    .line 2743
    sput p1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    .line 2744
    sput p2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    .line 2745
    sput p3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDpi:I

    .line 2746
    return-void
.end method

.method public setVideoRate(II)V
    .locals 0
    .param p1, "videoDataRate"    # I
    .param p2, "frameRate"    # I

    .line 2751
    sput p1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDataRate:I

    .line 2752
    sput p2, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoFrameRate:I

    .line 2753
    return-void
.end method

.method public settingsSystemShowTouch()Z
    .locals 3

    .line 2969
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/16 v2, 0x17

    if-lt v0, v2, :cond_1

    .line 2971
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2973
    const/4 v0, 0x0

    return v0

    .line 2977
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->enableShowTouch(Landroid/content/Context;Z)V

    goto :goto_0

    .line 2982
    :cond_1
    sget-object v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->ApplicationContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->enableShowTouch(Landroid/content/Context;Z)V

    .line 2984
    :goto_0
    return v1
.end method

.method public startRecord(Landroid/media/projection/MediaProjection;)Z
    .locals 10
    .param p1, "project"    # Landroid/media/projection/MediaProjection;

    .line 2778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[StartRecord] - running :  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2779
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    if-eqz v0, :cond_0

    .line 2780
    const/4 v0, 0x0

    return v0

    .line 2782
    :cond_0
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    .line 2784
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    sget v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->setWidth(I)V

    .line 2785
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    sget v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->setHeight(I)V

    .line 2786
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->prepareVideoEncoder()Z

    .line 2788
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    sget v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoWidth:I

    sget v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoHeight:I

    sget v5, Lcom/kint/kintframeworkaosaar/GlobalSet;->VideoDpi:I

    const/16 v6, 0x10

    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2790
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->getInputSurface()Landroid/view/Surface;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2789
    const-string v2, "MainScreen"

    invoke-virtual/range {v1 .. v9}, Landroid/media/projection/MediaProjection;->createVirtualDisplay(Ljava/lang/String;IIIILandroid/view/Surface;Landroid/hardware/display/VirtualDisplay$Callback;Landroid/os/Handler;)Landroid/hardware/display/VirtualDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 2792
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->start()V

    .line 2794
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    .line 2795
    return v0
.end method

.method public stopRecord()Z
    .locals 3

    .line 2800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[StopRecord] - running :  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "methinksPatcherBridge"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2801
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2802
    return v1

    .line 2804
    :cond_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 2806
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/VideoEncoder;->stop()V

    .line 2807
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->videoEncoder:Lcom/kint/kintframeworkaosaar/VideoEncoder;

    .line 2810
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    if-eqz v0, :cond_2

    .line 2812
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;->stop()V

    .line 2813
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->rtmpMuxer:Lcom/kint/kintframeworkaosaar/SrsFlvMuxer;

    .line 2816
    :cond_2
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->running:Z

    .line 2817
    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->paused:Z

    .line 2819
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    if-eqz v0, :cond_3

    .line 2821
    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 2822
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_virtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 2825
    :cond_3
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    if-eqz v0, :cond_4

    .line 2827
    invoke-virtual {v0}, Landroid/media/projection/MediaProjection;->stop()V

    .line 2828
    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_mediaProjection:Landroid/media/projection/MediaProjection;

    .line 2830
    :cond_4
    const/4 v0, 0x1

    return v0
.end method
