.class public Lcom/kint/kintframeworkaosaar/Data;
.super Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;
.source "Data.java"


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 26
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->DATA_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 32
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    return-object v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 46
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->readStringFrom(Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    .line 47
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->sizeOf(Ljava/lang/String;Z)I

    move-result v0

    .line 49
    .local v0, "bytesRead":I
    invoke-virtual {p0, p1, v0}, Lcom/kint/kintframeworkaosaar/Data;->readVariableData(Ljava/io/InputStream;I)V

    .line 50
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    .line 41
    return-void
.end method

.method protected size()I
    .locals 1

    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Data;->type:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/AmfString;->writeStringTo(Ljava/io/OutputStream;Ljava/lang/String;Z)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/Data;->writeVariableData(Ljava/io/OutputStream;)V

    .line 60
    return-void
.end method
