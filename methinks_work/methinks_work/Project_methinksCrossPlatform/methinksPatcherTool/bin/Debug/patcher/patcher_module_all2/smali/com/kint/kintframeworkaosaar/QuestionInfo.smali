.class public Lcom/kint/kintframeworkaosaar/QuestionInfo;
.super Ljava/lang/Object;
.source "QuestionInfo.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private _answerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private _choiceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private _id:Ljava/lang/String;

.field private _imageURL:Ljava/lang/String;

.field private _isMultipleChoice:Z

.field private _rangeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private _text:Ljava/lang/String;

.field private _type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "QuestionInfo"

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public AddAnswer(ZLjava/lang/String;)V
    .locals 1
    .param p1, "clear"    # Z
    .param p2, "answer"    # Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    .line 58
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public Answer()Ljava/lang/String;
    .locals 9

    .line 66
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 69
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "questionId"

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    const-string v1, "questionType"

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 73
    .local v1, "answerJsonArray":Lorg/json/JSONArray;
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    const v5, -0x4b4b31cf

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eq v4, v5, :cond_3

    const v5, 0x674393d

    if-eq v4, v5, :cond_2

    const v5, 0x1e0a23d1

    if-eq v4, v5, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const-string v4, "multipleChoice"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const-string v4, "range"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    const-string v4, "openEnd"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x2

    :goto_0
    if-eqz v3, :cond_7

    if-eq v3, v7, :cond_5

    if-eq v3, v6, :cond_4

    goto :goto_2

    .line 92
    :cond_4
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    aget-object v2, v2, v8

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    .line 83
    :cond_5
    move v2, v8

    .local v2, "countIndex":I
    :goto_1
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 85
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 88
    .end local v2    # "countIndex":I
    :cond_6
    goto :goto_2

    .line 77
    :cond_7
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_answerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    aget-object v2, v2, v8

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 79
    nop

    .line 96
    :goto_2
    const-string v2, "answer"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    nop

    .end local v1    # "answerJsonArray":Lorg/json/JSONArray;
    goto :goto_3

    .line 99
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 104
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public ChoiceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public GetID()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    return-object v0
.end method

.method public GetText()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_text:Ljava/lang/String;

    return-object v0
.end method

.method public GetType()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    return-object v0
.end method

.method public IsMultipleChoice()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_isMultipleChoice:Z

    return v0
.end method

.method public Parse(Lorg/json/JSONObject;)V
    .locals 12
    .param p1, "questionInfoJsonObject"    # Lorg/json/JSONObject;

    .line 112
    const-string v0, "QuestionInfo"

    :try_start_0
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 115
    :cond_0
    const-string v1, "questionId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    .line 116
    const-string v1, "questionType"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    .line 117
    const-string v1, "text"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_text:Ljava/lang/String;

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Question ID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , Question Type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_type:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const v4, -0x4b4b31cf

    const-string v5, "range"

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eq v3, v4, :cond_4

    const v4, 0x674393d

    if-eq v3, v4, :cond_3

    const v4, 0x1e0a23d1

    if-eq v3, v4, :cond_2

    :cond_1
    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "multipleChoice"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const-string v3, "openEnd"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_1

    const/4 v2, 0x2

    :goto_0
    const-string v1, "Question Multiple Choice Element["

    const-string v3, "isMultipleChoice"

    const-string v4, "choices"

    const-string v8, "] : "

    const-string v9, "/"

    if-eqz v2, :cond_8

    if-eq v2, v7, :cond_5

    goto/16 :goto_4

    .line 155
    :cond_5
    :try_start_2
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 156
    .local v2, "questionJsonArray":Lorg/json/JSONArray;
    move v4, v6

    .local v4, "countIndex":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 158
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    if-nez v5, :cond_6

    .line 159
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    .line 161
    :cond_6
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 162
    .local v5, "questionRangeElement":Ljava/lang/String;
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    nop

    .end local v5    # "questionRangeElement":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 167
    .end local v4    # "countIndex":I
    :cond_7
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_isMultipleChoice:Z

    .line 170
    .end local v2    # "questionJsonArray":Lorg/json/JSONArray;
    goto/16 :goto_4

    .line 124
    :cond_8
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 125
    .local v2, "questionRangeJsonArray":Lorg/json/JSONArray;
    move v5, v6

    .local v5, "countIndex":I
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v5, v7, :cond_a

    .line 127
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_rangeList:Ljava/util/ArrayList;

    if-nez v7, :cond_9

    .line 128
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_rangeList:Ljava/util/ArrayList;

    .line 130
    :cond_9
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 131
    .local v7, "questionRangeElement":Ljava/lang/String;
    iget-object v10, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_rangeList:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Question Range List["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    nop

    .end local v7    # "questionRangeElement":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 136
    .end local v5    # "countIndex":I
    :cond_a
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 137
    .local v4, "questionChoiceJsonArray":Lorg/json/JSONArray;
    move v5, v6

    .restart local v5    # "countIndex":I
    :goto_3
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_c

    .line 139
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    if-nez v6, :cond_b

    .line 140
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    .line 142
    :cond_b
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 143
    .local v6, "questionChoice":Ljava/lang/String;
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_choiceList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    nop

    .end local v6    # "questionChoice":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 148
    .end local v5    # "countIndex":I
    :cond_c
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_isMultipleChoice:Z

    .line 151
    .end local v2    # "questionRangeJsonArray":Lorg/json/JSONArray;
    .end local v4    # "questionChoiceJsonArray":Lorg/json/JSONArray;
    nop

    .line 179
    :goto_4
    const-string v1, "imageURL"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_imageURL:Ljava/lang/String;

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Question ImageURL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_imageURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 186
    goto :goto_5

    .line 183
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 187
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_5
    return-void
.end method

.method public RangeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/QuestionInfo;->_rangeList:Ljava/util/ArrayList;

    return-object v0
.end method
