.class public abstract Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "VariableBodyRtmpPacket.java"


# instance fields
.field protected data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/kint/kintframeworkaosaar/AmfData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 30
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 31
    return-void
.end method


# virtual methods
.method public addData(D)V
    .locals 1
    .param p1, "number"    # D

    .line 42
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfNumber;

    invoke-direct {v0, p1, p2}, Lcom/kint/kintframeworkaosaar/AmfNumber;-><init>(D)V

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 43
    return-void
.end method

.method public addData(Lcom/kint/kintframeworkaosaar/AmfData;)V
    .locals 1
    .param p1, "dataItem"    # Lcom/kint/kintframeworkaosaar/AmfData;

    .line 50
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->data:Ljava/util/List;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->data:Ljava/util/List;

    .line 53
    :cond_0
    if-nez p1, :cond_1

    .line 54
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfNull;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/AmfNull;-><init>()V

    move-object p1, v0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public addData(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .line 38
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfString;

    invoke-direct {v0, p1}, Lcom/kint/kintframeworkaosaar/AmfString;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 39
    return-void
.end method

.method public addData(Z)V
    .locals 1
    .param p1, "bool"    # Z

    .line 46
    new-instance v0, Lcom/kint/kintframeworkaosaar/AmfBoolean;

    invoke-direct {v0, p1}, Lcom/kint/kintframeworkaosaar/AmfBoolean;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 47
    return-void
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/kint/kintframeworkaosaar/AmfData;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->data:Ljava/util/List;

    return-object v0
.end method

.method protected readVariableData(Ljava/io/InputStream;I)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "bytesAlreadyRead"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 62
    :goto_0
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/AmfDecoder;->readFrom(Ljava/io/InputStream;)Lcom/kint/kintframeworkaosaar/AmfData;

    move-result-object v0

    .line 63
    .local v0, "dataItem":Lcom/kint/kintframeworkaosaar/AmfData;
    invoke-virtual {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->addData(Lcom/kint/kintframeworkaosaar/AmfData;)V

    .line 64
    invoke-interface {v0}, Lcom/kint/kintframeworkaosaar/AmfData;->getSize()I

    move-result v1

    add-int/2addr p2, v1

    .line 65
    .end local v0    # "dataItem":Lcom/kint/kintframeworkaosaar/AmfData;
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->header:Lcom/kint/kintframeworkaosaar/RtmpHeader;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getPacketLength()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 66
    return-void

    .line 65
    :cond_0
    goto :goto_0
.end method

.method protected writeVariableData(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;->data:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 70
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/kint/kintframeworkaosaar/AmfData;

    .line 71
    .local v1, "dataItem":Lcom/kint/kintframeworkaosaar/AmfData;
    invoke-interface {v1, p1}, Lcom/kint/kintframeworkaosaar/AmfData;->writeTo(Ljava/io/OutputStream;)V

    .line 72
    .end local v1    # "dataItem":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0

    :cond_0
    goto :goto_1

    .line 75
    :cond_1
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/AmfNull;->writeNullTo(Ljava/io/OutputStream;)V

    .line 77
    :goto_1
    return-void
.end method
