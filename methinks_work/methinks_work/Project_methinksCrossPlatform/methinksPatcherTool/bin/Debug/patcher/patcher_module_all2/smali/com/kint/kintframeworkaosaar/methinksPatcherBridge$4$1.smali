.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;
.super Ljava/lang/Thread;
.source "methinksPatcherBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    .line 1813
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 1816
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/MessagePack;

    .line 1817
    .local v0, "messagePack":Lcom/kint/kintframeworkaosaar/MessagePack;
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/MessagePack;->GetValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x7d9de7e2

    const/4 v5, 0x1

    if-eq v3, v4, :cond_2

    const v4, 0x5c163e31

    if-eq v3, v4, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const-string v3, "prepare_login"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_2
    const-string v1, "prepare_question"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    const/4 v2, 0x2

    if-eqz v1, :cond_4

    if-eq v1, v5, :cond_3

    .line 1878
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Lcom/kint/kintframeworkaosaar/MessagePack;)V

    goto :goto_3

    .line 1851
    :cond_3
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v3, "http://221.165.42.119/methinksPatcher/question_range.png"

    invoke-direct {v1, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1854
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    .line 1855
    .local v3, "httpURLConnection":Ljava/net/HttpURLConnection;
    invoke-virtual {v3, v5}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 1856
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 1857
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 1858
    .local v4, "inputStream":Ljava/io/InputStream;
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1860
    .local v5, "loginBitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v6, v6, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v6, v6, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 1861
    .local v6, "msg":Landroid/os/Message;
    const/16 v7, 0xb

    iput v7, v6, Landroid/os/Message;->what:I

    .line 1862
    iget-object v7, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v7, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v7, v7, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1864
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1870
    .end local v1    # "url":Ljava/net/URL;
    .end local v3    # "httpURLConnection":Ljava/net/HttpURLConnection;
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "loginBitmap":Landroid/graphics/Bitmap;
    .end local v6    # "msg":Landroid/os/Message;
    goto :goto_2

    .line 1866
    :catch_0
    move-exception v1

    .line 1868
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "methinksPatcherBridge"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1869
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1872
    .end local v1    # "e":Ljava/io/IOException;
    :goto_2
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I

    .line 1874
    goto :goto_3

    .line 1833
    :cond_4
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1834
    .local v1, "msg":Landroid/os/Message;
    const/4 v3, 0x6

    iput v3, v1, Landroid/os/Message;->what:I

    .line 1835
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1843
    .end local v1    # "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->this$1:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I

    .line 1845
    nop

    .line 1882
    :goto_3
    return-void
.end method
