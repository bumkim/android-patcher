.class public Lcom/kint/kintframeworkaosaar/AmfObject;
.super Ljava/lang/Object;
.source "AmfObject.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/AmfData;


# static fields
.field protected static final OBJECT_END_MARKER:[B


# instance fields
.field properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/kint/kintframeworkaosaar/AmfData;",
            ">;"
        }
    .end annotation
.end field

.field protected size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/kint/kintframeworkaosaar/AmfObject;->OBJECT_END_MARKER:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x9t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 23
    return-void
.end method


# virtual methods
.method public getProperty(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/AmfData;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/AmfData;

    return-object v0
.end method

.method public getSize()I
    .locals 5

    .line 99
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 100
    const/4 v0, 0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 101
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 102
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/kint/kintframeworkaosaar/AmfData;>;"
    iget v3, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->sizeOf(Ljava/lang/String;Z)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 103
    iget v3, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/kint/kintframeworkaosaar/AmfData;

    invoke-interface {v4}, Lcom/kint/kintframeworkaosaar/AmfData;->getSize()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 104
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/kint/kintframeworkaosaar/AmfData;>;"
    goto :goto_0

    .line 105
    :cond_0
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 107
    :cond_1
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    return v0
.end method

.method public readFrom(Ljava/io/InputStream;)V
    .locals 7
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 69
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 73
    .local v1, "markInputStream":Ljava/io/InputStream;
    :goto_0
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->mark(I)V

    .line 74
    new-array v3, v2, [B

    .line 75
    .local v3, "endMarker":[B
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    .line 77
    const/4 v4, 0x0

    aget-byte v5, v3, v4

    sget-object v6, Lcom/kint/kintframeworkaosaar/AmfObject;->OBJECT_END_MARKER:[B

    aget-byte v4, v6, v4

    if-ne v5, v4, :cond_1

    aget-byte v4, v3, v0

    aget-byte v5, v6, v0

    if-ne v4, v5, :cond_1

    const/4 v4, 0x2

    aget-byte v5, v3, v4

    aget-byte v4, v6, v4

    if-ne v5, v4, :cond_1

    .line 81
    iget v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 82
    return-void

    .line 85
    :cond_1
    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 87
    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->readStringFrom(Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "key":Ljava/lang/String;
    iget v4, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    invoke-static {v2, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->sizeOf(Ljava/lang/String;Z)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 90
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/AmfDecoder;->readFrom(Ljava/io/InputStream;)Lcom/kint/kintframeworkaosaar/AmfData;

    move-result-object v4

    .line 91
    .local v4, "value":Lcom/kint/kintframeworkaosaar/AmfData;
    iget v5, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    invoke-interface {v4}, Lcom/kint/kintframeworkaosaar/AmfData;->getSize()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->size:I

    .line 92
    iget-object v5, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "endMarker":[B
    .end local v4    # "value":Lcom/kint/kintframeworkaosaar/AmfData;
    goto :goto_0
.end method

.method public setProperty(Ljava/lang/String;D)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # D

    .line 46
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfNumber;

    invoke-direct {v1, p2, p3}, Lcom/kint/kintframeworkaosaar/AmfNumber;-><init>(D)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void
.end method

.method public setProperty(Ljava/lang/String;I)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 42
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfNumber;

    int-to-double v2, p2

    invoke-direct {v1, v2, v3}, Lcom/kint/kintframeworkaosaar/AmfNumber;-><init>(D)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public setProperty(Ljava/lang/String;Lcom/kint/kintframeworkaosaar/AmfData;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/kint/kintframeworkaosaar/AmfData;

    .line 30
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfString;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lcom/kint/kintframeworkaosaar/AmfString;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method public setProperty(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .line 34
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    new-instance v1, Lcom/kint/kintframeworkaosaar/AmfBoolean;

    invoke-direct {v1, p2}, Lcom/kint/kintframeworkaosaar/AmfBoolean;-><init>(Z)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 52
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfType;->OBJECT:Lcom/kint/kintframeworkaosaar/AmfType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/AmfType;->getValue()B

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 55
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/AmfObject;->properties:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 57
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/kint/kintframeworkaosaar/AmfData;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Lcom/kint/kintframeworkaosaar/AmfString;->writeStringTo(Ljava/io/OutputStream;Ljava/lang/String;Z)V

    .line 58
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/kint/kintframeworkaosaar/AmfData;

    invoke-interface {v2, p1}, Lcom/kint/kintframeworkaosaar/AmfData;->writeTo(Ljava/io/OutputStream;)V

    .line 59
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/kint/kintframeworkaosaar/AmfData;>;"
    goto :goto_0

    .line 62
    :cond_0
    sget-object v0, Lcom/kint/kintframeworkaosaar/AmfObject;->OBJECT_END_MARKER:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 63
    return-void
.end method
