.class public Lcom/kint/kintframeworkaosaar/Command;
.super Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;
.source "Command.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Command"


# instance fields
.field private commandName:Ljava/lang/String;

.field private transactionId:I


# direct methods
.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 29
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 4
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "transactionId"    # I

    .line 41
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    sget-object v2, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3, v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 43
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    .line 44
    iput p2, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 4
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "transactionId"    # I
    .param p3, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 33
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-virtual {p3, v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->canReusePrevHeaderTx(Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_1_RELATIVE_LARGE:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    :goto_0
    const/4 v2, 0x3

    sget-object v3, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->COMMAND_AMF0:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-direct {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/VariableBodyRtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 36
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    .line 37
    iput p2, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    .line 38
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionId()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    return v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 66
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->readStringFrom(Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/AmfNumber;->readNumberFrom(Ljava/io/InputStream;)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    .line 68
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/kint/kintframeworkaosaar/AmfString;->sizeOf(Ljava/lang/String;Z)I

    move-result v0

    add-int/lit8 v0, v0, 0x9

    .line 69
    .local v0, "bytesRead":I
    invoke-virtual {p0, p1, v0}, Lcom/kint/kintframeworkaosaar/Command;->readVariableData(Ljava/io/InputStream;I)V

    .line 70
    return-void
.end method

.method public setCommandName(Ljava/lang/String;)V
    .locals 0
    .param p1, "commandName"    # Ljava/lang/String;

    .line 52
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setTransactionId(I)V
    .locals 0
    .param p1, "transactionId"    # I

    .line 60
    iput p1, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    .line 61
    return-void
.end method

.method protected size()I
    .locals 1

    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RTMP Command (command: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", transaction ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/Command;->commandName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/AmfString;->writeStringTo(Ljava/io/OutputStream;Ljava/lang/String;Z)V

    .line 75
    iget v0, p0, Lcom/kint/kintframeworkaosaar/Command;->transactionId:I

    int-to-double v0, v0

    invoke-static {p1, v0, v1}, Lcom/kint/kintframeworkaosaar/AmfNumber;->writeNumberTo(Ljava/io/OutputStream;D)V

    .line 77
    invoke-virtual {p0, p1}, Lcom/kint/kintframeworkaosaar/Command;->writeVariableData(Ljava/io/OutputStream;)V

    .line 78
    return-void
.end method
