.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$3;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_DetectScreenShotService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 1226
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$3;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 3
    .param p1, "pathname"    # Ljava/io/File;

    .line 1229
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1231
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "screenshot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
