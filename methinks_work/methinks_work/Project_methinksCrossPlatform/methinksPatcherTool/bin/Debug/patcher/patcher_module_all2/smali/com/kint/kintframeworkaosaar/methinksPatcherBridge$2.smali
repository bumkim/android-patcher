.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$CallbackMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 462
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public SendMessage(I)V
    .locals 7
    .param p1, "messageType"    # I

    .line 465
    if-eqz p1, :cond_0

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 655
    :pswitch_0
    goto :goto_1

    .line 618
    :pswitch_1
    goto :goto_1

    .line 563
    :pswitch_2
    goto :goto_1

    .line 469
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->GetMainActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 471
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const/16 v1, 0xc

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v1

    .line 472
    .local v1, "methinksLogin":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 474
    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget v2, v2, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->LoginTryCount:I

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 477
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 482
    :cond_1
    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 485
    :goto_0
    new-instance v2, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->GetMainActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 486
    .local v2, "editText":Landroid/widget/EditText;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 488
    const/16 v4, 0xd

    invoke-static {v4}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v4

    .line 489
    .local v4, "confirm":Ljava/lang/String;
    new-instance v5, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$1;

    invoke-direct {v5, p0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;Landroid/widget/EditText;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 500
    const/16 v5, 0xe

    invoke-static {v5}, Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;->GetText(I)Ljava/lang/String;

    move-result-object v5

    .line 501
    .local v5, "cancel":Ljava/lang/String;
    new-instance v6, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$2;

    invoke-direct {v6, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$2;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 511
    new-instance v6, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$3;

    invoke-direct {v6, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2$3;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$2;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 518
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 519
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 521
    .end local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v1    # "methinksLogin":Ljava/lang/String;
    .end local v2    # "editText":Landroid/widget/EditText;
    .end local v4    # "confirm":Ljava/lang/String;
    .end local v5    # "cancel":Ljava/lang/String;
    nop

    .line 660
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
