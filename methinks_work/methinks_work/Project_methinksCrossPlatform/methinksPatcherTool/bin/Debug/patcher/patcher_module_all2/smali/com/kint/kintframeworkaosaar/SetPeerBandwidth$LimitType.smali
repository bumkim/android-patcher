.class public final enum Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
.super Ljava/lang/Enum;
.source "SetPeerBandwidth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LimitType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

.field public static final enum DYNAMIC:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

.field public static final enum HARD:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

.field public static final enum SOFT:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

.field private static final quickLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 29
    new-instance v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    const/4 v1, 0x0

    const-string v2, "HARD"

    invoke-direct {v0, v2, v1, v1}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->HARD:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 33
    new-instance v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    const/4 v2, 0x1

    const-string v3, "SOFT"

    invoke-direct {v0, v3, v2, v2}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->SOFT:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 36
    new-instance v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    const/4 v3, 0x2

    const-string v4, "DYNAMIC"

    invoke-direct {v0, v4, v3, v3}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->DYNAMIC:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    sget-object v4, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->HARD:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    aput-object v4, v0, v1

    sget-object v4, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->SOFT:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    aput-object v4, v0, v2

    sget-object v2, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->DYNAMIC:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    aput-object v2, v0, v3

    sput-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->$VALUES:[Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->quickLookupMap:Ljava/util/Map;

    .line 41
    invoke-static {}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->values()[Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 42
    .local v3, "type":Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    sget-object v4, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->quickLookupMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    .end local v3    # "type":Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->intValue:I

    .line 48
    return-void
.end method

.method public static valueOf(I)Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    .locals 2
    .param p0, "intValue"    # I

    .line 55
    sget-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 24
    const-class v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    .locals 1

    .line 24
    sget-object v0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->$VALUES:[Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->intValue:I

    return v0
.end method
