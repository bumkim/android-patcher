.class public Lcom/kint/kintframeworkaosaar/methinksMainActivity;
.super Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;
.source "methinksMainActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 64
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->OnActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 65
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 22
    invoke-super {p0, p1}, Lcom/nexxpace/changkiyoon/methinksandroidpatchersample/MainActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const/4 v0, 0x1

    const-string v1, "methinks Module Start"

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 28
    const-string v1, "{module:{}}"

    .line 29
    .local v1, "presetModule":Ljava/lang/String;
    const-string v2, "{project:{}}"

    .line 31
    .local v2, "presetProject":Ljava/lang/String;
    const-string v3, "integrate_mode"

    .line 32
    .local v3, "patchModeString":Ljava/lang/String;
    const-string v4, "patch_mode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33
    sput-boolean v0, Lcom/kint/kintframeworkaosaar/GlobalSet;->PatchMode:Z

    .line 35
    :cond_0
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->FirstInitialize(Landroid/app/Activity;)V

    .line 40
    sget-boolean v4, Lcom/kint/kintframeworkaosaar/GlobalSet;->PatchMode:Z

    if-ne v0, v4, :cond_1

    .line 42
    invoke-static {}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Instance()Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Initialize(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_1
    return-void
.end method
