.class public final enum Lcom/kint/kintframeworkaosaar/UserControl$Type;
.super Ljava/lang/Enum;
.source "UserControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kint/kintframeworkaosaar/UserControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kint/kintframeworkaosaar/UserControl$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum BUFFER_EMPTY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum BUFFER_READY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum PING_REQUEST:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum PONG_REPLY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum STREAM_BEGIN:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum STREAM_DRY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum STREAM_EOF:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field public static final enum STREAM_IS_RECORDED:Lcom/kint/kintframeworkaosaar/UserControl$Type;

.field private static final quickLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/kint/kintframeworkaosaar/UserControl$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 35
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v1, 0x0

    const-string v2, "STREAM_BEGIN"

    invoke-direct {v0, v2, v1, v1}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_BEGIN:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 45
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v2, 0x1

    const-string v3, "STREAM_EOF"

    invoke-direct {v0, v3, v2, v2}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_EOF:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 55
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v3, 0x2

    const-string v4, "STREAM_DRY"

    invoke-direct {v0, v4, v3, v3}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_DRY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 65
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v4, 0x3

    const-string v5, "SET_BUFFER_LENGTH"

    invoke-direct {v0, v5, v4, v4}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 73
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v5, 0x4

    const-string v6, "STREAM_IS_RECORDED"

    invoke-direct {v0, v6, v5, v5}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_IS_RECORDED:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 82
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v6, 0x5

    const/4 v7, 0x6

    const-string v8, "PING_REQUEST"

    invoke-direct {v0, v8, v6, v7}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->PING_REQUEST:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 89
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/4 v8, 0x7

    const-string v9, "PONG_REPLY"

    invoke-direct {v0, v9, v7, v8}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->PONG_REPLY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 103
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const-string v9, "BUFFER_EMPTY"

    const/16 v10, 0x1f

    invoke-direct {v0, v9, v8, v10}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->BUFFER_EMPTY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 120
    new-instance v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    const/16 v9, 0x8

    const-string v10, "BUFFER_READY"

    const/16 v11, 0x20

    invoke-direct {v0, v10, v9, v11}, Lcom/kint/kintframeworkaosaar/UserControl$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->BUFFER_READY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 23
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/kint/kintframeworkaosaar/UserControl$Type;

    sget-object v10, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_BEGIN:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v10, v0, v1

    sget-object v10, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_EOF:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v10, v0, v2

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_DRY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v3

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->SET_BUFFER_LENGTH:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v4

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->STREAM_IS_RECORDED:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v5

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->PING_REQUEST:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v6

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->PONG_REPLY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v7

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->BUFFER_EMPTY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v8

    sget-object v2, Lcom/kint/kintframeworkaosaar/UserControl$Type;->BUFFER_READY:Lcom/kint/kintframeworkaosaar/UserControl$Type;

    aput-object v2, v0, v9

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->$VALUES:[Lcom/kint/kintframeworkaosaar/UserControl$Type;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->quickLookupMap:Ljava/util/Map;

    .line 126
    invoke-static {}, Lcom/kint/kintframeworkaosaar/UserControl$Type;->values()[Lcom/kint/kintframeworkaosaar/UserControl$Type;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 127
    .local v3, "type":Lcom/kint/kintframeworkaosaar/UserControl$Type;
    sget-object v4, Lcom/kint/kintframeworkaosaar/UserControl$Type;->quickLookupMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/UserControl$Type;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    .end local v3    # "type":Lcom/kint/kintframeworkaosaar/UserControl$Type;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput p3, p0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->intValue:I

    .line 133
    return-void
.end method

.method public static valueOf(I)Lcom/kint/kintframeworkaosaar/UserControl$Type;
    .locals 2
    .param p0, "intValue"    # I

    .line 140
    sget-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->quickLookupMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kint/kintframeworkaosaar/UserControl$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 23
    const-class v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;

    return-object v0
.end method

.method public static values()[Lcom/kint/kintframeworkaosaar/UserControl$Type;
    .locals 1

    .line 23
    sget-object v0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->$VALUES:[Lcom/kint/kintframeworkaosaar/UserControl$Type;

    invoke-virtual {v0}, [Lcom/kint/kintframeworkaosaar/UserControl$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kint/kintframeworkaosaar/UserControl$Type;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .line 136
    iget v0, p0, Lcom/kint/kintframeworkaosaar/UserControl$Type;->intValue:I

    return v0
.end method
