.class public Lcom/kint/kintframeworkaosaar/RtmpHeader;
.super Ljava/lang/Object;
.source "RtmpHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;,
        Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RtmpHeader"


# instance fields
.field private absoluteTimestamp:I

.field private chunkStreamId:I

.field private chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

.field private extendedTimestamp:I

.field private messageStreamId:I

.field private messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

.field private packetLength:I

.field private timestampDelta:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 202
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V
    .locals 1
    .param p1, "chunkType"    # Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .param p2, "chunkStreamId"    # I
    .param p3, "messageType"    # Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    const/4 v0, -0x1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 205
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 206
    iput p2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    .line 207
    iput-object p3, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 208
    return-void
.end method

.method private parseBasicHeader(B)V
    .locals 1
    .param p1, "basicHeaderByte"    # B

    .line 351
    and-int/lit16 v0, p1, 0xff

    ushr-int/lit8 v0, v0, 0x6

    int-to-byte v0, v0

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->valueOf(B)Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 353
    and-int/lit8 v0, p1, 0x3f

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    .line 354
    return-void
.end method

.method public static readHeader(Ljava/io/InputStream;Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)Lcom/kint/kintframeworkaosaar/RtmpHeader;
    .locals 1
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "rtmpSessionInfo"    # Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 212
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    invoke-direct {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>()V

    .line 213
    .local v0, "rtmpHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    invoke-direct {v0, p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->readHeaderImpl(Ljava/io/InputStream;Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)V

    .line 214
    return-object v0
.end method

.method private readHeaderImpl(Ljava/io/InputStream;Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;)V
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "rtmpSessionInfo"    # Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 220
    .local v0, "basicHeaderByte":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    .line 224
    int-to-byte v1, v0

    invoke-direct {p0, v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->parseBasicHeader(B)V

    .line 226
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    invoke-virtual {v2}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x4

    const v4, 0xffffff

    const/4 v5, 0x0

    if-eq v1, v2, :cond_c

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    if-ne v1, v3, :cond_3

    .line 280
    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    invoke-virtual {p2, v1}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->prevHeaderRx()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    .line 282
    .local v1, "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    if-lt v2, v4, :cond_0

    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v5

    :cond_0
    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 283
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget v4, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    :goto_0
    iput v4, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 284
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    .line 285
    iget-object v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 286
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 287
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    add-int/2addr v2, v3

    :goto_1
    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 289
    goto/16 :goto_6

    .line 292
    .end local v1    # "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    :cond_3
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid chunk type; basic header byte was: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-byte v3, v0

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/Util;->toHexString(B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_4
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt24(Ljava/io/InputStream;)I

    move-result v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 270
    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    if-lt v1, v4, :cond_5

    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v5

    :cond_5
    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 271
    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    invoke-virtual {p2, v1}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->prevHeaderRx()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    .line 272
    .restart local v1    # "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    .line 273
    iget-object v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    iput-object v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 274
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 275
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_6

    goto :goto_2

    :cond_6
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    add-int/2addr v2, v3

    :goto_2
    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 277
    goto/16 :goto_6

    .line 248
    .end local v1    # "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    :cond_7
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt24(Ljava/io/InputStream;)I

    move-result v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 250
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt24(Ljava/io/InputStream;)I

    move-result v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    .line 252
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->valueOf(B)Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 254
    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    if-lt v1, v4, :cond_8

    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v1

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    :goto_3
    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 255
    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    invoke-virtual {p2, v1}, Lcom/kint/kintframeworkaosaar/RtmpSessionInfo;->getChunkStreamInfo(I)Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->prevHeaderRx()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v1

    .line 256
    .restart local v1    # "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    if-eqz v1, :cond_a

    .line 257
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 258
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_9

    goto :goto_4

    :cond_9
    iget v2, v1, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    iget v3, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    add-int/2addr v2, v3

    :goto_4
    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    goto :goto_6

    .line 261
    :cond_a
    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 262
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_b

    goto :goto_5

    :cond_b
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    :goto_5
    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 264
    goto :goto_6

    .line 229
    .end local v1    # "prevHeader":Lcom/kint/kintframeworkaosaar/RtmpHeader;
    :cond_c
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt24(Ljava/io/InputStream;)I

    move-result v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 230
    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 232
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt24(Ljava/io/InputStream;)I

    move-result v1

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    .line 234
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->valueOf(B)Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    move-result-object v1

    iput-object v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 236
    new-array v1, v3, [B

    .line 237
    .local v1, "messageStreamIdBytes":[B
    invoke-static {p1, v1}, Lcom/kint/kintframeworkaosaar/Util;->readBytesUntilFull(Ljava/io/InputStream;[B)V

    .line 238
    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/Util;->toUnsignedInt32LittleEndian([B)I

    move-result v2

    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 240
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v2, v4, :cond_d

    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v5

    :cond_d
    iput v5, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 241
    iget v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-eqz v2, :cond_e

    .line 242
    iput v2, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 295
    .end local v1    # "messageStreamIdBytes":[B
    :cond_e
    :goto_6
    return-void

    .line 221
    :cond_f
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "Unexpected EOF while reading RTMP packet basic header"

    invoke-direct {v1, v2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getAbsoluteTimestamp()I
    .locals 1

    .line 378
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    return v0
.end method

.method public getChunkStreamId()I
    .locals 1

    .line 358
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    return v0
.end method

.method public getChunkType()Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    return-object v0
.end method

.method public getMessageStreamId()I
    .locals 1

    .line 370
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    return v0
.end method

.method public getMessageType()Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    return-object v0
.end method

.method public getPacketLength()I
    .locals 1

    .line 366
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    return v0
.end method

.method public getTimestampDelta()I
    .locals 1

    .line 386
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    return v0
.end method

.method public setAbsoluteTimestamp(I)V
    .locals 0
    .param p1, "absoluteTimestamp"    # I

    .line 382
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 383
    return-void
.end method

.method public setChunkStreamId(I)V
    .locals 0
    .param p1, "channelId"    # I

    .line 395
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    .line 396
    return-void
.end method

.method public setChunkType(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;)V
    .locals 0
    .param p1, "chunkType"    # Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 399
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkType:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    .line 400
    return-void
.end method

.method public setMessageStreamId(I)V
    .locals 0
    .param p1, "messageStreamId"    # I

    .line 403
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    .line 404
    return-void
.end method

.method public setMessageType(Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V
    .locals 0
    .param p1, "messageType"    # Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 407
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    .line 408
    return-void
.end method

.method public setPacketLength(I)V
    .locals 0
    .param p1, "packetLength"    # I

    .line 411
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    .line 412
    return-void
.end method

.method public setTimestampDelta(I)V
    .locals 0
    .param p1, "timestampDelta"    # I

    .line 390
    iput p1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 391
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "chunkType"    # Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;
    .param p3, "chunkStreamInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    invoke-virtual {p2}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->getValue()B

    move-result v0

    shl-int/lit8 v0, v0, 0x6

    int-to-byte v0, v0

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->chunkStreamId:I

    or-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 301
    sget-object v0, Lcom/kint/kintframeworkaosaar/RtmpHeader$1;->$SwitchMap$com$kint$kintframeworkaosaar$RtmpHeader$ChunkType:[I

    invoke-virtual {p2}, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const v2, 0xffffff

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 340
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    if-lez v0, :cond_7

    .line 341
    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    goto/16 :goto_0

    .line 346
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid chunk type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_1
    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markDeltaTimestampTx()J

    move-result-wide v0

    long-to-int v1, v0

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 330
    nop

    .line 331
    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->getPrevHeaderTx()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getAbsoluteTimestamp()I

    move-result v0

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 332
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_2

    const v1, 0xffffff

    :cond_2
    invoke-static {p1, v1}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt24(Ljava/io/OutputStream;I)V

    .line 333
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_7

    .line 334
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 335
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    goto :goto_0

    .line 316
    :cond_3
    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markDeltaTimestampTx()J

    move-result-wide v0

    long-to-int v1, v0

    iput v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    .line 317
    nop

    .line 318
    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->getPrevHeaderTx()Lcom/kint/kintframeworkaosaar/RtmpHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader;->getAbsoluteTimestamp()I

    move-result v0

    iget v1, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->timestampDelta:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    .line 319
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_4

    const v1, 0xffffff

    :cond_4
    invoke-static {p1, v1}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt24(Ljava/io/OutputStream;I)V

    .line 320
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt24(Ljava/io/OutputStream;I)V

    .line 321
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->getValue()B

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 322
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_7

    .line 323
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 324
    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    goto :goto_0

    .line 303
    :cond_5
    invoke-virtual {p3}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->markDeltaTimestampTx()J

    .line 304
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_6

    const v0, 0xffffff

    :cond_6
    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt24(Ljava/io/OutputStream;I)V

    .line 306
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->packetLength:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt24(Ljava/io/OutputStream;I)V

    .line 307
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageType:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->getValue()B

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 308
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->messageStreamId:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32LittleEndian(Ljava/io/OutputStream;I)V

    .line 309
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->absoluteTimestamp:I

    if-lt v0, v2, :cond_7

    .line 310
    iput v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    .line 311
    iget v0, p0, Lcom/kint/kintframeworkaosaar/RtmpHeader;->extendedTimestamp:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 348
    :cond_7
    :goto_0
    return-void
.end method
