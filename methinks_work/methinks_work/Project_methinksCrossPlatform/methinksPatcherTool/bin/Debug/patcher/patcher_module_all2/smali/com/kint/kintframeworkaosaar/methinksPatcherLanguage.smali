.class public Lcom/kint/kintframeworkaosaar/methinksPatcherLanguage;
.super Ljava/lang/Object;
.source "methinksPatcherLanguage.java"


# static fields
.field public static final patcher_error_msg_can_not_connect_server:I = 0x2

.field public static final patcher_error_msg_invalid_user_code:I = 0x3

.field public static final patcher_error_msg_test_period_over:I = 0x4

.field public static final patcher_level_average:I = 0x11

.field public static final patcher_level_easy:I = 0x10

.field public static final patcher_level_hard:I = 0x12

.field public static final patcher_level_very_easy:I = 0xf

.field public static final patcher_level_very_hard:I = 0x13

.field public static final patcher_methinks_login:I = 0xc

.field public static final patcher_msg_enter_user_code:I = 0x0

.field public static final patcher_msg_methinks_user_code:I = 0x1

.field public static final patcher_select_cancel:I = 0xe

.field public static final patcher_select_confirm:I = 0xd


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetText(I)Ljava/lang/String;
    .locals 7
    .param p0, "id"    # I

    .line 31
    const-string v0, ""

    .line 32
    .local v0, "text":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "language":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PatcherLanguage - language : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "methinks"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/16 v3, 0xca9

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-eq v2, v3, :cond_3

    const/16 v3, 0xcae

    if-eq v2, v3, :cond_2

    const/16 v3, 0xd64

    if-eq v2, v3, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const-string v2, "ko"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    const-string v2, "es"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    goto :goto_1

    :cond_3
    const-string v2, "en"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    const/4 v3, 0x4

    const/4 v6, 0x1

    if-eq v2, v5, :cond_f

    if-eq v2, v4, :cond_9

    .line 39
    if-eqz p0, :cond_8

    if-eq p0, v6, :cond_7

    if-eq p0, v5, :cond_6

    if-eq p0, v4, :cond_5

    if-eq p0, v3, :cond_4

    packed-switch p0, :pswitch_data_0

    goto :goto_2

    .line 115
    :pswitch_0
    const-string v0, "Very hard"

    goto :goto_2

    .line 109
    :pswitch_1
    const-string v0, "Hard"

    .line 111
    goto :goto_2

    .line 103
    :pswitch_2
    const-string v0, "Average"

    .line 105
    goto :goto_2

    .line 97
    :pswitch_3
    const-string v0, "Easy"

    .line 99
    goto :goto_2

    .line 91
    :pswitch_4
    const-string v0, "Very easy"

    .line 93
    goto :goto_2

    .line 85
    :pswitch_5
    const-string v0, "Cancel"

    .line 87
    goto :goto_2

    .line 79
    :pswitch_6
    const-string v0, "OK"

    .line 81
    goto :goto_2

    .line 73
    :pswitch_7
    const-string v0, "methinks login"

    .line 75
    goto :goto_2

    .line 67
    :cond_4
    const-string v0, "Testing period is over. Thank you for your participation. Please delete this app from your device."

    .line 69
    goto :goto_2

    .line 61
    :cond_5
    const-string v0, "Invalid user code. Enter your user code again."

    .line 63
    goto :goto_2

    .line 55
    :cond_6
    const-string v0, "Can\'t connect to methinks server. Please try again or contact methinks."

    .line 57
    goto :goto_2

    .line 49
    :cond_7
    const-string v0, "Enter methinks user code."

    .line 51
    goto :goto_2

    .line 43
    :cond_8
    const-string v0, "Enter user code from methinks app."

    .line 45
    nop

    .line 121
    :goto_2
    goto/16 :goto_4

    .line 210
    :cond_9
    if-eqz p0, :cond_e

    if-eq p0, v6, :cond_d

    if-eq p0, v5, :cond_c

    if-eq p0, v4, :cond_b

    if-eq p0, v3, :cond_a

    packed-switch p0, :pswitch_data_1

    goto/16 :goto_4

    .line 286
    :pswitch_8
    const-string v0, "Es muy dificil"

    goto/16 :goto_4

    .line 280
    :pswitch_9
    const-string v0, "Es dificil"

    .line 282
    goto/16 :goto_4

    .line 274
    :pswitch_a
    const-string v0, "Promedio"

    .line 276
    goto/16 :goto_4

    .line 268
    :pswitch_b
    const-string v0, "Es facil"

    .line 270
    goto/16 :goto_4

    .line 262
    :pswitch_c
    const-string v0, "Es muy facil"

    .line 264
    goto/16 :goto_4

    .line 256
    :pswitch_d
    const-string v0, "Cancelar"

    .line 258
    goto :goto_4

    .line 250
    :pswitch_e
    const-string v0, "OK"

    .line 252
    goto :goto_4

    .line 244
    :pswitch_f
    const-string v0, "methinks iniciar sesi\u00f3n"

    .line 246
    goto :goto_4

    .line 238
    :cond_a
    const-string v0, "Per\u00edodo de prueba finalizado. Gracias por su particiapci\u00f3n. Por favor, borre la aplicaci\u00f3n de su dispositivo."

    .line 240
    goto :goto_4

    .line 232
    :cond_b
    const-string v0, "C\u00f3digo de usuario inv\u00e1lido. Introduzca su c\u00f3digo de usario de nuevo."

    .line 234
    goto :goto_4

    .line 226
    :cond_c
    const-string v0, "No se puede conectar al servidor de methinks. Int\u00e9ntelo de nuevo o contacte a methinks."

    .line 228
    goto :goto_4

    .line 220
    :cond_d
    const-string v0, "Introduzca el c\u00f3digo de usario de methinks"

    .line 222
    goto :goto_4

    .line 214
    :cond_e
    const-string v0, "Introduzca el c\u00f3digo de usuario de la aplicaci\u00f3n de methinks."

    .line 216
    goto :goto_4

    .line 125
    :cond_f
    if-eqz p0, :cond_14

    if-eq p0, v6, :cond_13

    if-eq p0, v5, :cond_12

    if-eq p0, v4, :cond_11

    if-eq p0, v3, :cond_10

    packed-switch p0, :pswitch_data_2

    goto :goto_3

    .line 201
    :pswitch_10
    const-string v0, "\ub9e4\uc6b0 \uc5b4\ub824\uc6c0"

    goto :goto_3

    .line 195
    :pswitch_11
    const-string v0, "\uc5b4\ub824\uc6c0"

    .line 197
    goto :goto_3

    .line 189
    :pswitch_12
    const-string v0, "\ubcf4\ud1b5"

    .line 191
    goto :goto_3

    .line 183
    :pswitch_13
    const-string v0, "\uc26c\uc6c0"

    .line 185
    goto :goto_3

    .line 177
    :pswitch_14
    const-string v0, "\ub9e4\uc6b0 \uc26c\uc6c0"

    .line 179
    goto :goto_3

    .line 171
    :pswitch_15
    const-string v0, "\ucde8\uc18c"

    .line 173
    goto :goto_3

    .line 165
    :pswitch_16
    const-string v0, "\ud655\uc778"

    .line 167
    goto :goto_3

    .line 159
    :pswitch_17
    const-string v0, "\ubbf8\ub775\uc2a4 \ub85c\uadf8\uc778"

    .line 161
    goto :goto_3

    .line 153
    :cond_10
    const-string v0, "\ud14c\uc2a4\ud2b8\uac00 \ubaa8\ub450 \uc885\ub8cc \ub418\uc5c8\uc73c\ubbc0\ub85c \uc774 \uc571\uc744 \uc0ad\uc81c\ud574 \uc8fc\uc2ed\uc2dc\uc624. \ucc38\uc5ec\ud574 \uc8fc\uc154\uc11c \uac10\uc0ac\ud569\ub2c8\ub2e4."

    .line 155
    goto :goto_3

    .line 147
    :cond_11
    const-string v0, "\uc0ac\uc6a9\uc790 \ucf54\ub4dc\uac00 \uc720\ud6a8\ud558\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4. \ub2e4\uc2dc \uc785\ub825\ud558\uc138\uc694."

    .line 149
    goto :goto_3

    .line 141
    :cond_12
    const-string v0, "\ubbf8\ub775\uc2a4 \uc11c\ubc84\uc5d0 \uc5f0\uacb0\ud560 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4. \ub2e4\uc2dc \uc2dc\ub3c4\ud558\uac70\ub098 \ubbf8\ub775\uc2a4 \uad00\ub9ac\uc790\uc5d0\uac8c \ubb38\uc758\ud558\uc138\uc694."

    .line 143
    goto :goto_3

    .line 135
    :cond_13
    const-string v0, "\uc0ac\uc6a9\uc790 \ucf54\ub4dc\ub97c \uc785\ub825\ud558\uc138\uc694."

    .line 137
    goto :goto_3

    .line 129
    :cond_14
    const-string v0, "\ubbf8\ub775\uc2a4\uc571\uc5d0\uc11c \ubc1c\uae09\ud55c \uc0ac\uc6a9\uc790 \ucf54\ub4dc\ub97c \uc785\ub825\ud558\uc138\uc694."

    .line 131
    nop

    .line 206
    :goto_3
    nop

    .line 294
    :goto_4
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xc
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xc
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch
.end method
