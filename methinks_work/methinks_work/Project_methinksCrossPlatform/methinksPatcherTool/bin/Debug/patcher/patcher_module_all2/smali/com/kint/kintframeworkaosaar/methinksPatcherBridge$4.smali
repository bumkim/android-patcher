.class Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;
.super Ljava/lang/Object;
.source "methinksPatcherBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_StartNetworkMessageLoop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    .line 1773
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1804
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_1

    .line 1808
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1810
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I

    .line 1812
    new-instance v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;

    invoke-direct {v0, p0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;-><init>(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;)V

    .line 1883
    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4$1;->start()V

    .line 1885
    :cond_0
    const/4 v0, 0x2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 1887
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1888
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;I)I

    .line 1923
    :cond_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    const/16 v3, 0x7d0

    invoke-virtual {v0, v3}, Lcom/kint/kintframeworkaosaar/DelayTimer;->ElapsedTickCount(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1926
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_DetectScreenShotService()V

    .line 1930
    :cond_2
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/kint/kintframeworkaosaar/DelayTimer;->ElapsedTickCount(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1932
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v0

    const-string v3, "methinksPatcherBridge"

    if-ne v2, v0, :cond_4

    .line 1934
    const-string v0, "------####### IsForeground ######------"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1936
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v0

    if-ne v2, v0, :cond_5

    .line 1938
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1939
    .local v0, "message":Landroid/os/Message;
    const/16 v3, 0x9

    iput v3, v0, Landroid/os/Message;->what:I

    .line 1940
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v3, v3, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1942
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$802(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 1943
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 1944
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/DelayTimer;->Reset()V

    .line 1946
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v3

    if-lez v3, :cond_3

    .line 1948
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->IsScreenStreamAllowed()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1950
    sget-boolean v3, Lcom/kint/kintframeworkaosaar/GlobalSet;->RunBackground:Z

    if-nez v3, :cond_3

    .line 1952
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1300(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v3

    if-ne v2, v3, :cond_3

    .line 1954
    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 1959
    .end local v0    # "message":Landroid/os/Message;
    :cond_3
    goto :goto_0

    .line 1963
    :cond_4
    const-string v0, "------####### IsBackground ######------"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1965
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1967
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v0, v0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1968
    .restart local v0    # "message":Landroid/os/Message;
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1969
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    iget-object v1, v1, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->_receivedMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1970
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$802(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 1972
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v1

    if-lez v1, :cond_5

    .line 1974
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1200(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$LoginInfo;->IsScreenStreamAllowed()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1976
    sget-boolean v1, Lcom/kint/kintframeworkaosaar/GlobalSet;->RunBackground:Z

    if-nez v1, :cond_5

    .line 1978
    iget-object v1, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v1, v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1302(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Z)Z

    .line 1986
    .end local v0    # "message":Landroid/os/Message;
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v0

    if-ne v2, v0, :cond_7

    .line 1988
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->ElapsedTickCount(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1990
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->stopRecord()Z

    .line 1993
    :cond_6
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    const v1, 0x927c0

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->ElapsedTickCount(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1995
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Quit()V

    goto :goto_1

    .line 2000
    :cond_7
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$100(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Disconnect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2002
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1400(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjection;

    move-result-object v0

    if-nez v0, :cond_8

    .line 2004
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1700(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/media/projection/MediaProjectionManager;

    move-result-object v1

    iget-object v2, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v2}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1500(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)I

    move-result v2

    iget-object v3, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v3}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1600(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/projection/MediaProjectionManager;->getMediaProjection(ILandroid/content/Intent;)Landroid/media/projection/MediaProjection;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1402(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Landroid/media/projection/MediaProjection;)Landroid/media/projection/MediaProjection;

    .line 2005
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1800(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->connectServer(Ljava/lang/String;)V

    .line 2006
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    const-string v1, "ConnectingRTMPServer"

    invoke-static {v0, v1}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$102(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;Ljava/lang/String;)Ljava/lang/String;

    .line 2039
    :cond_8
    :goto_1
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$1900(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2041
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->access$2000(Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;)Lcom/kint/kintframeworkaosaar/DelayTimer;

    move-result-object v0

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/kint/kintframeworkaosaar/DelayTimer;->ElapsedTickCount(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2043
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->this$0:Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge;->Quit()V

    .line 2047
    :cond_9
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/methinksPatcherBridge$4;->val$handler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2048
    return-void
.end method
