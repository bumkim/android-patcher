.class public Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;
.super Lcom/kint/kintframeworkaosaar/RtmpPacket;
.source "SetPeerBandwidth.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    }
.end annotation


# instance fields
.field private acknowledgementWindowSize:I

.field private limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;


# direct methods
.method public constructor <init>(ILcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;)V
    .locals 4
    .param p1, "acknowledgementWindowSize"    # I
    .param p2, "limitType"    # Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    .param p3, "channelInfo"    # Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;

    .line 68
    new-instance v0, Lcom/kint/kintframeworkaosaar/RtmpHeader;

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->SET_PEER_BANDWIDTH:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-virtual {p3, v1}, Lcom/kint/kintframeworkaosaar/ChunkStreamInfo;->canReusePrevHeaderTx(Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_2_RELATIVE_TIMESTAMP_ONLY:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;->TYPE_0_FULL:Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;

    :goto_0
    const/4 v2, 0x2

    sget-object v3, Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;->WINDOW_ACKNOWLEDGEMENT_SIZE:Lcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;

    invoke-direct {v0, v1, v2, v3}, Lcom/kint/kintframeworkaosaar/RtmpHeader;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader$ChunkType;ILcom/kint/kintframeworkaosaar/RtmpHeader$MessageType;)V

    invoke-direct {p0, v0}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 72
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->acknowledgementWindowSize:I

    .line 73
    iput-object p2, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V
    .locals 0
    .param p1, "header"    # Lcom/kint/kintframeworkaosaar/RtmpHeader;

    .line 63
    invoke-direct {p0, p1}, Lcom/kint/kintframeworkaosaar/RtmpPacket;-><init>(Lcom/kint/kintframeworkaosaar/RtmpHeader;)V

    .line 64
    return-void
.end method


# virtual methods
.method protected array()[B
    .locals 1

    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAcknowledgementWindowSize()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->acknowledgementWindowSize:I

    return v0
.end method

.method public getLimitType()Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    return-object v0
.end method

.method public readBody(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-static {p1}, Lcom/kint/kintframeworkaosaar/Util;->readUnsignedInt32(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->acknowledgementWindowSize:I

    .line 95
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-static {v0}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->valueOf(I)Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    move-result-object v0

    iput-object v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 96
    return-void
.end method

.method public setAcknowledgementWindowSize(I)V
    .locals 0
    .param p1, "acknowledgementWindowSize"    # I

    .line 81
    iput p1, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->acknowledgementWindowSize:I

    .line 82
    return-void
.end method

.method public setLimitType(Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;)V
    .locals 0
    .param p1, "limitType"    # Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 89
    iput-object p1, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    .line 90
    return-void
.end method

.method protected size()I
    .locals 1

    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 116
    const-string v0, "RTMP Set Peer Bandwidth"

    return-object v0
.end method

.method protected writeBody(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    iget v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->acknowledgementWindowSize:I

    invoke-static {p1, v0}, Lcom/kint/kintframeworkaosaar/Util;->writeUnsignedInt32(Ljava/io/OutputStream;I)V

    .line 101
    iget-object v0, p0, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth;->limitType:Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;

    invoke-virtual {v0}, Lcom/kint/kintframeworkaosaar/SetPeerBandwidth$LimitType;->getIntValue()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 102
    return-void
.end method
