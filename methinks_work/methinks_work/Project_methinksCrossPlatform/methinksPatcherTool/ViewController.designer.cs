﻿// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace methinksPatcherTool
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSTableColumn CommandColumn { get; set; }

		[Outlet]
		AppKit.NSTableView CommandProcessTable { get; set; }

		[Outlet]
		AppKit.NSTableColumn ProcessColumn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CommandColumn != null) {
				CommandColumn.Dispose ();
				CommandColumn = null;
			}

			if (CommandProcessTable != null) {
				CommandProcessTable.Dispose ();
				CommandProcessTable = null;
			}

			if (ProcessColumn != null) {
				ProcessColumn.Dispose ();
				ProcessColumn = null;
			}
		}
	}
}
