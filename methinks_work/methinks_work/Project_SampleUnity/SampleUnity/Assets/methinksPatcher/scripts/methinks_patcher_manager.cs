﻿using System;
using UnityEngine;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class methinks_patcher_manager
{
	private static methinks_patcher_manager __instance;
    private static readonly object __objectLock = new object();

#if !UNITY_EDITOR
#if UNITY_ANDROID
	AndroidJavaClass _androidPatcherClass;
	AndroidJavaObject _androidPatcherObject;
#elif UNITY_IOS
	[DllImport("__Internal")]
	static extern void __SendMessage(string eventNameKey,string evnetValue);
#endif
#endif
    
    public static methinks_patcher_manager Instance
    {
        get
        {
            lock (__objectLock)
            {
                if (null == __instance)
                {
                    __instance = new methinks_patcher_manager();
                }
                return __instance;
            }
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:methinks_patcher_manager"/> class.
    /// </summary>
	public methinks_patcher_manager()
	{
#if !UNITY_EDITOR
#if UNITY_ANDROID
        try
        {
            if(null == _androidPatcherClass)
                _androidPatcherClass = new AndroidJavaClass ("com.kint.kintframeworkaosaar.methinksPatcherBridge");
            if(null != _androidPatcherClass)
            {
                if(null == _androidPatcherObject)   
                    _androidPatcherObject = _androidPatcherClass.CallStatic<AndroidJavaObject> ("Instance");
            }
        }
        catch(AndroidJavaException ex)
        {

        }
#elif UNITY_IOS

#endif
#else

#endif
    }

    /// <summary>
    /// Initialize the specified projectID and debugMode.
    /// </summary>
    /// <param name="projectID">Project identifier.</param>
    /// <param name="debugMode">If set to <c>true</c> debug mode.</param>
    public void Initialize(string projectID,bool debugMode)
    {
        string presetProject = "{  \"id\": \"ProjectID\",  \"build\": true,  \"debug_mode\": DebugMode}";
        presetProject = presetProject.Replace("ProjectID", projectID);
        if (true == debugMode)
        {
            presetProject = presetProject.Replace("DebugMode", "true");
        }
        else
        {
            presetProject = presetProject.Replace("DebugMode", "false");
        }

#if !UNITY_EDITOR
#if UNITY_ANDROID
        if (null != _androidPatcherObject)
            _androidPatcherObject.Call("Initialize", new object[] { "", presetProject });

#elif UNITY_IOS

#endif
#else

#endif
    }

    /// <summary>
    /// Uninitialize this instance.
    /// </summary>
    public void Uninitialize()
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        if(null != _androidPatcherObject)
            _androidPatcherObject.Call("Uninitialize");
#elif UNITY_IOS

#endif
#else
        Debug.Log("[methinks_patcher_manager : _Uninitialize] - Not Work UnityEditor");
#endif
    }

    /// <summary>
    /// Preset4s the unity.
    /// </summary>
    /// <param name="sendToGameObjectName">Send to game object name.</param>
    /// <param name="sendToMethodName">Send to method name.</param>
    public void Preset4Unity(string sendToGameObjectName = "manage_scene", string sendToMethodName = "ReceiveMessage")
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        if (null != _androidPatcherObject)
        {
            _androidPatcherObject.Call("Preset4Unity", new object[] { sendToGameObjectName, sendToMethodName });
        }
#elif UNITY_IOS

#endif
#else

#endif
    }

    /// <summary>
    /// Sends the message.
    /// </summary>
    /// <param name="eventNameKey">Event name key.</param>
    /// <param name="eventValue">Event value.</param>
    public void SendMessage(string eventNameKey,string eventValue)
	{
#if !UNITY_EDITOR
#if UNITY_ANDROID
		if(null != _androidPatcherObject)
			_androidPatcherObject.Call("SendMessage", new object[] {eventNameKey, eventValue} );
#elif UNITY_IOS
		__SendMessage(eventNameKey,eventValue);
#endif
#else
		Debug.Log ("[methinks_patcher_manager : SendMessage] EventNameKey : "+eventNameKey+" , EventValue : "+eventValue+" - Not Work UnityEditor");
#endif
	}

    /// <summary>
    /// Tooltip the specified message and echo.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="echo">If set to <c>true</c> echo.</param>
    public void Tooltip(string message, int status)
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        if(null != _androidPatcherObject)
            _androidPatcherObject.Call("Tooltip", new object[] {message, status} );
#elif UNITY_IOS

#endif
#else
        Debug.Log("[methinks_patcher_manager : Tooltip] - Not Work UnityEditor");
#endif
    }

    /// <summary>
    /// Ons the enable.
    /// </summary>
    public void OnEnable()
    {
        Application.logMessageReceived += _LogCallback;
    }

    /// <summary>
    /// Ons the disable.
    /// </summary>
    public void OnDisable()
    {
        Application.logMessageReceived -= _LogCallback;
    }

    /// <summary>
    /// Logs the callback.
    /// </summary>
    /// <param name="condition">Condition.</param>
    /// <param name="stackTrace">Stack trace.</param>
    /// <param name="type">Type.</param>
    void _LogCallback(string condition, string stackTrace, LogType type)
    {
        if (type == LogType.Exception)
        {
            Debug.Log("LogCallback : " + stackTrace);

            //SendMessage("tooltip", stackTrace);
            //SendMessage("exception", stackTrace);
            SendMessage("crash_unity", stackTrace);
        }
    }

}
