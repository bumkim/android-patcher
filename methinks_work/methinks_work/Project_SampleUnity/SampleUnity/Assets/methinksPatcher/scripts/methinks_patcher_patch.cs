﻿using System;
using UnityEngine;
using System.Collections;


public class methinks_patcher_patch : MonoBehaviour
{

	/// <summary>
	/// 
	/// </summary>
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

    /// <summary>
    /// Ons the enable.
    /// </summary>
    void OnEnable()
    {
        methinks_patcher_manager.Instance.OnEnable();
    }

    /// <summary>
    /// Ons the disable.
    /// </summary>
    void OnDisable()
    {
        methinks_patcher_manager.Instance.OnDisable();
    }

    /// <summary>
    /// Start this instance.
    /// </summary>
    void Start()
	{
        methinks_patcher_manager.Instance.Preset4Unity(gameObject.name, "ReceiveMessage");
    }

    /// <summary>
    /// Ons the application quit.
    /// </summary>
	void OnApplicationQuit()
	{
        methinks_patcher_manager.Instance.Uninitialize();
    }

    /// <summary>
    /// Receives the message.
    /// </summary>
    /// <param name="message">Message.</param>
	public void ReceiveMessage(string message)
	{
		Debug.Log ("[methinks_patcher_patch : ReceiveMessage] - Message : " + message);
	}

}
