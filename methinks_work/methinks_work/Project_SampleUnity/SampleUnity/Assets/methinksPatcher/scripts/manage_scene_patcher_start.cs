﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class manage_scene_patcher_start : MonoBehaviour
{
    int _eventType;
    InputField _eventKeyInputField;
    InputField _eventValueInputField;
    Text _receivedMessageText;
    Text _currentTimeText;

    string[] _eventKeyValuePairs;
    int _eventKeyValueCountIndex;

    void Awake()
    {
        Screen.SetResolution(720, 1280, true);

        _eventKeyInputField = GameObject.Find("inputfield_event_key").GetComponent<InputField>();
        _eventValueInputField = GameObject.Find("inputfield_event_value").GetComponent<InputField>();
        _receivedMessageText = GameObject.Find("text_received_message").GetComponent<Text>();
        _currentTimeText = GameObject.Find("text_current_time").GetComponent<Text>();

        _currentTimeText.text = DateTime.Now.ToString("HH:mm:ss");

        /*
        _eventKeyValuePairs = new string[] {
            "V_stage1",
            "V_stage3",
            "V_stage4",
            "V_stage6",
            "V_stage7",
            "V_stage9",
            "V_stage11",
            "V_stage14",
            "V_stage17",
            "V_stage18",
            "V_stage19",
            "V_stage24",
            "V_stage30",
            "V_stage31",
            "V_stage33",
            "V_stage35",
            "V_stage171",
            "V_stage172",
            "V_stage173",
            "L_stage19",
            "L_stage24",
            "A_tutorial",
            "A_gift",
            "A_gift_fin",
            "L_stage23",
            "L_stage21",
            "L_stage27",
            "L_stage33",
            "L_stage29",
            "L_stage52",
            "L_stage69",
            "L_stage72",
            "L_stage63",
            "L_stage79",
            "L_stage60",
            "L_stage71",
            "L_stage86",
            "L_stage42",
            "L_stage74",
            "L_stage109",
            "L_stage95",
            "L_stage148",
            "L_stage169",
            "L_stage185"
        };
        */

        _eventKeyValuePairs = new string[] {
            "Rating question",
            "Test Bookmark 1",
            "Stage 1 start",
            "Choice Single Question",
            "Choice multiple question",
            "Open Ended Question",
            "Multi questions"
        };


        _eventKeyInputField.text = "event";
        _eventValueInputField.text = _eventKeyValuePairs[0];
    }

    // Use this for initialization
    void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		_currentTimeText.text = DateTime.Now.ToString("HH:mm:ss");

		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			Application.Quit ();
		}
	}

	public void OnTapEventKeyTitle()
	{
		Debug.Log ("[manage_scene : OnTapEventKeyTitle]");
		if (0 == _eventType) 
		{
			_eventType = 1;
			_eventKeyInputField.text = "test";
			_eventValueInputField.text = "crash";
		}
		else if (1 == _eventType)
		{
			_eventType = 0;
			_eventKeyInputField.text = "event";
            _eventValueInputField.text = _eventKeyValuePairs[_eventKeyValueCountIndex];
        }
	}

	public void OnTapEventValueTitle()
	{
		Debug.Log ("[manage_scene : OnTapEventValueTitle]");
		if (0 == _eventType) 
		{
            _eventValueInputField.text = _eventKeyValuePairs[_eventKeyValueCountIndex];

            ++_eventKeyValueCountIndex;
            if (_eventKeyValuePairs.Length == _eventKeyValueCountIndex)
                _eventKeyValueCountIndex = 0;
        }
		else if (1 == _eventType)
		{
			_eventValueInputField.text = "crash";
		}
	}


	public void OnTapSendEvent()
	{
		Debug.Log ("[manage_scene : OnTapSendEvent]");
        methinks_patcher_manager.Instance.SendMessage (_eventKeyInputField.text, _eventValueInputField.text);
    }


	public void OnTapTooltip()
	{
		Debug.Log ("[manage_scene : OnTapTooltip]");
        //methinks_patcher_manager.Instance.Tooltip("Unity->Native : "+DateTime.Now.ToLongTimeString(),true);
	}


	public void ReceiveMessage(string message)
	{
		Debug.Log ("[manage_scene : ReceiveMessage] - Message : "+message);
		_receivedMessageText.text = message;
	}


    public void OnTapStartStream()
    {
        Debug.Log("[manage_scene : OnTapStartStream]");
        methinks_patcher_manager.Instance.SendMessage("start", "stream");
    }

    public void OnTapStopStream()
    {
        Debug.Log("[manage_scene : OnTapStopStream]");
        methinks_patcher_manager.Instance.SendMessage("stop", "stream");
    }

    public void OnTapNext()
    {
        SceneManager.LoadScene("methinksPatcher_integrate_simulate");
    }
}
