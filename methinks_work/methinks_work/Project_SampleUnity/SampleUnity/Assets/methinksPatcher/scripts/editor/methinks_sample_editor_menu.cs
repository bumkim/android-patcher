﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Collections;

public class methinks_sample_editor_menu
{
     
#if UNITY_EDITOR  
	[MenuItem("methinks/BuildSetPatcherIntegrate")]
	private static void BuildSetPatcherIntegrate()
	{
		List<EditorBuildSettingsScene> editorBuildSettingsSceneList = new List<EditorBuildSettingsScene> ();

		EditorBuildSettingsScene _0 = new EditorBuildSettingsScene ();
		_0.path = "Assets/methinksPatcher/scenes/methinksPatcher_integrate.unity";
		_0.enabled = true;
		editorBuildSettingsSceneList.Add (_0);

		EditorBuildSettings.scenes = editorBuildSettingsSceneList.ToArray ();
	}

    [MenuItem("methinks/BuildSetPatcherPatch")]
    private static void BuildSetPatcherPatch()
    {
        List<EditorBuildSettingsScene> editorBuildSettingsSceneList = new List<EditorBuildSettingsScene>();

        EditorBuildSettingsScene _0 = new EditorBuildSettingsScene();
        _0.path = "Assets/methinksPatcher/scenes/methinksPatcher_patch.unity";
        _0.enabled = true;
        editorBuildSettingsSceneList.Add(_0);

        EditorBuildSettings.scenes = editorBuildSettingsSceneList.ToArray();
    }

    /*
    [MenuItem("methinks/ExportUnityPackageEventTrigger")]
    private static void ExportUnityPackageEventTrigger()
    {
        Debug.Log("Start ExportUnityPackageEventTrigger");

        List<string> assetPathList = new List<string>();
        assetPathList.Add("Assets/methinksPatcher/scenes/methinksPatcher_event_trigger.unity");
        assetPathList.Add("Assets/methinksPatcher/scripts/manage_scene_event_trigger.cs");
        assetPathList.Add("Assets/methinksPatcher/scripts/methinks_patcher_manager.cs");
        assetPathList.Add("Assets/Plugins/iOS/methinksEventTrigger.h");
        assetPathList.Add("Assets/Plugins/iOS/methinksEventTrigger.mm");
        AssetDatabase.ExportPackage(assetPathList.ToArray(), "methinksEventTrigger.unitypackage", ExportPackageOptions.Interactive | ExportPackageOptions.Recurse | ExportPackageOptions.IncludeDependencies);

        Debug.Log("Finish ExportUnityPackageEventTrigger");
    }
    */
#endif

}

