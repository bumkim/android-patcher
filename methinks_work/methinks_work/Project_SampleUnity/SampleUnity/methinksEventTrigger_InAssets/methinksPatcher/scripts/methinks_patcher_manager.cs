﻿using System;
using UnityEngine;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class methinks_patcher_manager
{
	private static methinks_patcher_manager instance;
    private static readonly object objectLock = new object();

#if !UNITY_EDITOR
#if UNITY_ANDROID
	AndroidJavaClass _androidPatcherClass;
	AndroidJavaObject _androidPatcherObject;
#elif UNITY_IOS
	[DllImport("__Internal")]
	static extern void __SendMessage(string eventNameKey,string evnetValue);
#endif
#endif

    public static methinks_patcher_manager Instance
    {
        get
        {
            lock (objectLock)
            {
                if (instance == null)
                {
                    instance = new methinks_patcher_manager();
                }
                return instance;
            }
        }
    }


	public methinks_patcher_manager()
	{
#if !UNITY_EDITOR
#if UNITY_ANDROID
		if(null == _androidPatcherClass)
			_androidPatcherClass = new AndroidJavaClass ("com.kint.kintframeworkaosaar.methinksPatcherBridge");
        if(null != _androidPatcherClass)
        {
            if(null == _androidPatcherObject)	
			    _androidPatcherObject = _androidPatcherClass.CallStatic<AndroidJavaObject> ("Instance");
        }
#elif UNITY_IOS

#endif
#else

#endif
    }


    public void SendMessage(string eventNameKey,string eventValue)
	{
#if !UNITY_EDITOR
		#if UNITY_ANDROID
		if(null != _androidPatcherObject)
			_androidPatcherObject.Call("SendMessage", new object[] {eventNameKey, eventValue} );
		#elif UNITY_IOS
		__SendMessage(eventNameKey,eventValue);
		#endif
		#else
		Debug.Log ("[methinks_patcher_manager : SendMessage] EventNameKey : "+eventNameKey+" , EventValue : "+eventValue+" - Not Work UnityEditor");
		#endif	
	}
		
	
}
