﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class methinks_sample : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void _EnterShop()
    {
        methinks_patcher_manager.Instance.SendMessage("event", "enter_shop");
    }

    void _FinishQuest1()
    {
        methinks_patcher_manager.Instance.SendMessage("event", "finish_quest1");
    }

    void _CallQuestion1()
    {
        methinks_patcher_manager.Instance.SendMessage("question", "call_question1");
    }
}
