//
//  methinksEventTrigger.mm
//  methinksEventTrigger
//
//  Created by methinks on 2018. 9. 7..
//  Copyright © 2018년 methinks. All rights reserved.
//

#import "methinksEventTrigger.h"


#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]

#if defined __cplusplus
extern "C"
{
#endif
    
    char * MakeStringCopy(const char * string)
    {
        if( string == NULL )
            return NULL;
        
        char * res = (char *)malloc( strlen( string ) + 1 );
        strcpy( res, string );
        return res;
    }
    
    void __SendMessage(const char* szEventNameKey,const char* szEvnetValue)
    {
        NSString * eventNameKey = GetStringParam(szEventNameKey);
        NSString * eventValue = GetStringParam(szEvnetValue);
        NSLog(@"__SendMessage - EventNameKey : %@ , EventValue : %@ ",eventNameKey,eventValue);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MT_EVENT_NOTIFICATION" object:nil userInfo:@{eventNameKey: eventValue}];
    }
    
    
    
#if defined __cplusplus
};
#endif

