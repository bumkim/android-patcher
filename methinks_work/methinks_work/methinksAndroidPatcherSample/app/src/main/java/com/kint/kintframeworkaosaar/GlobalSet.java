package com.kint.kintframeworkaosaar;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GlobalSet
{
    public static final int STREAM_MODULE_NONE = 0;
    public static final int STREAM_MODULE_RTMP = 1;
    public static final int STREAM_MODULE_WEBRTC = 2;

    private static final String TAG = "methinksGlobalSet";

    public static boolean EnableModule = true;
    public static boolean PatchMode = false;
    public static boolean SkipYetDate = false;
    //private String _rtmpServerBaseURL =  "rtmp://221.165.42.119:5391";//home
    public static String DebugRTMPServerBaseURL = "rtmp://ec2-184-72-75-118.compute-1.amazonaws.com:5391";
    public static String ReleaseRTMPServerBaseURL = "rtmp://ec2-54-162-108-19.compute-1.amazonaws.com:5391";
    public static String DebugServiceServerURL = "https://apptest-dev.methinks.io";
    public static String ReleaseServiceServerURL = "https://apptest.methinks.io";
    public static boolean RunBackground = false;
    public static int ShowDialogType;
    public static int ShowLogType;
    public static int LogDelayTickTime = 5000;
    public static int StreamModuleType = STREAM_MODULE_RTMP;

    public static int VideoWidth = 480;
    public static int VideoHeight = 854;
    public static int VideoDpi = 320;
    public static int VideoDataRate = 0;// = 1200 * 1024;//in kbps
    public static int VideoFrameRate = 10;//= 20;

    public static String ProjectID;
    public static boolean DebugMode = false;


    private static ArrayList _questionInfoList;

    public static QuestionInfo GetQuestionInfo(int countIndex)
    {
        if(null == _questionInfoList)
            return null;
        return (QuestionInfo)_questionInfoList.get(countIndex);
    }


    public static void ParseQuestionInfo(JSONObject questionInfoJson)
    {
        if (null != _questionInfoList)
            _questionInfoList.clear();

        try
        {
            JSONArray questionJsonArray = questionInfoJson.getJSONArray("questions");
            if(null != questionJsonArray)
            {
                for (int countIndex = 0; countIndex < questionJsonArray.length(); ++countIndex)
                {
                    if (null == _questionInfoList)
                        _questionInfoList = new ArrayList();

                    JSONObject questionInfoJsonObject = (JSONObject) questionJsonArray.get(countIndex);

                    QuestionInfo questionInfo = new QuestionInfo();
                    questionInfo.Parse(questionInfoJsonObject);

                    _questionInfoList.add(questionInfo);
                }
            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }

    public static int GetQuestionInfoCount()
    {
        if(null == _questionInfoList)
            return 0;
        return _questionInfoList.size();
    }


    public static void RemoveAtQuestionInfo(int countIndex)
    {
        _questionInfoList.remove(countIndex);
    }


    public static void ParsePresetModule(String presetModule)
    {
        if(null == presetModule || 0 == presetModule.length())
        {
            Log.d(TAG, "[ParsePresetModule] - presetModule : "+presetModule);
            return;
        }

        try
        {
            JSONObject presetModuleJSON = new JSONObject(presetModule);
            JSONObject moduleJSON = presetModuleJSON.getJSONObject("module");
            if(null == moduleJSON)
            {
                Log.d(TAG, "[ParsePresetModule] - null == moduleJSON");
                return;
            }
            else
            {
                Log.d(TAG, "[ParsePresetModule] - moduleJSON : "+moduleJSON.toString());

                if(moduleJSON.has("enable"))
                {
                    EnableModule = moduleJSON.getBoolean("enable");
                }
                Log.d(TAG, "[ParsePresetModule] - enable : "+EnableModule);

                if(moduleJSON.has("patch_mode"))
                {
                    PatchMode = moduleJSON.getBoolean("patch_mode");
                }
                Log.d(TAG, "[ParsePresetModule] - PatchMode : "+PatchMode);

                if(moduleJSON.has("skip_yet_date"))
                {
                    SkipYetDate = moduleJSON.getBoolean("skip_yet_date");
                }
                Log.d(TAG, "[ParsePresetModule] - SkipYetDate : "+SkipYetDate);

                if(moduleJSON.has("debug_rtmp_server_base_url"))
                {
                    DebugRTMPServerBaseURL = moduleJSON.getString("debug_rtmp_server_base_url");
                }
                Log.d(TAG, "[ParsePresetModule] - DebugRTMPServerBaseURL : "+DebugRTMPServerBaseURL);


                if(moduleJSON.has("release_rtmp_server_base_url"))
                {
                    ReleaseRTMPServerBaseURL = moduleJSON.getString("release_rtmp_server_base_url");
                }
                Log.d(TAG, "[ParsePresetModule] - ReleaseRTMPServerBaseURL : "+ReleaseRTMPServerBaseURL);

                if(moduleJSON.has("debug_service_server_url"))
                {
                    DebugServiceServerURL = moduleJSON.getString("debug_service_server_url");
                }
                Log.d(TAG, "[ParsePresetModule] - DebugServiceServerURL : "+DebugServiceServerURL);

                if(moduleJSON.has("release_service_server_url"))
                {
                    ReleaseServiceServerURL = moduleJSON.getString("release_service_server_url");
                }
                Log.d(TAG, "[ParsePresetModule] - ReleaseServiceServerURL : "+ReleaseServiceServerURL);

                if(moduleJSON.has("run_background"))
                {
                    RunBackground = moduleJSON.getBoolean("run_background");
                }
                Log.d(TAG, "[ParsePresetModule] - RunBackground : "+RunBackground);

                if(moduleJSON.has("video_width"))
                {
                    VideoWidth = moduleJSON.getInt("video_width");
                }
                Log.d(TAG, "[ParsePresetModule] - videoWidth : "+VideoWidth);


                if(moduleJSON.has("video_height"))
                {
                    VideoHeight = moduleJSON.getInt("video_height");
                }
                Log.d(TAG, "[ParsePresetModule] - videoHeight : "+VideoHeight);


                if(moduleJSON.has("video_dpi"))
                {
                    VideoDpi = moduleJSON.getInt("video_dpi");
                }
                Log.d(TAG, "[ParsePresetModule] - videoDpi : "+VideoDpi);


                if(moduleJSON.has("video_data_rate"))
                {
                    VideoDataRate = moduleJSON.getInt("video_data_rate");
                }
                Log.d(TAG, "[ParsePresetModule] - videoDataRate : "+VideoDataRate);


                if(moduleJSON.has("video_frame_rate"))
                {
                    VideoFrameRate = moduleJSON.getInt("video_frame_rate");
                }
                Log.d(TAG, "[ParsePresetModule] - videoFrameRate : "+VideoFrameRate);


                if(moduleJSON.has("show_dialog"))
                {
                    ShowDialogType = moduleJSON.getInt("show_dialog");
                }
                Log.d(TAG, "[ParsePresetModule] - ShowDialog : "+ShowDialogType);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    public static void ParsePresetProject(String presetProject)
    {
        if(null == presetProject || 0 == presetProject.length())
        {
            Log.d(TAG, "[PresetModule] - presetProject : "+presetProject);
            return;
        }

        try
        {
            JSONObject presetProjectJSON = new JSONObject(presetProject);
            Log.d(TAG, "[ParsePresetProject] - projectJSON : "+presetProjectJSON.toString());

            if(presetProjectJSON.has("id"))
            {
                ProjectID = presetProjectJSON.getString("id");
            }
            Log.d(TAG, "[ParsePresetProject] - ProjectID : "+ProjectID);

            if(presetProjectJSON.has("debug_mode"))
            {
                DebugMode = presetProjectJSON.getBoolean("debug_mode");
            }
            Log.d(TAG, "[ParsePresetProject] - DebugMode : "+DebugMode);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }
}
