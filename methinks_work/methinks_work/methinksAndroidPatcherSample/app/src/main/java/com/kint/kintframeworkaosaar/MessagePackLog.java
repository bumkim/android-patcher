package com.kint.kintframeworkaosaar;

public class MessagePackLog extends MessagePack
{
    public static int SessionTimeCheckStatus;
    public static long SessionTimeCheck;
    private static MessagePackLog __messagePackLog = new MessagePackLog();

    public MessagePackLog()
    {
        _value = CONNECTION_LOG;
        _key = _value.hashCode();
    }

    @Override
    public void Request()
    {
        _value = CONNECTION_LOG;
        _key = _value.hashCode();
    }

    @Override
    public String BuildRequest()
    {
        return "";
    }


    public static MessagePackLog GetMessagePackLog()
    {
        return __messagePackLog;
    }

}
