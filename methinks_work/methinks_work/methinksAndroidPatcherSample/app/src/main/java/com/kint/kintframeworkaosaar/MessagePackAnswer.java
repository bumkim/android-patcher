package com.kint.kintframeworkaosaar;

public class MessagePackAnswer extends MessagePack
{
    public MessagePackAnswer()
    {
        _value = CONNECTION_ANSWER;
        _key = _value.hashCode();
    }

    @Override
    public void Request()
    {
        _value = CONNECTION_ANSWER;
        _key = _value.hashCode();
    }

    @Override
    public String BuildRequest()
    {
        return "";
    }

}
