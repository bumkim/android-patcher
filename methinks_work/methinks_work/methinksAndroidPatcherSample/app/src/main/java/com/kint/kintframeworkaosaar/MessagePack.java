package com.kint.kintframeworkaosaar;

import android.media.AudioManager;
import android.media.AudioDeviceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.os.StatFs;
import android.os.Environment;
import android.view.Surface;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.Network;
import android.app.Activity;


public class MessagePack
{
    public static final String CONNECTION_CAPTURE = "capture";
    public static final String CONNECTION_CAMERA_CAPTURE = "picCapture";
    public static final String CONNECTION_CAMERA_CAPTURE_INFO = "picCaptureInfo";
    public static final String CONNECTION_CAPTURE_DETAIL = "captureDetail";
    public static final String CONNECTION_LOGIN = "login";
    public static final String CONNECTION_LOG = "log";
    public static final String CONNECTION_EVENT = "event";
    public static final String CONNECTION_QUESTION = "question";
    public static final String CONNECTION_ANSWER = "answer";
    public static final String CONNECTION_FRONT_CAM_SHOT = "frontStillShot";
    public static final String CONNECTION_NOTICE = "notice";
    
    public static final String PREPARE_LOGIN = "prepare_login";
    public static final String PREPARE_QUESTION = "prepare_question";
    
    protected int _key;
    protected String _value;
    
    public int GetKey()
    {
        return _key;
    }
    
    public String GetValue()
    {
        return _value;
    }
    
    public String BuildRequest()
    {
        return "";
    }
    
    
    public void Prepare()
    {
        
    }
    
    public void Request()
    {
        
    }
    
    public void Finish()
    {
        
    }
    
    private static String _Ping(String url)
    {
        String str = "";
        try
        {
            java.lang.Process process = Runtime.getRuntime().exec("ping -c 1 " + url);
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int i;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            String op[] = new String[64];
            String delay[] = new String[8];
            while ((i = reader.read(buffer)) > 0)
                output.append(buffer, 0, i);
            reader.close();
            op = output.toString().split("\n");
            Log.d("methinksPatcherApp.","Ping : output - "+output.toString());
            if(null == op || 2 > op.length)
                return "";
            Log.d("methinksPatcherApp.","Ping : delay - "+op[1]);
            delay = op[1].split("time=");
            if(null == delay || 2 > delay.length)
                return "";
            
            // body.append(output.toString()+"\n");
            str = delay[1];
            //Log.i("methinksPatcherApp", "Ping: " + delay[1]);
        }
        catch (IOException e)
        {
            // body.append("Error\n");
            e.printStackTrace();
        }
        return str;
    }
    
    
    private static float _PingAverageFloat(String url,int checkCount)
    {
        String ping = "";
        float pingTotal = 0.0f;
        int countIndex = 0;
        
        while(countIndex < checkCount)
        {
            ping = _Ping(url);
            if("" == ping || 0 == ping.length())
                continue;
            
            ++countIndex;
            
            Log.d("methinksPatcherApp.","ping : "+ping);
            
            String[] split = ping.split(" ");
            pingTotal += Float.parseFloat(split[0]);
        }
        
        float pingValue = pingTotal / (float) checkCount;
        pingValue = Float.parseFloat(String.format("%.1f", pingValue));
        return pingValue;
    }


    private static String _PingAverageString(String url,int checkCount)
    {
        String ping = "";
        float pingTotal = 0.0f;
        int countIndex = 0;

        while(countIndex < checkCount)
        {
            ping = _Ping(url);
            if("" == ping || 0 == ping.length())
                continue;

            ++countIndex;

            Log.d("methinksPatcherApp.","ping : "+ping);

            String[] split = ping.split(" ");
            pingTotal += Float.parseFloat(split[0]);
        }

        float pingValue = pingTotal / (float) checkCount;
        return String.format("%.1f", pingValue) + " ms";
    }
    
    
    private static String _GetNetworkClass(ConnectivityManager connectivityManager)
    {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        
        if(networkInfo == null || !networkInfo.isConnected())
            return "-"; //not connected
        if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
            return "Wifi";
        if(networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
        {
            int networkType = networkInfo.getSubtype();
            switch (networkType)
            {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return "EDGE";//"2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                case TelephonyManager.NETWORK_TYPE_TD_SCDMA:  //api<25 : replace by 17
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                case TelephonyManager.NETWORK_TYPE_IWLAN:  //api<25 : replace by 18
                case 19:  //LTE_CA
                    return "LTE";//"4G";
                default:
                    return "?";
            }
        }
        return "?";
    }
    
    /* array
    public static String GetDeviceInfoInLogin(AudioManager audioManager,ConnectivityManager connectivityManager,TelephonyManager telephonyManager,int screenBrightness)
    {
        String jsonString = "";
        try {
            
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonDeviceInfoArray = new JSONArray();
            
            JSONObject deviceJsonObject = new JSONObject();
            deviceJsonObject.put("Device", Build.MODEL);
            jsonDeviceInfoArray.put(deviceJsonObject);
            
            JSONObject osVersionJsonObject = new JSONObject();
            osVersionJsonObject.put("OS Version", Build.VERSION.RELEASE);
            jsonDeviceInfoArray.put(osVersionJsonObject);
            
            JSONObject carrierJsonObject = new JSONObject();
            carrierJsonObject.put("Carrier", telephonyManager.getNetworkOperatorName());
            jsonDeviceInfoArray.put(carrierJsonObject);
            
            JSONObject connectionTypeJsonObject = new JSONObject();
            connectionTypeJsonObject.put("Connectivity", _GetNetworkClass(connectivityManager));
            jsonDeviceInfoArray.put(connectionTypeJsonObject);
            
            boolean sendPing = false;
            if(true == sendPing)
            {
                int checkCount = 10;
                JSONArray latencyJsonArray = new JSONArray();
                JSONObject usWestJsonObject = new JSONObject();
                float averagePingValue = _PingAverage("13.52.0.0",checkCount);
                usWestJsonObject.put("US_WEST", averagePingValue);
                latencyJsonArray.put(usWestJsonObject);
                
                JSONObject usEastJsonObject = new JSONObject();
                averagePingValue = _PingAverage("23.23.255.255",checkCount);
                usEastJsonObject.put("US_EAST", averagePingValue);
                latencyJsonArray.put(usEastJsonObject);
                
                JSONObject seoulJsonObject = new JSONObject();
                averagePingValue = _PingAverage("13.124.63.251",checkCount);
                seoulJsonObject.put("SEOUL",  averagePingValue);
                latencyJsonArray.put(seoulJsonObject);
                
                JSONObject latencyJsonObject = new JSONObject();
                latencyJsonObject.put("Current Latency(ms)", latencyJsonArray);
                jsonDeviceInfoArray.put(latencyJsonObject);
            }
            
            //
            int audioStreamType = AudioManager.STREAM_SYSTEM;//AudioManager.STREAM_VOICE_CALL
            int currentStreamVolume = audioManager.getStreamVolume(audioStreamType);
            int maxStreamVolume = audioManager.getStreamMaxVolume(audioStreamType);
            int volumeLevel = (currentStreamVolume * 100) / maxStreamVolume;
            
            JSONObject volumeLevelJsonObject = new JSONObject();
            volumeLevelJsonObject.put("Volume Level(db)", volumeLevel + "%");
            jsonDeviceInfoArray.put(volumeLevelJsonObject);
            
            
            //
            int screenBrightnessLevel = (screenBrightness * 100) / 255;
            JSONObject brightnessJsonObject = new JSONObject();
            brightnessJsonObject.put("Screen Brightness", screenBrightnessLevel + "%");
            jsonDeviceInfoArray.put(brightnessJsonObject);
            
            jsonObject.put("deviceInfo", jsonDeviceInfoArray);
            jsonString = jsonObject.toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        
        return jsonString;
    }
    */


    public static double GetScreenInches(Activity currentActivity)
    {
        if(null == currentActivity)
            return 0;

        DisplayMetrics dm = new DisplayMetrics();
        currentActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        double wi = (double) width / (double) dm.xdpi;
        double hi = (double) height / (double) dm.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);

        //apply round
        screenInches = screenInches * 10.0;
        screenInches = Math.round(screenInches) / 10.0;

        Log.d("methinksPatcher","GetScreenInches - [" + screenInches + "] inches");
        return screenInches;
    }


    //object
    public static String GetDeviceInfoInLogin(AudioManager audioManager,ConnectivityManager connectivityManager,TelephonyManager telephonyManager,
                                              int screenBrightness, WindowManager windowManager,Context context,Activity currentActivity)
    {
        String jsonString = "";
        try {

            JSONObject jsonObjectDeviceInfo = new JSONObject();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("Device", Build.MODEL);
            jsonObject.put("OS Version", Build.VERSION.RELEASE);
            jsonObject.put("Carrier", telephonyManager.getNetworkOperatorName());
            jsonObject.put("Connectivity", _GetNetworkClass(connectivityManager));
            jsonObject.put("ScreenSize", GetScreenInches(currentActivity));


            String soundOutputDevice = "Built-in speaker on an Android device";
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            {
                if(true == methinksPatcherBridge.Instance().IsEarphoneOn)
                {
                    soundOutputDevice = "Headphones";
                }
                else
                {
                    if (audioManager.isBluetoothA2dpOn())
                    {
                        soundOutputDevice = "BluetoothA2DP";
                    }

                    if (audioManager.isSpeakerphoneOn())
                    {
                        soundOutputDevice = "Speakerphone";
                    }

                    if (audioManager.isBluetoothScoOn())
                    {
                        soundOutputDevice = "BluetoothSCO";
                    }
                }


            }
            else
            {
                AudioDeviceInfo[] audioDeviceInfos = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
                for (int countIndex = 0; countIndex < audioDeviceInfos.length; countIndex++)
                {
                    AudioDeviceInfo audioDeviceInfo = audioDeviceInfos[countIndex];
                    switch(audioDeviceInfo.getType())
                    {
                        case AudioDeviceInfo.TYPE_WIRED_HEADPHONES:
                        {
                            soundOutputDevice = "Headphones";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BLUETOOTH_A2DP:
                        {
                            soundOutputDevice = "BluetoothA2DP";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BLUETOOTH_SCO:
                        {
                            soundOutputDevice = "BluetoothSCO";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BUILTIN_EARPIECE:
                        {
                            soundOutputDevice = "Built in Earpiece";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BUILTIN_SPEAKER:
                        {
                            soundOutputDevice = "Built-in speaker on an Android device";
                        }
                        break;
                    }
                }
            }
            jsonObject.put("Sound Output Device" , soundOutputDevice);

            Runtime info = Runtime.getRuntime();
            long freeSize = info.freeMemory();
            long totalSize = info.totalMemory();
            long usedSize = totalSize - freeSize;

            jsonObject.put("Memory Usage",(int)((float)usedSize / (float)totalSize * (float)100) + "%");
            Intent batteryStatus = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            int plugged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

            String pluggedStatus = "plugged out";
            if(plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB || plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS)
                pluggedStatus = "plugged in";

            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = level / (float)scale;
            int batteryPercent = (int)(batteryPct * 100);

            jsonObject.put("Battery Status",pluggedStatus + ", "+batteryPercent+"%");

            jsonObject.put("Free Space",(checkInternalAvailableMemory() / (1024 * 1024))+" MB / "+(checkInternalStorageAllMemory() / (1024 * 1024))+" MB ");


            String screenOrientation = "";
            switch (windowManager.getDefaultDisplay().getRotation())
            {
                case Surface.ROTATION_0:
                {
                    //Portrait 0
                    screenOrientation = "Device oriented vertically, home button on the bottom";
                }
                break;

                case Surface.ROTATION_90:
                {
                    //Landscape 90
                    screenOrientation = "Device oriented horizontally, home button on the right";
                }
                break;

                case Surface.ROTATION_180:
                {
                    //Portrait 180
                    screenOrientation = "Device oriented vertically, home button on the top";
                }
                break;

                case Surface.ROTATION_270:
                {
                    //Landscape 270
                    screenOrientation = "Device oriented horizontally, home button on the left";
                }
                break;
            }

            jsonObject.put("Screen Orientation", screenOrientation);

            /*
            boolean sendPing = false;
            if(true == sendPing)
            {
                int checkCount = 10;
                JSONArray latencyJsonArray = new JSONArray();
                JSONObject usWestJsonObject = new JSONObject();
                float averagePingValue = _PingAverage("13.52.0.0",checkCount);
                usWestJsonObject.put("US_WEST", averagePingValue);
                latencyJsonArray.put(usWestJsonObject);

                JSONObject usEastJsonObject = new JSONObject();
                averagePingValue = _PingAverage("23.23.255.255",checkCount);
                usEastJsonObject.put("US_EAST", averagePingValue);
                latencyJsonArray.put(usEastJsonObject);

                JSONObject seoulJsonObject = new JSONObject();
                averagePingValue = _PingAverage("13.124.63.251",checkCount);
                seoulJsonObject.put("SEOUL",  averagePingValue);
                latencyJsonArray.put(seoulJsonObject);

                JSONObject latencyJsonObject = new JSONObject();
                latencyJsonObject.put("Current Latency(ms)", latencyJsonArray);
                jsonObject.put(latencyJsonObject);
            }
            */

            //
            int audioStreamType = AudioManager.STREAM_SYSTEM;//AudioManager.STREAM_VOICE_CALL
            int currentStreamVolume = audioManager.getStreamVolume(audioStreamType);
            int maxStreamVolume = audioManager.getStreamMaxVolume(audioStreamType);
            int volumeLevel = (currentStreamVolume * 100) / maxStreamVolume;

            jsonObject.put("Volume Level(db)", volumeLevel + "%");

            //
            int screenBrightnessLevel = (screenBrightness * 100) / 255;
            jsonObject.put("Screen Brightness", screenBrightnessLevel + "%");

            jsonObjectDeviceInfo.put("deviceInfo",jsonObject);

            //jsonString = jsonObject.toString();
            jsonString = jsonObjectDeviceInfo.toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return jsonString;
    }
    
    private static long checkInternalStorageAllMemory()
    {
        StatFs stat= new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return blockSize * totalBlocks;
    }
    
    
    private static long checkInternalAvailableMemory()
    {
        StatFs stat= new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return blockSize * availableBlocks;
    }



    private static String _GetWifiSpeed(Context context,boolean withUnit)
    {
        float speed = 0.5f;//getWiFiSpeed(context);
        if(true == withUnit)
            return speed + WifiInfo.LINK_SPEED_UNITS;
        return ""+speed;
    }


    private static float getWiFiSpeed(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return getWifiNetworkSpeed(wifiInfo);
    }

    private static float getWifiNetworkSpeed(WifiInfo wifiInfo)
    {
        if (wifiInfo == null)
        {
            return 0;
        }
        int linkSpeed = wifiInfo.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
        return linkSpeed;
    }


    private static String _GetMobileSpeed(Context context,boolean withUnit)
    {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = tm.getNetworkType();
        float netSpeed = getMobileNetworkSpeed(networkType);

        if(true == withUnit)
            return ""+netSpeed + getMobileNetworkSpeedUnit(networkType);
        return ""+netSpeed;
    }


    private static String getMobileNetworkSpeedUnit(int networkType) {
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case 16: // TelephonyManager.NETWORK_TYPE_GSM:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "KBps";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
            case 17: // TelephonyManager.NETWORK_TYPE_TD_SCDMA:
            case TelephonyManager.NETWORK_TYPE_LTE:
            case 18: // TelephonyManager.NETWORK_TYPE_IWLAN:
                return "MBps";
            default:
                return "KBps";
        }
    }


    private static float getMobileNetworkSpeed(int networkType) {
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return 114;
            case 16: // TelephonyManager.NETWORK_TYPE_GSM:
                return 0;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return 296;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return 115;
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return 153;
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return 60;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return 384;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return 2.46F;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return 3.1F;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return 21.6F;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return 5.76F;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return 14;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return 4.9F;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return 1.285F;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return 42;
            case 17: // TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return 0;
            case TelephonyManager.NETWORK_TYPE_LTE:
                return 100;
            case 18: // TelephonyManager.NETWORK_TYPE_IWLAN:
                return 0;
            default:
                return 0;
        }
    }

    
    public static String GetDeviceInfoInLog(AudioManager audioManager, ConnectivityManager connectivityManager,
                                            TelephonyManager telephonyManager, int screenBrightness, WindowManager windowManager,
                                            Context context,Activity currentActivity)
    {
        /*
         {
         “Sound Output Device”: “Built-in speaker on an iOS device”,
         “Memory Usage”: “91%“,
         “Battery Satus”: “plugged in, less than 100%“,
         “OS Version”: “12.0",
         “Volume Level(db)“: “25%“,
         “Current Connection Speed”: “0.57MB/s”,
         “Device”: “iPad”,
         “Free Space”: “5634 MB / 61035 MB”,
         “Current Latency”: {
         “US_EAST”: “240 ms”,
         “US_WEST”: “112 ms”,
         “SEOUL”: “270 ms”
         },
         “Connectivity”: “WiFi Connection”,
         “Screen Brightness”: “56%“,
         “Screen Orientation”: “Device oriented horizontally, home button on the right”,
         “Carrier”: “Unknown”
         }
         */
        
        String jsonString = "";
        try
        {
            JSONObject jsonObjectDeviceInfo = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            
            //
            int audioStreamType = AudioManager.STREAM_SYSTEM;//AudioManager.STREAM_VOICE_CALL
            int currentStreamVolume = audioManager.getStreamVolume(audioStreamType);
            int maxStreamVolume = audioManager.getStreamMaxVolume(audioStreamType);
            int volumeLevel = (currentStreamVolume * 100) / maxStreamVolume;

            String soundOutputDevice = "Built-in speaker on an Android device";
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            {
                if(true == methinksPatcherBridge.Instance().IsEarphoneOn)
                {
                    soundOutputDevice = "Headphones";
                }
                else
                {
                    if (audioManager.isBluetoothA2dpOn())
                    {
                        soundOutputDevice = "BluetoothA2DP";
                    }

                    if (audioManager.isSpeakerphoneOn())
                    {
                        soundOutputDevice = "Speakerphone";
                    }

                    if (audioManager.isBluetoothScoOn())
                    {
                        soundOutputDevice = "BluetoothSCO";
                    }
                }


            }
            else
            {
                AudioDeviceInfo[] audioDeviceInfos = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
                for (int countIndex = 0; countIndex < audioDeviceInfos.length; countIndex++)
                {
                    AudioDeviceInfo audioDeviceInfo = audioDeviceInfos[countIndex];
                    switch(audioDeviceInfo.getType())
                    {
                        case AudioDeviceInfo.TYPE_WIRED_HEADPHONES:
                        {
                            soundOutputDevice = "Headphones";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BLUETOOTH_A2DP:
                        {
                            soundOutputDevice = "BluetoothA2DP";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BLUETOOTH_SCO:
                        {
                            soundOutputDevice = "BluetoothSCO";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BUILTIN_EARPIECE:
                        {
                            soundOutputDevice = "Built in Earpiece";
                        }
                        break;

                        case AudioDeviceInfo.TYPE_BUILTIN_SPEAKER:
                        {
                            soundOutputDevice = "Built-in speaker on an Android device";
                        }
                        break;
                    }
                }
            }
            jsonObject.put("Sound Output Device" , soundOutputDevice);
            jsonObject.put("ScreenSize", GetScreenInches(currentActivity));

            Runtime info = Runtime.getRuntime();
            long freeSize = info.freeMemory();
            long totalSize = info.totalMemory();
            long usedSize = totalSize - freeSize;
            
            jsonObject.put("Memory Usage",(int)((float)usedSize / (float)totalSize * (float)100) + "%");
            Intent batteryStatus = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            int plugged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            
            String pluggedStatus = "plugged out";
            if(plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB || plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS)
                pluggedStatus = "plugged in";
            
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            
            float batteryPct = level / (float)scale;
            int batteryPercent = (int)(batteryPct * 100);
            
            jsonObject.put("Battery Status",pluggedStatus + ", "+batteryPercent+"%");
            jsonObject.put("OS Version", Build.VERSION.RELEASE);
            jsonObject.put("Volume Level(db)", volumeLevel + "%");

            String connectivity = _GetNetworkClass(connectivityManager);
            String currrentConnectionSpeed = "0.587MB/s";
            switch(connectivity)
            {
                case "Wifi":
                {
                    currrentConnectionSpeed = _GetWifiSpeed(context,true);
                }
                break;

                case "EDGE":
                case "3G":
                case "LTE":
                {
                    currrentConnectionSpeed = _GetMobileSpeed(context,true);
                }
                break;
            }

            jsonObject.put("Current Connection Speed",currrentConnectionSpeed);
            jsonObject.put("Device", Build.MODEL);
            jsonObject.put("Free Space",(checkInternalAvailableMemory() / (1024 * 1024))+" MB / "+(checkInternalStorageAllMemory() / (1024 * 1024))+" MB ");
            
            
            int checkCount = 10;

            /* array
            JSONArray latencyJsonArray = new JSONArray();
            JSONObject usWestJsonObject = new JSONObject();
            float averagePingValue = _PingAverage("13.52.0.0",checkCount);
            usWestJsonObject.put("US_WEST", averagePingValue);
            latencyJsonArray.put(usWestJsonObject);
            
            JSONObject usEastJsonObject = new JSONObject();
            averagePingValue = _PingAverage("23.23.255.255",checkCount);
            usEastJsonObject.put("US_EAST", averagePingValue);
            latencyJsonArray.put(usEastJsonObject);
            
            JSONObject seoulJsonObject = new JSONObject();
            averagePingValue = _PingAverage("13.124.63.251",checkCount);
            seoulJsonObject.put("SEOUL",  averagePingValue);
            latencyJsonArray.put(seoulJsonObject);
            
            jsonObject.put("Current Latency", latencyJsonArray);
            */

            //object
            JSONObject latencyJsonObject = new JSONObject();
            //JSONObject usWestJsonObject = new JSONObject();
            String averagePingValue = _PingAverageString("13.52.0.0",checkCount);
            latencyJsonObject.put("US_WEST", averagePingValue);
            //usWestJsonObject.put("US_WEST", averagePingValue);
            //latencyJsonArray.put(usWestJsonObject);


            //JSONObject usEastJsonObject = new JSONObject();
            averagePingValue = _PingAverageString("23.23.255.255",checkCount);
            latencyJsonObject.put("US_EAST", averagePingValue);
            //usEastJsonObject.put("US_EAST", averagePingValue);
            //latencyJsonArray.put(usEastJsonObject);

            //JSONObject seoulJsonObject = new JSONObject();
            averagePingValue = _PingAverageString("13.124.63.251",checkCount);
            latencyJsonObject.put("SEOUL",  averagePingValue);
            //seoulJsonObject.put("SEOUL",  averagePingValue);
            //latencyJsonArray.put(seoulJsonObject);

            jsonObject.put("Current Latency", latencyJsonObject);


            jsonObject.put("Connectivity", connectivity);
            int screenBrightnessLevel = (screenBrightness * 100) / 255;
            jsonObject.put("Screen Brightness", screenBrightnessLevel + "%");
            
            
            String screenOrientation = "";
            switch (windowManager.getDefaultDisplay().getRotation())
            {
                case Surface.ROTATION_0:
                {
                    //Portrait 0
                    screenOrientation = "Device oriented vertically, home button on the bottom";
                }
                    break;
                    
                case Surface.ROTATION_90:
                {
                    //Landscape 90
                    screenOrientation = "Device oriented horizontally, home button on the right";
                }
                    break;
                    
                case Surface.ROTATION_180:
                {
                    //Portrait 180
                    screenOrientation = "Device oriented vertically, home button on the top";
                }
                    break;
                    
                case Surface.ROTATION_270:
                {
                    //Landscape 270
                    screenOrientation = "Device oriented horizontally, home button on the left";
                }
                    break;
            }
            
            jsonObject.put("Screen Orientation", screenOrientation);
            jsonObject.put("Carrier", telephonyManager.getNetworkOperatorName());

            jsonObjectDeviceInfo.put("deviceInfo",jsonObject);

            //jsonString = jsonObject.toString();
            jsonString = jsonObjectDeviceInfo.toString();

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        
        return jsonString;
    }
}
