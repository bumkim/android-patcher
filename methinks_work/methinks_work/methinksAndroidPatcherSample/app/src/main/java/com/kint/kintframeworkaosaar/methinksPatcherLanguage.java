package com.kint.kintframeworkaosaar;

import java.util.Locale;

public class methinksPatcherLanguage
{

    public static final int patcher_msg_enter_user_code = 0;//MESSAGE_TITLE_LOGIN_RETRY = "Enter user code from methinks app.";
    public static final int patcher_msg_methinks_user_code = 1;//MESSAGE_TITLE_LOGIN = "Enter methinks user code.";
    public static final int patcher_error_msg_can_not_connect_server = 2;//MESSAGE_TITLE_SERVER_ERROR = 2;//"Can't connect to methinks server. Please try again or contact methinks.";
    public static final int patcher_error_msg_invalid_user_code = 3;//MESSAGE_TITLE_INVALID_USER = 3;//"Invalid user code. Enter your user code again.";
    public static final int patcher_error_msg_test_period_over = 4;//MESSAGE_TITLE_INVALID_PROJECT = 4;//"Testing period is over. Thank you for your participation. Please delete this app from your device.";
    //public static final int MESSAGE_TITLE_PERMISSION_SETTING = 5;//"Please allow the required permission in the Setting menu.";
    //public static final int MESSAGE_TITLE_PHOTO_PERMISSION_CHECK = 6;//“Camera will be used to calculate distance from your head and measure the brightness of the environment and/or take still picture periodically. Please allow Camera permission.“, ,
    //public static final int MESSAGE_TITLE_SCREENSHOT_ACTION = 7;//“Please choose why you took a screenshot.“, ,
    //public static final int MESSAGE_TITLE_SCREENSHOT_BLOCKED = 8;//“You are not allowed to take screenshots of this app. Please delete the screenshot immediately, or you may be subject to legal actions.“, ,
    //public static final int MESSAGE_TITLE_RECORDING_START = 9;//“Screen recording started. Please stay in good WiFi connection.“, ,
    //public static final int MESSAGE_TITLE_PERMISSION_CHECK = 10;//“Please allow permissions required for screen recordings.“, ,
    //public static final int MESSAGE_TITLE_SCREENSHOT_DETAIL = 11;//"More details please!";
    public static final int patcher_methinks_login = 12;
    public static final int patcher_select_confirm = 13;
    public static final int patcher_select_cancel = 14;
    public static final int patcher_level_very_easy = 15;
    public static final int patcher_level_easy = 16;
    public static final int patcher_level_average = 17;
    public static final int patcher_level_hard = 18;
    public static final int patcher_level_very_hard = 19;

    public static String GetText(int id)
    {
        String text = "";
        String language = Locale.getDefault().getLanguage();
        android.util.Log.d("methinks","PatcherLanguage - language : "+language);
        switch(language)
        {
            case "en":
            default:
            {
                switch(id)
                {
                    case patcher_msg_enter_user_code:
                    {
                        text = "Enter user code from methinks app.";
                    }
                    break;

                    case patcher_msg_methinks_user_code:
                    {
                        text = "Enter methinks user code.";
                    }
                    break;

                    case patcher_error_msg_can_not_connect_server:
                    {
                        text = "Can't connect to methinks server. Please try again or contact methinks.";
                    }
                    break;

                    case patcher_error_msg_invalid_user_code:
                    {
                        text = "Invalid user code. Enter your user code again.";
                    }
                    break;

                    case patcher_error_msg_test_period_over:
                    {
                        text = "Testing period is over. Thank you for your participation. Please delete this app from your device.";
                    }
                    break;

                    case patcher_methinks_login:
                    {
                        text = "methinks login";
                    }
                    break;

                    case patcher_select_confirm:
                    {
                        text = "OK";
                    }
                    break;

                    case patcher_select_cancel:
                    {
                        text = "Cancel";
                    }
                    break;

                    case patcher_level_very_easy:
                    {
                        text = "Very easy";
                    }
                    break;

                    case patcher_level_easy:
                    {
                        text = "Easy";
                    }
                    break;

                    case patcher_level_average:
                    {
                        text = "Average";
                    }
                    break;

                    case patcher_level_hard:
                    {
                        text = "Hard";
                    }
                    break;

                    case patcher_level_very_hard:
                    {
                        text = "Very hard";
                    }
                    break;
                }

            }
            break;

            case "ko":
            {
                switch(id)
                {
                    case patcher_msg_enter_user_code:
                    {
                        text = "미띵스앱에서 발급한 사용자 코드를 입력하세요.";
                    }
                    break;

                    case patcher_msg_methinks_user_code:
                    {
                        text = "사용자 코드를 입력하세요.";
                    }
                    break;

                    case patcher_error_msg_can_not_connect_server:
                    {
                        text = "미띵스 서버에 연결할 수 없습니다. 다시 시도하거나 미띵스 관리자에게 문의하세요.";
                    }
                    break;

                    case patcher_error_msg_invalid_user_code:
                    {
                        text = "사용자 코드가 유효하지 않습니다. 다시 입력하세요.";
                    }
                    break;

                    case patcher_error_msg_test_period_over:
                    {
                        text = "테스트가 모두 종료 되었으므로 이 앱을 삭제해 주십시오. 참여해 주셔서 감사합니다.";
                    }
                    break;

                    case patcher_methinks_login:
                    {
                        text = "미띵스 로그인";
                    }
                    break;

                    case patcher_select_confirm:
                    {
                        text = "확인";
                    }
                    break;

                    case patcher_select_cancel:
                    {
                        text = "취소";
                    }
                    break;

                    case patcher_level_very_easy:
                    {
                        text = "매우 쉬움";
                    }
                    break;

                    case patcher_level_easy:
                    {
                        text = "쉬움";
                    }
                    break;

                    case patcher_level_average:
                    {
                        text = "보통";
                    }
                    break;

                    case patcher_level_hard:
                    {
                        text = "어려움";
                    }
                    break;

                    case patcher_level_very_hard:
                    {
                        text = "매우 어려움";
                    }
                    break;
                }
            }
            break;

            case "es":
            {
                switch(id)
                {
                    case patcher_msg_enter_user_code:
                    {
                        text = "Introduzca el código de usuario de la aplicación de methinks.";
                    }
                    break;

                    case patcher_msg_methinks_user_code:
                    {
                        text = "Introduzca el código de usario de methinks";
                    }
                    break;

                    case patcher_error_msg_can_not_connect_server:
                    {
                        text = "No se puede conectar al servidor de methinks. Inténtelo de nuevo o contacte a methinks.";
                    }
                    break;

                    case patcher_error_msg_invalid_user_code:
                    {
                        text = "Código de usuario inválido. Introduzca su código de usario de nuevo.";
                    }
                    break;

                    case patcher_error_msg_test_period_over:
                    {
                        text = "Período de prueba finalizado. Gracias por su particiapción. Por favor, borre la aplicación de su dispositivo.";
                    }
                    break;

                    case patcher_methinks_login:
                    {
                        text = "methinks iniciar sesión";
                    }
                    break;

                    case patcher_select_confirm:
                    {
                        text = "OK";
                    }
                    break;

                    case patcher_select_cancel:
                    {
                        text = "Cancelar";
                    }
                    break;

                    case patcher_level_very_easy:
                    {
                        text = "Es muy facil";
                    }
                    break;

                    case patcher_level_easy:
                    {
                        text = "Es facil";
                    }
                    break;

                    case patcher_level_average:
                    {
                        text = "Promedio";
                    }
                    break;

                    case patcher_level_hard:
                    {
                        text = "Es dificil";
                    }
                    break;

                    case patcher_level_very_hard:
                    {
                        text = "Es muy dificil";
                    }
                    break;
                }
            }
            break;
        }

        return text;
    }
}
