package com.kint.kintframeworkaosaar;

import android.os.Bundle;
import android.content.Intent;
import android.widget.Toast;

import com.nexxpace.changkiyoon.methinksandroidpatchersample.MainActivity;
//import com.unity3d.player.UnityPlayerActivity;



public class methinksMainActivity extends MainActivity//UnityPlayerActivity
{
    //static
    //{
    //    System.loadLibrary("native-lib");
    //}

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Toast.makeText(this, "methinks Module Started", Toast.LENGTH_LONG).show();
        //Toast.makeText(this, stringFromJNI(), Toast.LENGTH_LONG).show();


        String presetModule = "{module:{}}";
        String presetProject = "{project:{}}";

        String  patchModeString = "integrate_mode";
        if(patchModeString.equals("patch_mode"))
            GlobalSet.PatchMode = true;

        methinksPatcherBridge.Instance().FirstInitialize(this);
        //String superClassName = getClass().getSuperclass().getName();
        //android.util.Log.d("methinksMainActivity","Super Class Name : "+superClassName+" , PackageName : "+getPackageName());
        //if(false == superClassName.contains("UnityPlayerActivity"))

        if(true == GlobalSet.PatchMode)
        {
            methinksPatcherBridge.Instance().Initialize(presetModule,presetProject);
        }


        //methinksPatcherBridge.Instance().InitialzeExceptionHandler();
        //try
        //{
        //    Thread.sleep(5000);
        //}
        //catch(java.lang.InterruptedException e)
        //{

        //}

        //int [] i = null;
        //i[3] = 3;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        methinksPatcherBridge.Instance().OnActivityResult(this,requestCode, resultCode, data);
    }

    //public native String stringFromJNI();
}

