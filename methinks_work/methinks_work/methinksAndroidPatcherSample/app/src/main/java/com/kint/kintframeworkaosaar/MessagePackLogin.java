package com.kint.kintframeworkaosaar;

public class MessagePackLogin extends MessagePack
{
    public MessagePackLogin()
    {
        _value = PREPARE_LOGIN;
        _key = _value.hashCode();
    }

    @Override
    public void Prepare()
    {
        _value = PREPARE_LOGIN;
        _key = _value.hashCode();
    }

    @Override
    public void Request()
    {
        _value = CONNECTION_LOGIN;
        _key = _value.hashCode();
    }

    @Override
    public void Finish()
    {

    }


    @Override
    public String BuildRequest()
    {
        return "";
    }

}
