package com.kint.kintframeworkaosaar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
import java.util.List;

import java.util.ArrayList;

public class QuestionInfo
{
    private final String TAG = "QuestionInfo";

    private String _id;
    private String _type;
    private ArrayList<String> _choiceList;
    private ArrayList<String> _rangeList;
    private String _imageURL;
    private ArrayList<String> _answerList;
    private String _text;
    private boolean _isMultipleChoice;

    public String GetType()
    {
        return _type;
    }

    public String GetID()
    {
        return _id;
    }

    public String GetText()
    {
        return _text;
    }

    public boolean IsMultipleChoice()
    {
        return _isMultipleChoice;
    }

    public ArrayList<String> ChoiceList()
    {
        return _choiceList;
    }

    public ArrayList<String> RangeList()
    {
        return _rangeList;
    }

    public void AddAnswer(boolean clear,String answer)
    {
        if(null == _answerList)
            _answerList = new ArrayList();
        if(true == clear)
            _answerList.clear();

        _answerList.add(answer);
    }

    public String Answer()
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("questionId", _id);
            jsonObject.put("questionType", _type);

            JSONArray answerJsonArray = new JSONArray();
            switch (_type)
            {
                case "range":
                {
                    answerJsonArray.put(_answerList.toArray()[0]);
                }
                break;

                case "multipleChoice":
                {
                    for(int countIndex=0; countIndex<_answerList.size(); ++countIndex)
                    {
                        answerJsonArray.put(_answerList.toArray()[countIndex]);
                    }
                }
                break;

                case "openEnd":
                {
                    answerJsonArray.put(_answerList.toArray()[0]);
                }
                break;
             }
             jsonObject.put("answer", answerJsonArray);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }


    public void Parse(JSONObject questionInfoJsonObject)
    {
        try
        {
            if (null != _choiceList)
                _choiceList.clear();

            _id = questionInfoJsonObject.getString("questionId");
            _type = questionInfoJsonObject.getString("questionType");
            _text = questionInfoJsonObject.getString("text");

            Log.d(TAG,"Question ID : "+_id+" , Question Type : "+_type);
            switch(_type)
            {
                case "range":
                {
                    JSONArray questionRangeJsonArray = questionInfoJsonObject.getJSONArray("range");
                    for (int countIndex = 0; countIndex < questionRangeJsonArray.length(); ++countIndex)
                    {
                        if (null == _rangeList)
                            _rangeList = new ArrayList();

                        String questionRangeElement = (String) questionRangeJsonArray.get(countIndex);
                        _rangeList.add(questionRangeElement);

                        Log.d(TAG,"Question Range List["+countIndex+"/"+questionRangeJsonArray.length()+"] : "+questionRangeElement);
                    }

                    JSONArray questionChoiceJsonArray = questionInfoJsonObject.getJSONArray("choices");
                    for (int countIndex = 0; countIndex < questionChoiceJsonArray.length(); ++countIndex)
                    {
                        if (null == _choiceList)
                            _choiceList = new ArrayList();

                        String questionChoice = (String) questionChoiceJsonArray.get(countIndex);
                        _choiceList.add(questionChoice);

                        Log.d(TAG,"Question Multiple Choice Element["+countIndex+"/"+questionChoiceJsonArray.length()+"] : "+questionChoice);
                    }

                    _isMultipleChoice = Boolean.parseBoolean(questionInfoJsonObject.getString("isMultipleChoice"));

                }
                break;

                case "multipleChoice":
                {
                    JSONArray questionJsonArray = questionInfoJsonObject.getJSONArray("choices");
                    for (int countIndex = 0; countIndex < questionJsonArray.length(); ++countIndex)
                    {
                        if (null == _choiceList)
                            _choiceList = new ArrayList();

                        String questionRangeElement = (String) questionJsonArray.get(countIndex);
                        _choiceList.add(questionRangeElement);

                        Log.d(TAG,"Question Multiple Choice Element["+countIndex+"/"+questionJsonArray.length()+"] : "+questionRangeElement);
                    }

                    _isMultipleChoice = Boolean.parseBoolean(questionInfoJsonObject.getString("isMultipleChoice"));

                }
                break;

                case "openEnd":
                {

                }
                break;
            }

            _imageURL = questionInfoJsonObject.getString("imageURL");
            Log.d(TAG,"Question ImageURL : "+_imageURL);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
