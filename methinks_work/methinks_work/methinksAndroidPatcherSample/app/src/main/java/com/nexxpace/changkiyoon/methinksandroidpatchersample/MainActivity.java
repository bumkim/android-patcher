package com.nexxpace.changkiyoon.methinksandroidpatchersample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;


public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //usage
        //methinks_patcher_manager.Instance().SendMessage("eventNameKey","eventValue");
    }
}
