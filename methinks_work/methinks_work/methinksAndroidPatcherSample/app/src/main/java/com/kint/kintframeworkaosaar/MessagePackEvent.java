package com.kint.kintframeworkaosaar;

public class MessagePackEvent extends MessagePack
{
    String _eventKey;
    String _eventValue;

    public MessagePackEvent(String eventKey,String eventValue)
    {
        _value = CONNECTION_EVENT;
        _key = _value.hashCode();

        _eventKey = eventKey;
        _eventValue = eventValue;
    }

    public String GetEventKey()
    {
        return _eventKey;
    }

    public String GetEventValue()
    {
        return _eventValue;
    }

    @Override
    public void Request()
    {
        _value = CONNECTION_EVENT;
        _key = _value.hashCode();
    }

    @Override
    public String BuildRequest()
    {
        return "";
    }

}
