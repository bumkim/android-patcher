package com.kint.kintframeworkaosaar;

public class DelayTimer
{
    private long _startTime;
    private boolean _stopFlag = false;
    private int _delayTickCount = -1;
    private int _callCount = -1;

    public DelayTimer()
    {
        Reset();
    }

    public DelayTimer(long startTime)
    {
        _startTime = startTime;
    }

    public void Reset()
    {
        _startTime = System.currentTimeMillis();
    }


    public long ElapsedSeconds()
    {
        long elapsedTimeMillis = System.currentTimeMillis() - _startTime;
        return elapsedTimeMillis / 1000;
    }

    public boolean ElapsedTickCount (int iDelayTickCount)
    {
        long elapsedTimeMillis = System.currentTimeMillis() - _startTime;
        if (elapsedTimeMillis >= iDelayTickCount)
        {
            Reset();
            return true;
        }

        return false;
    }
}


