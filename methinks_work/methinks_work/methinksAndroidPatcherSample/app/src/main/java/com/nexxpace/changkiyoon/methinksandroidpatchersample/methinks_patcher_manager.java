package com.nexxpace.changkiyoon.methinksandroidpatchersample;
//package io.methinks.android.mtkrtc;

import java.lang.reflect.Method;


public class methinks_patcher_manager
{
    private static methinks_patcher_manager __instance;

    private Class _methinksPatcherBridgeClass;
    private Method _methinksPatcherBridgeInstanceMethod;
    private Method _methinksPatcherBridgeSendMessageMethod;

    public static methinks_patcher_manager Instance()
    {
        if(null == __instance)
            __instance = new methinks_patcher_manager();
        return __instance;
    }

    public void SendMessage(String eventNameKey,String eventValue)
    {
        if(null == _methinksPatcherBridgeClass)
        {
            try
            {
                _methinksPatcherBridgeClass = Class.forName("com.kint.kintframeworkaosaar.methinksPatcherBridge");
                if(null != _methinksPatcherBridgeClass)
                {
                    if (null == _methinksPatcherBridgeInstanceMethod)
                        _methinksPatcherBridgeInstanceMethod = _methinksPatcherBridgeClass.getMethod("Instance");
                    if (null == _methinksPatcherBridgeSendMessageMethod)
                        _methinksPatcherBridgeSendMessageMethod = _methinksPatcherBridgeClass.getMethod("SendMessage", String.class, String.class);
                }
            }
            catch(java.lang.ClassNotFoundException ex)
            {
                ex.printStackTrace();
            }
            catch(java.lang.NoSuchMethodException ex)
            {
                ex.printStackTrace();
            }
        }

        if (null != _methinksPatcherBridgeInstanceMethod && null != _methinksPatcherBridgeSendMessageMethod)
        {
            try
            {
                _methinksPatcherBridgeSendMessageMethod.invoke(_methinksPatcherBridgeInstanceMethod.invoke(null), eventNameKey, eventValue);
            }
            catch(java.lang.IllegalAccessException ex)
            {
                ex.printStackTrace();
            }
            catch(java.lang.reflect.InvocationTargetException ex)
            {
                ex.printStackTrace();
            }
        }

    }


}
