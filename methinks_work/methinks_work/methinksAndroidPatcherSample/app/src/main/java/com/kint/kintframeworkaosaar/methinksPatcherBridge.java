package com.kint.kintframeworkaosaar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import java.util.ArrayList;
import java.util.List;
import android.content.DialogInterface.OnDismissListener;
//import com.unity3d.player.UnityPlayer;
import android.app.ActivityManager;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.app.KeyguardManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.telephony.TelephonyManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.media.AudioManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.nio.ByteBuffer;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.view.Surface;
import android.net.ConnectivityManager;
import java.util.Locale;
import android.net.NetworkInfo;
import java.io.InputStreamReader;
import android.content.res.XmlResourceParser;
import java.util.Calendar;
import java.util.Date;
import android.app.Application;
import android.util.DisplayMetrics;




public class methinksPatcherBridge implements Application.ActivityLifecycleCallbacks , ConnectCheckerRtmp , GetH264Data
{
    private final String TAG = "methinksPatcherBridge";
    private static methinksPatcherBridge __instance;

    private String _sendToUnityGameObjectName;
    private String _sendToUnityMethodName;

    public static Application patcherApplication;
    public static Context ApplicationContext;

    public int _viewDeveloperMode = 0;
    private int _questionStatus = 0;

    private Thread.UncaughtExceptionHandler _threadUncaughtExceptionHandler;
    private UncaughtExceptionHandler _uncaughtExceptionHandler;



    public static methinksPatcherBridge Instance()
    {
        if(null == __instance)
            __instance = new methinksPatcherBridge();
        return __instance;
    }


    public void FirstInitialize(Activity mainActivity)
    {
        Log.d(TAG,"[FirstInitialize]");

        patcherApplication = mainActivity.getApplication();
        ApplicationContext = mainActivity.getApplicationContext();
        SetMainActivity(mainActivity);

        patcherApplication.registerActivityLifecycleCallbacks(this);


    }


    public void InitialzeExceptionHandler()
    {
        //
        _threadUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        _uncaughtExceptionHandler = new UncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(_uncaughtExceptionHandler);
    }



    public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler
    {
        @Override
        public void uncaughtException(Thread thread, Throwable ex)
        {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();

            long year = calendar.get(Calendar.YEAR);
            long month = calendar.get(Calendar.MONTH)+1;
            long day = calendar.get(Calendar.DAY_OF_MONTH);
            long hour = calendar.get(Calendar.HOUR_OF_DAY);
            long minute = calendar.get(Calendar.MINUTE);
            long second = calendar.get(Calendar.SECOND);

            long nowDateTimeExceptTick = date.getTime() / 1000;

            final java.io.Writer result = new java.io.StringWriter();
            final java.io.PrintWriter printWriter = new java.io.PrintWriter(result);
            ex.printStackTrace(printWriter);
            String stacktrace = result.toString();
            printWriter.close();

            Log.e(TAG,"UncaughtExceptionHandler : "+stacktrace);

            /*
            //String filename = timestamp + ".stacktrace";

            //if (localPath != null) {
            //    writeToFile(stacktrace, filename);
            //}
            //if (url != null) {
            //    sendToServer(stacktrace, filename);
            //}

            //Thread.setDefaultUncaughtExceptionHandler(_threadUncaughtExceptionHandler);

            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);

            _threadUncaughtExceptionHandler.uncaughtException(thread, ex);
            */
        }
    }




    public void OnCreate(Activity mainActivity)
    {
        if(1 == GetProcessStatus())
            return;

        Log.d(TAG,"[onCreate] - GetProcessStatus Next");

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
        {
            intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        }

        /*
        BroadcastReceiver screenOnOffReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String strAction = intent.getAction();

                KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                if(strAction.equals(Intent.ACTION_USER_PRESENT) || strAction.equals(Intent.ACTION_SCREEN_OFF) || strAction.equals(Intent.ACTION_SCREEN_ON)  )
                    if( myKM.inKeyguardRestrictedInputMode())
                    {
                        System.out.println("Screen off " + "LOCKED");
                    } else
                    {
                        System.out.println("Screen off " + "UNLOCKED");
                    }
            }
        };
        .registerReceiver(screenOnOffReceiver, theFilter);
        */

        ApplicationContext.registerReceiver(new BroadcastReceiver()
        {
            @Override public void onReceive(Context context, Intent intent)
            {
                if(intentFilter.matchAction(Intent.ACTION_SCREEN_ON))
                {
                    Log.d(TAG, "[onCreate] : Screen On");
                    /*
                    if (_isBackground)
                    {
                        _isBackground = false;
                        //notifyForeground();
                        //Log.d("TTTT","Foreground");
                    }
                    */
                }
                else if(intentFilter.matchAction(Intent.ACTION_SCREEN_OFF))
                {
                    Log.d(TAG, "[onCreate] : Screen Off");
                }
                else if(intentFilter.matchAction(Intent.ACTION_HEADSET_PLUG))
                {
                    IsEarphoneOn = (intent.getIntExtra("state", 0) > 0) ? true : false;
                    if (IsEarphoneOn)
                    {
                        Log.d(TAG, "[onCreate] : Earphone is plugged");
                    }
                    else
                    {
                        Log.e(TAG, "[onCreate] : Earphone is unPlugged");
                    }
                }
            }
        }, intentFilter);



        _windowManager = (WindowManager)ApplicationContext.getSystemService(Context.WINDOW_SERVICE);
        _requestMessageList = new ArrayList();
        switch (_windowManager.getDefaultDisplay().getRotation())
        {
            case Surface.ROTATION_0:
            {
                //Portrait 0
            }
            break;

            case Surface.ROTATION_90:
            {
                int temp = GlobalSet.VideoWidth;
                GlobalSet.VideoWidth = GlobalSet.VideoHeight;
                GlobalSet.VideoHeight = temp;

                //Landscape 90
                _screenRotationAngle = -90;
            }
            break;

            case Surface.ROTATION_180:
            {
                //Portrait 180
                _screenRotationAngle = 180;
            }
            break;

            case Surface.ROTATION_270:
            {
                int temp = GlobalSet.VideoWidth;
                GlobalSet.VideoWidth = GlobalSet.VideoHeight;
                GlobalSet.VideoHeight = temp;

                //Landscape 270
                _screenRotationAngle = 90;
            }
            break;
        }

        Log.d(TAG,"[onCreate] - Language : "+Locale.getDefault().getLanguage());

        boolean firstRequestOverlayPermission = false;
        if(true == firstRequestOverlayPermission)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                if (!Settings.canDrawOverlays(mainActivity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + mainActivity.getPackageName()));
                    mainActivity.startActivityForResult(intent, SCREEN_OVERRAY_REQUEST_CODE);
                } else {
                    Intent settingsSystemIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                    mainActivity.startActivityForResult(settingsSystemIntent, SYSTEM_SETTING_REQUEST_CODE);
                }
            }
            else
            {
                Intent settingsSystemIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                mainActivity.startActivityForResult(settingsSystemIntent, SYSTEM_SETTING_REQUEST_CODE);
            }
        }
        else
        {
            if(0 == _viewDeveloperMode)
            {
                if(false == settingsSystemShowTouch())
                {
                    Intent settingsSystemIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                    mainActivity.startActivityForResult(settingsSystemIntent, SYSTEM_SETTING_REQUEST_CODE);
                }
                else
                {
                    Intent captureIntent = CreateScreenCaptureIntent();
                    mainActivity.startActivityForResult(captureIntent, SCREEN_CAPTURE_REQUEST_CODE);
                }
                _viewDeveloperMode = 1;
            }
        }
    }


    public void OnActivityResult(Activity mainActivity,int requestCode, int resultCode, Intent data)
    {
        if(android.app.Activity.RESULT_OK == resultCode)
        {
            Log.d(TAG,"onActivityResult -  resultCode : RESULT_OK , requestCode : "+requestCode);
            switch (requestCode)
            {
                case SCREEN_OVERRAY_REQUEST_CODE:
                {
                    Intent settingsSystemIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                    mainActivity.startActivityForResult(settingsSystemIntent, SYSTEM_SETTING_REQUEST_CODE);
                }
                break;

                case SYSTEM_SETTING_REQUEST_CODE:
                {
                    Intent captureIntent = CreateScreenCaptureIntent();
                    mainActivity.startActivityForResult(captureIntent, SCREEN_CAPTURE_REQUEST_CODE);
                }
                break;

                case SCREEN_CAPTURE_REQUEST_CODE:
                {
                    SetMediaProjection(resultCode, data);
                    Login();
                }
                break;
            }
        }
        else
        {
            Log.d(TAG,"onActivityResult -  resultCode : "+resultCode+" , requestCode : "+requestCode);
            switch(requestCode)
            {
                case SCREEN_OVERRAY_REQUEST_CODE:
                {
                    Toast.makeText(mainActivity, "Draw Overlay Permission Required. Restart App.", Toast.LENGTH_SHORT).show();
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch(InterruptedException ex)
                    {
                        ex.printStackTrace();
                    }

                    mainActivity.finish();
                    System.exit(0);
                }
                break;

                case SCREEN_CAPTURE_REQUEST_CODE:
                {
                    Toast.makeText(mainActivity, "Screen Capture Permission Required. Restart App.", Toast.LENGTH_SHORT).show();
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch(InterruptedException ex)
                    {
                        ex.printStackTrace();
                    }
                    mainActivity.finish();
                    System.exit(0);
                }
                break;

                case SYSTEM_SETTING_REQUEST_CODE:
                {
                    //boolean checked = Application.getShowTouchCheck();
                    Intent captureIntent = CreateScreenCaptureIntent();
                    mainActivity.startActivityForResult(captureIntent, SCREEN_CAPTURE_REQUEST_CODE);
                }
                break;
            }
        }
    }


    public void Initialize(String presetModule,String presetProject)
    {
        GlobalSet.ParsePresetModule(presetModule);
        GlobalSet.ParsePresetProject(presetProject);

        if(true == GlobalSet.DebugMode)
            MessagePackLog.SessionTimeCheck = 60;//second
        else
            MessagePackLog.SessionTimeCheck = 600;//second

        SetCallbackMessage(_callBackMessage);

        if(true == GlobalSet.EnableModule)
            OnCreate(GetMainActivity());
    }

    public void Preset4Unity(String sendToUnityGameObjectName,String sendToMethodName)
    {
        Log.d(TAG,"[Preset4Unity] - sendToUnityGameObjectName :  "+sendToUnityGameObjectName+" , sendToMethodName : "+sendToMethodName);
        _sendToUnityGameObjectName = sendToUnityGameObjectName;
        _sendToUnityMethodName = sendToMethodName;
    }

    public void Uninitialize()
    {

    }

    public void SendMessage(String eventKey,String eventValue)
    {
        SendRequestMessage(eventKey,eventValue);
    }

    public void Tooltip(String message,boolean echo)
    {
        Toast.makeText(ApplicationContext,message,Toast.LENGTH_SHORT).show();
        if(true == echo)
        {
            //not use yet
            //UnityPlayer.UnitySendMessage(_gameObjectName,_sendToMethodName,message);
        }
    }

    private CallbackMessage _callBackMessage = new CallbackMessage()
    {
        public void SendMessage(int messageType)
        {
            switch(messageType)
            {
                case 0:
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(GetMainActivity());

                    String methinksLogin = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_methinks_login);
                    alertDialog.setTitle(methinksLogin);

                    if(0 == LoginTryCount)
                    {
                        //alertDialog.setMessage(MESSAGE_TITLE_LOGIN);
                        alertDialog.setMessage(methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_msg_methinks_user_code));
                    }
                    else
                    {
                        //alertDialog.setMessage(MESSAGE_TITLE_LOGIN_RETRY);
                        alertDialog.setMessage(methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_msg_enter_user_code));
                    }

                    final EditText editText = new EditText(GetMainActivity());
                    alertDialog.setView(editText);

                    String confirm = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_confirm);
                    alertDialog.setPositiveButton(confirm, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            SetUserCode(editText.getText().toString());
                            dialog.dismiss();

                        }
                    });

                    String cancel = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_cancel);
                    alertDialog.setNegativeButton(cancel, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                                Quit();
                            }
                        });

                    alertDialog.setOnDismissListener(new OnDismissListener()
                    {
                        @Override
                        public void onDismiss(DialogInterface dialog)
                        {}
                    });

                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
                break;

                case 1000://text question
                {
                    /*
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(GetMainActivity());
                    alertDialog.setTitle("methinks Login");
                    alertDialog.setMessage(MESSAGE_TITLE_LOGIN);
                    final EditText editText = new EditText(GetMainActivity());
                    alertDialog.setView(editText);

                    alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            SetUserCode(editText.getText().toString());
                            dialog.dismiss();

                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                            Quit();
                        }
                    });

                    alertDialog.setOnDismissListener(new OnDismissListener()
                    {
                        @Override
                        public void onDismiss(DialogInterface dialog)
                        {}
                    });

                    alertDialog.setCancelable(false);
                    alertDialog.show();
                    */
                }
                break;

                case 1001://multiple question
                {
                    /*
                    final List<String> selectedItems = new ArrayList<String>();
                    final String[] items = new String[]{"IT/Computer", "Game", "Fashion", "VR", "Kidult", "Sports", "Music", "Movie"};

                    AlertDialog.Builder dialog = new AlertDialog.Builder(ApplicationContext);
                    dialog.setTitle("Select favorite things")
                            .setMultiChoiceItems(items,
                                    new boolean[]{false, false, false, false, false, false, false, false},
                                    new DialogInterface.OnMultiChoiceClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which, boolean isChecked)
                                        {
                                            if(isChecked)
                                            {
                                                //Toast.makeText(ApplicationContext, items[which], Toast.LENGTH_SHORT).show();
                                                selectedItems.add(items[which]);
                                            }
                                            else
                                            {
                                                selectedItems.remove(items[which]);
                                            }
                                        }
                                    })
                            .setPositiveButton("Confirm", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if( selectedItems.size() == 0 )
                                    {
                                        //Toast.makeText(ApplicationContext, "Nothing Selected Item", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        String items = "";
                                        for(String selectedItem : selectedItems)
                                        {
                                            items += (selectedItem + ", ");
                                        }

                                        selectedItems.clear();

                                        items = items.substring(0, items.length() - 2);
                                        //Toast.makeText(ApplicationContext, items, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).create().show();
                            */

                }
                break;

                case 1002://range question
                {
                    /*
                    final AlertDialog.Builder popDialog = new AlertDialog.Builder(ApplicationContext);
                    final SeekBar seekBar = new SeekBar(ApplicationContext);
                    seekBar.setMax(255);
                    seekBar.setKeyProgressIncrement(1);

                    popDialog.setTitle("Please Select Into Your Desired Brightness ");
                    popDialog.setView(seekBar);


                    OnSeekBarChangeListener onSeekBarChangeListener = new OnSeekBarChangeListener()
                    {
                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar)
                        {

                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar)
                        {

                        }

                        @Override
                        public void onProgressChanged(SeekBar seekBark, int progress, boolean fromUser)
                        {

                        }
                    };
                    seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
                    */
                }
                break;

                default:
                    break;
            }
        }
    };


    @Override
    public void onActivityStopped(Activity activity)
    {
        OnActivityStopped(activity);
    }

    @Override
    public void onActivityStarted(Activity activity)
    {
        OnActivityStarted(activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState)
    {
        OnActivitySaveInstanceState(activity,outState);
    }

    @Override
    public void onActivityPaused(Activity activity)
    {
        OnActivityPaused(activity);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState)
    {
        OnActivityCreated(activity, savedInstanceState);
    }


    @Override
    public void onActivityResumed(Activity activity)
    {
        OnActivityResumed(activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity)
    {
        OnActivityDestroyed(activity);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class LoginInfo
    {
        private String _status;
        private boolean _isScreenCaptureAllowed;
        private boolean _isScreenStreamAllowed;
        private boolean _isCameraStreamAllowed;
        private String _screenName;
        private String _campaignName;
        private String _notice;
        private List<String> _permissionList;
        private int _minimumTestBuildNumber;
        private long _statusResultStartDate;
        private String _statusResultScreenName;
        private String _statusResultStatus;
        private int _statusResultSurveyCount;
        private int _statusResultAnnouncementCount;
        private String _error;

        public boolean IsScreenCaptureAllowed()
        {
            return _isScreenCaptureAllowed;
        }

        public void SetScreenCaptureAllowed(boolean isScreenCaptureAllowed)
        {
            _isScreenCaptureAllowed = isScreenCaptureAllowed;
        }

        public boolean IsScreenStreamAllowed()
        {
            return _isScreenStreamAllowed;
        }
        public void SetScreenStreamAllowed(boolean isScreenStreamAllowed)
        {
            _isScreenStreamAllowed = isScreenStreamAllowed;
        }

        public boolean IsCameraStreamAllowed()
        {
            return _isCameraStreamAllowed;
        }

        public void SetCameraStreamAllowed(boolean isCameraStreamAllowed)
        {
            _isCameraStreamAllowed = isCameraStreamAllowed;
        }


        public long GetStartData()
        {
            return _statusResultStartDate;
        }

        public String GetResultStatus()
        {
            return _statusResultStatus;
        }

        public String GetError()
        {
            return _error;
        }

        public boolean CheckValidData()
        {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();

            long year = calendar.get(Calendar.YEAR);
            long month = calendar.get(Calendar.MONTH)+1;
            long day = calendar.get(Calendar.DAY_OF_MONTH);
            long hour = calendar.get(Calendar.HOUR_OF_DAY);
            long nowDateTime = year * 1000000 + month * 10000 + day * 100 + hour;

            long nowDateTimeExceptTick = date.getTime() / 1000;

            Log.d(TAG,"[CheckValidate] - nowDataTime : "+nowDateTime + " , nowDateTimeExceptTick : "+nowDateTimeExceptTick +" , StartDateTime : "+_statusResultStartDate);
            if(nowDateTimeExceptTick >= _statusResultStartDate)
                return true;
            return false;
        }


        public Message Parse(JSONObject responseJSON)
        {
            //{
            // "status":"ok",
            // "isScreenCaptureAllowed":true,
            // "isScreenStreamAllowed":false,
            // "isCameraStreamAllowed":true,
            // "notice":"",
            // "permissions":
            // [
            // "cameraPermission","microphonePermission",
            // "speechRecognitionPermission"
            // ],
            // "screenName":"Bilbo",
            // "campaignName":"MBrowser(Don't change name)",
            // "minimumTestBuildNumber":0,
            // "statusResult":
            // {
            // "startDate":1509494400,
            // "screenName":"Bilbo",
            // "status":"Total testing time not fulfilled, keep going! Required: 120 hours. Completed: 9 hours 56 minutes.",
            // "surveyCount":0,
            // "announcementCount":0
            // }
            // }


            //{
            // "status":"error",
            // "error":"invalidProject"
            // }

            Message message = null;
            try
            {
                _status = responseJSON.getString("status");
                if (_status.equals("ok"))
                {
                    _isScreenCaptureAllowed = responseJSON.getBoolean("isScreenCaptureAllowed");
                    Log.d(TAG, "isScreenCaptureAllowed : " + _isScreenCaptureAllowed);
                    _isScreenStreamAllowed = responseJSON.getBoolean("isScreenStreamAllowed");
                    Log.d(TAG, "isScreenStreamAllowed : " + _isScreenStreamAllowed);
                    _isCameraStreamAllowed = responseJSON.getBoolean("isCameraStreamAllowed");
                    Log.d(TAG, "isCameraStreamAllowed : " + _isCameraStreamAllowed);
                    _notice = responseJSON.getString("notice");
                    Log.d(TAG, "notice : " + _notice);
                    JSONArray permissionsJsonArray = responseJSON.getJSONArray("permissions");
                    if(null != permissionsJsonArray)
                    {
                        for(int countIndex=0; countIndex<permissionsJsonArray.length(); ++countIndex)
                        {
                            if(0 == countIndex)
                                _permissionList = new ArrayList<>();
                            _permissionList.add(permissionsJsonArray.getString(countIndex));
                        }
                    }

                    // "permissions":
                    // [
                    // "cameraPermission","microphonePermission",
                    // "speechRecognitionPermission"
                    // ],

                    _screenName = responseJSON.getString("screenName");
                    Log.d(TAG, "screenName : " + _screenName);
                    _campaignName = responseJSON.getString("campaignName");
                    Log.d(TAG, "campaignName : " + _campaignName);
                    _minimumTestBuildNumber = responseJSON.getInt("minimumTestBuildNumber");
                    Log.d(TAG, "minimumTestBuildNumber : " + _minimumTestBuildNumber);
                    JSONObject statusResultJSON = responseJSON.getJSONObject("statusResult");
                    _statusResultStartDate = statusResultJSON.getLong("startDate");
                    Log.d(TAG, "statusResultStartDate : " + _statusResultStartDate);

                    _statusResultScreenName = statusResultJSON.getString("screenName");
                    Log.d(TAG, "statusResultScreenName : " + _statusResultScreenName);
                    _statusResultStatus = statusResultJSON.getString("status");
                    Log.d(TAG, "statusResultStatus : " + _statusResultStatus);
                    _statusResultSurveyCount  = statusResultJSON.getInt("surveyCount");
                    Log.d(TAG, "statusResultSurveyCount : " + _statusResultSurveyCount);
                    _statusResultAnnouncementCount = statusResultJSON.getInt("announcementCount");
                    Log.d(TAG, "statusResultAnnouncementCount : " + _statusResultAnnouncementCount);

                    // "statusResult":
                    // {
                    // "startDate":1509494400,
                    // "screenName":"Bilbo",
                    // "status":"Total testing time not fulfilled, keep going! Required: 120 hours. Completed: 9 hours 56 minutes.",
                    // "surveyCount":0,
                    // "announcementCount":0
                    // }
                    // }
                    message = _receivedMessageHandler.obtainMessage();
                    message.what = MESSAGE_LOGIN_SUCCESS;

                }
                else if (_status.equals("error"))
                {
                    _error = responseJSON.getString("error");
                    Log.d(TAG, "Login Failure : " + _error);

                    if (_error.equals("invalidProject"))
                    {
                        _RemoveAllUserCode();

                        //application quit
                        Log.d(TAG, "Application Quit");
                        networkStatus = "invalidProject";

                        message = _receivedMessageHandler.obtainMessage();
                        message.what = MESSAGE_LOGIN_FAILURE;

                    }
                    else if (_error.equals("invalidUserCode"))
                    {
                        _RemoveAllUserCode();
                        networkStatus = "invalidUserCode";

                        message = _receivedMessageHandler.obtainMessage();
                        message.what = MESSAGE_LOGIN_FAILURE;

                    }
                }

                message.obj = this;
                _receivedMessageHandler.sendMessage(message);


            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            return message;
        }
    }

    //private final String TAG = "methinksPatcherApp.";

    public static final int SCREEN_CAPTURE_REQUEST_CODE  = 1000;
    public static final int SYSTEM_SETTING_REQUEST_CODE = 1001;
    public static final int SCREEN_OVERRAY_REQUEST_CODE = 1002;

    public static final int MESSAGE_CONNECTED_SUCCESS_RTMP_SERVER = 0;
    public static final int MESSAGE_CONNECTED_FAILURE_RTMP_SERVER = 1;
    public static final int MESSAGE_START_SEND_STREAM = 2;
    public static final int MESSAGE_DISCONNECTED_RTMP_SERVER = 3;
    public static final int MESSAGE_ORIENTATION_ROTATE_PORTRAIT = 4;
    public static final int MESSAGE_ORIENTATION_ROTATE_LANDSCAPE = 5;
    public static final int MESSAGE_LOGIN_PREPARE = 6;
    public static final int MESSAGE_LOGIN_SUCCESS = 7;
    public static final int MESSAGE_LOGIN_FAILURE = 8;
    public static final int MESSAGE_NOTIFICATION_FOREGROUND = 9;
    public static final int MESSAGE_NOTIFICATION_BACKGROUND = 10;

    public static final int MESSAGE_QUESTION_PREPARE = 11;
    public static final int MESSAGE_QUESTION_START = 12;
    public static final int MESSAGE_QUESTION_FINISH = 13;

    public static final int MESSAGE_CONNECT_SERVER_FAILURE = 14;


    //public static final String MESSAGE_TITLE_LOGIN_RETRY = "Enter user code from methinks app.";
    //public static final String MESSAGE_TITLE_LOGIN = "Enter methinks user code.";
    //public static final String MESSAGE_TITLE_SERVER_ERROR = "Can't connect to methinks server. Please try again or contact methinks.";
    //public static final String MESSAGE_TITLE_INVALID_USER = "Invalid user code. Enter your user code again.";
    //public static final String MESSAGE_TITLE_INVALID_PROJECT = "Testing period is over. Thank you for your participation. Please delete this app from your device.";

    //public static final String MESSAGE_TITLE_PERMISSION_SETTING = "Please allow the required permission in the Setting menu.";
    //public static final String MESSAGE_TITLE_PHOTO_PERMISSION_CHECK = "";//“Camera will be used to calculate distance from your head and measure the brightness of the environment and/or take still picture periodically. Please allow Camera permission.“, ,
    //public static final String MESSAGE_TITLE_SCREENSHOT_ACTION = "";//“Please choose why you took a screenshot.“, ,
    //public static final String MESSAGE_TITLE_SCREENSHOT_BLOCKED = "";//“You are not allowed to take screenshots of this app. Please delete the screenshot immediately, or you may be subject to legal actions.“, ,
    //public static final String MESSAGE_TITLE_RECORDING_START = "";//“Screen recording started. Please stay in good WiFi connection.“, ,
    //public static final String MESSAGE_TITLE_PERMISSION_CHECK = "";//“Please allow permissions required for screen recordings.“, ,
    //public static final String MESSAGE_TITLE_SCREENSHOT_DETAIL = "More details please!";

    private int _backgroundModeCheckTickTime = 1000*60*2;


    private MediaProjection _mediaProjection;
    private MediaProjectionManager _mediaProjectionManager;
    private VirtualDisplay _virtualDisplay;
    private int _projectionResultCode;
    private Intent _projectionIntentData;


    private boolean paused;
    private boolean running;
    private String _rtmpServerURL =  "rtmp://221.165.42.119:5391/screen/bilbo915?projectID=oulj264yvC";//home

    private SrsFlvMuxer rtmpMuxer;
    private VideoEncoder videoEncoder;
    private boolean connectedRTMPServer;
    private boolean sendRTMPVideo;
    private String networkStatus = "Ready";

    private int _screenRotationAngle = 0;
    private boolean _backgroundMode = false;
    private Activity _mainActivity;
    private Activity _currentActivity;
    private Handler _detectScreenShotHandler;

    private ArrayList _requestMessageList;
    private String _userCode;
    private int _processStatus;
    private int _dispatchNetworkStatus = 0;
    private DelayTimer _logDelayTimer = new DelayTimer();
    private DelayTimer _detectScreenShotTimer = new DelayTimer();
    private DelayTimer _checkBackgroundTimer = new DelayTimer();
    private DelayTimer _checkBackgroundTimer2 = new DelayTimer();
    private DelayTimer _checkBackgroundTimer3 = new DelayTimer();
    private DelayTimer _checkForegroundTimer = new DelayTimer();
    private boolean _quitApplication;

    private WindowManager _windowManager;
    View _loginLayoutView;
    ImageView _loginImageView;
    EditText _userCodeEditText;
    private CallbackMessage _callbackMessage;

    public int LoginTryCount;
    public  static boolean IsEarphoneOn = false;
    private LoginInfo _loginInfo;


    /*
    View _questionRangeView;
    View _questionMultipleChoiceView;
    View _questionOpenEndView;
    Button _answerRangeButton;
    Button _answerMultipleButton;
    Button _answerOpenEndButton;
    */

    public int GetProcessStatus()
    {
        return _processStatus;
    }

    public Intent CreateScreenCaptureIntent()
    {
        if(null == _mediaProjectionManager)
            _mediaProjectionManager = (MediaProjectionManager) ApplicationContext.getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        return _mediaProjectionManager.createScreenCaptureIntent();
    }

    public void SetMediaProjection(int resultCode, Intent data)
    {
        _projectionResultCode = resultCode;
        _projectionIntentData = data;

        //if(null == _mediaProjection)
        //    _mediaProjection = _mediaProjectionManager.getMediaProjection(resultCode, data);
    }

    public interface CallbackMessage
    {
        void SendMessage(int messageType);
    }

    public void SetCallbackMessage(CallbackMessage callbackMessage)
    {
        _callbackMessage = callbackMessage;
    }

    private void _SendCallbackMessage(int messageType)
    {
        if(null != _callbackMessage)
            _callbackMessage.SendMessage(messageType);
    }

    public void SetMainActivity(Activity mainActivity)
    {
        _mainActivity = mainActivity;
    }

    public Activity GetMainActivity()
    {
        return _mainActivity;
    }


    public void OnActivityStopped(Activity activity)
    {
        Log.d(TAG,"[onActivityStopped] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName());
    }

    public void OnActivityStarted(Activity activity)
    {
        Log.d(TAG,"[onActivityStarted] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName());
    }

    public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
    {
        Log.d(TAG,"[onActivitySaveInstanceState] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName());
    }

    public void OnActivityPaused(Activity activity)
    {
        Log.d(TAG,"[onActivityPaused] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName()+" , PackageName : "+ApplicationContext.getPackageName());

        /*
        Message message = _receivedMessageHandler.obtainMessage();
        message.what = MESSAGE_NOTIFICATION_BACKGROUND;
        _receivedMessageHandler.sendMessage(message);
        _backgroundMode = true;

        if(0 < _processStatus)
        {
            if(_loginInfo.IsScreenStreamAllowed())
            {
                if(false == _enableBackground)
                {
                    paused = true;
                    //stopRecord();
                }
            }
        }
        */
    }

    public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
    {
        Log.d(TAG,"[onActivityCreated] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName());
    }

    //
    public void OnActivityResumed(Activity activity)
    {
        Log.d(TAG,"[onActivityResumed] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName()+" , PackageName : " +ApplicationContext.getPackageName());

        _currentActivity = activity;

        //GetScreenInches();


        /*
        Message message = _receivedMessageHandler.obtainMessage();
        message.what = MESSAGE_NOTIFICATION_FOREGROUND;
        _receivedMessageHandler.sendMessage(message);

        _backgroundMode = false;
        _checkBackgroundTimer.Reset();

        if(0 < _processStatus)
        {
            if(_loginInfo.IsScreenStreamAllowed())
            {
                if(false == _enableBackground)
                {
                    if(true == paused)
                    {
                        paused = false;
                    }
                }
            }
        }
        */
    }


    public void OnActivityDestroyed(Activity activity)
    {
        Log.d(TAG,"[onActivityDestroyed] - "+activity.getCallingPackage()+" , "+activity.getLocalClassName());
        //_currentActivity = null;

        /*
        if(null != _detectScreenShotHandler)
        {
            _detectScreenShotHandler.removeMessages(0);
            _detectScreenShotHandler = null;
        }
        */
    }

    public void Login()
    {
        if(1 == _processStatus)
        {
            return;
        }

        _StartNetworkMessageLoop();
        String userCode = _ReadUserCode();
        if(userCode.equals(""))
        {
            MessagePackLogin messagePackLogin = new MessagePackLogin();
            messagePackLogin.Prepare();
            SendRequestMessage(messagePackLogin);
        }
        else
        {
            //Tooltip("Auto Login : "+userCode);
            SetUserCode(userCode);
        }
    }

    public void SetUserCode(String userCode)
    {
        _userCode = userCode;

        if(true == GlobalSet.DebugMode)
        {
            _rtmpServerURL = GlobalSet.DebugRTMPServerBaseURL + "/screen/"+_userCode+"?projectID="+GlobalSet.ProjectID;
        }
        else
        {
            _rtmpServerURL = GlobalSet.ReleaseRTMPServerBaseURL + "/screen/"+_userCode+"?projectID="+GlobalSet.ProjectID;
        }

        Log.d(TAG, "[SetUserCode] - RTMPServerURL : "+_rtmpServerURL);

        MessagePackLogin messagePackLogin = new MessagePackLogin();
        messagePackLogin.Request();
        SendRequestMessage(messagePackLogin);
    }

    public void Quit()
    {
        if(null != _currentActivity)
            _currentActivity.finish();
        System.exit(0);
    }

    void _DetectScreenShotService()
    {
        ActivityManager activityManager = (ActivityManager)ApplicationContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServiceInfoList = activityManager.getRunningServices(200);
        for(ActivityManager.RunningServiceInfo runningServiceInfo : runningServiceInfoList)
        {
            if(runningServiceInfo.process.equals("com.android.systemui:screenshot"))
            {
                File[] files = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()+"/Screenshots").
                        listFiles(new FileFilter()
                        {
                            public boolean accept(File pathname)
                            {
                                String name = pathname.getName();
                                //Toast.makeText(activity,"accept - "+name,Toast.LENGTH_LONG).show();
                                return pathname.isFile() && name.toLowerCase().startsWith("screenshot") && name.toLowerCase().endsWith(".png");
                            }
                        });

                if(null == files || 0 == files.length)
                    continue;

                Arrays.sort(files);
                File lastFile = files[files.length - 1];

                String screenCaptureMessage = "Screenshot captured!! - "+lastFile.getAbsolutePath()+"("+lastFile.length()+")";
                //Toast.makeText(ApplicationContext,screenCaptureMessage,Toast.LENGTH_LONG).show();
                Log.d(TAG,"[_DetectScreenShotService] - "+screenCaptureMessage);


                //read bytes from file

                //int size = (int) lastFile.length();
                //byte[] bytes = new byte[size];
                //try
                //{
                //    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(lastFile));
                //    buf.read(bytes, 0, bytes.length);
                //    buf.close();
                //}
                //catch (FileNotFoundException e)
                //{
                // TODO Auto-generated catch block
                //    e.printStackTrace();
                //}
                //catch (IOException e)
                //{
                // TODO Auto-generated catch block
                //    e.printStackTrace();
                //}
            }
        }
    }


    private boolean _IsForegrounded()
    {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND ||
                appProcessInfo.importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE);
    }


    public void SendRequestMessage(MessagePack requestMessagePack)
    {
        Log.d(TAG,"SendRequestMessage - " + requestMessagePack.GetValue());
        _requestMessageList.add(requestMessagePack);
    }

    public void SendRequestMessage(String eventKey,String eventValue)
    {
        if(0 == _processStatus)
        {
            Tooltip("Login Required");
            return;
        }

        switch(eventKey)
        {
            case "stop":
            {
                switch(eventValue)
                {
                    case "stream_log":
                    {

                    }
                    return;
                }
            }
            break;

            default:
                break;
        }

        MessagePackEvent messagePackEvent = new MessagePackEvent(eventKey,eventValue);
        SendRequestMessage(messagePackEvent);
    }

    private void _RequestServer(MessagePack messagePack)
    {
        try
        {
            Log.d(TAG, "[_RequestServer] - " + messagePack.GetValue());
            String fullURL;
            if (true == GlobalSet.DebugMode)
            {
                fullURL = GlobalSet.DebugServiceServerURL + "/";
            }
            else
            {
                fullURL = GlobalSet.ReleaseServiceServerURL + "/";
            }

            fullURL += messagePack.GetValue();
            Log.d(TAG,"[_RequestServer] - URL : "+fullURL);

            URL url = new URL(fullURL);
            HttpURLConnection httpURLConnection = null;

            OutputStream outputStream = null;
            InputStream inputStream = null;
            ByteArrayOutputStream byteArrayOutputStream = null;

            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setRequestProperty("project-name", GlobalSet.ProjectID);
            httpURLConnection.setRequestProperty("user-code", _userCode);
            Log.d(TAG,"ProjectID : "+GlobalSet.ProjectID + " , UserCode : "+_userCode);

            switch (messagePack.GetValue())
            {
                case MessagePack.CONNECTION_CAPTURE:
                case MessagePack.CONNECTION_CAMERA_CAPTURE:
                {
                    String BoundaryConstant = "----------V2ymHFg03ehb4238qgZCaK128O6jy";
                    String contentType = "multipart/form-data; boundary=" + BoundaryConstant;
                    httpURLConnection.setRequestProperty("Content-Type", contentType);

                    //NSMutableData *body = [NSMutableData data];
                    //[body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding: NSUTF8StringEncoding]];
                    //[body appendData:[@"Content-Disposition: form-data; name=\"file\"; filename=\"a.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    //[body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    //[body appendData:data];
                    //[body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    //[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                    //[req setHTTPBody:body];
                }
                break;

                case MessagePack.CONNECTION_LOGIN:
                {
                    Log.d(TAG, MessagePack.CONNECTION_LOGIN);

                    // {"deviceInfo": {}}
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");

                    TelephonyManager telephonyManager = (TelephonyManager)ApplicationContext.getSystemService(Context.TELEPHONY_SERVICE);
                    AudioManager audioManager = (AudioManager) ApplicationContext.getSystemService(Context.AUDIO_SERVICE);
                    ConnectivityManager connectivityManager = (ConnectivityManager) ApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                    int screenBrightness = _GetScreenBrightness();
                    String deviceInfoJsonString =
                            MessagePack.GetDeviceInfoInLogin(audioManager,connectivityManager,telephonyManager,screenBrightness,_windowManager,ApplicationContext,_currentActivity);

                    Log.d(TAG, "DeviceInfoJsonString : " + deviceInfoJsonString);

                    byte[] jsonBytes = deviceInfoJsonString.getBytes(/*StandardCharsets.US_ASCII*/);
                    int jsonBytesLength = jsonBytes.length;
                    Log.d(TAG, "jsonBytesLength : " + jsonBytesLength);
                    httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonBytesLength));
                    outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(jsonBytes);
                    outputStream.flush();
                    outputStream.close();


                }
                break;

                case MessagePack.CONNECTION_LOG:
                {
                    if (1 != MessagePackLog.SessionTimeCheckStatus)
                    {}
                    else
                    {
                        httpURLConnection.setRequestProperty("Content-Type", "application/json");

                        TelephonyManager telephonyManager = (TelephonyManager)ApplicationContext.getSystemService(Context.TELEPHONY_SERVICE);
                        AudioManager audioManager = (AudioManager) ApplicationContext.getSystemService(Context.AUDIO_SERVICE);
                        ConnectivityManager connectivityManager = (ConnectivityManager) ApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                        int screenBrightness = _GetScreenBrightness();
                        String deviceInfoJsonString = MessagePack.GetDeviceInfoInLog(audioManager,connectivityManager,telephonyManager,screenBrightness,_windowManager,ApplicationContext,_currentActivity);

                        Log.d(TAG, "DeviceInfoJsonString : " + deviceInfoJsonString);

                        byte[] jsonBytes = deviceInfoJsonString.getBytes();
                        int jsonBytesLength = jsonBytes.length;
                        httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonBytesLength));
                        outputStream = httpURLConnection.getOutputStream();
                        outputStream.write(jsonBytes);
                        outputStream.flush();
                        outputStream.close();

                        MessagePackLog.SessionTimeCheckStatus = 0;
                    }

                }
                break;

                case MessagePack.CONNECTION_ANSWER:
                {
                /*
                -(void) onSubmitTextViewQuestion:(UITextView*) textView questionObject:(NSDictionary*) questionDic {
                [textView resignFirstResponder];
                [self closePopup:YES];
                __weak typeof(self) weakSelf = self;
                popup.didFinishDismissingCompletion = ^{
                if (questionDic) {
                NSMutableDictionary* dic = [NSMutableDictionary new];
                [dic setObject:[questionDic objectForKey:QUESTION_ID_KEY] forKey:QUESTION_ID_KEY];
                [dic setObject:@[textView.text] forKey:ANSWER_KEY];
                [dic setObject:[questionDic objectForKey:QUESTION_TYPE_KEY] forKey:QUESTION_TYPE_KEY];
                NSData* data = [NSJSONSerialization dataWithJSONObject:[dic copy] options:0 error:nil];
                [[MTKNetworkManager sharedManager] connectServer:ANSWER_KEY data:data onResult:^(NSDictionary * result) {
                if ([[result objectForKey:@"status"] isEqualToString:@"ok"]) {
                    [weakSelf checkNextQuestion];
                }
                }];
                } else {
                answerTextBlock(textView.text);
                }
                };
                }

                //when select done button to move on
                -(void) onMultipleChoiceAnswerConfirm:(MTKUIButton*) button {
                MTKQuestionPopupContentView* view = (MTKQuestionPopupContentView*) button.superview;
                [self closePopup:YES];
                __weak typeof(self) weakSelf = self;
                __weak typeof(NSMutableArray*) weakAnswerArray = view.choiceView.selectedChoices;
                popup.didFinishDismissingCompletion = ^{
                NSMutableDictionary* dic = [NSMutableDictionary new];
                [dic setObject:[view.questionObject objectForKey:QUESTION_ID_KEY] forKey:QUESTION_ID_KEY];
                //[dic setObject:[view.questionObject objectForKey:CHECKLIST_ID_KEY] forKey:CHECKLIST_ID_KEY];
                [dic setObject:[weakAnswerArray copy] forKey:ANSWER_KEY];
                [dic setObject:[view.questionObject objectForKey:QUESTION_TYPE_KEY] forKey:QUESTION_TYPE_KEY];
                NSData* data = [NSJSONSerialization dataWithJSONObject:[dic copy] options:0 error:nil];
                [[MTKNetworkManager sharedManager] connectServer:ANSWER_KEY data:data onResult:^(NSDictionary * result) {
                 if ([[result objectForKey:@"status"] isEqualToString:@"ok"]) {
                [weakSelf checkNextQuestion];
                }
                }];
                };
                }

                -(void) onRangeQuestionAnswerConfirmed:(MTKUIButton*) button {
                MTKQuestionPopupContentView* view = (MTKQuestionPopupContentView*) button.superview;
                [self closePopup:YES];
                __weak typeof(self) weakSelf = self;
                __weak typeof(NSMutableArray*) weakAnswerArray = view.answerArray;
                popup.didFinishDismissingCompletion = ^{
                NSMutableDictionary* dic = [NSMutableDictionary new];
                [dic setObject:[view.questionObject objectForKey:QUESTION_ID_KEY] forKey:QUESTION_ID_KEY];
                //[dic setObject:[view.questionObject objectForKey:CHECKLIST_ID_KEY] forKey:CHECKLIST_ID_KEY];
                [dic setObject:[weakAnswerArray copy] forKey:ANSWER_KEY];
                [dic setObject:[view.questionObject objectForKey:QUESTION_TYPE_KEY] forKey:QUESTION_TYPE_KEY];
                NSData* data = [NSJSONSerialization dataWithJSONObject:[dic copy] options:0 error:nil];
                [[MTKNetworkManager sharedManager] connectServer:ANSWER_KEY data:data onResult:^(NSDictionary * result) {
                if ([[result objectForKey:@"status"] isEqualToString:@"ok"]) {
                [weakSelf checkNextQuestion];
                }
                }];
                };
                }
                */

                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    String answer = GlobalSet.GetQuestionInfo(0).Answer();
                    Log.d(TAG, messagePack.GetValue() + " : " + answer);
                    byte[] jsonBytes = answer.getBytes();
                    httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonBytes.length));

                    outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(jsonBytes);
                    outputStream.flush();
                }
                break;

                case MessagePack.CONNECTION_EVENT:
                {
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    JSONObject jasonObject = new JSONObject();
                    //device info

                    MessagePackEvent messagePackEvent = (MessagePackEvent)messagePack;
                    jasonObject.put(messagePackEvent.GetEventKey(), messagePackEvent.GetEventValue());
                    byte[] jsonBytes = jasonObject.toString().getBytes();
                    int jsonBytesLength = jsonBytes.length;
                    httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonBytesLength));
                    outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(jsonBytes);
                    outputStream.flush();
                }
                break;

                case MessagePack.CONNECTION_QUESTION:
                case MessagePack.CONNECTION_CAMERA_CAPTURE_INFO:
                case MessagePack.CONNECTION_CAPTURE_DETAIL:
                {
                    boolean isNull = true;
                    if (true == isNull)
                    {}
                    else
                    {
                        httpURLConnection.setRequestProperty("Content-Type", "application/json");
                        httpURLConnection.setRequestProperty("Content-Length", "1");
                        JSONObject jsonObject = new JSONObject();

                        outputStream = httpURLConnection.getOutputStream();
                        outputStream.write(jsonObject.toString().getBytes());
                        outputStream.flush();
                    }

                }
                break;
            }

            httpURLConnection.connect();
            String serverResponseMessage = "None";
            int serverResponseCode = httpURLConnection.getResponseCode();
            Log.d(TAG, "httpURLConnection.getResponseCode : " + serverResponseCode);

            switch (serverResponseCode)
            {
                case HttpURLConnection.HTTP_OK:
                {
                    Log.d(TAG, "ResponseCode : HTTP_OK");

                    inputStream = httpURLConnection.getInputStream();
                    byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] byteBuffer = new byte[2048];
                    byte[] byteData = null;
                    int nLength = 0;
                    while ((nLength = inputStream.read(byteBuffer, 0, byteBuffer.length)) != -1)
                    {
                        byteArrayOutputStream.write(byteBuffer, 0, nLength);
                    }

                    byteData = byteArrayOutputStream.toByteArray();
                    serverResponseMessage = new String(byteData);
                    JSONObject responseJSON = new JSONObject(serverResponseMessage);
                    String status = responseJSON.getString("status");
                    Log.d(TAG, "status : " + status);

                    switch (messagePack.GetValue())
                    {
                        case MessagePack.CONNECTION_LOG:
                        {
                            //{
                            // "status":"ok",
                            // "isScreenCaptureAllowed":true,
                            // "isScreenStreamAllowed":false,
                            // "isCameraStreamAllowed":true,
                            // "minimumTestBuildNumber":0,
                            // "sessionTime":
                            // {
                            // "accumulated":36437,
                            // "current":646
                            // }
                            // }

                            if (status.equals("ok"))
                            {
                                boolean isScreenCaptureAllowed = responseJSON.getBoolean("isScreenCaptureAllowed");
                                Log.d(TAG, "isScreenCaptureAllowed : " + isScreenCaptureAllowed);
                                //_loginInfo.SetScreenCaptureAllowed(isScreenCaptureAllowed);

                                boolean isScreenStreamAllowed = responseJSON.getBoolean("isScreenStreamAllowed");
                                Log.d(TAG, "isScreenStreamAllowed : " + isScreenStreamAllowed);
                                //_loginInfo.SetScreenStreamAllowed(isScreenStreamAllowed);

                                boolean isCameraStreamAllowed = responseJSON.getBoolean("isCameraStreamAllowed");
                                Log.d(TAG, "isCameraStreamAllowed : " + isCameraStreamAllowed);
                                //_loginInfo.SetCameraStreamAllowed(isCameraStreamAllowed);

                                int minimumTestBuildNumber = responseJSON.getInt("minimumTestBuildNumber");
                                Log.d(TAG, "minimumTestBuildNumber : " + minimumTestBuildNumber);
                                JSONObject sessionTimeJSON = responseJSON.getJSONObject("sessionTime");
                                long accumulated = sessionTimeJSON.getLong("accumulated");
                                Log.d(TAG, "accumulated : " + accumulated);
                                long current = sessionTimeJSON.getLong("current");
                                Log.d(TAG, "current : " + current);

                                if (0 == MessagePackLog.SessionTimeCheckStatus)
                                {
                                    if (current >= MessagePackLog.SessionTimeCheck)
                                    {
                                        if(false == GlobalSet.DebugMode)
                                            MessagePackLog.SessionTimeCheck += 600;
                                        else
                                            MessagePackLog.SessionTimeCheck += 60;

                                        MessagePackLog.SessionTimeCheckStatus = 1;
                                    }
                                }
                            }
                        }
                        break;

                        case MessagePack.CONNECTION_LOGIN:
                        {
                            _loginInfo = new LoginInfo();
                            _loginInfo.Parse(responseJSON);


                        }
                        break;

                        case MessagePack.CONNECTION_EVENT:
                        {
                            //{
                            // "status":"ok",
                            // "questions":
                            // [
                            // {
                            // "questionId":"cuMBpwHiTP",
                            // "questionType":"range",
                            // "range":
                            // [
                            // "Low",
                            // "Fare",
                            // "High",
                            // "10"
                            // ],
                            // "imageURL":""
                            // },
                            //
                            // {
                            // "questionId":"GZEHnWiMZd",
                            // "questionType":"multipleChoice",
                            // "choices":
                            // [
                            // "choice1",
                            // "choice2",
                            // "choice3"
                            // ],
                            // "imageURL":""
                            // },
                            //
                            // {
                            // "questionId":"VTELdcPSGH",
                            // "questionType":"openEnd",
                            // "imageURL":""
                            // }
                            // ]
                            // }

                            if (status.equals("ok"))
                            {
                                GlobalSet.ParseQuestionInfo(responseJSON);
                                if(GlobalSet.GetQuestionInfoCount() > 0)
                                {
                                    Message msg = _receivedMessageHandler.obtainMessage();
                                    msg.what = MESSAGE_QUESTION_PREPARE;
                                    _receivedMessageHandler.sendMessage(msg);
                                }

                                //disconnect rtmp server
                                //stopRecord();
                            }
                            else
                            {

                            }
                        }
                        break;

                        case MessagePack.CONNECTION_ANSWER:
                        {
                            if (status.equals("ok"))
                            {

                            }
                            else
                            {

                            }

                            GlobalSet.RemoveAtQuestionInfo(0);
                            if(GlobalSet.GetQuestionInfoCount() > 0)
                            {
                                Message msg = _receivedMessageHandler.obtainMessage();
                                msg.what = MESSAGE_QUESTION_PREPARE;
                                _receivedMessageHandler.sendMessage(msg);
                            }
                            else
                            {
                                //reconnect rtmp server
                                /*
                                _mediaProjection = _mediaProjectionManager.getMediaProjection(_projectionResultCode, _projectionIntentData);
                                connectServer(_rtmpServerURL);
                                networkStatus = "ConnectingRTMPServer";
                                */
                            }

                        }
                        break;
                    }

                    Log.i(TAG, "-----------------------------> Server Response - " + messagePack.GetValue() + " : " + serverResponseMessage);
                }
                break;

                default:
                {
                    Log.d(TAG, "ResponseCode : Not HTTP_Error - QuitApplication");

                    //error connect server
                    Message msg = _receivedMessageHandler.obtainMessage();
                    msg.what = MESSAGE_CONNECT_SERVER_FAILURE;
                    _receivedMessageHandler.sendMessage(msg);
                }
                break;
            }

            httpURLConnection.disconnect();
            _dispatchNetworkStatus = 2;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();

            //error connect server
            Message msg = _receivedMessageHandler.obtainMessage();
            msg.what = MESSAGE_CONNECT_SERVER_FAILURE;
            _receivedMessageHandler.sendMessage(msg);
        }
    }

    private void _StartNetworkMessageLoop()
    {
        //final ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        final Handler handler = new Handler();
        final int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                //Toast.makeText(ScreenCastingService.this, "WorkEventTrigger ...", Toast.LENGTH_SHORT).show();
                /*
                List<ActivityManager.AppTask> runningTasks = manager.getAppTasks();
                if (runningTasks != null && runningTasks.size() > 0)
                {
                    ActivityManager.RecentTaskInfo topActivity = runningTasks.get(0).getTaskInfo();
                    //ComponentName topActivity = runningTasks.get(0).getTaskInfo().topActivity;
                    if (topActivity.getPackageName().equals("xxx.xxx.xxx"))
                    {
                        // Other Application is opened
                        return true;
                    }
                }
                */

                /*
                String packageName = "";
                if(Build.VERSION.SDK_INT > 20)
                {
                    packageName = activityManager.getRunningAppProcesses().get(0).processName;
                }
                else
                {
                    packageName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
                }
                */


                if(_requestMessageList.size() > 0)
                {
                    //Log.d(TAG,"Work Event Trigger Start");

                    if(0 == _dispatchNetworkStatus)
                    {
                        _dispatchNetworkStatus = 1;

                        new Thread()
                        {
                            public void run()
                            {
                                MessagePack messagePack = (MessagePack)_requestMessageList.get(0);
                                switch(messagePack.GetValue())
                                {
                                    case MessagePack.PREPARE_LOGIN:
                                    {
                                        //try
                                        {
                                            /*
                                            URL url = new URL("http://221.165.42.119/methinksPatcher/login.png");
                                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                                            httpURLConnection.setDoInput(true);
                                            httpURLConnection.connect();
                                            InputStream inputStream = httpURLConnection.getInputStream();
                                            Bitmap loginBitmap = BitmapFactory.decodeStream(inputStream);
                                            httpURLConnection.disconnect();
                                            */

                                            Message msg = _receivedMessageHandler.obtainMessage();
                                            msg.what = MESSAGE_LOGIN_PREPARE;
                                            _receivedMessageHandler.sendMessage(msg);
                                        }
                                        //catch (IOException e)
                                        //{
                                        //    Log.d(TAG,e.getMessage());
                                        //    e.printStackTrace();
                                        //}

                                        _dispatchNetworkStatus = 2;
                                    }
                                    break;

                                    case MessagePack.PREPARE_QUESTION:
                                    {
                                        try
                                        {
                                            URL url = new URL("http://221.165.42.119/methinksPatcher/question_range.png");
                                            //URL url = new URL("http://221.165.42.119/methinksPatcher/question_multiple_choice.png");
                                            //URL url = new URL("http://221.165.42.119/methinksPatcher/question_open_end.png");
                                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                                            httpURLConnection.setDoInput(true);
                                            httpURLConnection.connect();
                                            InputStream inputStream = httpURLConnection.getInputStream();
                                            Bitmap loginBitmap = BitmapFactory.decodeStream(inputStream);

                                            Message msg = _receivedMessageHandler.obtainMessage();
                                            msg.what = MESSAGE_QUESTION_PREPARE;
                                            _receivedMessageHandler.sendMessage(msg);

                                            httpURLConnection.disconnect();
                                        }
                                        catch (IOException e)
                                        {
                                            Log.d(TAG,e.getMessage());
                                            e.printStackTrace();
                                        }

                                        _dispatchNetworkStatus = 2;
                                    }
                                    break;

                                    default:
                                    {
                                        _RequestServer(messagePack);
                                    }
                                    break;
                                }
                            }
                        }.start();
                    }
                    if(2 == _dispatchNetworkStatus)
                    {
                        _requestMessageList.remove(0);
                        _dispatchNetworkStatus = 0;
                    }
                }


                //request log message
                if(0 < _processStatus)
                {
                    if(_logDelayTimer.ElapsedTickCount(GlobalSet.LogDelayTickTime))
                    {
                        switch(_questionStatus)
                        {
                            case 0:
                            {
                                if(false == GlobalSet.RunBackground)
                                {
                                    if(false == _backgroundMode)
                                    {
                                        Log.d(TAG, "LOG TimeMiliseconds : " + System.currentTimeMillis());
                                        SendRequestMessage(MessagePackLog.GetMessagePackLog());
                                    }
                                }
                                else
                                {
                                    Log.d(TAG, "LOG TimeMiliseconds : " + System.currentTimeMillis());
                                    SendRequestMessage(MessagePackLog.GetMessagePackLog());
                                }
                            }
                            break;
                        }
                    }
                }


                if(_detectScreenShotTimer.ElapsedTickCount(2000))
                {
                    //Log.d(TAG, "LOG : " + System.currentTimeMillis());
                    _DetectScreenShotService();
                }


                if(_checkForegroundTimer.ElapsedTickCount(2000))
                {
                    if (true == _IsForegrounded())
                    {
                        Log.d(TAG,"------####### IsForeground ######------");

                        if(true == _backgroundMode)
                        {
                            Message message = _receivedMessageHandler.obtainMessage();
                            message.what = MESSAGE_NOTIFICATION_FOREGROUND;
                            _receivedMessageHandler.sendMessage(message);

                            _backgroundMode = false;
                            _checkBackgroundTimer.Reset();
                            _checkBackgroundTimer3.Reset();

                            if (0 < _processStatus)
                            {
                                if (_loginInfo.IsScreenStreamAllowed())
                                {
                                    if (false == GlobalSet.RunBackground)
                                    {
                                        if (true == paused)
                                        {
                                            paused = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.d(TAG,"------####### IsBackground ######------");

                        if(false == _backgroundMode)
                        {
                            Message message = _receivedMessageHandler.obtainMessage();
                            message.what = MESSAGE_NOTIFICATION_BACKGROUND;
                            _receivedMessageHandler.sendMessage(message);
                            _backgroundMode = true;

                            if (0 < _processStatus)
                            {
                                if (_loginInfo.IsScreenStreamAllowed())
                                {
                                    if (false == GlobalSet.RunBackground)
                                    {
                                        paused = true;
                                    }
                                }
                            }
                        }
                    }
                }

                if(true == _backgroundMode)
                {
                    if(_checkBackgroundTimer.ElapsedTickCount(5000))
                    {
                        stopRecord();
                    }

                    if(_checkBackgroundTimer3.ElapsedTickCount(600000))
                    {
                        Quit();
                    }
                }
                else
                {
                    if(networkStatus.equals("Disconnect"))
                    {
                        if(null == _mediaProjection)
                        {
                            _mediaProjection = _mediaProjectionManager.getMediaProjection(_projectionResultCode, _projectionIntentData);
                            connectServer(_rtmpServerURL);
                            networkStatus = "ConnectingRTMPServer";
                        }
                    }
                }

                /*
                switch(_questionStatus)
                {
                    case 0:
                        break;

                    case 1:
                    {
                        stopRecord();
                        _questionStatus = 2;
                    }
                    break;

                    case 2:
                        break;

                    case 3:
                    {
                        _mediaProjection = _mediaProjectionManager.getMediaProjection(_projectionResultCode, _projectionIntentData);
                        connectServer(_rtmpServerURL);
                        networkStatus = "ConnectingRTMPServer";

                        _questionStatus = 0;
                    }
                    break;
                }
                */

                if(_quitApplication)
                {
                    if(_checkBackgroundTimer2.ElapsedTickCount(10000))
                    {
                        Quit();
                    }
                }

                handler.postDelayed(this, delay);
            }
        }, delay);
    }


    Handler _receivedMessageHandler = new Handler()
    {
        public void handleMessage(Message message)
        {
            switch(message.what)
            {
                case MESSAGE_LOGIN_PREPARE:
                {
                    if(-1 == GlobalSet.ShowDialogType)
                    {
                        float density = ApplicationContext.getResources().getDisplayMetrics().density;
                        int width = ApplicationContext.getResources().getDisplayMetrics().widthPixels;
                        int height = ApplicationContext.getResources().getDisplayMetrics().heightPixels;

                        WindowManager.LayoutParams windowManagerLayoutParams2 = _CreateWindowManagerLayoutParams();
                        windowManagerLayoutParams2.gravity = Gravity.LEFT | Gravity.TOP;
                        windowManagerLayoutParams2.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION;
                        windowManagerLayoutParams2.flags |= WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;

                        //
                        LayoutInflater loginLayoutInflater = (LayoutInflater) ApplicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        int resourceID = _GetLayoutResourceID("login_layout");
                        _loginLayoutView = loginLayoutInflater.inflate(resourceID, null);
                        _loginLayoutView.setBackgroundColor(Color.WHITE);

                        final Button loginButton = (Button) _GetResourceID(_loginLayoutView, "button_login");
                        if (null != loginButton) {
                            loginButton.setOnClickListener(new Button.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    loginButton.setEnabled(false);

                                    if (null != _userCodeEditText)
                                        _userCode = _userCodeEditText.getText().toString();

                                    //String rtmpServerBaseURL = "rtmp://221.165.42.119:5391";
                                    //_rtmpServerURL =  rtmpServerBaseURL + "/screen/"+_userCode+"?projectID="+_projectID;


                                    Log.d(TAG, "Tap Login Button");

                                    MessagePackLogin messagePackLogin = new MessagePackLogin();
                                    messagePackLogin.Request();
                                    SendRequestMessage(messagePackLogin);
                                }
                            });
                        }

                        _userCodeEditText = (EditText) _GetResourceID(_loginLayoutView, "editText_user_code");
                        if (null != _userCodeEditText) {
                            //if (false == _release)
                            //  _userCodeEditText.setText(_userCode);

                            final InputMethodManager inputMethodManager = (InputMethodManager) ApplicationContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                            _userCodeEditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //inputMethodManager.showSoftInput(_userCodeEditText,InputMethodManager.SHOW_FORCED);
                                    //inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0 );
                                    //inputMethodManager.showSoftInputFromInputMethod (_userCodeEditText.getApplicationWindowToken(),InputMethodManager.SHOW_FORCED);
                                    //inputMethodManager.toggleSoftInputFromWindow(_userCodeEditText.getApplicationWindowToken(),  InputMethodManager.SHOW_FORCED, 0);

                                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                                }
                            });

                            _userCodeEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        //inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0 );
                                    } else {
                                        //inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0 );
                                        //inputMethodManager.showSoftInput(_userCodeEditText,InputMethodManager.HIDE_IMPLICIT_ONLY);
                                    }
                                }
                            });

                        }
                        _windowManager.addView(_loginLayoutView, windowManagerLayoutParams2);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

                        WindowManager.LayoutParams windowManagerLayoutParams = _CreateWindowManagerLayoutParams();
                        windowManagerLayoutParams.gravity = Gravity.LEFT | Gravity.TOP;
                        windowManagerLayoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
                        windowManagerLayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

                        Bitmap bitmap = _GetResourceBitmap("img_methinks_logo");
                        windowManagerLayoutParams.x = (width - bitmap.getWidth()) / 2;
                        windowManagerLayoutParams.y = (int) (100 * density);

                        _loginImageView = new ImageView(ApplicationContext);
                        _loginImageView.setImageBitmap(/*_loginBitmap*/bitmap);

                        _windowManager.addView(_loginImageView, windowManagerLayoutParams);
                    }
                    else if(1 == GlobalSet.ShowDialogType)
                    {
                        _SendCallbackMessage(0);
                    }

                }
                break;

                case MESSAGE_LOGIN_SUCCESS:
                {
                    LoginInfo loginInfo = (LoginInfo)message.obj;
                    Log.d("TAG",loginInfo.GetResultStatus());
                    Tooltip(loginInfo.GetResultStatus());


                    if(loginInfo.CheckValidData())
                    {
                        //if(loginInfo.GetResultStatus().contains("gig has not started yet"))
                        //{
                        //    _checkBackgroundTimer2.Reset();
                        //    _quitApplication = true;
                        //}
                        //else
                        {
                            //save user code
                            _WriteUserCode(_userCode);
                            _processStatus = 1;

                             if (null != _loginImageView)
                                _windowManager.removeView(_loginImageView);
                             if (null != _loginLayoutView)
                                _windowManager.removeView(_loginLayoutView);

                             if(loginInfo.IsScreenStreamAllowed())
                             {
                                _mediaProjection = _mediaProjectionManager.getMediaProjection(_projectionResultCode, _projectionIntentData);
                                connectServer(_rtmpServerURL);
                             }
                        }
                    }
                    else
                    {
                        //if(loginInfo.GetResultStatus().contains("gig has not started yet"))
                        {
                            _checkBackgroundTimer2.Reset();
                            _quitApplication = true;
                        }
                    }

                }
                break;

                case MESSAGE_LOGIN_FAILURE:
                {
                    if(null != _loginImageView)
                        _windowManager.removeView(_loginImageView);
                    if(null != _loginLayoutView)
                        _windowManager.removeView(_loginLayoutView);

                    LoginInfo loginInfo = (LoginInfo)message.obj;
                    Tooltip(loginInfo.GetResultStatus());

                    switch(loginInfo.GetError())
                    {
                        case "invalidProject":
                        {
                            //Tooltip(MESSAGE_TITLE_INVALID_PROJECT);
                            Tooltip(methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_error_msg_test_period_over));
                            _checkBackgroundTimer2.Reset();
                            _quitApplication = true;
                        }
                        break;

                        case "invalidUserCode":
                        {
                            ++ LoginTryCount;
                            //Tooltip(MESSAGE_TITLE_INVALID_USER);
                            Tooltip(methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_error_msg_invalid_user_code));
                            _SendCallbackMessage(0);
                        }
                        break;

                        default:
                        {
                            /*
                            if(loginInfo.GetResultStatus().contains("gig has not started yet"))
                            {}
                            */
                        }
                        break;
                    }
                }
                break;

                case MESSAGE_CONNECTED_SUCCESS_RTMP_SERVER:
                {
                    startRecord(_mediaProjection);
                }
                break;

                case MESSAGE_CONNECTED_FAILURE_RTMP_SERVER:
                {

                }
                break;

                case MESSAGE_DISCONNECTED_RTMP_SERVER:
                {

                }
                break;

                case MESSAGE_NOTIFICATION_BACKGROUND:
                {
                    //_PauseRecordScreenCapture();
                }
                break;

                case MESSAGE_NOTIFICATION_FOREGROUND:
                {
                    //_ResumeRecordScreenCapture();
                }
                break;

                case MESSAGE_QUESTION_PREPARE:
                {
                    QuestionInfo questionInfo = GlobalSet.GetQuestionInfo(0);
                    //Log.d(TAG,"###### QuestionInfo - "+questionInfo.GetType());
                    switch(questionInfo.GetType())
                    {
                        case "range":
                        {
                            //final CharSequence[] items = {"1", "2", "3","4","5"};
                            final CharSequence[] items = {
                                    methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_level_very_easy),
                                    methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_level_easy),
                                    methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_level_average),
                                    methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_level_hard),
                                    methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_level_very_hard)
                            };


                            AlertDialog.Builder builder = new AlertDialog.Builder(_currentActivity);
                            builder.setTitle(questionInfo.GetText());
                            builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int item)
                                {
                                    //Tooltip(items[item].toString());
                                    int selectedItem = item + 1;
                                    GlobalSet.GetQuestionInfo(0).AddAnswer(true,selectedItem+"");
                                }
                            });

                            String confirm = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_confirm);
                            builder.setPositiveButton(confirm, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    MessagePackAnswer messagePackAnswer = new MessagePackAnswer();
                                    messagePackAnswer.Request();
                                    SendRequestMessage(messagePackAnswer);

                                    dialog.dismiss();
                                }
                            });

                            builder.setOnDismissListener(new OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {}
                            });

                            builder.setCancelable(false);

                            AlertDialog alert = builder.create();
                            alert.show();


                            /*
                            final int progressValue = 0;
                            final AlertDialog.Builder popDialog = new AlertDialog.Builder(_currentActivity);
                            //final SeekBar seekBar = new SeekBar(_currentActivity);
                            final TextThumbSeekBar seekBar = new TextThumbSeekBar(_currentActivity);
                            seekBar.setMax(10);
                            seekBar.setKeyProgressIncrement(1);

                            popDialog.setTitle(questionInfo.GetText());
                            popDialog.setView(seekBar);


                            OnSeekBarChangeListener onSeekBarChangeListener = new OnSeekBarChangeListener()
                            {
                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar)
                                {

                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar)
                                {

                                }

                                @Override
                                public void onProgressChanged(SeekBar seekBark, int progress, boolean fromUser)
                                {

                                    //Tooltip("CurrentProgress : "+progress);
                                }
                            };

                            seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
                            popDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {

                                    //dialog.dismiss();

                                }
                            });

                            popDialog.setOnDismissListener(new OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {}
                            });

                            popDialog.setCancelable(false);
                            popDialog.show();
                            */

                        }
                        break;

                        case "multipleChoice":
                        {
                            if(questionInfo.IsMultipleChoice())
                            {
                                final List<String> selectedItems = new ArrayList<String>();
                                final String[] items = questionInfo.ChoiceList().toArray(new String[questionInfo.ChoiceList().size()]);;//(String[])questionInfo.ChoiceList().toArray();
                                final boolean [] checked = new boolean[items.length];
                                for(int i=0; i<checked.length; ++i)
                                    checked[i] = false;

                                String confirm = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_confirm);

                                AlertDialog.Builder dialog = new AlertDialog.Builder(_currentActivity);
                                dialog.setTitle(questionInfo.GetText())
                                        .setMultiChoiceItems(items,
                                                checked,
                                                new DialogInterface.OnMultiChoiceClickListener()
                                                {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which, boolean isChecked)
                                                    {
                                                        if(isChecked)
                                                        {
                                                            //Tooltip(items[which]);
                                                            selectedItems.add(items[which]);
                                                        }
                                                        else
                                                        {
                                                            selectedItems.remove(items[which]);
                                                        }
                                                    }
                                                })
                                        .setPositiveButton(confirm, new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                if( selectedItems.size() == 0 )
                                                {
                                                    //Toast.makeText(ApplicationContext, "Nothing Selected Item", Toast.LENGTH_SHORT).show();
                                                }
                                                else
                                                {
                                                    String items = "";
                                                    for(String selectedItem : selectedItems)
                                                    {
                                                        items += (selectedItem + ", ");
                                                        GlobalSet.GetQuestionInfo(0).AddAnswer(false,selectedItem);
                                                    }

                                                    selectedItems.clear();

                                                    items = items.substring(0, items.length() - 2);
                                                    //Toast.makeText(ApplicationContext, items, Toast.LENGTH_SHORT).show();
                                                }


                                                MessagePackAnswer messagePackAnswer = new MessagePackAnswer();
                                                messagePackAnswer.Request();
                                                SendRequestMessage(messagePackAnswer);

                                                dialog.dismiss();
                                            }
                                        });


                                dialog.setOnDismissListener(new OnDismissListener()
                                {
                                    @Override
                                    public void onDismiss(DialogInterface dialog)
                                    {}
                                });

                                dialog.setCancelable(false);

                                AlertDialog alert = dialog.create();
                                alert.show();
                            }
                            else
                            {
                                final CharSequence[] items  = questionInfo.ChoiceList().toArray(new CharSequence[questionInfo.ChoiceList().size()]);

                                AlertDialog.Builder builder = new AlertDialog.Builder(_currentActivity);
                                builder.setTitle(questionInfo.GetText());
                                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int item)
                                    {
                                        //Tooltip(items[item].toString());
                                        GlobalSet.GetQuestionInfo(0).AddAnswer(true,items[item].toString());
                                    }
                                });

                                String confirm = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_confirm);
                                builder.setPositiveButton(confirm, new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        MessagePackAnswer messagePackAnswer = new MessagePackAnswer();
                                        messagePackAnswer.Request();
                                        SendRequestMessage(messagePackAnswer);

                                        dialog.dismiss();
                                    }
                                });

                                builder.setOnDismissListener(new OnDismissListener()
                                {
                                    @Override
                                    public void onDismiss(DialogInterface dialog)
                                    {}
                                });

                                builder.setCancelable(false);

                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        }
                        break;

                        case "openEnd":
                        {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(_currentActivity);
                            alertDialog.setTitle(questionInfo.GetText());

                            final EditText editText = new EditText(_currentActivity);
                            alertDialog.setView(editText);

                            String confirm = methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_select_confirm);
                            alertDialog.setPositiveButton(confirm, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {

                                    GlobalSet.GetQuestionInfo(0).AddAnswer(true,editText.getText().toString());


                                    MessagePackAnswer messagePackAnswer = new MessagePackAnswer();
                                    messagePackAnswer.Request();
                                    SendRequestMessage(messagePackAnswer);

                                    dialog.dismiss();

                                }
                            });

                            alertDialog.setOnDismissListener(new OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {}
                            });

                            alertDialog.setCancelable(false);
                            alertDialog.show();
                        }
                        break;
                    }
                }
                break;

                case MESSAGE_CONNECT_SERVER_FAILURE:
                {
                    Tooltip(methinksPatcherLanguage.GetText(methinksPatcherLanguage.patcher_error_msg_can_not_connect_server));
                    _checkBackgroundTimer2.Reset();
                    _quitApplication = true;
                }
                break;
            }

        }
    };


    /*
    LayoutInflater loginLayoutInflater = LayoutInflater.from(this);

        //_loginLayoutView = loginLayoutInflater.inflate(R.layout.login_layout, null);
        //_loginLayoutView.setBackgroundColor(Color.WHITE);
        //_loginLayoutView.setVisibility(View.GONE);

        LayoutInflater questionRangeLayoutInflater = LayoutInflater.from(this);
        _questionRangeView = questionRangeLayoutInflater.inflate(R.layout.question_range_layout, null);
        _questionRangeView.setBackgroundColor(Color.BLUE);
        _questionRangeView.setVisibility(View.GONE);

        LayoutInflater questionMultipleChoiceLayoutInflater = LayoutInflater.from(this);
        _questionMultipleChoiceView = questionMultipleChoiceLayoutInflater.inflate(R.layout.question_multiple_choice_layout, null);
        _questionMultipleChoiceView.setBackgroundColor(Color.BLUE);
        _questionMultipleChoiceView.setVisibility(View.GONE);

        LayoutInflater questionOpenEndLayoutInflater = LayoutInflater.from(this);
        _questionOpenEndView = questionOpenEndLayoutInflater.inflate(R.layout.question_open_end_layout, null);
        _questionOpenEndView.setBackgroundColor(Color.BLUE);
        _questionOpenEndView.setVisibility(View.GONE);


        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
        //this.addContentView(_loginLayoutView, layoutParams);
        this.addContentView(_questionRangeView, layoutParams);
        this.addContentView(_questionMultipleChoiceView,layoutParams);
        this.addContentView(_questionOpenEndView,layoutParams);

        int buttonID = -1;

        buttonID = getResources().getIdentifier("button_answer_range", "id", getPackageName());
        _answerRangeButton = (Button) _questionRangeView.findViewById(buttonID);
        if(null != _answerRangeButton)
        {
            _answerRangeButton.setOnClickListener(new Button.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //Toast.makeText(methinksMainActivity.this, "Button Tap", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"Tap Button");

                    _questionRangeView.setVisibility(View.GONE);
                    int questionSequence = GlobalSet.GetQuestionSequence();
                    ++questionSequence;

                    if(questionSequence < GlobalSet.GetQuestionInfoCount()-1)
                        _questionMultipleChoiceView.setVisibility(View.VISIBLE);
                    else
                        GlobalSet.SetQuestionSequence(questionSequence);

                    //_screenCastingService.SendRequestMessage(MessagePack.CONNECTION_ANSWER);

                }
            });
        }

        buttonID = getResources().getIdentifier("button_answer_multiple_choice", "id", getPackageName());
        _answerMultipleButton = (Button) _questionMultipleChoiceView.findViewById(buttonID);
        if(null != _answerMultipleButton)
        {
            _answerMultipleButton.setOnClickListener(new Button.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //Toast.makeText(methinksMainActivity.this, "Button Tap", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"Tap Button");

                    _questionMultipleChoiceView.setVisibility(View.GONE);
                    int questionSequence = GlobalSet.GetQuestionSequence();
                    ++questionSequence;

                    if(questionSequence < GlobalSet.GetQuestionInfoCount()-1)
                        _questionOpenEndView.setVisibility(View.VISIBLE);
                    else
                        GlobalSet.SetQuestionSequence(questionSequence);

                    //_screenCastingService.SendRequestMessage(MessagePack.CONNECTION_ANSWER);
                }
            });
        }

        buttonID = getResources().getIdentifier("button_answer_open_end", "id", getPackageName());
        _answerOpenEndButton = (Button) _questionOpenEndView.findViewById(buttonID);
        if(null != _answerOpenEndButton)
        {
            _answerOpenEndButton.setOnClickListener(new Button.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //Toast.makeText(methinksMainActivity.this, "Button Tap", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"Tap Button");

                    _questionOpenEndView.setVisibility(View.GONE);
                    int questionSequence = GlobalSet.GetQuestionSequence();
                    ++questionSequence;

                    if(questionSequence < GlobalSet.GetQuestionInfoCount()-1)
                    {

                    }
                    else
                        GlobalSet.SetQuestionSequence(questionSequence);

                    //_screenCastingService.SendRequestMessage(MessagePack.CONNECTION_ANSWER);
                }
            });
        }
    */



    public void Tooltip(String message)
    {
        Toast.makeText(ApplicationContext, message, Toast.LENGTH_LONG).show();
    }


    private void _WriteUserCode(String userCode)
    {
        SharedPreferences pref = ApplicationContext.getSharedPreferences("methinksPatcher", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        if(false == GlobalSet.DebugMode)
            editor.putString("UserCode_Release", userCode);
        else
            editor.putString("UserCode_Debug", userCode);
        editor.commit();
    }

    private String _ReadUserCode()
    {
        SharedPreferences pref = ApplicationContext.getSharedPreferences("methinksPatcher", Context.MODE_PRIVATE);

        if(false == GlobalSet.DebugMode)
            return pref.getString("UserCode_Release", "");
        return pref.getString("UserCode_Debug", "");
    }

    private void _RemoveUserCode()
    {
        SharedPreferences pref = ApplicationContext.getSharedPreferences("methinksPatcher", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        if(false == GlobalSet.DebugMode)
            editor.remove("UserCode_Release");
        else
            editor.remove("UserCode_Debug");

        editor.commit();
    }


    private void _RemoveAllUserCode()
    {
        SharedPreferences pref = ApplicationContext.getSharedPreferences("methinksPatcher", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.remove("UserCode_Release");
        editor.remove("UserCode_Debug");

        editor.commit();
    }


    public boolean isRunning()
    {
        return running;
    }

    public void setResolution(int width, int height, int dpi)
    {
        GlobalSet.VideoWidth = width;
        GlobalSet.VideoHeight = height;
        GlobalSet.VideoDpi = dpi;
    }


    public void setVideoRate(int videoDataRate,int frameRate)
    {
        GlobalSet.VideoDataRate = videoDataRate;
        GlobalSet.VideoFrameRate = frameRate;
    }

    public void connectServer(String rtmpServerURL)
    {
        Log.d(TAG,"[ConnectServer] - "+rtmpServerURL);
        networkStatus = "Ready";

        _rtmpServerURL = rtmpServerURL;

        rtmpMuxer = new SrsFlvMuxer(this);
        rtmpMuxer.setVideoResolution(GlobalSet.VideoWidth,GlobalSet.VideoHeight);
        rtmpMuxer.setVideoRate(GlobalSet.VideoDataRate,GlobalSet.VideoFrameRate);

        //_rtmpURL = "rtmp://ec2-184-72-75-118.compute-1.amazonaws.com:5391/screen/bilbo915?projectID=oulj264yvC";
        //_rtmpURL = "rtmp://ec2-52-90-147-115.compute-1.amazonaws.com:5391/screen/bilbo915?projectID=oulj264yvC";//korea
        //_rtmpURL = "rtmp://live-api-s.facebook.com:80/rtmp/1768346039944842?ds=1&s_sw=0&s_vt=api-s&a=ATh-1YIrhAJGAPa6";
        //_rtmpURL = "rtmp://221.165.42.119:5391/screen/bilbo915?projectID=oulj264yvC";//home
        //_rtmpURL = "rtmp://211.245.121.137:5391/screen/bilbo915?projectID=oulj264yvC";//office

        rtmpMuxer.start(_rtmpServerURL);
        videoEncoder = new VideoEncoder(this);
    }

    public boolean startRecord(MediaProjection project)
    {
        Log.d(TAG,"[StartRecord] - running :  "+running);
        if (running)
            return false;

        _mediaProjection = project;

        videoEncoder.setWidth(GlobalSet.VideoWidth);
        videoEncoder.setHeight(GlobalSet.VideoHeight);
        videoEncoder.prepareVideoEncoder();

        _virtualDisplay =
                _mediaProjection.createVirtualDisplay("MainScreen", GlobalSet.VideoWidth, GlobalSet.VideoHeight, GlobalSet.VideoDpi,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, videoEncoder.getInputSurface(), null, null);

        videoEncoder.start();

        running = true;
        return true;
    }

    public boolean stopRecord()
    {
        Log.d(TAG,"[StopRecord] - running :  "+running);
        if (!running)
            return false;

        if(null != videoEncoder)
        {
            videoEncoder.stop();
            videoEncoder = null;
        }

        if(null != rtmpMuxer)
        {
            rtmpMuxer.stop();
            rtmpMuxer = null;
        }

        running = false;
        paused = false;

        if(null != _virtualDisplay)
        {
            _virtualDisplay.release();
            _virtualDisplay = null;
        }

        if(null != _mediaProjection)
        {
            _mediaProjection.stop();
            _mediaProjection = null;
        }
        return true;
    }

    public String getNetworkStatus()
    {
        return networkStatus;
    }


    @Override
    public void onConnectionSuccessRtmp()
    {
        Log.d(TAG,"[onConnectionSuccessRtmp]");
        networkStatus = "ConnectionSuccess";
        connectedRTMPServer = true;
        paused = false;

        Message message = _receivedMessageHandler.obtainMessage();
        message.what = MESSAGE_CONNECTED_SUCCESS_RTMP_SERVER;
        _receivedMessageHandler.sendMessage(message);
    }

    @Override
    public void onConnectionFailedRtmp(String reason)
    {
        Log.d(TAG,"[onConnectionFailedRtmp] - reason : "+reason);
        networkStatus = "ConnectionFailed : "+reason;

        Message message = _receivedMessageHandler.obtainMessage();
        message.what = MESSAGE_CONNECTED_FAILURE_RTMP_SERVER;
        _receivedMessageHandler.sendMessage(message);
    }

    @Override
    public void onDisconnectRtmp()
    {
        Log.d(TAG,"[onDisconnectRtmp]");
        networkStatus = "Disconnect";
        connectedRTMPServer = false;
        sendRTMPVideo = false;

        Message message = _receivedMessageHandler.obtainMessage();
        message.what = MESSAGE_DISCONNECTED_RTMP_SERVER;
        _receivedMessageHandler.sendMessage(message);
    }

    @Override
    public void onAuthErrorRtmp()
    {
        Log.d(TAG,"[onAuthErrorRtmp]");
        networkStatus = "AuthError";
    }

    @Override
    public void onAuthSuccessRtmp()
    {
        Log.d(TAG,"[onAuthSuccessRtmp]");
        networkStatus = "AuthSuccess";
    }


    @Override
    public void onSPSandPPS(ByteBuffer sps, ByteBuffer pps)
    {
        Log.d(TAG,"[onSPSandPPS]");
        networkStatus = "SPSandPPS";
        if(true == connectedRTMPServer)
        {
            if (null != rtmpMuxer)
            {
                rtmpMuxer.setSpsPPs(sps,pps);
            }
        }
    }

    @Override
    public void getH264Data(ByteBuffer h264Buffer, MediaCodec.BufferInfo info)
    {
        long checkTime = System.currentTimeMillis()%2000;
        if(checkTime > 1950)
        {
            Log.d(TAG,"[getH264Data] captured : "+checkTime);
        }

        if(true == connectedRTMPServer)
        {
            //Log.d(TAG,"[getH264Data] true == connectedRTMPServer");
            if (null != rtmpMuxer)
            {
                //Log.d(TAG,"[getH264Data] null != rtmpMuxer");
                if(false == paused)
                {
                    //Log.d(TAG,"[getH264Data]false == paused");
                    if(checkTime > 1950)
                    {
                        Log.d(TAG,"[getH264Data] - Sending Stream : "+checkTime);
                    }

                    networkStatus = "Sending...";
                    rtmpMuxer.sendVideo(h264Buffer, info);

                    if (false == sendRTMPVideo)
                    {
                        Log.d(TAG,"[getH264Data] - Start Send Stream");
                        Message message = _receivedMessageHandler.obtainMessage();
                        message.what = MESSAGE_START_SEND_STREAM;
                        _receivedMessageHandler.sendMessage(message);

                        sendRTMPVideo = true;
                    }
                }
                else
                {
                    Log.d(TAG,"[getH264Data] - Pause Send Stream");
                    networkStatus = "Paused";
                }
            }
        }
        else
        {

        }
    }

    @Override
    public void onVideoFormat(MediaFormat mediaFormat)
    {
        Log.d(TAG,"[onVideoFormat] - "+mediaFormat.toString());
        networkStatus = "VideoFormat";
    }


    public boolean getShowTouchCheck()
    {
        return getShowTouchCheck(ApplicationContext);
    }

    public boolean settingsSystemShowTouch()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (!Settings.System.canWrite(ApplicationContext))
            {
                return false;
            }
            else
            {
                enableShowTouch(ApplicationContext,true);
            }
        }
        else
        {
            enableShowTouch(ApplicationContext,true);
        }
        return true;
    }

    static boolean settingsSystemCanWrite(Context context)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            return Settings.System.canWrite(context);
        }
        return true;
    }

    static void enableShowTouch(Context context,boolean isChecked)
    {
        if(true == isChecked)
        {
            Settings.System.putInt(context.getContentResolver(),"show_touches", 1);
        }
        else
        {
            Settings.System.putInt(context.getContentResolver(),"show_touches", 0);
        }
    }

    static boolean getShowTouchCheck(Context context)
    {
        int showTouchStatus = Settings.System.getInt(context.getContentResolver(),"show_touches",0);
        if(0 == showTouchStatus)
            return false;
        return true;
    }

    private int _GetScreenBrightness()
    {
        return GetScreenBrightness(ApplicationContext);
    }

    static int GetScreenBrightness(Context context)
    {
        int screenBrightness = -1;
        try
        {
            screenBrightness = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        }
        catch(Exception e)
        {
            screenBrightness = -1;
            e.printStackTrace();
        }

        return screenBrightness;
    }


    private String _GetResourceStringValue(String name)
    {
        int resourceID = ApplicationContext.getResources().getIdentifier(name, "string", ApplicationContext.getPackageName());
        return ApplicationContext.getResources().getString(resourceID);
    }

    private Bitmap _GetResourceBitmap(String name)
    {
        int resourceID = ApplicationContext.getResources().getIdentifier(name, "drawable", ApplicationContext.getPackageName());
        return BitmapFactory.decodeResource(ApplicationContext.getResources(),resourceID);
    }

    private View _GetResourceID(View view,String name)
    {
        int resourceID = ApplicationContext.getResources().getIdentifier(name, "id", ApplicationContext.getPackageName());
        return view.findViewById(resourceID);
    }

    private int _GetLayoutResourceID(String name)
    {
        return ApplicationContext.getResources().getIdentifier(name, "layout", ApplicationContext.getPackageName());
    }

    private XmlResourceParser _GetXmlResourceParser(String name)
    {
        int resourceID = ApplicationContext.getResources().getIdentifier(name, "xml", ApplicationContext.getPackageName());
        XmlResourceParser xmlResourceParser = ApplicationContext.getResources().getXml(resourceID);
        return xmlResourceParser;

       /*
       while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_DOCUMENT) {
            } else if (eventType == XmlPullParser.START_TAG) {
                stringBuffer.append("\nSTART_TAG: " + xpp.getName());
            } else if (eventType == XmlPullParser.END_TAG) {
                stringBuffer.append("\nEND_TAG: " + xpp.getName());
            } else if (eventType == XmlPullParser.TEXT) {
                stringBuffer.append("\nTEXT: " + xpp.getText());
            }
            eventType = xpp.next();
        }
        */
    }

    private WindowManager.LayoutParams _CreateWindowManagerLayoutParams()
    {
        WindowManager.LayoutParams windowManagerLayoutParams = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
        {
            windowManagerLayoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                    PixelFormat.TRANSPARENT);
        }
        else
        {
            windowManagerLayoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                    PixelFormat.TRANSPARENT);
        }

        return windowManagerLayoutParams;
    }

    /*
    private ScreenCastingService.ICallbackMessage _screenCastingCallbackMessage = new ScreenCastingService.ICallbackMessage()
    {
        public void sendScreenCastingEvent(int eventType)
        {
            switch(eventType)
            {
                case ScreenCastingService.MESSAGE_CONNECTED_SERVER:
                {
                    methinksPatcherBridge.Service.startRecord(_mediaProjection);
                }
                break;

                case ScreenCastingService.MESSAGE_START_SEND_STREAM:
                {

                }
                break;

                case ScreenCastingService.MESSAGE_DISCONNECTED_SERVER:
                {

                }
                break;

                case ScreenCastingService.MESSAGE_CONNECT_FAIL_SERVER:
                {
                    Toast.makeText(methinksMainActivity.this,
                            "screenCastingEventBroadcastReceiver : " + eventType+" : "+methinksPatcherBridge.Service.getNetworkStatus(),
                            Toast.LENGTH_SHORT).show();
                }
                break;

                case ScreenCastingService.MESSAGE_LOGIN_SUCCESS:
                {
                   Message message = _handler.obtainMessage();
                   message.what = ScreenCastingService.MESSAGE_LOGIN_SUCCESS;

                   _handler.sendMessage(message);
                }
                break;

                case ScreenCastingService.MESSAGE_LOGIN_FAILURE:
                {
                    //Toast.makeText(methinksMainActivity.this,
                    //       "Login Failure : "+_screenCastingService.getNetworkStatus(),
                    //        Toast.LENGTH_SHORT).show();
                    //ApplicationQuit();
                }
                break;

                case ScreenCastingService.MESSAGE_START_QUESTION:
                {
                    Message message = _handler.obtainMessage();
                    message.what = ScreenCastingService.MESSAGE_START_QUESTION;

                    _handler.sendMessage(message);
                }
                break;

                case ScreenCastingService.MESSAGE_FINISH_QUESTION:
                {
                    Message message = _handler.obtainMessage();
                    message.what = ScreenCastingService.MESSAGE_FINISH_QUESTION;

                    _handler.sendMessage(message);
                }
                break;

            }
        }
    };
    */

}
